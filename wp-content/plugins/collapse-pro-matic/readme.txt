=== Collapse-Pro-Matic ===

Contributors: twinpictures
Donate link: http://plugins.twinpictures.de/flying-houseboat
Tags: collapse, expand, collapsible, expandable, expandable content, collapsable content, shortcode, hidden, hide, display, accordion, accordion, jQuery, javascript, roll-your-own, twinpictures, read me, read more, more
Requires at least: 4.7
Tested up to: 4.9.9
Stable tag: 1.3.11
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Remove clutter, save space: display and hide additional content in a SEO friendly way by wrapping content in an [expand] shortcode.

== Description ==

Collapse-Pro-Matic adds an `[expand title="trigger text"]hidden content[/expand]` shortcode that will wrap any content, including other shortcodes, into a lovely jQuery expanding and collapsing div.  A <a href='http://plugins.twinpictures.de/plugins/collapse-o-matic/documentation/'>complete listing of shortcode options and attribute demos</a> are available, along with a <href='http://plugins.twinpictures.de/plugins/collapse-pro-matic/documentation/'>list of the extra features</a> that are offered with this pro version.

== Installation ==

1. Old-school: upload the `collapse-pro-matic` folder to the `/wp-content/plug-ins/` directory via FTP.  Hipster: Ironically add collapse-pro-matic via the WordPress Plug-ins menu.
1. Activate the Plug-in
1. Add a the shortcode to your post like so: `[expand title="Displayed Title Goes Here"]Hidden content goes here[/expand]`
1. Test that the this plug-in meets your demanding needs.
1. Tweak the CSS to match your flavor.
1. Rate the plug-in and verify if it works at wordpress.org.

== Frequently Asked Questions ==

= Where can I translate this plugin into my favorite language? =
<a href='http://translate.twinpictures.de/projects/colpromat'>Community translation for Collapse-Pro-Matic</a> has been set up. All are <a href='http://translate.twinpictures.de/wordpress/wp-login.php?action=register'>welcome to join</a>.

= I am a Social Netwookiee, might Twinpictures have a Facebook page? =
Yes, yes... <a href='http://www.facebook.com/twinpictures'>Twinpictures is on Facebook</a>.

= Does Twinpictures do the Twitter? =
Ah yes! <a href='http://twitter.com/#!/twinpictures'>@Twinpictures</a> does the twitter tweeting around here.

= How does one use the shortcode, exactly? =
A <a href='http://plugins.twinpictures.de/plugins/collapse-pro-matic/documentation/'>complete listing of shortcode options</a> has been provided to answer this exact question.

= How does this pro version differ from the free version? =
A <a href='http://plugins.twinpictures.de/plugins/collapse-pro-matic/documentation/'>list of the extra features</a> has also been provided.

= Is Galato the same as Ice Cream? =
No. Not even close.

== Screenshots ==

1. Hmmm... wonder what happens this text by that arrow is clicked?
2. Holy Crap! That other text just showed up like magic!  Thank you Collapse-Pro-Matic!
3. Options Page? Yes, Options Page!

== Changelog ==

= 1.3.11 =
* improved random id generation when no id is assigned
* added .expandanchor class and function to scroll-to and expand items that already have been triggered via hashtag anchors.
* added scroll to speed option
* updated the plugin licensing and update system to version 1.6.17

= 1.3.10 =
* check if colomatpauseInit is defined

= 1.3.9 =
* jQuery selector for expandanchor no longer needed.
* jQuery selectors look for exact match for targets, preventing multiple targets expanding if the id’s end the same.
* alt tag not used unless alt value is provided
* reworked the way url hashtags are handled to include manual hash changes in url
* fixed issue with global scroll offset being overwritten

= 1.3.8 =
* added Initial Pause option to pause initial collapse of expand elements on page load
* added scrolltarget attribute
* global offset attribute is now applied to all scrolling actions
* if cid is provided and no cid exists, shortcode will return blank
* updated the plugin licensing and update system to version 1.6.15
* integrated collapse-commander id and title options for shortcodes

= 1.3.7 =
* added ability to include plugin on login/registration pages
* fully tested with WordPress 4.8.1
* updated the plugin licensing and update system to version 1.6.14

= 1.3.6 =
* fixed issue with licences not validating in some instances

= 1.3.5 =
* updated the plugin licensing and update system to version 1.6.9

= 1.3.4 =
* updated the plugin licensing and update system to version 1.6.8

= 1.3.3 =
* fixed issue with duration when using a number value
* updated the plugin licensing and update system
* fixed must-be-one feature
* fixed issue with external links for nested elements
* changed links to https

= 1.3.2 =
* added must-be-one class for highlander elements
* added external class triggers

= 1.3.1 =
* integrated new collapse-commander image trigger feature
* added the ability to load plugin css only when shortcode is used
* opening a highlander grouped item will also close the nested children of the other group members as well.
* fully tested to work with WordPress 4.5

= 1.3.0 =
* Added pre expand, pre collapse and pre expand/collapse callbacks… or more clearly named: precalls
* Removed border that twas being displayed around expand on focus in some themes
* Updated the plugin licensing and updating system
* fully tested with WordPress 4.4.0

= 1.2.9 =
* Better integration for Google Maps Builder, resize on expand
* Highlander grouping will assign the a special class to non-expanded elements when a single element is expanded. See http://spacedonkey.de/2195/collapse-pro-matic-new-highlander-grouping-class-tricks/ for more info.
* Highlander grouping closed class of group_name-highlander_closed can also be automatically assigned to external elements by adding either a matching rel attribute or assigning a class with a name matching the highlander grouping rel attribute value.
* hash-bang (#!) in urls that still think it’s 2011 will now be ignored
* check if a url anchor is actually an existing element before trying to process it
* fix: maptastic class removed form target if cookie status is open
* added colomat_external_triggers. See: http://spacedonkey.de/2233/collapse-pro-matic-advanced-external-triggers/

= 1.2.8 =
* Accessibility improvements
* Fix: internal collapse triggers now work with cookies 
* Fix: anchor links will only expand elements
* Fix: tabindex attribute now allows value of 0

= 1.2.7 =
* Fix: header no longer dumped out in update screens

= 1.2.6 =
* Fix: XSS security flaw

= 1.2.5 =
* fixed bug with maptastic and google map initialise function

= 1.2.4 =
* added support for expand templates from collapse commander
* added advanced content filtering option via filter attribute and default option setting
* added edit link if using collapse-commander

= 1.2.3 =
* fixed undefined variable check for colomatoffset

= 1.2.2 =
* added togglegroup attribute for controlling collapseall/expandall/setall toggle triggers
* added intro to Collapse-Commander in settings page

= 1.2.1 =
* addressed alt attribute issue that prevented html validation

= 1.2.0 =
* added new jQuery UI animation effect: toggle slide
* added toggle slide default direction to options page
* added slideEffect, duration and direction attributes for individual expand elements
* maptastic class only removed when present
* maptastic has a max-width assigned to prevent content from entering display area.
* added new update and licensing system
* tested up to: WordPress 4.2-alpha-31047

= 1.1.5 =
* calls wp_reset_postdata function only if cid value is being used
 
= 1.1.4 =
* added trigclass and targclass to the plugin options page
* added new tabindex attribute to shortcode and options page
* changed receiptID and email registration system to password fields
* updated plugin update library

= 1.1.3 =
* added shortcode and html support for the title, swaptitle, startwrap, enwrap and excerpt attributes using placeholders
* added wpex_clean_shortcodes filter to strip unwanted p and br tags from the shortcode

= 1.1.2 =
* fixed bug with empty offset value

= 1.1.1 =
* added colomat-swap class to the swaptitle element
* added expand_on conditional auto-expand attribute
* expand_on overrides cookies
* added global offset value for URL anchors

= 1.1 =
* fixed issue when using cookies with highlander grouping
* added support for Collapse Commander add-on
* streamlined code

= 1.0.10 =
* fixed collapse/expand-all bug
* added the ability to prevent all of the plugin’s default css from loading

= 1.0.9 =
* added new colomat_expandall and colomat_collpaseall functions that can be triggered externally

= 1.0.8 =
* added expand, collapse and expand/collapse callbacks
* fixed issue with find me scrolling (again)

= 1.0.7 =
* scroll-to-trigger and scroll-on-close will now calculate scroll target after closing animation.

= 1.0.6 =
* fixed issue with offset not being calculated correctly

= 1.0.5 =
* fixed all scripts to follow script load location
* added swaptarget feature that allows single trigger to toggle between two targets
* added lockheight feature that locks the height required for the expand, even when collapsed. See: http://spacedonkey.de/1068/swapexcerpt-test/ for full demo
* streamlined code for expand/collapse all and .setall class
* advanced the .setall class to include auto toggle of expand/collapse all functions using the rel grouping attribute.  See: http://spacedonkey.de/1101/collapse-pro-matic-setall-expandcollapse-toggle/ for full demo.
* added auto to scrollonclose attribute

= 1.0.4 =
* added global cookie to set and save collapse status of all elements
* added option to only load scripts when shortcode is used
* added option to load scripts in header or footer

= 1.0.3 =
* default tag is now div
* scripts only load if shortcode is in use
* script loaded in footer
* loading js and css now hooked to wp_enqueue_scripts rather than init
* added Serbian language

= 1.0.2 =
* Updated plugin update system

= 1.0.1 =
* Fixed targtag attribute

= 1.0 =
* Registration System for secure plugin updates

= 0.6 =
* elements that have a cookie set, will be assigned the colomat-visited class
* added swapalt attribute
* removed php4 constructor
* added DE and RU languages

= 0.5 =
* fixed bug with findme
* gussied up the options page

= 0.4 =
* added list of pro features
* updated the readme file

= 0.3 =
* added the ability to trigger multiple targets

= 0.2 =
* added targtag and targpos attributes
* added the ability to insert extra external triggers

= 0.1b =
* Expanded Page Options to include Element Wrap Tag and Class

= 0.1a =
* Added Pro Page Options

== Upgrade Notice ==
* improved random id generation when no id is assigned
* added .expandanchor class and function to scroll-to and expand items that already have been triggered via hashtag anchors.
* added scroll to speed option
* updated the plugin licensing and update system to version 1.6.17