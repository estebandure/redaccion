# Copyright (C) 2015 Collapse-Pro-Matic
# This file is distributed under the same license as the Collapse-Pro-Matic package.
msgid ""
msgstr ""
"Project-Id-Version: Collapse-Pro-Matic 1.2.1\n"
"Report-Msgid-Bugs-To: http://translate.twinpictures.de/projects/colpromat\n"
"POT-Creation-Date: 2015-01-05 11:59:08+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2015-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"

#: collapse-pro-matic.php:476
msgid "Settings"
msgstr ""

#: collapse-pro-matic.php:489
msgid "Options"
msgstr ""

#: collapse-pro-matic.php:495
msgid "Collapse-Pro-Matic Settings"
msgstr ""

#: collapse-pro-matic.php:505
msgid "Style"
msgstr ""

#: collapse-pro-matic.php:512
msgid "Light"
msgstr ""

#: collapse-pro-matic.php:513
msgid "Dark"
msgstr ""

#: collapse-pro-matic.php:524
msgid "Select Light for sites with lighter backgrounds. Select Dark for sites with darker backgrounds."
msgstr ""

#: collapse-pro-matic.php:530
msgid "CID Attribute"
msgstr ""

#: collapse-pro-matic.php:532
msgid "Default %sCollapse Commander%s ID"
msgstr ""

#: collapse-pro-matic.php:537
msgid "Collapse Managment"
msgstr ""

#: collapse-pro-matic.php:538
msgid "%sCollapse Commander%s is an add-on plugin that introduces an advanced management interface to better organize expand elements and simplify expand shortcodes."
msgstr ""

#: collapse-pro-matic.php:544
msgid "Tag Attribute"
msgstr ""

#: collapse-pro-matic.php:546
msgid "HTML tag use to wrap the trigger text. See %sTag Attribute%s in the documentation for more info."
msgstr ""

#: collapse-pro-matic.php:551
msgid "Trigclass Attribute"
msgstr ""

#: collapse-pro-matic.php:553
msgid "Default class assigned to the trigger element. See %sTrigclass Attribute%s in the documentation for more info."
msgstr ""

#: collapse-pro-matic.php:558
msgid "Tabindex Attribute"
msgstr ""

#: collapse-pro-matic.php:560
msgid "Default tabindex value to be assigned to the trigger element. See %sTabindex Attribute%s in the documentation for more info."
msgstr ""

#: collapse-pro-matic.php:565
msgid "Offset Attribute"
msgstr ""

#: collapse-pro-matic.php:567
msgid "Default offset value for Findme Attribute."
msgstr ""

#: collapse-pro-matic.php:572
msgid "Wrap Tag Attribute"
msgstr ""

#: collapse-pro-matic.php:574
msgid "HTML tag use to wrap the entire collapse element. See %sElwraptag Attribute%s in the documentation for more info."
msgstr ""

#: collapse-pro-matic.php:579
msgid "Wrap Class Attribute"
msgstr ""

#: collapse-pro-matic.php:581
msgid "Class used to wrap the entire collapse element. See %sElwrapclass Attribute%s in the documentation for more info."
msgstr ""

#: collapse-pro-matic.php:586
msgid "Targtag Attribute"
msgstr ""

#: collapse-pro-matic.php:588
msgid "HTML tag use for the target element. See %sTargtag Attribute%s in the documentation for more info."
msgstr ""

#: collapse-pro-matic.php:593
msgid "Targclass Attribute"
msgstr ""

#: collapse-pro-matic.php:595
msgid "Default class assigned to the target element. See %sTargclass Attribute%s in the documentation for more info."
msgstr ""

#: collapse-pro-matic.php:600
msgid "Trigpos Attribute"
msgstr ""

#: collapse-pro-matic.php:604
msgid "Above"
msgstr ""

#: collapse-pro-matic.php:605
msgid "Below"
msgstr ""

#: collapse-pro-matic.php:616
msgid "Position the title trigger above or below the target. See %sTrigpos Attribute%s in the documentation for more info."
msgstr ""

#: collapse-pro-matic.php:621
msgid "Targpos Attribute"
msgstr ""

#: collapse-pro-matic.php:625
msgid "Block"
msgstr ""

#: collapse-pro-matic.php:626
msgid "Inline"
msgstr ""

#: collapse-pro-matic.php:637
msgid "Position the target as an inline or block element. See %sTargpos Attribute%s in the documentation for more info."
msgstr ""

#: collapse-pro-matic.php:647
msgid "Collapse/Expand Duration"
msgstr ""

#: collapse-pro-matic.php:649
msgid "A string or number determining how long the animation will run. See %sDuration%s in the documentation for more info."
msgstr ""

#: collapse-pro-matic.php:654
msgid "Animation Effect"
msgstr ""

#: collapse-pro-matic.php:661
msgid "Slide Only"
msgstr ""

#: collapse-pro-matic.php:662
msgid "Slide & Fade"
msgstr ""

#: collapse-pro-matic.php:663
msgid "Directional Slide"
msgstr ""

#: collapse-pro-matic.php:674
msgid "Animation effect to use while collapsing and expanding. See %sAnimation Effect%s in the documentation for more info."
msgstr ""

#: collapse-pro-matic.php:679
msgid "Collapse/Expand Direction"
msgstr ""

#: collapse-pro-matic.php:686
msgid "Left"
msgstr ""

#: collapse-pro-matic.php:687
msgid "Right"
msgstr ""

#: collapse-pro-matic.php:688
msgid "Up"
msgstr ""

#: collapse-pro-matic.php:689
msgid "Down"
msgstr ""

#: collapse-pro-matic.php:690
msgid "Both"
msgstr ""

#: collapse-pro-matic.php:701
msgid "The direction of the Toggle effect. See %sSlide Effect%s in the jQuery documentation for more info."
msgstr ""

#: collapse-pro-matic.php:706
msgid "Global Cookie Name"
msgstr ""

#: collapse-pro-matic.php:708
msgid "Name of global cookie used to set collapse status for ALL expand elements."
msgstr ""

#: collapse-pro-matic.php:713
msgid "Cookie Expires"
msgstr ""

#: collapse-pro-matic.php:714
msgid "Day"
msgid_plural "Days"
msgstr[0] ""
msgstr[1] ""

#: collapse-pro-matic.php:715
msgid "How many days before cookie expires."
msgstr ""

#: collapse-pro-matic.php:720
msgid "Nested Sub Expands"
msgstr ""

#: collapse-pro-matic.php:722
msgid "How many nested subexpand levels."
msgstr ""

#: collapse-pro-matic.php:727 collapse-pro-matic.php:728
msgid "Omit Default CSS"
msgstr ""

#: collapse-pro-matic.php:729
msgid "Prevent all default Collapse-Pro-Matic CSS from being loaded."
msgstr ""

#: collapse-pro-matic.php:734
msgid "Custom Style"
msgstr ""

#: collapse-pro-matic.php:736
msgid "Custom CSS style for <em>ultimate flexibility</em>"
msgstr ""

#: collapse-pro-matic.php:741
msgid "Shortcode Loads Scripts"
msgstr ""

#: collapse-pro-matic.php:742
msgid "Only load scripts with shortcode."
msgstr ""

#: collapse-pro-matic.php:743
msgid "Only load Collapse-Pro-Matic scripts if [expand] shortcode is used."
msgstr ""

#: collapse-pro-matic.php:748
msgid "Script Load Location"
msgstr ""

#: collapse-pro-matic.php:755
msgid "Header"
msgstr ""

#: collapse-pro-matic.php:756
msgid "Footer"
msgstr ""

#: collapse-pro-matic.php:767
msgid "Where should the script be loaded, in the Header or the Footer?"
msgstr ""

#: collapse-pro-matic.php:772
msgid "Expand On"
msgstr ""

#: collapse-pro-matic.php:774
msgid "A comma separated list of %sWordPress Conditional Tags%s that will auto-expand the element if true."
msgstr ""

#: collapse-pro-matic.php:779
msgid "Expand Callback"
msgstr ""

#: collapse-pro-matic.php:781
msgid "Callback to fire after any element is expanded"
msgstr ""

#: collapse-pro-matic.php:786
msgid "Collapse Callback"
msgstr ""

#: collapse-pro-matic.php:788
msgid "Callback to fire after any element is collapsed"
msgstr ""

#: collapse-pro-matic.php:793
msgid "Expand/Collapse Callback"
msgstr ""

#: collapse-pro-matic.php:795
msgid "Callback to fire after any element is expanded or collapsed"
msgstr ""

#: collapse-pro-matic.php:803
msgid "Save Changes"
msgstr ""

#: collapse-pro-matic.php:814
msgid "About"
msgstr ""

#: collapse-pro-matic.php:817
msgid "Remove clutter, save space. Display and hide additional content in a SEO friendly way. Wrap any content&mdash;including other shortcodes&mdash;into a lovely jQuery expanding and collapsing element."
msgstr ""

#: collapse-pro-matic.php:820
msgid "%sDetailed documentation%s, complete with working demonstrations of all shortcode attributes, is available for your instructional enjoyment."
msgstr ""

#: collapse-pro-matic.php:821
msgid "A %scomplete list of pro features%s is also available."
msgstr ""

#: collapse-pro-matic.php:823
msgid "If this plugin %s, please consider %ssharing your story%s with others."
msgstr ""

#: collapse-pro-matic.php:834
msgid "Register Collapse-Pro-Matic"
msgstr ""

#: collapse-pro-matic.php:836
msgid "To receive plugin updates you must register your plugin. Enter your Collapse-Pro-Matic licence key below. Licence keys may be viewed and managed by logging into %syour account%s."
msgstr ""

#: collapse-pro-matic.php:848 collapse-pro-matic.php:895
msgid "License Key"
msgstr ""

#: collapse-pro-matic.php:850 collapse-pro-matic.php:897
msgid "Enter your license key"
msgstr ""

#: collapse-pro-matic.php:857 collapse-pro-matic.php:904
msgid "License Status"
msgstr ""

#: collapse-pro-matic.php:860 collapse-pro-matic.php:907
msgid "active"
msgstr ""

#: collapse-pro-matic.php:861 collapse-pro-matic.php:908
msgid "Deactivate License"
msgstr ""

#: collapse-pro-matic.php:868 collapse-pro-matic.php:915
msgid "Activate License"
msgstr ""

#: collapse-pro-matic.php:876 collapse-pro-matic.php:923
msgid "Register"
msgstr ""

#: collapse-pro-matic.php:888
msgid "Register Collapse Commander"
msgstr ""

#: collapse-pro-matic.php:890
msgid "To receive plugin updates you must register your plugin. Enter your Collapse Commander licence key below. Licence keys may be viewed and manged by logging into %syour account%s."
msgstr ""

#: plugin-updates/EDD_SL_Plugin_Updater.php:177
msgid "There is a new version of %1$s available. <a target=\"_blank\" class=\"thickbox\" href=\"%2$s\">View version %3$s details</a>."
msgstr ""

#: plugin-updates/EDD_SL_Plugin_Updater.php:184
msgid "There is a new version of %1$s available. <a target=\"_blank\" class=\"thickbox\" href=\"%2$s\">View version %3$s details</a> or <a href=\"%4$s\">update now</a>."
msgstr ""

#: plugin-updates/EDD_SL_Plugin_Updater.php:324
msgid "You do not have permission to install plugin updates"
msgstr ""

#: plugin-updates/EDD_SL_Plugin_Updater.php:324
msgid "Error"
msgstr ""
#. Plugin Name of the plugin/theme
msgid "Collapse-Pro-Matic"
msgstr ""

#. Plugin URI of the plugin/theme
msgid "http://plugins.twinpictures.de/premium-plugins/collapse-pro-matic/"
msgstr ""

#. Description of the plugin/theme
msgid "Collapse-Pro-Matic is the premium version of Collapse-O-Matic."
msgstr ""

#. Author of the plugin/theme
msgid "twinpictures, baden03"
msgstr ""

#. Author URI of the plugin/theme
msgid "http://twinpictures.de/"
msgstr ""
