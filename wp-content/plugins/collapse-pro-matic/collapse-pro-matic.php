<?php
/**
 * @link              https://plugins.twinpictures.de/premium-plugins/collapse-pro-matic
 * @since             1.2.0
 * @package           Collapse_Pro_Matic
 *
 * @wordpress-plugin
 * Plugin Name:       Collapse-Pro-Matic
 * Plugin URI:        https://plugins.twinpictures.de/premium-plugins/collapse-pro-matic/
 * Description:       Advanced accordion plugin that adds the ability to create expanding and collapsing content elements.
 * Version:           1.3.11
 * Author:            twinpictures
 * Author URI:        https://twinpictures.de/
 * License:           GPL-2.0+
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       colpromat
 * Domain Path:       /languages
*/

if(!defined('PLUGIN_OVEN_URL')){
	define( 'PLUGIN_OVEN_URL', 'https://plugins.twinpictures.de' );
}

define( 'PLUGIN_OVEN_CPM_ID', 1669 );

if(!defined('PLUGIN_OVEN_CC_ID')){
	define( 'PLUGIN_OVEN_CC_ID', 5101 );
}

if( !class_exists( 'EDD_SL_Plugin_Updater' ) ) {
	include( dirname( __FILE__ ) . '/plugin-updates/EDD_SL_Plugin_Updater.php' );
}


/**
 * Class WP_Collapse_Pro_Matic
 * @package WP_Collapse_Pro_Matic
 * @category WordPress Plugins
 */

class WP_Collapse_Pro_Matic {

	/**
	 * Current version
	 * @var string
	 */
	var $version = '1.3.11';

	/**
	 * Used as prefix for options entry
	 * @var string
	 */
	var $domain = 'colomat';

	/**
	 * Name of the options
	 * @var string
	 */
	var $options_name = 'WP_Collapse_Pro_Matic_options';

	/**
	 * @var array
	 */
	var $options = array(
		'style' => 'light',
		'cid' => '',
		'tag' => 'div',
		'offset' => 0,
		'trigpos' => 'above',
		'trigclass' => '',
		'tabindex' => '0',
		'wraptag' => '',
		'wrapclass' => '',
		'lockheight' => '',
		'targtag' => 'div',
		'targpos' => '',
		'targclass' => '',
		'duration' => 'fast',
		'direction' => 'left',
		'slideEffect' => 'slideFade',
		'global_cookiename' => '',
		'cookie_expire' => 7,
		'custom_css' => '',
		'omit_css' => '',
		'sub_expands' => 30,
		'css_check' => '',
		'script_check' => '',
		'script_location' => 'footer',
		'expand_callback' => '',
		'collapse_callback' => '',
		'colomat_callback' => '',
		'pre_expand_callback' => '',
		'pre_collapse_callback' => '',
		'pre_colomat_callback' => '',
		'expand_on' => '',
		'filter_content' => '',
		'use_in_login' => '',
		'pauseinit' => '',
		'cc_display_id' => '',
		'cc_display_title' => '',
		'colomatscrollspeed' => 500,
	);

        var $license_group = 'colomat_licenseing';

        var $license_name = 'WP_Collapse_Pro_Matic_license';

        var $license_options = array(
                'collapse_pro_matic_license_key' => '',
                'collapse_pro_matic_license_status' => '',
                'collapse_commander_license_key' => '',
                'collapse_commander_license_status' => ''
        );

	/**
	 * PHP5 constructor
	 */
	function __construct() {

		//deal with any migration issues
		add_action( 'admin_init', array( $this, 'check_version' ) );

		// set option values
		$this->_set_options();

		// load text domain for translations
		load_plugin_textdomain( 'colpromat', FALSE, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

		//load the script and style if viewing the fontend
		add_action('wp_enqueue_scripts', array( $this, 'collapsTronicInit' ) );

		//load the scripts and style if viewing on the login/registration page
		if ( !empty($this->options['use_in_login']) ){
			add_action('login_enqueue_scripts', array( $this, 'collapsTronicInit' ) );
			add_action('login_enqueue_scripts', array( $this, 'colomat_js_vars' ) );
			//add_filter('login_message', array( $this, 'eraseme_function' ) );
		}

		// add actions
		add_action( 'admin_menu', array( $this, 'admin_menu' ) );
		add_action( 'plugin_action_links_' . plugin_basename(__FILE__), array( $this, 'plugin_actions' ) );
		add_action( 'admin_init', array( $this, 'admin_init' ) );

		//update actions
		add_action( 'admin_init', array( $this, 'edd_sl_collapse_pro_matic_updater'), 0 );

		if($this->options['script_location'] == 'footer' ){
			add_action( 'wp_footer', array( $this, 'colomat_js_vars' ) );
		}
		else{
			add_action('wp_head', array( $this, 'colomat_js_vars' ) );
		}
		add_shortcode('expand', array($this, 'shortcode'));

		//add expandsub shortcodes
		for ($i=1; $i<$this->options['sub_expands']; $i++) {
			add_shortcode('expandsub'.$i, array($this, 'shortcode'));
		}
		// Add shortcode support for widgets
		add_filter('widget_text', 'do_shortcode');
	}

	//handle upgrading from free version
	function check_version() {
		//deactivate free version if installed
		if( is_plugin_active( 'jquery-collapse-o-matic/collapse-o-matic.php' ) ){
			deactivate_plugins( 'jquery-collapse-o-matic/collapse-o-matic.php' );
		}
	}

	/*
	function eraseme_function(){
		echo 'this is a login message, how do you like it?';
		echo do_shortcode("[expand title='click me']this is the hidden content that will show up[/expand]");
	}
	*/

	//update update checker
	function edd_sl_collapse_pro_matic_updater() {
		$options = get_option($this->license_name);
		$license_key = ( !isset( $options['collapse_pro_matic_license_key'] ) ) ? '' : $options['collapse_pro_matic_license_key'];

		// setup the updater
		$edd_updater = new EDD_SL_Plugin_Updater( PLUGIN_OVEN_URL, __FILE__, array(
				'version' 	=> $this->version,
				'license' 	=> $license_key,
				'item_id'	=> PLUGIN_OVEN_CPM_ID,
				'author' 	=> 'twinpictures',
				'url'       => home_url(),
				'beta'      => false
			)
		);
	}

	//global javascript vars
	function colomat_js_vars(){
		echo "<script type='text/javascript'>\n";
		echo "var colomatduration = '".$this->options['duration']."';\n";
		echo "var colomatdirection = '".$this->options['direction']."';\n";
		echo "var colomatslideEffect = '".$this->options['slideEffect']."';\n";
		echo "var colomatpauseInit = '".$this->options['pauseinit']."';\n";
		if( empty( $this->options['colomatscrollspeed'] ) ){
			$this->options['colomatscrollspeed'] = 500;
		}
		echo "var colomatscrollspeed = ".$this->options['colomatscrollspeed'].";\n";

		if(empty($this->options['cookie_expire'])){
			$this->options['cookie_expire'] = 0;
		}
		echo "var colomatcookielife = ".$this->options['cookie_expire'].";\n";
		if(!empty($this->options['offset'])){
			echo "var colomatoffset = ".$this->options['offset'].";\n";
		}
		if(!empty($this->options['pre_expand_callback'])){
			echo "var pre_expand_callback = function(){ ".$this->options['pre_expand_callback']." };\n";
		}
		if(!empty($this->options['expand_callback'])){
			echo "var expand_callback = function(){ ".$this->options['expand_callback']." };\n";
		}
		if(!empty($this->options['pre_collapse_callback'])){
			echo "var pre_collapse_callback = function(){ ".$this->options['pre_collapse_callback']." };\n";
		}
		if(!empty($this->options['collapse_callback'])){
			echo "var collapse_callback = function(){ ".$this->options['collapse_callback']." };\n";
		}
		if(!empty($this->options['pre_colomat_callback'])){
			echo "var pre_colomat_callback = function(){ ".$this->options['pre_colomat_callback']." };\n";
		}
		if(!empty($this->options['colomat_callback'])){
			echo "var colomat_callback = function(){ ".$this->options['colomat_callback']." };\n";
		}
		echo "</script>";
		if( !empty( $this->options['custom_css'] ) ){
			echo "\n<style>\n";
			echo $this->options['custom_css'];
			echo "\n</style>\n";
		}
	}

	/**
	 * Callback init
	 */
	function collapsTronicInit() {
		//collapse script
		$load_in_footer = false;
		if($this->options['script_location'] == 'footer' ){
			$load_in_footer = true;
		}
		wp_register_script('collapseomatic-js', plugins_url('js/collapsepro.js', __FILE__), array('jquery', 'jquery-effects-slide'), '1.5.19', $load_in_footer);
		if( empty($this->options['script_check']) ){
			wp_enqueue_script('collapseomatic-js');
		}

		//css
		wp_register_style( 'collapseomatic-css', plugins_url('/'.$this->options['style'].'_style.css', __FILE__) , array (), '1.5.3' );
		if( empty($this->options['omit_css']) && empty($this->options['css_check'])){
			wp_enqueue_style( 'collapseomatic-css' );
		}

	}

	/**
	 * Callback admin_menu
	 */
	function admin_menu() {
		if ( function_exists( 'add_options_page' ) AND current_user_can( 'manage_options' ) ) {
			// add options page
			$page = add_options_page('Collapse-Pro-Matic Options', 'Collapse-Pro-Matic', 'manage_options', 'collapse-o-matic-options', array( $this, 'options_page' ));
		}
	}

	/**
	 * Callback admin_init
	 */
	function admin_init() {
		// register settings
		register_setting( $this->domain, $this->options_name );
                register_setting( $this->license_group, $this->license_name, array($this, 'edd_sanitize_license') );
	}

	/**
	 * Callback shortcode
	 */
	function shortcode($atts, $content = null){
		$options = $this->options;
		if( !empty($this->options['script_check']) ){
			wp_enqueue_script('collapseomatic-js');
		}

		if( empty($this->options['omit_css']) && !empty($this->options['css_check']) ){
			wp_enqueue_style( 'collapseomatic-css' );
		}
		//find a random number, if no id is assigned
		$ran = uniqid();
		extract(shortcode_atts(array(
			'cid' => $options['cid'],
			'template_id' => '',
			'title' => '',
			'swaptitle' => '',
			'alt' => '',
			'swapalt' => '',
			'notitle' => '',
			'id' => 'id'.$ran,
			'tag' => $options['tag'],
			'trigclass' => $options['trigclass'],
			'trigpos' => $options['trigpos'],
			'lockheight' => $options['lockheight'],
			'targtag' => $options['targtag'],
			'targclass' => $options['targclass'],
			'targpos' => $options['targpos'],
			'tabindex' => $options['tabindex'],
			'rel' => '',
			'togglegroup' => '',
			'expanded' => '',
			'expand_on' => $options['expand_on'],
			'excerpt' => '',
			'excerptpos' => 'below-trigger',
			'excerpttag' => 'div',
			'excerptclass' => '',
			'external_trigger' => '',
			'swapexcerpt' => false,
			'findme' => '',
			'offset' => $options['offset'],
			'scrollonclose' => '',
			'scrolltarget' => '',
			'startwrap' => '',
			'endwrap' => '',
			'elwraptag' => $options['wraptag'],
			'elwrapclass' => $options['wrapclass'],
			'cookiename' => '',
			'global_cookiename' => $options['global_cookiename'],
			'filter' => $options['filter_content'],
			'animation_effect' => '',
			'duration' => '',
			'direction' => '',
			'distance' => '',
		), $atts));

		if(!empty($cid)){
			$args = array(
				'post_type'	=> 'expand-element',
				'p'		=> $cid,
			);
			$query_commander = new WP_Query( $args );
			if ( $query_commander->have_posts() ) {
				while ( $query_commander->have_posts() ) {
					$query_commander->the_post();
					$title = get_the_title();

					//meta values
					$meta_values = get_post_meta( $cid );
					foreach($meta_values as $key => $value){
						if(!empty($value) && $key[0] != '_'){
							//var_dump( substr($key, 9) );
							${substr($key, 9)} = $value[0];
						}
					}
					if(!empty($triggertext)){
						$title = $triggertext;
					}
					if(!empty($highlander) && !empty($rel)){
						$rel .= '-highlander';
					}

					//content
					$content = get_the_content();
				}
			}
			else {
				return;
			}
			wp_reset_postdata();
		}

		//content filtering
		if(empty($filter) || $filter == 'false'){
			$content = do_shortcode($content);
		}
		else{
			$content = apply_filters( 'the_content', $content );
			$content = str_replace( ']]>', ']]&gt;', $content );
		}

		if( !empty($cid) && get_edit_post_link($cid) ){
			$content .= '<div class="com_edit_link"><a class="post-edit-link" href="'.get_edit_post_link($cid).'">'.__('Edit').'</a></div>';
		}

		if(!empty($template_id)){
			$args = array(
				'post_type'	=> 'expand-template',
				'p'			=> $template_id,
			);
			$query_commander = new WP_Query( $args );
			if ( $query_commander->have_posts() ) {
				while ( $query_commander->have_posts() ) {
					$query_commander->the_post();
					$meta_values = get_post_meta( $template_id );
					foreach($meta_values as $key => $value){
						if(!empty($value) && $key[0] != '_'){
							//var_dump( substr($key, 9) );
							${substr($key, 9)} = $value[0];
						}
					}
					if(!empty($highlander) && !empty($rel)){
						$rel .= '-highlander';
					}
				}
			}
			wp_reset_postdata();
		}

		//id does not allow spaces
		$id = preg_replace('/\s+/', '_', $id);

		$ewo = '';
		$ewc = '';

		//placeholders
		$placeholder_arr = array('%(%', '%)%', '%{%', '%}%');
		$swapout_arr = array('<', '>', '[', ']');

		if(!empty($trigtype) && $trigtype == 'hidden'){
			$title = '';
		}
		else{
			$title = do_shortcode(str_replace($placeholder_arr, $swapout_arr, $title));
		}

		if($swaptitle){
			$swaptitle = do_shortcode(str_replace($placeholder_arr, $swapout_arr, $swaptitle));
		}
		if($startwrap){
			$startwrap = do_shortcode(str_replace($placeholder_arr, $swapout_arr, $startwrap));
		}
		if($endwrap){
			$endwrap = do_shortcode(str_replace($placeholder_arr, $swapout_arr, $endwrap));
		}

		if($elwraptag){
			$ewclass = '';
			if($elwrapclass){
				$ewclass = 'class="'.$elwrapclass.'"';
			}
			$ewo = '<'.$elwraptag.' '.$ewclass.'>';
			$ewc = '</'.$elwraptag.'>';
		}
		$eDiv = '';
		if($content){
			$inline_class = '';
			$collapse_class = 'collapseomatic_content ';
			if($targpos == 'inline'){
				$inline_class = 'colomat-inline ';
				$collapse_class = 'collapseomatic_content_inline ';
			}
			$eDiv = '<'.$targtag.' id="target-'.$id.'" class="'.$collapse_class.$inline_class.$targclass.'">'.$content.'</'.$targtag.'>';
		}
		if($excerpt){
			$excerpt = str_replace($placeholder_arr, $swapout_arr, $excerpt);
			if(empty($filter) || $filter == 'false'){
				$excerpt = do_shortcode($excerpt);
			}
			else{
				$excerpt = apply_filters( 'the_content', $excerpt );
				$excerpt = str_replace( ']]>', ']]&gt;', $excerpt );
			}

			if($targpos == 'inline'){
				$excerpt .= $eDiv;
				$eDiv = '';
			}
			if($excerptpos == 'above-trigger'){
				$nibble = '<'.$excerpttag.' id="excerpt-'.$id.'" class="'.$excerptclass.'">'.$excerpt.'</'.$excerpttag.'>';
			}
			else{
				$nibble = '<'.$excerpttag.' id="excerpt-'.$id.'" class="collapseomatic_excerpt '.$excerptclass.'">'.$excerpt.'</'.$excerpttag.'>';
			}
			//swapexcerpt
			if($swapexcerpt !== false){
				$swapexcerpt = str_replace($placeholder_arr, $swapout_arr, $swapexcerpt);
				if(empty($filter) || $filter == 'false'){
					$swapexcerpt = do_shortcode($swapexcerpt);
				}
				else{
					$swapexcerpt = apply_filters( 'the_content', $swapexcerpt );
					$swapexcerpt = str_replace( ']]>', ']]&gt;', $swapexcerpt );
				}
				$nibble .= '<'.$excerpttag.' id="swapexcerpt-'.$id.'" style="display:none;">'.$swapexcerpt.'</'.$excerpttag.'>';
			}
		}
		$altatt = '';
		if(!empty($alt)){
			$altatt = 'title="'.$alt.'"';
		}
		else if( empty($notitle) ){
			$altatt = 'title="'.$title.'"';
		}
		$relatt = '';
		if(!empty($rel)){
			$relatt = 'rel="'.$rel.'"';
		}
		if($expanded){
			$trigclass .= ' colomat-close colomat-expanded';
		}
		else if($expand_on){
			$condition_arr = explode(',',$expand_on);
			foreach($condition_arr as $wp_cond){
				if(function_exists(trim($wp_cond)) && call_user_func(trim($wp_cond))){
					$trigclass .= ' colomat-close';
					//override cookies
					$cookiename = '';
					$global_cookiename = '';
					break;
				}
			}
		}
		$anchor = '';
		if($findme){
			$trigclass .= ' find-me';
			$loc = 'auto';
			if($findme != 'true' && $findme != 'auto'){
				$loc = $findme;
			}
			//$anchor = '<a id="find-'.$id.'" name="'.$loc.'" data-offset="'.$offset.'"> </a>';
			$anchor = ' data-offset="'.$offset.'" data-findme="'.$loc.'"';
		}
		$closeanchor = '';
		if($scrollonclose && (is_numeric($scrollonclose) || $scrollonclose == 0)){
			$trigclass .= ' scroll-to-trigger';
			if(empty($scrolltarget)){
				$closeanchor = '<a id="scrollonclose-'.$id.'" name="'.$scrollonclose.'"> </a>';
			}
		}
		$cookie = '';
		$gcookie = '';

		if($cookiename){
			$cookie = 'cookie = "'.$cookiename.'"';
		}
		if($global_cookiename){
			$gcookie = 'gcookie = "'.$global_cookiename.'"';
		}

		$inexatt = '';
		if(!empty($tabindex) || $tabindex == 0 ){
			$inexatt = 'tabindex="'.$tabindex.'"';
		}

		$data_str = '';
		if(!empty($animation_effect)){
			$data_str = 'data-animation-effect="'.$animation_effect.'"';
		}
		if(!empty($duration)){
			$data_str .= ' data-duration="'.$duration.'"';
		}
		if(!empty($direction)){
			$data_str .= ' data-direction="'.$direction.'"';
		}
		if(!empty($distance)){
			$data_str .= ' data-distance="'.$distance.'"';
		}
		if(!empty($togglegroup)){
			$data_str .= ' data-togglegroup="'.$togglegroup.'"';
		}
		if(!empty($external_trigger)){
			$data_str .= ' data-external-trigger="'.$external_trigger.'"';
		}
		if(!empty($scrolltarget)){
			$data_str .= ' data-scroll-target="'.$scrolltarget.'"';
		}
		if(!empty($anchor)){
			$data_str .= $anchor;
		}

		if( !empty($trigtype) && $trigtype == 'image' && !empty($triggerimage) && strtolower($tag) == 'img' ){
			$imageclass = 'collapseomatic noarrow' . $trigclass;
			$image_atts = array( 'id' => $id, 'class' => $imageclass, 'alt' => $alt );
			if(empty($notitle)){
				$image_atts['title'] = $alt;
			}
			$link = $closeanchor.wp_get_attachment_image( $triggerimage, 'full', false, $image_atts );
		}
		else{
			if(!empty($trigtype) && $trigtype == 'image' && !empty($triggerimage)){
				$title =  wp_get_attachment_image( $triggerimage, 'full' );
			}
			$link = $closeanchor.'<'.$tag.' class="collapseomatic '.$trigclass.'" id="'.$id.'" '.$relatt.' '.$altatt.' '.$inexatt.' '.$cookie.' '.$gcookie.' '.$anchor.' '.$data_str.'>'.$startwrap.$title.$endwrap.'</'.$tag.'>';
		}

		//swap image
		if( !empty($trigtype) && $trigtype == 'image' && !empty($swapimage) && strtolower($tag) == 'img' ){
			$link .= wp_get_attachment_image( $swapimage, 'full', false, array( 'id' => 'swap-'.$id, 'class' => 'colomat-swap', 'alt' => $swapalt, 'style' => 'display:none;' ) );
		}
		else{
			if(!empty($trigtype) && $trigtype == 'image' && !empty($swapimage)){
				$swaptitle = wp_get_attachment_image( $swapimage, 'full' );
			}
		}

		//swap title
		if($swaptitle){
			$swap_fly = '';
			if($swapalt && empty($notitle)){
				$swap_fly = 'title="'.$swapalt.'"';
			}
			$link .= "<".$tag." id='swap-".$id."' ".$swap_fly." class='colomat-swap' style='display:none;'>".$startwrap.$swaptitle.$endwrap."</".$tag.">";
		}

		if($lockheight){
			$eDiv = '<div id="lockheight-'.$id.'">'.$eDiv.'</div>';
		}

		if($excerpt){
			if($excerptpos == 'above-trigger'){
				if($trigpos == 'below'){
					$retStr = $ewo.$eDiv.$nibble.$link.$ewc;
				}
				else{
					$retStr = $ewo.$nibble.$link.$eDiv.$ewc;
				}
			}
			else if($excerptpos == 'below-trigger'){
				if($trigpos == 'below'){
					$retStr =  $ewo.$eDiv.$link.$nibble.$ewc;
				}
				else{
					$retStr = $ewo.$link.$nibble.$eDiv.$ewc;
				}
			}
			else{
				if($trigpos == 'below'){
					$retStr = $ewo.$eDiv.$link.$nibble.$ewc;
				}
				else{
					$retStr = $ewo.$link.$eDiv.$nibble.$ewc;
				}
			}
		}
		else{
			if($trigpos == 'below'){
				$retStr = $ewo.$eDiv.$link.$ewc;
			}
			else{
				$retStr = $ewo.$link.$eDiv.$ewc;
			}
		}
		return $retStr;
	}

	// Add link to options page from plugin list
	function plugin_actions($links) {
		$new_links = array();
		$new_links[] = '<a href="options-general.php?page=collapse-o-matic-options">' . __('Settings', 'colpromat') . '</a>';
		return array_merge($new_links, $links);
	}

	/**
	 * Admin options page
	 */
	function options_page() {
		$like_it_arr = array('made you feel all warm and fuzzy on the inside', 'restored your faith in humanity... even if only for a fleeting second', 'rocked your world', 'provided a positive vision of future living', 'inspired you to commit a random act of kindness', 'encouraged more regular flossing of the teeth', 'helped organize your life in the small ways that matter', 'saved your minutes--if not tens of minutes--writing your own solution', 'brightened your day... or darkened if if you are trying to sleep in', 'caused you to dance a little jig of joy and joyousness', 'inspired you to tweet a little @twinpictues social love', 'tasted great, while also being less filling');
		$rand_key = array_rand($like_it_arr);
		$like_it = $like_it_arr[$rand_key];
	?>
		<div class="wrap">
			<h2>Collapse-Pro-Matic <?php _e('Options', 'colpromat'); ?></h2>
		</div>

		<div class="postbox-container metabox-holder meta-box-sortables" style="width: 69%">
			<div style="margin:0 5px;">
				<div class="postbox">
					<h3 class="handle"><?php _e( 'Collapse-Pro-Matic Settings', 'colpromat' ) ?></h3>
					<div class="inside">
						<form method="post" action="options.php">
							<?php
								settings_fields( $this->domain );
								$options = $this->options;
							?>
							<fieldset class="options">
								<table class="form-table">
								<tr>
									<th><?php _e( 'Style', 'colpromat' ) ?>:</th>
									<td><label><select id="<?php echo $this->options_name ?>[style]" name="<?php echo $this->options_name ?>[style]">
										<?php
											if(empty($options['style'])){
												$options['style'] = 'light';
											}
											$st_array = array(
												__('Light', 'colpromat') => 'light',
												__('Dark', 'colpromat') => 'dark'
											);
											foreach( $st_array as $key => $value){
												$selected = '';
												if($options['style'] == $value){
													$selected = 'SELECTED';
												}
												echo '<option value="'.$value.'" '.$selected.'>'.$key.'</option>';
											}
										?>
										</select>
										<br /><span class="description"><?php _e('Select Light for sites with lighter backgrounds. Select Dark for sites with darker backgrounds.', 'colpromat'); ?></span></label>
									</td>
								</tr>

								<?php if( is_plugin_active( 'collapse-commander/collapse-commander.php' ) ) : ?>
								<tr>
									<th><?php _e( 'CID Attribute', 'colpromat' ) ?>:</th>
									<td><label><input type="text" id="<?php echo $this->options_name ?>[cid]" name="<?php echo $this->options_name ?>[cid]" value="<?php echo $options['cid']; ?>" />
										<br /><span class="description"><?php printf( __('Default %sCollapse Commander%s ID', 'colpromat'), '<a href="https://plugins.twinpictures.de/premium-plugins/collapse-commander/" target="_blank">', '</a>'); ?></span></label>
									</td>
								</tr>
								<?php else: ?>
								<tr>
									<th><?php _e( 'Collapse Managment', 'colpromat' ) ?></th>
									<td><?php printf(__( '%sCollapse Commander%s is an add-on plugin that introduces an advanced management interface to better organize expand elements and simplify expand shortcodes.', 'colpromat' ), '<a href="https://plugins.twinpictures.de/premium-plugins/collapse-commander/">', '</a>'); ?>
									</td>
								</tr>
								<?php endif; ?>

								<tr>
									<th><?php _e( 'Tag Attribute', 'colpromat' ) ?>:</th>
									<td><label><input type="text" id="<?php echo $this->options_name ?>[tag]" name="<?php echo $this->options_name ?>[tag]" value="<?php echo $options['tag']; ?>" />
										<br /><span class="description"><?php printf(__('HTML tag use to wrap the trigger text. See %sTag Attribute%s in the documentation for more info.', 'colpromat'), '<a href="https://plugins.twinpictures.de/plugins/collapse-o-matic/documentation/#tag" target="_blank">', '</a>'); ?></span></label>
									</td>
								</tr>

								<tr>
									<th><?php _e( 'Trigclass Attribute', 'colpromat' ) ?>:</th>
									<td><label><input type="text" id="<?php echo $this->options_name ?>[trigclass]" name="<?php echo $this->options_name ?>[trigclass]" value="<?php echo $options['trigclass']; ?>" />
										<br /><span class="description"><?php printf(__('Default class assigned to the trigger element. See %sTrigclass Attribute%s in the documentation for more info.', 'colpromat'), '<a href="https://plugins.twinpictures.de/plugins/collapse-o-matic/documentation/#trigclass" target="_blank">', '</a>'); ?></span></label>
									</td>
								</tr>

								<tr>
									<th><?php _e( 'Tabindex Attribute', 'colpromat' ) ?>:</th>
									<td><label><input type="text" id="<?php echo $this->options_name ?>[tabindex]" name="<?php echo $this->options_name ?>[tabindex]" value="<?php echo $options['tabindex']; ?>" />
										<br /><span class="description"><?php printf(__('Default tabindex value to be assigned to the trigger element. See %sTabindex Attribute%s in the documentation for more info.', 'colpromat'), '<a href="https://plugins.twinpictures.de/plugins/collapse-o-matic/documentation/#tabindex" target="_blank">', '</a>'); ?></span></label>
									</td>
								</tr>

								<tr>
									<th><?php _e( 'Scroll Target Offset', 'colpromat' ) ?>:</th>
									<td><label><input type="text" id="<?php echo $this->options_name ?>[offset]" name="<?php echo $this->options_name ?>[offset]" value="<?php echo $options['offset']; ?>" />
										<br /><span class="description"><?php _e('Global offset value for all targeted scrolling.', 'colpromat'); ?></span></label>
									</td>
								</tr>

								<?php
									if( empty( $options['colomatscrollspeed'] ) ){
										$options['colomatscrollspeed'] = 500;
									}
								?>
								<tr>
									<th><?php _e( 'Scroll Speed', 'colpromat' ) ?>:</th>
									<td><label><input type="text" id="<?php echo $this->options_name ?>[colomatscrollspeed]" name="<?php echo $this->options_name ?>[colomatscrollspeed]" value="<?php echo $options['colomatscrollspeed']; ?>" />
										<br /><span class="description"><?php _e('Global scrolling speed for all targeted scrolling.', 'colpromat'); ?></span></label>
									</td>
								</tr>

								<tr>
									<th><?php _e( 'Wrap Tag Attribute', 'colpromat' ) ?>:</th>
									<td><label><input type="text" id="<?php echo $this->options_name ?>[wraptag]" name="<?php echo $this->options_name ?>[wraptag]" value="<?php echo $options['wraptag']; ?>" />
										<br /><span class="description"><?php printf(__('HTML tag use to wrap the entire collapse element. See %sElwraptag Attribute%s in the documentation for more info.', 'colpromat'), '<a href="https://plugins.twinpictures.de/plugins/collapse-o-matic/documentation/#elwraptag" target="_blank">', '</a>'); ?></span></label>
									</td>
								</tr>

								<tr>
									<th><?php _e( 'Wrap Class Attribute', 'colpromat' ) ?>:</th>
									<td><label><input type="text" id="<?php echo $this->options_name ?>[wrapclass]" name="<?php echo $this->options_name ?>[wrapclass]" value="<?php echo $options['wrapclass']; ?>" />
										<br /><span class="description"><?php printf(__('Class used to wrap the entire collapse element. See %sElwrapclass Attribute%s in the documentation for more info.', 'colpromat'), '<a href="https://plugins.twinpictures.de/plugins/collapse-o-matic/documentation/#elwrapclass" target="_blank">', '</a>'); ?></span></label>
									</td>
								</tr>

								<tr>
									<th><?php _e( 'Targtag Attribute', 'colpromat' ) ?>:</th>
									<td><label><input type="text" id="<?php echo $this->options_name ?>[targtag]" name="<?php echo $this->options_name ?>[targtag]" value="<?php echo $options['targtag']; ?>" />
										<br /><span class="description"><?php printf(__('HTML tag use for the target element. See %sTargtag Attribute%s in the documentation for more info.', 'colpromat'), '<a href="https://plugins.twinpictures.de/plugins/collapse-o-matic/documentation/#targtag" target="_blank">', '</a>'); ?></span></label>
									</td>
								</tr>

								<tr>
									<th><?php _e( 'Targclass Attribute', 'colpromat' ) ?>:</th>
									<td><label><input type="text" id="<?php echo $this->options_name ?>[targclass]" name="<?php echo $this->options_name ?>[targclass]" value="<?php echo $options['targclass']; ?>" />
										<br /><span class="description"><?php printf(__('Default class assigned to the target element. See %sTargclass Attribute%s in the documentation for more info.', 'colpromat'), '<a href="https://plugins.twinpictures.de/plugins/collapse-o-matic/documentation/#targclass" target="_blank">', '</a>'); ?></span></label>
									</td>
								</tr>

								<tr>
									<th><?php _e( 'Trigpos Attribute', 'colpromat' ) ?>:</th>
									<td><label><select id="<?php echo $this->options_name ?>[trigpos]" name="<?php echo $this->options_name ?>[trigpos]">
										<?php
											$tp_array = array(
												__('Above', 'colpromat') => 'above',
												__('Below', 'colpromat') => 'below'
											);
											foreach( $tp_array as $key => $value){
												$selected = '';
												if($options['trigpos'] == $value){
													$selected = 'SELECTED';
												}
												echo '<option value="'.$value.'" '.$selected.'>'.$key.'</option>';
											}
										?>
										</select>
										<br /><span class="description"><?php printf(__('Position the title trigger above or below the target. See %sTrigpos Attribute%s in the documentation for more info.', 'colpromat'), '<a href="https://plugins.twinpictures.de/plugins/collapse-o-matic/documentation/#trigpos" target="_blank">', '</a>'); ?></span></label>
									</td>
								</tr>

								<tr>
									<th><?php _e( 'Targpos Attribute', 'colpromat' ) ?>:</th>
									<td><label><select id="<?php echo $this->options_name ?>[targpos]" name="<?php echo $this->options_name ?>[targpos]">
										<?php
											$tap_array = array(
												__('Block', 'colpromat') => '',
												__('Inline', 'colpromat') => 'inline'
											);
											foreach( $tap_array as $key => $value){
												$selected = '';
												if($options['targpos'] == $value){
													$selected = 'SELECTED';
												}
												echo '<option value="'.$value.'" '.$selected.'>'.$key.'</option>';
											}
										?>
										</select>
										<br /><span class="description"><?php printf(__('Position the target as an inline or block element. See %sTargpos Attribute%s in the documentation for more info.', 'colpromat'), '<a href="https://plugins.twinpictures.de/plugins/collapse-o-matic/documentation/#targpos" target="_blank">', '</a>'); ?></span></label>
									</td>
								</tr>

								<tr>
									<th><?php _e( 'Initial Pause', 'colpromat' ) ?>:</th>
									<td><label><input type="number" id="<?php echo $this->options_name ?>[pauseinit]" name="<?php echo $this->options_name ?>[pauseinit]" value="<?php echo $options['pauseinit']; ?>" />
										<br /><span class="description"><?php _e('Amount of time in milliseconds to pause before the initial collapse is triggered on page load.', 'colpromat'); ?></span></label>
									</td>
								</tr>

								<tr>
									<?php
										if(empty($options['duration'])){
												$options['duration'] = 'fast';
										}
									?>
									<th><?php _e( 'Collapse/Expand Duration', 'colpromat' ) ?>:</th>
									<td><label><input type="text" id="<?php echo $this->options_name ?>[duration]" name="<?php echo $this->options_name ?>[duration]" value="<?php echo $options['duration']; ?>" />
										<br /><span class="description"><?php printf(__('A string or number determining how long the animation will run. See %sDuration%s in the documentation for more info.', 'colpromat'), '<a href="https://plugins.twinpictures.de/plugins/collapse-o-matic/documentation/#duration" target="_blank">', '</a>'); ?></span></label>
									</td>
								</tr>

								<tr>
									<th><?php _e( 'Animation Effect', 'colpromat' ) ?>:</th>
									<td><label><select id="<?php echo $this->options_name ?>[slideEffect]" name="<?php echo $this->options_name ?>[slideEffect]">
										<?php
											if(empty($options['slideEffect'])){
												$options['slideEffect'] = 'slideFade';
											}
											$se_array = array(
												__('Slide Only', 'colpromat') => 'slideToggle',
												__('Slide & Fade', 'colpromat') => 'slideFade',
												__('Directional Slide', 'colpromat') => 'toggle'
											);
											foreach( $se_array as $key => $value){
												$selected = '';
												if($options['slideEffect'] == $value){
													$selected = 'SELECTED';
												}
												echo '<option value="'.$value.'" '.$selected.'>'.$key.'</option>';
											}
										?>
										</select>
										<br /><span class="description"><?php printf(__('Animation effect to use while collapsing and expanding. See %sAnimation Effect%s in the documentation for more info.', 'colpromat'), '<a href="https://plugins.twinpictures.de/plugins/collapse-o-matic/documentation/#animation-effect" target="_blank">', '</a>'); ?></span></label>
									</td>
								</tr>

								<tr>
									<th><?php _e( 'Collapse/Expand Direction', 'colpromat' ) ?>:</th>
									<td><label><select id="<?php echo $this->options_name ?>[direction]" name="<?php echo $this->options_name ?>[direction]">
										<?php
											if(empty($options['direction'])){
												$options['direction'] = 'left';
											}
											$dir_array = array(
												__('Left', 'colpromat') => 'left',
												__('Right', 'colpromat') => 'right',
												__('Up', 'colpromat') => 'up',
												__('Down', 'colpromat') => 'down',
												__('Both', 'colpromat') => 'both'
											);
											foreach( $dir_array as $key => $value){
												$selected = '';
												if($options['direction'] == $value){
													$selected = 'SELECTED';
												}
												echo '<option value="'.$value.'" '.$selected.'>'.$key.'</option>';
											}
										?>
										</select>
										<br /><span class="description"><?php printf(__('The direction of the Toggle effect. See %sSlide Effect%s in the jQuery documentation for more info.', 'colpromat'), '<a href="https://api.jqueryui.com/slide-effect/" target="_blank">', '</a>'); ?></span></label>
									</td>
								</tr>

								<tr>
									<th><?php _e( 'Global Cookie Name', 'colpromat' ) ?>:</th>
									<td><label><input type="text" id="<?php echo $this->options_name ?>[global_cookiename]" name="<?php echo $this->options_name ?>[global_cookiename]" value="<?php echo $options['global_cookiename']; ?>" />
										<br /><span class="description"><?php _e('Name of global cookie used to set collapse status for ALL expand elements.', 'colpromat'); ?></span></label>
									</td>
								</tr>

								<tr>
									<th><?php _e( 'Cookie Expires', 'colpromat' ) ?>:</th>
									<td><label><input type="text" id="<?php echo $this->options_name ?>[cookie_expire]" name="<?php echo $this->options_name ?>[cookie_expire]" value="<?php echo $options['cookie_expire']; ?>" /> <?php echo _n( 'Day', 'Days', $options['cookie_expire'], 'colpromat' ); ?>
										<br /><span class="description"><?php _e('How many days before cookie expires.', 'colpromat'); ?></span></label>
									</td>
								</tr>

								<tr>
									<th><?php _e( 'Nested Sub Expands', 'colpromat' ) ?>:</th>
									<td><label><input type="text" id="<?php echo $this->options_name ?>[sub_expands]" name="<?php echo $this->options_name ?>[sub_expands]" value="<?php echo $options['sub_expands']; ?>" />
										<br /><span class="description"><?php _e('How many nested subexpand levels.', 'colpromat'); ?></span></label>
									</td>
								</tr>

								<tr>
									<th><?php _e( 'Omit Default CSS', 'colpromat' ) ?>:</th>
									<td><label><input type="checkbox" id="<?php echo $this->options_name ?>[omit_css]" name="<?php echo $this->options_name ?>[omit_css]" value="1"  <?php echo checked( $options['omit_css'], 1 ); ?> /> <?php _e('Omit Default CSS', 'colpromat'); ?>
										<br /><span class="description"><?php _e('Prevent all default Collapse-Pro-Matic CSS from being loaded.', 'colpromat'); ?></span></label>
									</td>
								</tr>

								<tr>
									<th><?php _e( 'Custom Style', 'colpromat' ) ?>:</th>
									<td><label><textarea id="<?php echo $this->options_name ?>[custom_css]" name="<?php echo $this->options_name ?>[custom_css]" style="width: 100%; height: 150px;"><?php echo $options['custom_css']; ?></textarea>
										<br /><span class="description"><?php _e( 'Custom CSS style for <em>ultimate flexibility</em>', 'colpromat' ) ?></span></label>
									</td>
								</tr>

								<tr>
									<th><?php _e( 'Shortcode Loads CSS', 'colpromat' ) ?>:</th>
									<td><label><input type="checkbox" id="<?php echo $this->options_name ?>[css_check]" name="<?php echo $this->options_name ?>[css_check]" value="1"  <?php echo checked( $options['css_check'], 1 ); ?> /> <?php _e('Only load CSS with shortcode', 'colpromat'); ?>
										<br /><span class="description"><?php _e('Only load Collapse-Pro-Matic CSS if [expand] shortcode is used.', 'colpromat'); ?></span></label>
									</td>
								</tr>

								<tr>
									<th><?php _e( 'Shortcode Loads Scripts', 'colpromat' ) ?>:</th>
									<td><label><input type="checkbox" id="<?php echo $this->options_name ?>[script_check]" name="<?php echo $this->options_name ?>[script_check]" value="1"  <?php echo checked( $options['script_check'], 1 ); ?> /> <?php _e('Only load scripts with shortcode', 'colpromat'); ?>
										<br /><span class="description"><?php _e('Only load Collapse-Pro-Matic scripts if [expand] shortcode is used.', 'colpromat'); ?></span></label>
									</td>
								</tr>

								<tr>
									<th><?php _e( 'Use in Login/Registration Pages:', 'printomat' ) ?></th>
									<td><label><input type="checkbox" id="<?php echo $this->options_name ?>[use_in_login]" name="<?php echo $this->options_name ?>[use_in_login]" value="1"  <?php echo checked( $options['use_in_login'], 1 ); ?> /> <?php _e('Use in Login/Registration', 'colpromat'); ?>
										<br /><span class="description"><?php _e('Use Collapse-Pro-Matic in WordPress login and registration pages.', 'colpromat'); ?></span></label>
									</td>
								</tr>

								<tr>
									<th><?php _e( 'Filter Content', 'colpromat' ) ?>:</th>
									<td><label><input type="checkbox" id="<?php echo $this->options_name ?>[filter_content]" name="<?php echo $this->options_name ?>[filter_content]" value="1"  <?php echo checked( $options['filter_content'], 1 ); ?> /> <?php _e('Apply filter', 'colpromat'); ?>
										<br /><span class="description"><?php _e('Apply the_content filter to target content.', 'colpromat'); ?></span></label>
									</td>
								</tr>

								<?php
									//if collapse-commander is installed, display options for displaying id and text in shortocdes
									if( is_plugin_active( 'collapse-commander/collapse-commander.php' ) ) :
								?>
								<tr>
									<th><?php _e( 'Display ID', 'colpromat' ) ?>:</th>
									<td><label><input type="checkbox" id="<?php echo $this->options_name ?>[cc_display_id]" name="<?php echo $this->options_name ?>[cc_display_id]" value="1"  <?php echo checked( $options['cc_display_id'], 1 ); ?> /> <?php _e('Display ID', 'colpromat'); ?>
										<br /><span class="description"><?php _e('Display custom ID attribute in shortcodes if set for easier shortcode managment.', 'colpromat'); ?></span></label>
									</td>
								</tr>

								<tr>
									<th><?php _e( 'Display Title', 'colpromat' ) ?>:</th>
									<td><label><input type="checkbox" id="<?php echo $this->options_name ?>[cc_display_title]" name="<?php echo $this->options_name ?>[cc_display_title]" value="1"  <?php echo checked( $options['cc_display_title'], 1 ); ?> /> <?php _e('Display Title', 'colpromat'); ?>
										<br /><span class="description"><?php _e('Display custom eT attribute in shortcodes that shows expand title for easier shortcode managment.', 'colpromat'); ?></span></label>
									</td>
								</tr>
								<?php endif; ?>

								<tr>
									<th><?php _e( 'Script Load Location', 'colpromat' ) ?>:</th>
									<td><label><select id="<?php echo $this->options_name ?>[script_location]" name="<?php echo $this->options_name ?>[script_location]">
										<?php
											if(empty($options['script_location'])){
												$options['script_location'] = 'footer';
											}
											$sl_array = array(
												__('Header', 'colpromat') => 'header',
												__('Footer', 'colpromat') => 'footer'
											);
											foreach( $sl_array as $key => $value){
												$selected = '';
												if($options['script_location'] == $value){
													$selected = 'SELECTED';
												}
												echo '<option value="'.$value.'" '.$selected.'>'.$key.'</option>';
											}
										?>
										</select>
										<br /><span class="description"><?php _e('Where should the script be loaded, in the Header or the Footer?', 'colpromat'); ?></span></label>
									</td>
								</tr>

								<tr>
									<th><?php _e( 'Expand On', 'colpromat' ) ?>:</th>
									<td><label><input type="text" id="<?php echo $this->options_name ?>[expand_on]" name="<?php echo $this->options_name ?>[expand_on]" value="<?php echo $options['expand_on']; ?> " style="width: 100%;"/>
										<br /><span class="description"><?php printf( __('A comma separated list of %sWordPress Conditional Tags%s that will auto-expand the element if true.', 'colpromat'), '<a href="https://codex.wordpress.org/Conditional_Tags" target="_blank">', '</a>'); ?></span></label>
									</td>
								</tr>

								<tr>
									<th><?php _e( 'Pre Expand Callback', 'colpromat' ) ?>:</th>
									<td><label><textarea id="<?php echo $this->options_name ?>[pre_expand_callback]" name="<?php echo $this->options_name ?>[pre_expand_callback]" style="width: 100%; height: 150px;"><?php echo $options['pre_expand_callback']; ?></textarea>
										<br /><span class="description"><?php _e( 'Callback to fire before any element is expanded', 'colpromat' ) ?></span></label>
									</td>
								</tr>

								<tr>
									<th><?php _e( 'Expand Callback', 'colpromat' ) ?>:</th>
									<td><label><textarea id="<?php echo $this->options_name ?>[expand_callback]" name="<?php echo $this->options_name ?>[expand_callback]" style="width: 100%; height: 150px;"><?php echo $options['expand_callback']; ?></textarea>
										<br /><span class="description"><?php _e( 'Callback to fire after any element is expanded', 'colpromat' ) ?></span></label>
									</td>
								</tr>

								<tr>
									<th><?php _e( 'Pre Collapse Callback', 'colpromat' ) ?>:</th>
									<td><label><textarea id="<?php echo $this->options_name ?>[pre_collapse_callback]" name="<?php echo $this->options_name ?>[pre_collapse_callback]" style="width: 100%; height: 150px;"><?php echo $options['pre_collapse_callback']; ?></textarea>
										<br /><span class="description"><?php _e( 'Callback to fire before any element is collapsed', 'colpromat' ) ?></span></label>
									</td>
								</tr>

								<tr>
									<th><?php _e( 'Collapse Callback', 'colpromat' ) ?>:</th>
									<td><label><textarea id="<?php echo $this->options_name ?>[collapse_callback]" name="<?php echo $this->options_name ?>[collapse_callback]" style="width: 100%; height: 150px;"><?php echo $options['collapse_callback']; ?></textarea>
										<br /><span class="description"><?php _e( 'Callback to fire after any element is collapsed', 'colpromat' ) ?></span></label>
									</td>
								</tr>

								<tr>
									<th><?php _e( 'Pre Expand/Collapse Callback', 'colpromat' ) ?>:</th>
									<td><label><textarea id="<?php echo $this->options_name ?>[pre_colomat_callback]" name="<?php echo $this->options_name ?>[pre_colomat_callback]" style="width: 100%; height: 150px;"><?php echo $options['pre_colomat_callback']; ?></textarea>
										<br /><span class="description"><?php _e( 'Callback to fire before any element is expanded or collapsed', 'colpromat' ) ?></span></label>
									</td>
								</tr>

								<tr>
									<th><?php _e( 'Expand/Collapse Callback', 'colpromat' ) ?>:</th>
									<td><label><textarea id="<?php echo $this->options_name ?>[colomat_callback]" name="<?php echo $this->options_name ?>[colomat_callback]" style="width: 100%; height: 150px;"><?php echo $options['colomat_callback']; ?></textarea>
										<br /><span class="description"><?php _e( 'Callback to fire after any element is expanded or collapsed', 'colpromat' ) ?></span></label>
									</td>
								</tr>
								<?php if( !is_plugin_active( 'collapse-commander/collapse-commander.php' ) ) : ?>
								<tr>
									<th><strong><?php _e( 'Take Command!', 'colomat' ) ?></strong></th>
									<td><?php printf(__( '%sCollapse Commander%s is an add-on plugin that introduces an advanced management interface to better organize expand elements and simplify expand shortcodes.', 'colomat' ), '<a href="https://plugins.twinpictures.de/premium-plugins/collapse-commander/?utm_source=collapse-pro-matic&utm_medium=plugin-settings-page&utm_content=collapse-commander&utm_campaign=collapse-pro-matic-commander">', '</a>'); ?>
									</td>
								</tr>
								<?php endif; ?>
								</table>
							</fieldset>

							<p class="submit" style="margin-bottom: 20px;">
								<input class="button-primary" type="submit" value="<?php _e( 'Save Changes' ) ?>" style="float: right;" />
							</p>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="postbox-container side metabox-holder" style="width:29%;">
			<div style="margin:0 5px;">
				<div class="postbox">
					<h3><?php _e( 'About' ) ?></h3>
					<div class="inside">
						<h4><img src="<?php echo plugins_url( 'images/collapse-o-matic-icon.png', __FILE__ ) ?>" width="16" height="16"/> Collapse-Pro-Matic Version <?php echo $this->version; ?></h4>
						<p><?php _e( 'Remove clutter, save space. Display and hide additional content in a SEO friendly way. Wrap any content&mdash;including other shortcodes&mdash;into a lovely jQuery expanding and collapsing element.', 'colpromat') ?></p>
						<ul>
							<li>
								<?php printf( __( '%sDetailed documentation%s, complete with working demonstrations of all shortcode attributes, is available for your instructional enjoyment.', 'colpromat'), '<a href="https://plugins.twinpictures.de/plugins/collapse-o-matic/documentation/" target="_blank">', '</a>'); ?>
								<?php printf( __( 'A %scomplete list of pro features%s is also available.', 'colpromat'), '<a href="https://plugins.twinpictures.de/plugins/collapse-pro-matic/documentation/" target="_blank">', '</a>'); ?>
							</li>
							<li><?php printf( __('If this plugin %s, please consider %ssharing your story%s with others.', 'colpromat'), $like_it, '<a href="https://www.facebook.com/twinpictures" target="_blank">', '</a>' ) ?></li>
							<li>Your comments, bug-reports, feedback and cocktail recipes are always welcomed at the <a href="https://plugins.twinpictures.de/premium-plugins/collapse-pro-matic/" target="_blank">Twinpictues Plugin Oven</a>.</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="postbox-container side metabox-holder" style="width:29%;">
			<div style="margin:0 5px;">
				<div class="postbox">
					<h3 class="handle"><?php _e( 'Register Collapse-Pro-Matic', 'colpromat') ?></h3>
					<div class="inside">
						<p><?php printf( __('To receive plugin updates you must register your plugin. Enter your Collapse-Pro-Matic licence key below. Licence keys may be viewed and manged by logging into %syour account%s.', 'colpromat'), '<a href="https://plugins.twinpictures.de/your-account/" target="_blank">', '</a>'); ?></p>
						<form method="post" action="options.php">
                                                    <?php
                                                        settings_fields( $this->license_group );
                                                        $options = get_option($this->license_name);
                                                        $cpm_licence = ( !isset( $options['collapse_pro_matic_license_key'] ) ) ? '' : $options['collapse_pro_matic_license_key'];
                                                        $cc_licence = ( !isset( $options['collapse_commander_license_key'] ) ) ? '' : $options['collapse_commander_license_key'];
                                                    ?>
							<fieldset>
								<table style="width: 100%">
									<tbody>
										<tr>
											<th><?php _e( 'License Key', 'colpromat' ) ?>:</th>
											<td><label for="<?php echo $this->license_name ?>[collapse_pro_matic_license_key]"><input type="text" id="<?php echo $this->license_name ?>[collapse_pro_matic_license_key]" name="<?php echo $this->license_name ?>[collapse_pro_matic_license_key]" value="<?php esc_attr_e( $cpm_licence ); ?>" style="width: 100%" />
												<br /><span class="description"><?php _e('Enter your license key', 'colpromat'); ?></span></label>
                                                                                        </td>

										</tr>

										<?php if( isset($options['collapse_pro_matic_license_key']) ) { ?>
                                                                                    <tr valign="top">
                                                                                        <th><?php _e('License Status', 'colpromat'); ?>:</th>
                                                                                        <td>
                                                                                            <?php if( isset($options['collapse_pro_matic_license_status']) && $options['collapse_pro_matic_license_status'] == 'valid' ) { ?>
                                                                                                <span style="color:green;"><?php _e('active'); ?></span><br/>
                                                                                                <input type="submit" class="button-secondary" name="edd_cpm_license_deactivate" value="<?php _e('Deactivate License'); ?>"/>
                                                                                            <?php } else {
                                                                                                    if( isset($options['collapse_pro_matic_license_status'])){ ?>
                                                                                                        <span style="color: red"><?php echo $options['collapse_pro_matic_license_status']; ?></span><br/>
                                                                                                <?php } else { ?>
                                                                                                        <span style="color: grey">inactive</span><br/>
                                                                                                <?php } ?>
                                                                                                    <input type="submit" class="button-secondary" name="edd_cpm_license_activate" value="<?php _e('Activate License'); ?>"/>
                                                                                            <?php } ?>
                                                                                            </td>
                                                                                    </tr>
										<?php } ?>
									</tbody>
								</table>
							</fieldset>
							<?php submit_button( __( 'Register', 'colpromat') ); ?>
						<!--</form>-->
					</div>
				</div>
			</div>
		</div>

		<?php if( is_plugin_active( 'collapse-commander/collapse-commander.php' ) ) : ?>

		<div class="postbox-container side metabox-holder" style="width:29%;">
			<div style="margin:0 5px;">
				<div class="postbox">
					<h3 class="handle"><?php _e( 'Register Collapse Commander', 'colpromat') ?></h3>
					<div class="inside">
                                            <p><?php printf( __('To receive plugin updates you must register your plugin. Enter your Collapse Commander licence key below. Licence keys may be viewed and managed by logging into %syour account%s.', 'colpromat'), '<a href="https://plugins.twinpictures.de/your-account/" target="_blank">', '</a>'); ?></p>
                                                <fieldset>
                                                        <table style="width: 100%">
                                                                <tbody>
                                                                        <tr>
                                                                                <th><?php _e( 'License Key', 'colpromat' ) ?>:</th>
                                                                                <td><label for="<?php echo $this->license_name ?>[collapse_commander_license_key]"><input type="text" id="<?php echo $this->license_name ?>[collapse_commander_license_key]" name="<?php echo $this->license_name ?>[collapse_commander_license_key]" value="<?php esc_attr_e( $cc_licence ); ?>" style="width: 100%" />
                                                                                        <br /><span class="description"><?php _e('Enter your license key', 'colpromat'); ?></span></label>
                                                                                </td>

                                                                        </tr>

                                                                        <?php if( isset($options['collapse_commander_license_key']) ) { ?>
                                                                            <tr valign="top">
                                                                                <th><?php _e('License Status', 'colpromat'); ?>:</th>
                                                                                <td>
                                                                                    <?php if( isset($options['collapse_commander_license_status']) && $options['collapse_commander_license_status'] == 'valid' ) { ?>
                                                                                        <span style="color:green;"><?php _e('active'); ?></span><br/>
                                                                                        <input type="submit" class="button-secondary" name="edd_cc_license_deactivate" value="<?php _e('Deactivate License'); ?>"/>
                                                                                    <?php } else {
                                                                                            if( isset($options['collapse_commander_license_status']) ){ ?>
                                                                                                <span style="color: red"><?php echo $options['collapse_commander_license_status']; ?></span><br/>
                                                                                        <?php } else { ?>
                                                                                                <span style="color: grey">inactive</span><br/>
                                                                                        <?php } ?>
                                                                                            <input type="submit" class="button-secondary" name="edd_cc_license_activate" value="<?php _e('Activate License'); ?>"/>
                                                                                    <?php } ?>
                                                                                    </td>
                                                                            </tr>
                                                                        <?php } ?>
                                                                </tbody>
                                                        </table>
                                                </fieldset>
                                                <?php submit_button( __( 'Register', 'colpromat') ); ?>
					</div>
				</div>
			</div>
		</div>
		<?php else: ?>
		<div class="postbox-container side metabox-holder meta-box-sortables" style="width:29%;">
			<div style="margin:0 5px;">
				<div class="postbox">
					<div class="handlediv" title="<?php _e( 'Click to toggle', 'colomat' ) ?>"><br/></div>
					<h3 class="hndle">Collapse Commander</h3>
						<div class="inside">
							<p>A brief overview of <a href="https://plugins.twinpictures.de/premium-plugins/collapse-commander/?utm_source=collapse-pro-matic&utm_medium=plugin-settings-page&utm_content=collapse-commander&utm_campaign=collapse-pro-matic-commander">Collapse Commander</a>, a new add-on plugin for Collapse-Pro-Matic that adds and advanded expand shortcode management system.</p>
							<iframe width="100%" height="300" src="//www.youtube.com/embed/w9X4nXpAEfo" frameborder="0" allowfullscreen></iframe>
						</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		<?php endif; ?>
		</form>
	<?php
	}

	/**
	 * Set options from save values or defaults
	 */
	function _set_options() {
		// set options
		$saved_options = get_option( $this->options_name );

		// backwards compatible (old values)
		if ( empty( $saved_options ) ) {
			$saved_options = get_option( $this->domain . 'options' );
		}

		// set all options
		if ( ! empty( $saved_options ) ) {
			foreach ( $this->options AS $key => $option ) {
				if($key == 'tabindex'){
					$this->options[ $key ] = ( empty( $saved_options[ $key ] ) ) ? '0' : $saved_options[ $key ];
				}
				else{
					$this->options[ $key ] = ( empty( $saved_options[ $key ] ) ) ? '' : $saved_options[ $key ];
				}

			}
		}
	}

    function edd_sanitize_license( $new ) {
        //collapse-pro-matic
        $options = get_option($this->license_name);
        $old_cpm = ( !isset( $options['collapse_pro_matic_license_key'] ) ) ? '' : $options['collapse_pro_matic_license_key'];
        $old_cc = ( !isset( $options['collapse_commander_license_key'] ) ) ? '' : $options['collapse_commander_license_key'];
        $old_cpm_status = ( !isset( $options['collapse_pro_matic_license_status'] ) ) ? '' : $options['collapse_pro_matic_license_status'];
        $old_cc_status = ( !isset( $options['collapse_commander_license_status'] ) ) ? '' : $options['collapse_commander_license_status'];

        //$old_cpm = $this->license_options['collapse_pro_matic_license_key'];
        if( !empty($old_cpm) && $old_cpm != $new['collapse_pro_matic_license_key'] ) {
                $new['collapse_pro_matic_license_status'] = '';
        }
        else{
            $new['collapse_pro_matic_license_status'] = $old_cpm_status;
        }

        if( isset( $_POST['edd_cpm_license_activate'] ) ) {
            $new['collapse_pro_matic_license_status'] = $this->collapse_pro_matic_activate_license( PLUGIN_OVEN_CPM_ID, $new['collapse_pro_matic_license_key'], 'activate_license');
        }

        if( isset( $_POST['edd_cpm_license_deactivate'] ) ) {
            $new['collapse_pro_matic_license_status'] = $this->collapse_pro_matic_activate_license( PLUGIN_OVEN_CPM_ID, $new['collapse_pro_matic_license_key'], 'deactivate_license');
        }

        //collapse commander
        if( !empty($old_cc) && $old_cc != $new['collapse_commander_license_key'] ) {
                $new['collapse_commander_license_status'] = '';
        }
        else{
            $new['collapse_commander_license_status'] = $old_cc_status;
        }

        if( isset( $_POST['edd_cc_license_activate'] ) ) {
            $new['collapse_commander_license_status'] = $this->collapse_pro_matic_activate_license( PLUGIN_OVEN_CC_ID, $new['collapse_commander_license_key'], 'activate_license');
        }

        if( isset( $_POST['edd_cc_license_deactivate'] ) ) {
            $new['collapse_commander_license_status'] = $this->collapse_pro_matic_activate_license( PLUGIN_OVEN_CC_ID, $new['collapse_commander_license_key'], 'deactivate_license');
        }
        return $new;
    }


	/************************************
	* activate/deactivate license key
	*************************************/

	function collapse_pro_matic_activate_license($plugin_id, $license_key, $edd_action) {
            // data to send in our API request
            $api_params = array(
                    'edd_action'    => $edd_action,
                    'license' 	    => $license_key,
                    'item_id'     	=> $plugin_id,
                    'url'           => home_url()
            );

            // Call the custom API.
			$response = wp_remote_post( PLUGIN_OVEN_URL, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

			// make sure the response came back okay
            if ( is_wp_error( $response ) || 200 !== wp_remote_retrieve_response_code( $response ) ) {
                    return false;
			}
            // decode the license data
            $license_data = json_decode( wp_remote_retrieve_body( $response ) );
            // $license_data->license will be either "valid" or "invalid"
            return $license_data->license;
	}

} // end class WP_Collapse_Pro_Matic


/**
 * Create instance
 */
$WP_Collapse_Pro_Matic = new WP_Collapse_Pro_Matic;

//clean unwanted p and br tags from shortcodes
//http://www.wpexplorer.com/clean-up-wordpress-shortcode-formatting
if (!function_exists('tp_clean_shortcodes')) {
	function tp_clean_shortcodes($content){
		$array = array (
		    '<p>[' => '[',
		    ']</p>' => ']',
		    ']<br />' => ']'
		);
		$content = strtr($content, $array);
		return $content;
	}
	add_filter('the_content', 'tp_clean_shortcodes');
}
?>
