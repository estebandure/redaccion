            <div class="wp-block-group PagosHeader">
               <div class="wp-block-group__inner-container">
                  <h3 class="has-text-align-center Miembrosx1Col PFTittlePagos wpsm_panelTT">¡Gracias por sumarte a nuestra comunidad de co-responsables!</h3>
                  <p class="has-text-align-center"><em>Tu aporte es fundamental para que nuestro periodismo sea abierto, sume más voces y logre un mayor impacto.</em></p>
                  <div class="PagosHeaderSep"></div>
               </div>
            </div>
            <div class="wp-block-group BloquePagos">
               <div class="wp-block-group__inner-container">
                  <p><strong>Como co-responsable vas a acceder a:</strong></p>
                  <ul class="BeneficiosCo-responsables">
                     <li>El Reporte de los Lunes <em>(El panorama semanal de Chani Guyot)</em></li>
                     <li>Acceso a servicios exclusivos <em>(recibí nuestros contenidos por WhatsApp)</em></li>
                     <li>Invitaciones exclusivas a los eventos de RED/ACCIÓN</li>
                     <li>La posibilidad de participar de nuestro periodismo sugiriendo temas y enfoques</li>
                     <li>La newsletter semanal para miembros</li>
                     <li>Lo más importante: vas a ayudar a que este periodismo con impacto siga siendo abierto para todos</li>
                  </ul>
                  <p class="has-text-color has-vivid-red-color"><br><strong>Como agradecimiento, tenemos un regalo de bienvenida</strong>...</p>
                  <div class="wp-block-columns">
                     <div class="wp-block-column" style="flex-basis:18%">
                        <div class="wp-block-image BloquesPagoLibro">
                           <figure class="aligncenter size-medium"><img data-attachment-id="56919" data-permalink="https://www.redaccion.com.ar/100-libros-para-entender-el-mundo/redaccion_100libros/" data-orig-file="https://i0.wp.com/www.redaccion.com.ar/wp-content/uploads/2019/11/Redaccion_100libros.png?fit=600%2C600&amp;ssl=1" data-orig-size="600,600" data-comments-opened="0" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="Redaccion_100libros" data-image-description="" data-medium-file="https://i0.wp.com/www.redaccion.com.ar/wp-content/uploads/2019/11/Redaccion_100libros.png?fit=238%2C238&amp;ssl=1" data-large-file="https://i0.wp.com/www.redaccion.com.ar/wp-content/uploads/2019/11/Redaccion_100libros.png?fit=600%2C600&amp;ssl=1" loading="lazy" src="https://i0.wp.com/www.redaccion.com.ar/wp-content/uploads/2019/11/Redaccion_100libros.png?resize=238%2C238&amp;ssl=1" alt="" class="wp-image-56919" srcset="https://i0.wp.com/www.redaccion.com.ar/wp-content/uploads/2019/11/Redaccion_100libros.png?resize=238%2C238&amp;ssl=1 238w, https://i0.wp.com/www.redaccion.com.ar/wp-content/uploads/2019/11/Redaccion_100libros.png?resize=300%2C300&amp;ssl=1 300w, https://i0.wp.com/www.redaccion.com.ar/wp-content/uploads/2019/11/Redaccion_100libros.png?w=600&amp;ssl=1 600w, https://i0.wp.com/www.redaccion.com.ar/wp-content/uploads/2019/11/Redaccion_100libros.png?resize=100%2C100&amp;ssl=1 100w" sizes="(max-width: 238px) 100vw, 238px" data-recalc-dims="1" width="238" height="238"></figure>
                        </div>
                        <p></p>
                     </div>
                     <div class="wp-block-column BloquesPagoLibroCol" style="flex-basis:82%">
                        <p><br><strong>“100 libros para entender el mundo”</strong>. Te enviamos a tu casa un ejemplar del libro en el que escritores, editores, pensadores y periodistas recomiendan sus libros favoritos. Con texto del María O'Donell, Andrés Malamud, Claudia Piñeiro, Juan Llach, Graciela Fernández Meijide y Marcos Novaro, entre muchos otros. (Envíos dentro de la Argentina. PDF para el exterior)</p>
                     </div>
                  </div>
                  <div style="height:20px" aria-hidden="true" class="wp-block-spacer"></div>
               </div>
            </div>

