
            <div class="wp-block-group BloquePagos Paso1">
               <div class="wp-block-group__inner-container">
                  <div style="height:20px" aria-hidden="true" class="wp-block-spacer"></div>

                  <p><strong>Tu email: </strong></p>

                  <div class="wpforms-container wpforms-container-full" id="wpforms-99552">
                        <noscript class="wpforms-error-noscript">Por favor, activa JavaScript en tu navegador para completar este formulario.</noscript>
                        <div class="wpforms-field-container">
                           <div id="wpforms-99552-field_2-container" class="wpforms-field wpforms-field-email correo-miembros-pagos wpforms-one-half wpforms-first" data-field-id="2">
                               <label class="wpforms-field-label wpforms-label-hide" for="wpforms-99552-field_2">Correo electrónico <span class="wpforms-required-label">*</span></label>
                               <input type="email" id="email" class="wpforms-field-medium wpforms-field-required" name="email" placeholder="Ingresá tu e-mail" style="width: 75%;" required>
                           </div>
                        </div>
                  </div>

                  <div style="height:20px" aria-hidden="true" class="wp-block-spacer"></div>
               </div>
            </div>
