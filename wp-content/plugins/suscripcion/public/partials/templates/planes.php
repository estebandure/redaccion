<script type="text/javascript">

    function setPlanCustomMensual(){

        jQuery('#paso1-email-form #plan_id').val('CM');
        jQuery("#membresiasPlanes div.boxPlan").removeClass('boxPlanSelected');
        jQuery('#boxPlanCM').addClass('boxPlanSelected');
        jQuery('#montoMensual').show(500);
        jQuery('#montoAnual').hide(500);

        clearError()

        return false;
    }

    function setPlanCustomAnual(){

         jQuery('#paso1-email-form #plan_id').val('CA');
         jQuery("#membresiasPlanes div.boxPlan").removeClass('boxPlanSelected');
         jQuery('#boxPlanCA').addClass('boxPlanSelected');

         jQuery('#montoAnual').show(500);
         jQuery('#montoMensual').hide(500);

         clearError()

         return false;
    }

    function setPlan(id){

        jQuery('#paso1-email-form #plan_id').val(id);

        jQuery("#membresiasPlanes div.boxPlan").removeClass('boxPlanSelected');
        jQuery('#boxPlan'+ id).addClass('boxPlanSelected');

        jQuery('#montoMensual').hide(500);
        jQuery('#montoAnual').hide(500);

        clearError();

        return false;
    }

    jQuery( document ).ready(function() {
        jQuery(".numeric").numeric({
            allowMinus     : false,
            allowThouSep     : false,
            allowDecSep      : false,
            maxDigits        : 5 ,
            maxDecimalPlaces:0,

        });
    });


</script>

            <div class="wp-block-group BloquePagos Paso2 BloquePagosActivo " id="membresiasPlanes">
               <div class="wp-block-group__inner-container">
                  <div style="height:20px" aria-hidden="true" class="wp-block-spacer"></div>
                  <p class="Miembros-Aporte-SubTit"><strong>Elegí cuánto querés aportar: </strong></p>
                  <p class="Miembros-Aporte-SubTit">Mensual:</p>
                  <div class="wp-block-columns Bt_pagos">
                     <div class="wp-block-column boxPlan" id="boxPlanCM">
                        <p><a href="/membresias/mensual" onclick="return setPlanCustomMensual();">Otro monto</a></p>

                        <input type="text" id="montoMensual" class="numeric h2-field-medium wpforms-field-required" value="" name="montoMensual" placeholder="Monto" maxlength="5" style="width: 100%; text-align: center; display: none;">
                     </div>
                     <?php
                        $planesMensuales = Usuario_Plan::getUsuarioPlanMesuales();

                        foreach($planesMensuales as $plan){
                     ?>
                     <div class="wp-block-column boxPlan" id="boxPlan<?php echo $plan->plan_id; ?>">
                        <p><a href="#" onclick="return setPlan(<?php echo $plan->plan_id; ?>);" class="PagoActivo">$ <?php echo intval($plan->valor); ?></a></p>
                     </div>
                     <?php
                        }
                     ?>
                  </div>
                  <p class="Miembros-Aporte-SubTit">Anual:</p>
                  <div class="wp-block-columns Bt_pagos">
                     <div class="wp-block-column boxPlan" id="boxPlanCA" >
                        <p><a href="/membresias/anual" onclick="return setPlanCustomAnual();">Otro monto</a></p>

                        <input type="text" id="montoAnual" class="numeric wpforms-field-medium wpforms-field-required" value="" name="montoAnual" placeholder="Monto" maxlength="5" style="width: 100%; text-align: center; display: none;">
                     </div>
                     <?php
                        $planesAnuales = Usuario_Plan::getUsuarioPlanAnuales();
                        foreach($planesAnuales as $plan){
                     ?>
                     <div class="wp-block-column boxPlan" id="boxPlan<?php echo $plan->plan_id; ?>">
                        <p><a href="#" onclick="return setPlan(<?php echo $plan->plan_id; ?>);" class="PagoActivo">$ <?php echo intval($plan->valor); ?></a></p>
                     </div>
                     <?php
                        }
                     ?>
                  </div>
               </div>
            </div>

