<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://christophercasper.com/
 * @since      1.0.0
 *
 * @package    Suscripcion_Plugin
 * @subpackage Suscripcion_Plugin/public/partials
 */


global $wp_query;
global $wp_session;
global $wpdb;

if(!session_id()) {session_start(); }

$wp_query->is_home = false;


// veo si existe como usuario temporal - registro en curso
$userTemp = null;

$user_temp_id = isset($_SESSION['suscripcion_user_temp_id']) ? $_SESSION['suscripcion_user_temp_id']: null;
if(! empty($user_temp_id )  && is_numeric($user_temp_id)){
   $userTemp = Usuario_Temp::getUsuarioTempById($user_temp_id);
}


if(is_null($userTemp)){
      // voy al paso 1
   wp_redirect('/membresias');
   exit;
}

//el usuario Temp existe

if($userTemp->paso4){
   // ya esta registrado!!

   // borro la session
   $_SESSION['suscripcion_user_temp_id'] = null;
   unset($_SESSION['suscripcion_user_temp_id']);  // ??
   // guardo la session....


   // voy al login
   wp_redirect('/membresias/login');
   exit;
}

if($userTemp->paso3 && !$userTemp->paso4 ){
   // falta cargar los datos
   // voy al paso 4
   wp_redirect('/membresias/paso4');
   exit;

}

if(! empty( $_POST['plan_id'])){
    // proceso

    $plan_id =  (int)$_POST['plan_id'];

    $userPlan = Usuario_Plan::getUsuarioPlanById($plan_id);

    // veo si existe el plan_id;

    if(is_null($userPlan)){
        // no existe el plan!!
        // borro la session
        $_SESSION['suscripcion_user_temp_id'] = null;
        unset($_SESSION['suscripcion_user_temp_id']);  // ??

        // voy al paso 1
        wp_redirect('/membresias/paso1');
        exit;
    }


    // lo agrego al usuario temporal
    $dataUserTemp = [
      'paso2'          => 1,
      'plan_id'        => $plan_id,
      'preapproval_id' => $userPlan->plan_mp_id,
      'fecha'          => date("Y-m-d H:i:s")
    ];

    $where = [ 'utemp_id' => $userTemp->utemp_id];
    $wpdb->update( $wpdb->prefix . 'users_temp', $dataUserTemp, $where );


    // voy a pagar con la tarjeta - paso 3
    wp_redirect('/membresias/pagar');
    exit;

}

 wp_enqueue_script("jquery");

// Lets load the header of the site
get_header();
?>

<form action="" method="post" id="formPlan">
    <input type="hidden" id="plan_id" name="plan_id" value="" >
</form>
<script type="text/javascript">
    function setPlan($id){
        jQuery('#formPlan #plan_id').val($id);
        jQuery('#formPlan').submit();
        return false;
    }
</script>

<div id="content" class="site-content-full">
   <main id="main" class="site-main">
      <div id="post-92841" class="post-92841 page type-page status-publish hentry">
         <div class="">
            <?php /*
            <div class="wp-block-group PagosHeader">
               <div class="wp-block-group__inner-container">
                  <h3 class="has-text-align-center Miembrosx1Col PFTittlePagos wpsm_panelTT">¡Gracias por sumarte a nuestra comunidad de co-responsables!</h3>
                  <p class="has-text-align-center"><em>Tu aporte es fundamental para que nuestro periodismo sea abierto, sume más voces y logre un mayor impacto.</em></p>
                  <div class="PagosHeaderSep"></div>
               </div>
            </div>
            <div class="wp-block-group BloquePagos">
               <div class="wp-block-group__inner-container">
                  <p><strong>Como co-responsable vas a acceder a:</strong></p>
                  <ul class="BeneficiosCo-responsables">
                     <li>El Reporte de los Lunes <em>(El panorama semanal de Chani Guyot)</em></li>
                     <li>Acceso a servicios exclusivos <em>(recibí nuestros contenidos por WhatsApp)</em></li>
                     <li>Invitaciones exclusivas a los eventos de RED/ACCIÓN</li>
                     <li>La posibilidad de participar de nuestro periodismo sugiriendo temas y enfoques</li>
                     <li>La newsletter semanal para miembros</li>
                     <li>Lo más importante: vas a ayudar a que este periodismo con impacto siga siendo abierto para todos</li>
                  </ul>
                  <p class="has-text-color has-vivid-red-color"><br><strong>Como agradecimiento, tenemos un regalo de bienvenida</strong>...</p>
                  <div class="wp-block-columns">
                     <div class="wp-block-column" style="flex-basis:18%">
                        <div class="wp-block-image BloquesPagoLibro">
                           <figure class="aligncenter size-medium"><img data-attachment-id="56919" data-permalink="https://www.redaccion.com.ar/100-libros-para-entender-el-mundo/redaccion_100libros/" data-orig-file="https://i0.wp.com/www.redaccion.com.ar/wp-content/uploads/2019/11/Redaccion_100libros.png?fit=600%2C600&amp;ssl=1" data-orig-size="600,600" data-comments-opened="0" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="Redaccion_100libros" data-image-description="" data-medium-file="https://i0.wp.com/www.redaccion.com.ar/wp-content/uploads/2019/11/Redaccion_100libros.png?fit=238%2C238&amp;ssl=1" data-large-file="https://i0.wp.com/www.redaccion.com.ar/wp-content/uploads/2019/11/Redaccion_100libros.png?fit=600%2C600&amp;ssl=1" loading="lazy" src="https://i0.wp.com/www.redaccion.com.ar/wp-content/uploads/2019/11/Redaccion_100libros.png?resize=238%2C238&amp;ssl=1" alt="" class="wp-image-56919" srcset="https://i0.wp.com/www.redaccion.com.ar/wp-content/uploads/2019/11/Redaccion_100libros.png?resize=238%2C238&amp;ssl=1 238w, https://i0.wp.com/www.redaccion.com.ar/wp-content/uploads/2019/11/Redaccion_100libros.png?resize=300%2C300&amp;ssl=1 300w, https://i0.wp.com/www.redaccion.com.ar/wp-content/uploads/2019/11/Redaccion_100libros.png?w=600&amp;ssl=1 600w, https://i0.wp.com/www.redaccion.com.ar/wp-content/uploads/2019/11/Redaccion_100libros.png?resize=100%2C100&amp;ssl=1 100w" sizes="(max-width: 238px) 100vw, 238px" data-recalc-dims="1" width="238" height="238"></figure>
                        </div>
                        <p></p>
                     </div>
                     <div class="wp-block-column BloquesPagoLibroCol" style="flex-basis:82%">
                        <p><br><strong>“100 libros para entender el mundo”</strong>. Te enviamos a tu casa un ejemplar del libro en el que escritores, editores, pensadores y periodistas recomiendan sus libros favoritos. Con texto del María O'Donell, Andrés Malamud, Claudia Piñeiro, Juan Llach, Graciela Fernández Meijide y Marcos Novaro, entre muchos otros. (Envíos dentro de la Argentina. PDF para el exterior)</p>
                     </div>
                  </div>
                  <div style="height:20px" aria-hidden="true" class="wp-block-spacer"></div>
               </div>
            </div>
            */ ?>
            <div class="wp-block-group BloquePagos Paso1">
               <div class="wp-block-group__inner-container">
                  <div style="height:20px" aria-hidden="true" class="wp-block-spacer"></div>
                  <p><strong>Tu email: </strong></p>
                  <div class="wpforms-confirmation-container-full wpforms-confirmation-scroll" id="wpforms-confirmation-99552">
                     <p>¡Genial, avancemos!</p>
                  </div>
                  <div style="height:20px" aria-hidden="true" class="wp-block-spacer"></div>
               </div>
            </div>


             <?php   require_once( plugin_dir_path( __FILE__ ) . 'templates/planes.php'); ?>


            <div class="wp-block-group BloquePagos">
               <div class="wp-block-group__inner-container">
                  <div style="height:36px" aria-hidden="true" class="wp-block-spacer"></div>
                  <p>Podés cancelar tu membresía en cualquier momento. Si necesitás ayuda para activarla o darla de baja, por favor mandanos un mail a <a href="mailto:miembros@redaccion.com.ar">miembros@redaccion.com.ar</a> o llamanos al +54 1149171763 </p>
                  <div style="height:36px" aria-hidden="true" class="wp-block-spacer"></div>
               </div>
            </div>
         </div>
      </div>
   </main>
</div>




<?php
// Load the footer
get_footer();
