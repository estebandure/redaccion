<?php

global $wp_query;
global $wp_session;
global $wpdb;

$wp_query->is_home = false;

if(!session_id()) {session_start(); }

//chekeo que sea una llamada ajax
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    // OK
}else {
    // Error!
    wp_redirect('/membresias');
    exit;
}


if(empty( $_POST['email'])){
    // Error!
    $result = [
        'error'    =>  true,
        'msgError' =>  'Debe ingresar un email',
    ];
    wp_send_json($result);
    exit();
}

if(empty( $_POST['plan_id'])){
    // Error!
    $result = [
        'error'    =>  true,
        'msgError' =>  'Debe seleccionar un Plan',
    ];
    wp_send_json($result);
    exit();
}

/*******************************************************************/
/*                     proceso  el email                           */
/*******************************************************************/

    $email =  $_POST['email'];

    // veo se hay un usuario logueado  - lo envio al area del usuario
    if(is_user_logged_in()){
       $userLogged = wp_get_current_user();
       if($userLogged->user_email == $email){
           // es el email del usaurio logueado...
           $result = [
                'error'       =>  false,
                'urlRedirect' =>  '/membresias/usuario',
           ];
           wp_send_json($result);
           exit;
       }
    }

    // veo si existe como usuario registrado
    $usaurioRegistrado = get_user_by( 'email', $email );
    if($usaurioRegistrado !== false){
       $result = [
            'error'       =>  false,
            'urlRedirect' =>  '/membresias/login',
       ];
       wp_send_json($result);
       exit;
    }

    // veo si existe como usuario temporal - registro en curso
    $userTemp = null;

    $user_temp_id = isset($_SESSION['suscripcion_user_temp_id']) ? $_SESSION['suscripcion_user_temp_id']: null;
    if(! empty($user_temp_id )  && is_numeric($user_temp_id)){
        $userTemp = Usuario_Temp::getUsuarioTempById($user_temp_id);
    }

    $existsUserTemp  = false;

    if(!is_null($userTemp)){

       if($userTemp->email != $email){
           // es otro usuario
           $existsUserTemp  = false;
       }else{
           // es el mismo usuario
           $existsUserTemp = true;
       }
    }else{
        // No existe
        $existsUserTemp  = false;
    }

    if(!$existsUserTemp){
        // lo busco por email entre los que no completaron el registro
        $userTemp = Usuario_Temp::getUsuarioTempByEmail($email);
        if(!is_null($userTemp)){
            $existsUserTemp = true;
            // lo guardo en el session
            $_SESSION['suscripcion_user_temp_id'] = $userTemp->utemp_id;
        }
    }


    if(!$existsUserTemp){
        // creo el usuario
        $dataUserTemp = [
          'user_id' => null,
          'email'   => $email,
          'paso1'   => 1,
          'paso2'   => 0,
          'paso3'   => 0,
          'paso4'   => 0,
          'plan_id' => null,   //todavia no eligio el plan
          'fecha'   => date("Y-m-d H:i:s")
        ];

        $wpdb->insert( $wpdb->prefix . 'users_temp', $dataUserTemp );
        $user_temp_id  = $wpdb->insert_id;

        // lo guardo en el session
        $_SESSION['suscripcion_user_temp_id'] = $user_temp_id;

        //recargo
        $userTemp = Usuario_Temp::getUsuarioTempById($user_temp_id);
    }

    if($userTemp->paso3 && $userTemp->pago_aprobado && !$userTemp->paso4){
       // falta el registro
       // voy al paso 4
       $result = [
            'error'       =>  false,
            'urlRedirect' =>  '/membresias/crearCuenta',
       ];
       wp_send_json($result);
       exit;
    }

    if($userTemp->paso3 && $userTemp->pago_pendiente && !$userTemp->paso4){
        // falta confirmacion del pago
        // Pagina de retomar el pago
        // voy a la pagina de procesando...
        $result = [
            'error'       =>  false,
            'urlRedirect' =>  '/membresias/procesandoPago',
        ];
        wp_send_json($result);
        exit;
    }

    if($userTemp->paso3){
       // ya esta registrado!!

       // borro la session
       //$_SESSION['suscripcion_user_temp_id'] = null;
       //unset($_SESSION['suscripcion_user_temp_id']);  // ??

       // voy al login
       $result = [
            'error'       =>  false,
            'urlRedirect' =>  '/membresias/login',
       ];
       wp_send_json($result);
       exit;
    }

//  Ver si se da el caso...!

//    if($userTemp->paso2 && !$userTemp->paso3 ){
//       // falta cargar los datos
//       // voy al paso 3
//       $result = [
//            'error'       =>  false,
//            'urlRedirect' =>  '/membresias/pagar',
//       ];
//       wp_send_json($result);
//       exit;
//    }


    /*************************************************************/
    // Analizo los planes
    /*************************************************************/


    if($userTemp->paso4){
       // ya esta registrado!!

       // borro la session
       $_SESSION['suscripcion_user_temp_id'] = null;
       unset($_SESSION['suscripcion_user_temp_id']);  // ??
       // guardo la session....

       // voy al login
       $result = [
            'error'       =>  false,
            'urlRedirect' =>  '/membresias/login',
       ];
       wp_send_json($result);
       exit;
    }

    if($userTemp->paso3 && !$userTemp->paso4 ){
       // falta cargar los datos
       // voy al paso 4
       $result = [
            'error'       =>  false,
            'urlRedirect' =>  '/membresias/crearCuenta',
       ];
       wp_send_json($result);
       exit;
    }

    if(empty( $_POST['plan_id'])){
        // Error!
        $result = [
            'error'    =>  true,
            'msgError' =>  'Debe seleccionar un Plan',
        ];
        wp_send_json($result);
        exit();
    }


    /***********************************************************************/
    // Plan Custom Mensual
    /***********************************************************************/
    if($_POST['plan_id'] == 'CM'){

        if(empty( $_POST['montoMensual'])){
            // Error!
            $result = [
                'error'    =>  true,
                'msgError' =>  'Debe seleccionar un Plan',
            ];
            wp_send_json($result);
            exit();
        }

        // proceso

        $monto =  (int)$_POST['montoMensual'];

        $userPlan = Usuario_Plan::getUsuarioPlanMensualByValor($monto);

        if(is_null($userPlan)){
            // no existe el plan!!
            // lo creo...
            include_once(plugin_dir_path(__FILE__) . '../../vendor/autoload.php');

            if( get_option( 'suscripcion_plugin_mp_sandbox_mode' ) ){
                $access_token = get_option( 'suscripcion_plugin_mp_sandbox_access_token' );
            }else{
               $access_token = get_option( 'suscripcion_plugin_mp_production_access_token' );
            }

            $urlparts = parse_url(home_url());

            $ch = curl_init();
            $headers  = [
                        'Authorization: Bearer '. $access_token,
                        'Content-Type: application/json'
                    ];
            $postData = [
                "back_url"  => $urlparts['scheme'].'://'. $urlparts['host'] . "/membresias/procesandoPago",
                'auto_recurring' =>  [
                        "currency_id"        => "ARS",
                        "transaction_amount" => (int)$monto,
                        "repetitions"        => 12,
                        "frequency"          => 1,
                        "frequency_type"     => "months",
                    ],
                'reason' => "Plan Mensual Personalizado $" . $monto,
            ];
            curl_setopt($ch, CURLOPT_URL,"https://api.mercadopago.com/preapproval_plan");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $result     = curl_exec ($ch);
            $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);


            if($statusCode != 200 && $statusCode != 201){
                // Error
                $resultAjax = [
                    'error'    =>  true,
                    'msgError' =>  'Ocurrio un error al crear el Plan Mensual Personalizado',
                ];
                wp_send_json($resultAjax);
                exit();
            }

            // Todo OK   => se creo el plan
            // Lo agrego a planes

            $plan =  json_decode($result, false);

            $preapproval_id = $plan->id;

            $dataUsersPlan = [
                'plan_mp_id' => $plan->id,
                'nombre'     => $plan->reason,
                'tipo'       => 'mensual',
                'valor'      => (int)$monto,
                'activo'     => 0,
                'data'       => json_encode($plan),
                'fecha'      => date("Y-m-d H:i:s")
            ];

            $wpdb->insert( $wpdb->prefix . 'users_plans', $dataUsersPlan );;

            $plan_id = $wpdb->insert_id;

            // lo agrego al usuario temporal
            $dataUserTemp = [
              'paso2'          => 1,
              'plan_id'        => $plan_id,
              'preapproval_id' => $preapproval_id,
              'fecha'          => date("Y-m-d H:i:s")
            ];

            $where = [ 'utemp_id' => $userTemp->utemp_id];
            $wpdb->update( $wpdb->prefix . 'users_temp', $dataUserTemp, $where );


            // voy a pagar con la tarjeta - paso 3
            $result = [
                'error'       =>  false,
                'urlRedirect' =>  '/membresias/pagar',
            ];
            wp_send_json($result);
            exit;

        }else{
            // el plan ya existe!!!
            // lo selecciono
            // lo agrego al usuario temporal
            $dataUserTemp = [
              'paso2'          => 1,
              'plan_id'        => $userPlan->plan_id,
              'preapproval_id' => $userPlan->plan_mp_id,
              'fecha'          => date("Y-m-d H:i:s")
            ];

            $where = [ 'utemp_id' => $userTemp->utemp_id];
            $wpdb->update( $wpdb->prefix . 'users_temp', $dataUserTemp, $where );

            // voy a pagar con la tarjeta - paso 3
            $result = [
                'error'       =>  false,
                'urlRedirect' =>  '/membresias/pagar',
            ];
            wp_send_json($result);
            exit;
        }
    }

    /***********************************************************************/
    // Plan Custom Anual
    /***********************************************************************/
    if($_POST['plan_id'] == 'CA'){

        if(empty( $_POST['montoAnual'])){
            // Error!
            $result = [
                'error'    =>  true,
                'msgError' =>  'Debe seleccionar un Plan',
            ];
            wp_send_json($result);
            exit();
        }

        // proceso
        $monto =  (int)$_POST['montoAnual'];

        $userPlan = Usuario_Plan::getUsuarioPlanAnualByValor($monto);

        if(is_null($userPlan)){
            // no existe el plan!!
            // lo creo...
            include_once(plugin_dir_path(__FILE__) . '../../vendor/autoload.php');

            if( get_option( 'suscripcion_plugin_mp_sandbox_mode' ) ){
                $access_token = get_option( 'suscripcion_plugin_mp_sandbox_access_token' );
            }else{
               $access_token = get_option( 'suscripcion_plugin_mp_production_access_token' );
            }

            $urlparts = parse_url(home_url());

            $ch = curl_init();
            $headers  = [
                        'Authorization: Bearer '. $access_token,
                        'Content-Type: application/json'
                    ];
            $postData = [
                "back_url"  => $urlparts['scheme'].'://'. $urlparts['host'] . "/membresias/procesandoPago",
                'auto_recurring' =>  [
                        "currency_id"        => "ARS",
                        "transaction_amount" => (int)$monto,
                        "frequency"          => 12,
                        "frequency_type"     => "months",
                    ],
                'reason' => "Plan Anual Personalizado $" . $monto,
            ];
            curl_setopt($ch, CURLOPT_URL,"https://api.mercadopago.com/preapproval_plan");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $result     = curl_exec ($ch);
            $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);


            if($statusCode != 200 && $statusCode != 201){
                // Error
                $resultAjax = [
                    'error'    =>  true,
                    'msgError' =>  'Ocurrio un error al crear el Plan Anual Personalizado',
                ];
                wp_send_json($resultAjax);
                exit;
            }

            // Todo OK   => se creo el plan
            // Lo agrego a planes

            $plan =  json_decode($result, false);

            $preapproval_id = $plan->id;

            $dataUsersPlan = [
                'plan_mp_id' => $plan->id,
                'nombre'     => $plan->reason,
                'tipo'       => 'anual',
                'valor'      => (int)$monto,
                'activo'     => 0,
                'data'       => json_encode($plan),
                'fecha'      => date("Y-m-d H:i:s")
            ];

            $wpdb->insert( $wpdb->prefix . 'users_plans', $dataUsersPlan );

            $plan_id = $wpdb->insert_id;

            // lo agrego al usuario temporal
            $dataUserTemp = [
              'paso2'          => 1,
              'plan_id'        => $plan_id,
              'preapproval_id' => $preapproval_id,
              'fecha'          => date("Y-m-d H:i:s")
            ];

            $where = [ 'utemp_id' => $userTemp->utemp_id];
            $wpdb->update( $wpdb->prefix . 'users_temp', $dataUserTemp, $where );

            // voy a pagar con la tarjeta - paso 3
            $result = [
                'error'       =>  false,
                'urlRedirect' =>  '/membresias/pagar',
            ];
            wp_send_json($result);
            exit;

        }else{
            // el plan ya existe!!!
            // lo selecciono
            // lo agrego al usuario temporal
            $dataUserTemp = [
              'paso2'          => 1,
              'plan_id'        => $userPlan->plan_id,
              'preapproval_id' => $userPlan->plan_mp_id,
              'fecha'          => date("Y-m-d H:i:s")
            ];

            $where = [ 'utemp_id' => $userTemp->utemp_id];
            $wpdb->update( $wpdb->prefix . 'users_temp', $dataUserTemp, $where );

            // voy a pagar con la tarjeta - paso 3
            $result = [
                'error'       =>  false,
                'urlRedirect' =>  '/membresias/pagar',
            ];
            wp_send_json($result);
            exit;

        }

    }  //


    /***********************************************************************/
    //  Plan ya existente
    /***********************************************************************/

    $plan_id =  (int)$_POST['plan_id'];

    $userPlan = Usuario_Plan::getUsuarioPlanById($plan_id);

    // veo si existe el plan_id;

    if(is_null($userPlan)){
        // no existe el plan!!
        // borro la session
        $_SESSION['suscripcion_user_temp_id'] = null;
        unset($_SESSION['suscripcion_user_temp_id']);  // ??

        // voy al paso 1
        $result = [
            'error'    =>  true,
            'msgError' =>  'Debe seleccionar un Plan',
        ];
        wp_send_json($result);
        exit;
    }


    // lo agrego al usuario temporal
    $dataUserTemp = [
      'paso2'          => 1,
      'plan_id'        => $plan_id,
      'preapproval_id' => $userPlan->plan_mp_id,
      'fecha'          => date("Y-m-d H:i:s")
    ];

    $where = [ 'utemp_id' => $userTemp->utemp_id];
    $wpdb->update( $wpdb->prefix . 'users_temp', $dataUserTemp, $where );


    // voy a pagar con la tarjeta - paso 3
    $result = [
        'error'       =>  false,
        'urlRedirect' =>  '/membresias/pagar',
    ];
    wp_send_json($result);
    exit;
