<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://christophercasper.com/
 * @since      1.0.0
 *
 * @package    Suscripcion_Plugin
 * @subpackage Suscripcion_Plugin/public/partials
 */


global $wp_query;
global $wp_session;

$wp_query->is_home = false;

if(!session_id()) {session_start(); }

// veo si existe como usuario temporal - registro en curso
$userTemp = null;

$user_temp_id = isset($_SESSION['suscripcion_user_temp_id']) ? $_SESSION['suscripcion_user_temp_id']: null;
if(! empty($user_temp_id )  && is_numeric($user_temp_id)){
   $userTemp = Usuario_Temp::getUsuarioTempById($user_temp_id);
}

if(is_null($userTemp)){
   // voy al paso 1
   wp_redirect('/membresias');
   exit;
}


if(!is_null($userTemp)){

   $existsUserTemp = true;

   if($userTemp->paso4){
       // ya esta registrado!!

       // borro la session
       $_SESSION['suscripcion_user_temp_id'] = null;
       unset($_SESSION['suscripcion_user_temp_id']);  // ??
       // guardo la session....


       // voy al login
       wp_redirect('/membresias/login');
       exit;
   }

   if($userTemp->paso3 && !$userTemp->paso4 ){
       // falta cargar los datos
       // voy al paso 3
       wp_redirect('/membresias/crearCuenta');
       exit;

   }

   if(!$userTemp->paso3){
      // no hago nada...
   }
}

// muestro el formulario de pago
// busco el plan
 $plan_id =  (int)$userTemp->plan_id;

 $userPlan = Usuario_Plan::getUsuarioPlanById($plan_id);



if(!empty( $_POST['token'])){

        include_once(plugin_dir_path(__FILE__) . '../../vendor/autoload.php');

        if( get_option( 'suscripcion_plugin_mp_sandbox_mode' ) ){
            MercadoPago\SDK::setAccessToken( get_option( 'suscripcion_plugin_mp_sandbox_access_token' ) );
            $access_token =  get_option( 'suscripcion_plugin_mp_sandbox_access_token' );
        }else{
            MercadoPago\SDK::setAccessToken( get_option( 'suscripcion_plugin_mp_production_access_token' ) );
            $access_token =  get_option( 'suscripcion_plugin_mp_production_access_token' );
        }

        $card_token_id = trim($_POST['token']);    // token para el pago

        $reference          = $userTemp->utemp_id.'-temp-'.$userPlan->plan_id;
        $external_reference = $reference.'-'. substr(md5($reference.'-redaccion-MP'),0,5);

        $preapproval_data = new MercadoPago\Preapproval();

        $preapproval_data->preapproval_plan_id = $userPlan->plan_mp_id;
        $preapproval_data->card_token_id       = $card_token_id;
        $preapproval_data->payer_email         = $userTemp->email;
        $preapproval_data->external_reference  = $external_reference;

        $preapproval_data->save();

        //echo var_dump($preapproval_data);
        if( $preapproval_data->error instanceof MercadoPago\RecuperableError){     //object(MercadoPago\RecuperableError)
            // Error!
            $errorText = $preapproval_data->error->message;
            if($errorText == 'Card token service not found'){
                 $errorTextEsp = 'Token de la tarjeta no encontrado.';
            }elseif($errorText == 'Card token was used, please generate new'){
                 $errorTextEsp = 'Token de la tarjeta ya fue usado, por favor genero otro nuevo.';
            }elseif($errorText == 'CC_VAL_433 Credit card validation has failed'){
                 $errorTextEsp = 'La validación de la tarjeta de credito falló.';
            }else{
                 $errorTextEsp = $errorText;
            }

            $_SESSION['errorText'] = $errorTextEsp;

            wp_redirect('/membresias/pagar');
            exit;
        }

        // No hay Error =>  Todo OK

        // guado en el log
        $json = json_encode($preapproval_data->toArray());

        $mp_logs = [
          'external_id' => $external_reference,
          'user_id'     => $userTemp->utemp_id,
          'result'      => '',
          'data'        => $json,
          'fecha'       => date("Y-m-d H:i:s"),
        ];

        $wpdb->insert( $wpdb->prefix . 'users_mp_logs', $mp_logs );


        if($preapproval_data->status == "authorized"){
            //Pago aprobado   => OK

            //log de pagos
            $users_payment = [
              'external_id' => $external_reference,
              'user_id'     => null,
              'utemp_id'    => $userTemp->utemp_id,
              'plan_id'     => $userPlan->plan_id,
              'monto'       => $userPlan->valor,
              'status'      => 'authorized',
              'data'        => $json,
              'fecha'       => date("Y-m-d H:i:s"),
            ];

            $wpdb->insert( $wpdb->prefix . 'users_payments', $users_payment );


            // Marco el usuario
            $dataUserTemp = [
              'paso3'          => 1,
              'pago_aprobado'  => 1,
              'preapproval_id' => $preapproval_data->id,
              'fecha'          => date("Y-m-d H:i:s")
            ];

            $where = [ 'utemp_id' => $userTemp->utemp_id];
            $wpdb->update( $wpdb->prefix . 'users_temp', $dataUserTemp, $where );

            // voy a la pagina de registro -  paso 4
            wp_redirect('/membresias/crearCuenta');
            exit;

        }elseif($preapproval_data->status == "pending" || $preapproval_data->status == "authorized"){
            // Se espera la confirmacion por IPN

            // Marco el usuario
            $dataUserTemp = [
              'paso3'          => 1,
              'pago_pendiente' => 1,
              'fecha'          => date("Y-m-d H:i:s")
            ];

            $where = [ 'utemp_id' => $userTemp->utemp_id];
            $wpdb->update( $wpdb->prefix . 'users_temp', $dataUserTemp, $where );


            // Pagina de retomar el pago
            // voy a la pagina de procesando...
            wp_redirect('/membresias/procesandoPago');
            exit;

        }else{
            // Todos los demas casos..
            // Error!

            switch($preapproval_data->status){
               case 'in_mediation' :  {
                   //Users have initiated a dispute.
                   $errorTextEsp = 'El usuario a iniciado una disputa por el pago.';
                   break;
               }
               case 'rejected' :  {
                   // Payment was rejected. The user may retry payment.
                   $errorTextEsp = 'El pago a sido rechazado. Puede intentarlo de nuevo.';
                   break;
               }
               case 'cancelled' :  {
                   // Payment was cancelled by one of the parties or because time for payment has expired
                   $errorTextEsp = 'El pago a sido cancelado por una de las partes, o porque el tiempo para el pago a expirado. ';
                   break;
               }
               case 'refunded' :  {
                   // Payment was refunded to the user.
                   $errorTextEsp = 'El pago fue devuelto al usuario.';
                   break;
               }
               case 'charged_back' :  {
                   // Was made a chargeback in the buyer’s credit card.
                   $errorTextEsp = 'Se realizó un contracargo en la tarjeta de crédito del comprador.';
                   break;
               }
               default:{
                   $errorTextEsp = 'Ocurrio un error desconocido.';
               }
            }

            $_SESSION['errorText'] = $errorTextEsp;

            wp_redirect('/membresias/pagar');
            exit;
/*
    APRO: Pago aprobado.
    CONT: Pago pendiente.
    OTHE: Rechazado por error general.
    CALL: Rechazado con validación para autorizar.
    FUND: Rechazado por monto insuficiente.
    SECU: Rechazado por código de seguridad inválido.
    EXPI: Rechazado por problema con la fecha de expiración.
    FORM: Rechazado por error en formulario.

     binary_mode

Payment status:

pending             The user has not yet completed the payment process.
approved            The payment has been approved and accredited.
authorized          The payment has been authorized but not captured yet.
in_process          Payment is being reviewed.

in_mediation        Users have initiated a dispute.
rejected            Payment was rejected. The user may retry payment.
cancelled           Payment was cancelled by one of the parties or because time for payment has expired
refunded            Payment was refunded to the user.
charged_back        Was made a chargeback in the buyer’s credit card.

*/




        }

        exit();

}


// Lets load the header of the site
get_header();
?>


<div id="content" class="site-content-full">
   <main id="main" class="site-main">

      <div class="membresiasRow">
         <div class="membresiasBreadcrumb">
             <ul>
                <li class="">Elegí Monto</li>
                <li class="activo">Pagar</li>
                <li class="">Crear Cuenta</li>
             </ul>
         </div>
      </div>

      <div id="post-92841" class="post-92841 page type-page status-publish hentry">
         <div class="">
            <div class="wp-block-group PagosHeader">
               <div class="wp-block-group__inner-container">
                  <h3 class="has-text-align-center Miembrosx1Col PFTittlePagos wpsm_panelTT">¡Gracias por sumarte a nuestra comunidad de co-responsables!</h3>
                  <p class="has-text-align-center"><em>Tu aporte es fundamental para que nuestro periodismo sea abierto, sumá más voces y lográ un mayor impacto.</em></p>
                  <div class="PagosHeaderSep"></div>
               </div>
            </div>
            <?php
                if(isset($_SESSION['errorText']) && $_SESSION['errorText'] != ''){
            ?>
            <div class="wp-block-group BloquePagos" style="color: red;">
               <div class="wp-block-group__inner-container">
                  <div class="wp-block-columns">
                      <?php echo  $_SESSION['errorText'];
                            unset($_SESSION['errorText']);
                      ?>
                  </div>
               </div>
            </div>
            <?php
                }
            ?>
            <div class="wp-block-group BloquePagos">
               <div class="wp-block-group__inner-container">
                  <div class="wp-block-columns">
                       Hola, tu membresía para "<?php echo $userPlan->nombre ?>" será de $<?php echo $userPlan->valor; ?>.

                  </div>
                  <div class="wp-block-columns">

<?php
    if( get_option( 'suscripcion_plugin_mp_sandbox_mode' ) ){
        $public_Key =  get_option( 'suscripcion_plugin_mp_sandbox_public_Key' );
    }else{
        $public_Key =  get_option( 'suscripcion_plugin_mp_production_public_Key' );
    }
?>
<form action="" method="POST">
  <script
    src="https://www.mercadopago.com.ar/integrations/v1/web-tokenize-checkout.js"
    data-public-key="<?php echo $public_Key  ?>"
    data-transaction-amount="<?php echo  $userPlan->valor?>"
    data-button-label="Pagar <?php echo $userPlan->nombre?>"
    data-summary-product="<?php echo  $userPlan->nombre?>"
    data-min-installments="1"
    data-max-installments="1"
    data-summary-product-label="Suscripcion"
    data-button-label="Reintentar el Pago"
    data-binary-mode="true" <?php // ver si funciona... ?>  >
  </script>
</form>


<script type="text/javascript">
   jQuery(document).ready(function() {
      jQuery(".mercadopago-button").click();
   });
</script>
                  </div>
                  <div class="wp-block-columns">
                         <br />
                         <br />
                         <a href="/membresias" >Volver a elegir planes de Membresía</a>

                  </div>
                  <div style="height:20px" aria-hidden="true" class="wp-block-spacer"></div>
               </div>
            </div>

            <div class="wp-block-group BloquePagos">
               <div class="wp-block-group__inner-container">
                  <div style="height:36px" aria-hidden="true" class="wp-block-spacer"></div>
                  <p>Podés cancelar tu membresía en cualquier momento. Si necesitás ayuda para activarla o darla de baja, por favor mandanos un mail a <a href="mailto:miembros@redaccion.com.ar">miembros@redaccion.com.ar</a> o llamanos al +54 1149171763 </p>
                  <div style="height:36px" aria-hidden="true" class="wp-block-spacer"></div>
               </div>
            </div>
         </div>
      </div>
   </main>
</div>




<?php
// Load the footer
get_footer();
