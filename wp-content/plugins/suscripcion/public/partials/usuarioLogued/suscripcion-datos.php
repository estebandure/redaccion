<?php

global $wp_query;
global $wp_session;
global $wpdb;

$wp_query->is_home = false;

if(!session_id()) {session_start(); }

include_once(plugin_dir_path(__FILE__) . '../../../vendor/autoload.php');


// veo se hay un usuario logueado
if(!is_user_logged_in()){
    // lo saco al login
    wp_redirect('/membresias/login');
    exit();
}

$userLogged = wp_get_current_user();

$user_id = $userLogged->ID;

// busco la infromacion del usuario
$usuarioInfo = Usuario_Information::getUsuarioInfoByUsuerId($user_id);





if(! empty( $_POST['wpforms']['fields'][6])){   // Nombre

    // Proceso el formulario
    $nombre           = $_POST['wpforms']['fields'][6];
    $apellido         = $_POST['wpforms']['fields'][7];

    // ver si se usa...!
    $email            = $_POST['wpforms']['fields'][8];

    $fecha_nac        = $_POST['wpforms']['fields'][10];  //date

    try{
        $fechaCarbon = Carbon\Carbon::createFromFormat("d/m/Y", $fecha_nac);
        $fecha_nac =  $fechaCarbon->format("Y-m-d");
    }catch(\Exception $e){
        $fecha_nac = null;
    }

    $documento_tipo   = $_POST['wpforms']['fields'][63];  // 'Pasaporte', 'DNI'
    $numero_documento = $_POST['wpforms']['fields'][66];

    $telefono         = $_POST['wpforms']['fields'][9];
    $direccion1       = $_POST['wpforms']['fields'][72]['address1'];
    $direccion2       = $_POST['wpforms']['fields'][72]['address2'];
    $ciudad           = $_POST['wpforms']['fields'][72]['city'];
    $estado_region    = $_POST['wpforms']['fields'][72]['state'];
    $codigo_postal    = $_POST['wpforms']['fields'][72]['postal'];
    $pais             = $_POST['pais'];

    //value : Vivo fuera de la Argentina
    //$fuera_de_argentina_arr = isset($_POST['wpforms']['fields'][74]) ?  $_POST['wpforms']['fields'][74] : null;
    //$fuera_de_argentina_arr = is_array($fuera_de_argentina_arr) ?  $fuera_de_argentina_arr : [];
    //$fuera_de_argentina     = in_array('Vivo fuera de la Argentina', $fuera_de_argentina_arr)? 1 : 0;

    //$profesion          = $_POST['wpforms']['fields'][11];

    $contacto_por     = $_POST['wpforms']['fields'][70]; // Array
    $contacto_por     = is_array($contacto_por) ?  $contacto_por : [];

    $twitter          = in_array('Twitter'    , $contacto_por) ? 1 : 0; //Twitter
    $facebook         = in_array('Facebook'   , $contacto_por) ? 1 : 0; //Facebook
    $instagram        = in_array('Instagram'  , $contacto_por) ? 1 : 0; //Instagram
    $newsletters      = in_array('Newsletters', $contacto_por) ? 1 : 0; //Newsletters
    $coResponsable    = in_array('Co-Responsable de RED/ACCIÓN', $contacto_por) ? 1 : 0; //Co-Responsable de RED/ACCIÓN
    $unEvento         = in_array('Un evento'  , $contacto_por) ? 1 : 0; //Un evento
    $opcionOtro       = in_array('Otro'       , $contacto_por) ? 1 : 0; //Otro

    $contacto_otro    = $_POST['wpforms']['fields'][71];   //  ¿Nos contás cómo llegaste?
    $dato_curioso     = $_POST['dato_curioso'];


    $dataUserInformation = [
         'nombre'           => $nombre,
         'apellido'         => $apellido,
         'email'            => $email,
         'fecha_nac'        => $fecha_nac,
         'documento_tipo'   => $documento_tipo,
         'numero_documento' => $numero_documento,
         'telefono'         => $telefono,

         'direccion1'       => $direccion1,
         'direccion2'       => $direccion2,
         'ciudad'           => $ciudad,
         'estado_region'    => $estado_region,
         'codigo_postal'    => $codigo_postal,

         //'fuera_de_argentina' => $fuera_de_argentina,
         //'profesion'          => $profesion,


         'twitter'       => $twitter,
         'facebook'      => $facebook,
         'instagram'     => $instagram,
         'newsletters'   => $newsletters,
         'coResponsable' => $coResponsable,
         'unEvento'      => $unEvento,
         'opcionOtro'    => $opcionOtro,

         'contacto_otro'      => $contacto_otro,

         'pais'          => $pais,           // nuevo
         'dato_curioso'  => $dato_curioso,   // nuevo

       //  'fecha'         => date("Y-m-d H:i:s")
    ];

    $where = [ 'uinfo_id' => $usuarioInfo->uinfo_id];

    $wpdb->update( $wpdb->prefix . 'users_information', $dataUserInformation , $where);


    // voy a la pagina del usuario
    wp_redirect('/membresias/usuario');
    exit;

}


    // cargo los datos del formulario
    $nombre           = $usuarioInfo->nombre;
    $apellido         = $usuarioInfo->apellido;

    // ver si se usa...!
    $email            = $usuarioInfo->email;

    try{
        $fechaCarbon = Carbon\Carbon::createFromFormat("Y-m-d", $usuarioInfo->fecha_nac);
        $fecha_nac =  $fechaCarbon->format("d/m/Y");
    }catch(\Exception $e){
        $fecha_nac = null;
    }

    $documento_tipo   = $usuarioInfo->documento_tipo;  // 'Pasaporte', 'DNI'
    $numero_documento = $usuarioInfo->numero_documento;

    $telefono         = $usuarioInfo->telefono;
    $direccion1       = $usuarioInfo->direccion1;
    $direccion2       = $usuarioInfo->direccion2;
    $ciudad           = $usuarioInfo->ciudad;
    $estado_region    = $usuarioInfo->estado_region;
    $codigo_postal    = $usuarioInfo->codigo_postal;
    $pais             = $usuarioInfo->pais;           // Nuevo

    //$fuera_de_argentina     = $usuarioInfo->fuera_de_argentina;
    //$profesion          = $usuarioInfo->profesion;


    $twitter          = $usuarioInfo->twitter; //Twitter
    $facebook         = $usuarioInfo->facebook; //Facebook
    $instagram        = $usuarioInfo->instagram; //Instagram
    $newsletters      = $usuarioInfo->newsletters; //Newsletters
    $coResponsable    = $usuarioInfo->coResponsable; //Co-Responsable de RED/ACCIÓN
    $unEvento         = $usuarioInfo->unEvento; //Un evento
    $opcionOtro       = $usuarioInfo->opcionOtro; //Otro

    $contacto_otro    = $usuarioInfo->contacto_otro;   //  ¿Nos contás cómo llegaste?
    $dato_curioso     = $usuarioInfo->dato_curioso;   // Nuevo



// Lets load the header of the site
get_header();
?>



<div id="content" class="site-content-full">
   <main id="main" class="site-main">
      <div  class="page type-page status-publish hentry">
         <div class="">
            <div class="wp-block-group PagosHeader">
               <div class="wp-block-group__inner-container">
                  <h3 class="has-text-align-center Miembrosx1Col PFTittlePagos wpsm_panelTT"><?php echo $usuarioInfo->nombre.' '. $usuarioInfo->apellido; ?></h3>
                  <p class="has-text-align-center">
                     <?php if($usuarioInfo->ciudad        != '') echo ' '.$usuarioInfo->ciudad        . '.'; ?>
                     <?php if($usuarioInfo->estado_region != '') echo ' '.$usuarioInfo->estado_region . '.'; ?>
                  </p>
                  <div class="PagosHeaderSep"></div>
                  <p class="has-text-align-center">
                     <a href="/membresias/usuario">
                         <input type="button"  value="Volver">
                     </a>
                  </p>
               </div>
            </div>
         </div>





               <div class="MiembrosWBox">
                  <h3 class="has-text-align-center Miembrosx1Col Pad10">Mis Datos</h3>

                  <div class="MiembrosWBox">
                     <div class="wpforms-container wpforms-container-full" id="wpforms-64938">
                        <form id="paso4-info-form" class="wpforms-validate wpforms-form" <?php /* data-formid="64938" */?> method="post" enctype="multipart/form-data" action="" novalidate="novalidate">
                           <noscript class="wpforms-error-noscript">Por favor, activa JavaScript en tu navegador para completar este formulario.</noscript>
                           <div class="wpforms-field-container">
                              <div id="wpforms-64938-field_6-container" class="wpforms-field wpforms-field-name wpforms-one-half wpforms-first" data-field-id="6"><label class="wpforms-field-label" for="wpforms-64938-field_6">Nombre <span class="wpforms-required-label">*</span></label><input type="text" id="wpforms-64938-field_6" class="wpforms-field-large wpforms-field-required" name="wpforms[fields][6]" placeholder="Nombre" value="<?php echo $nombre;?>" required=""></div>
                              <div id="wpforms-64938-field_7-container" class="wpforms-field wpforms-field-name wpforms-one-half" data-field-id="7"><label class="wpforms-field-label" for="wpforms-64938-field_7">Apellido <span class="wpforms-required-label">*</span></label><input type="text" id="wpforms-64938-field_7" class="wpforms-field-large wpforms-field-required" name="wpforms[fields][7]" placeholder="Apellido"  value="<?php echo $apellido;?>" required=""></div>
                              <div id="wpforms-64938-field_8-container" class="wpforms-field wpforms-field-email wpforms-one-half wpforms-first" data-field-id="8"><label class="wpforms-field-label" for="wpforms-64938-field_8">Dirección de correo electrónico <span class="wpforms-required-label">*</span></label><input type="email" id="wpforms-64938-field_8" class="wpforms-field-large wpforms-field-required" name="wpforms[fields][8]" placeholder="Ej: miembros@redaccion.com.ar"  readonly="readonly"  value="<?php echo $email;?>" required=""></div>
                              <div id="wpforms-64938-field_10-container" class="wpforms-field wpforms-field-text wpforms-one-half" data-field-id="10"><label class="wpforms-field-label" for="wpforms-64938-field_10">Fecha de nacimiento <span class="wpforms-required-label">*</span></label><input type="text" id="wpforms-64938-field_10" class="wpforms-field-large wpforms-field-required " name="wpforms[fields][10]" placeholder="DD/MM/AAAA" value="<?php echo $fecha_nac;?>"   required=""></div>
                              <div id="wpforms-64938-field_63-container" class="wpforms-field wpforms-field-select" data-field-id="63">
                                 <label class="wpforms-field-label" for="wpforms-64938-field_63">Tipo de documento <span class="wpforms-required-label">*</span></label>
                                 <select id="wpforms-64938-field_63" class="wpforms-field-medium wpforms-field-required" name="wpforms[fields][63]" required="required">
                                    <option value="DNI"       <?php if($usuarioInfo->documento_tipo =='DNI'      ){ ?> selected <?php }?> >DNI</option>
                                    <option value="Pasaporte" <?php if($usuarioInfo->documento_tipo =='Pasaporte'){ ?> selected <?php }?> >Pasaporte</option>
                                 </select>
                              </div>
                              <div id="wpforms-64938-field_66-container" class="wpforms-field wpforms-field-text" data-field-id="66"><label class="wpforms-field-label" for="wpforms-64938-field_66">Número de documento <span class="wpforms-required-label">*</span></label><input type="text" id="wpforms-64938-field_66" class="wpforms-field-large wpforms-field-required" name="wpforms[fields][66]" placeholder="Número de documento" value="<?php echo  $numero_documento;?>" required=""></div>
                              <div id="wpforms-64938-field_9-container" class="wpforms-field wpforms-field-text" data-field-id="9"><label class="wpforms-field-label" for="wpforms-64938-field_9">Teléfono de contacto <span class="wpforms-required-label">*</span></label><input type="text" id="wpforms-64938-field_9" class="wpforms-field-large wpforms-field-required" name="wpforms[fields][9]" placeholder="___ ____ _____" value="<?php echo  $telefono;?>" required=""></div>
                              <div id="wpforms-64938-field_72-container" class="wpforms-field wpforms-field-address" data-field-id="72">
                                 <label class="wpforms-field-label" for="wpforms-64938-field_72">Dirección (donde querés recibir el libro de regalo) <span class="wpforms-required-label">*</span></label>
                                 <div class="wpforms-field-row wpforms-field-medium">
                                    <div><input type="text" id="wpforms-64938-field_72" class="wpforms-field-address-address1 wpforms-field-required" name="wpforms[fields][72][address1]" value="<?php echo  $usuarioInfo->direccion1;?>" ><label for="wpforms-64938-field_72" class="wpforms-field-sublabel after ">Dirección (línea 1)</label></div>
                                 </div>
                                 <div class="wpforms-field-row wpforms-field-medium">
                                    <div><input type="text" id="wpforms-64938-field_72-address2" class="wpforms-field-address-address2" name="wpforms[fields][72][address2]" value="<?php echo  $usuarioInfo->direccion2;?>"><label for="wpforms-64938-field_72-address2" class="wpforms-field-sublabel after ">Dirección 2</label></div>
                                 </div>
                                 <div class="wpforms-field-row wpforms-field-medium">
                                    <div class="wpforms-field-row-block wpforms-one-half wpforms-first"><input type="text" id="wpforms-64938-field_72-city" class="wpforms-field-address-city wpforms-field-required" name="wpforms[fields][72][city]" value="<?php echo $ciudad; ?>" required=""><label for="wpforms-64938-field_72-city" class="wpforms-field-sublabel after ">Ciudad</label></div>
                                    <div class="wpforms-field-row-block wpforms-one-half"><input type="text" id="wpforms-64938-field_72-state" class="wpforms-field-address-state wpforms-field-required" name="wpforms[fields][72][state]" value="<?php echo $estado_region; ?>" required=""><label for="wpforms-64938-field_72-state" class="wpforms-field-sublabel after ">Estado / Provincia / Región</label></div>
                                 </div>
                                 <div class="wpforms-field-row wpforms-field-medium">
                                    <div class="wpforms-field-row-block wpforms-one-half wpforms-first"><input type="text" id="wpforms-64938-field_72-postal" class="wpforms-field-address-postal wpforms-field-required" name="wpforms[fields][72][postal]" value="<?php echo $codigo_postal; ?>" required=""><label for="wpforms-64938-field_72-postal" class="wpforms-field-sublabel after ">Código postal</label></div>
                                    <div class="wpforms-field-row-block wpforms-one-half"><input type="text" id="wpforms-64938-field_72-pais" class="wpforms-field-address-state wpforms-field-required" name="pais" value="<?php echo $pais; ?>" required=""><label for="wpforms-64938-field_72-pais" class="wpforms-field-sublabel after ">País</label></div>
                                 </div>
                              </div>
                              <?php /*

                              <div id="wpforms-64938-field_74-container" class="wpforms-field wpforms-field-checkbox" data-field-id="74">
                                 <label class="wpforms-field-label" for="wpforms-64938-field_74">Si no vivís en la Argentina, marcá esta opción y te enviamos el PDF</label>
                                 <ul id="wpforms-64938-field_74">
                                    <li class="choice-1 depth-1"><input type="checkbox" id="wpforms-64938-field_74_1" name="wpforms[fields][74][]" value="Vivo fuera de la Argentina" <?php if($fuera_de_argentina){ echo 'checked="checked"';} ?>><label class="wpforms-field-label-inline" for="wpforms-64938-field_74_1">Vivo fuera de la Argentina</label></li>
                                 </ul>
                              </div>
                              <div id="wpforms-64938-field_11-container" class="wpforms-field wpforms-field-text" data-field-id="11"><label class="wpforms-field-label" for="wpforms-64938-field_11">Profesión</label><input type="text" id="wpforms-64938-field_11" class="wpforms-field-large" name="wpforms[fields][11]" value="<?php echo $profesion; ?>" ></div>
                              */ ?>
                              <div id="wpforms-64938-field_70-container" class="wpforms-field wpforms-field-checkbox wpforms-list-2-columns wpforms-conditional-trigger" data-field-id="70">
                                 <label class="wpforms-field-label" for="wpforms-64938-field_70">¿Cómo llegaste a RED/ACCIÓN?</label>
                                 <ul id="wpforms-64938-field_70">
                                    <li class="choice-1 depth-1"><input type="checkbox" id="wpforms-64938-field_70_1" name="wpforms[fields][70][]" value="Twitter"  <?php if($twitter){ echo 'checked="checked"';} ?>  ><label class="wpforms-field-label-inline" for="wpforms-64938-field_70_1">Twitter</label></li>
                                    <li class="choice-2 depth-1"><input type="checkbox" id="wpforms-64938-field_70_2" name="wpforms[fields][70][]" value="Facebook" <?php if($facebook){ echo 'checked="checked"';} ?>  ><label class="wpforms-field-label-inline" for="wpforms-64938-field_70_2">Facebook</label></li>
                                    <li class="choice-3 depth-1"><input type="checkbox" id="wpforms-64938-field_70_3" name="wpforms[fields][70][]" value="Instagram" <?php if($instagram){ echo 'checked="checked"';} ?> ><label class="wpforms-field-label-inline" for="wpforms-64938-field_70_3">Instagram</label></li>
                                    <li class="choice-8 depth-1"><input type="checkbox" id="wpforms-64938-field_70_8" name="wpforms[fields][70][]" value="Newsletters" <?php if($newsletters){ echo 'checked="checked"';} ?>><label class="wpforms-field-label-inline" for="wpforms-64938-field_70_8">Newsletters</label></li>
                                    <li class="choice-4 depth-1"><input type="checkbox" id="wpforms-64938-field_70_4" name="wpforms[fields][70][]" value="Co-Responsable de RED/ACCIÓN" <?php if($coResponsable){ echo 'checked="checked"';} ?>><label class="wpforms-field-label-inline" for="wpforms-64938-field_70_4">Co-Responsable de RED/ACCIÓN</label></li>
                                    <li class="choice-5 depth-1"><input type="checkbox" id="wpforms-64938-field_70_5" name="wpforms[fields][70][]" value="Un evento" <?php if($unEvento){ echo 'checked="checked"';} ?> ><label class="wpforms-field-label-inline" for="wpforms-64938-field_70_5">Un evento</label></li>
                                    <li class="choice-6 depth-1"><input type="checkbox" id="wpforms-64938-field_70_6" name="wpforms[fields][70][]" value="Otro"     <?php if($opcionOtro){ echo 'checked="checked"';} ?>  ><label class="wpforms-field-label-inline" for="wpforms-64938-field_70_6">Otro</label></li>
                                 </ul>
                              </div>
                              <div id="wpforms-64938-field_71-container" class="wpforms-field wpforms-field-text wpforms-conditional-field wpforms-conditional-hide" data-field-id="71" style="display:n one;">
                                 <label class="wpforms-field-label" for="wpforms-64938-field_71">¿Nos contás cómo llegaste?</label>
                                 <input type="text" id="wpforms-64938-field_71" class="wpforms-field-medium" name="wpforms[fields][71]" value="<?php echo $contacto_otro; ?>">
                              </div>
                           </div>

                           <div class="wpforms-field-row wpforms-field-medium">
                              <div id="wpforms-64938-field_71-container" class="wpforms-field wpforms-field-text wpforms-conditional-field " style="display:no ne;">
                                 <label class="wpforms-field-label" for="wpforms-64938-field_dato_curioso">Dato curioso que nos quieras contar</label>
                                 <textarea id="wpforms-64938-field_dato_curioso" class="wpforms-field-medium" name="dato_curioso"><?php echo $dato_curioso; ?></textarea>
                              </div>
                           </div>

                           <div class="wpforms-field wpforms-field-hp">
                               <label for="wpforms-64938-field-hp" class="wpforms-field-label">Message</label>
                               <input type="text" name="wpforms[hp]" id="wpforms-64938-field-hp" class="wpforms-field-medium">
                           </div>

                           <div class="wpforms-submit-container">
                              <button type="submit" name="wpforms[submit]" class="wpforms-submit " id="wpforms-submit-64938" value="wpforms-submit" aria-live="assertive">Actualizar Datos</button>
                           </div>
                        </form>
                        <script type="text/javascript">
                            jQuery(document).ready(function(){

                                jQuery("#paso4-info-form").validate({
                                    rules: {
                                        'wpforms[fields][10]':{
                                            required: true,
                                            //date: true,
                                            dateITA: true
                                        }
                                    }
                                });

                            });
                        </script>

                     </div>
                     <div class="MiembrosWBox"></div>
                  </div>
               </div>










      </div>
   </main>
</div>














<?php
// Load the footer
get_footer();
