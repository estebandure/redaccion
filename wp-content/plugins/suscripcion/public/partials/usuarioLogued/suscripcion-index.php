<?php

global $wp_query;
global $wp_session;
global $wpdb;

$wp_query->is_home = false;

if(!session_id()) {session_start(); }

// veo se hay un usuario logueado
if(!is_user_logged_in()){
    // lo saco al login
    wp_redirect('/membresias/login');
    exit();
}

$userLogged = wp_get_current_user();

$user_id = $userLogged->ID;

// busco la infromacion del usuario
$usuarioInfo = Usuario_Information::getUsuarioInfoByUsuerId($user_id);










// Lets load the header of the site
get_header();
?>





<div id="content" class="site-content-full">
   <main id="main" class="site-main">
      <div  class="page type-page status-publish hentry">
         <div class="">
            <div class="wp-block-group PagosHeader">
               <div class="wp-block-group__inner-container">
                  <h3 class="has-text-align-center Miembrosx1Col PFTittlePagos wpsm_panelTT"><?php echo $usuarioInfo->nombre.' '. $usuarioInfo->apellido; ?></h3>
                  <p class="has-text-align-center">
                     <?php if($usuarioInfo->ciudad        != '') echo ' '.$usuarioInfo->ciudad        . '.'; ?>
                     <?php if($usuarioInfo->estado_region != '') echo ' '.$usuarioInfo->estado_region . '.'; ?>
                  </p>
                  <div class="PagosHeaderSep"></div>
                  <p class="has-text-align-center">
                     <a href="/membresias/usuario/datos">
                         <input type="button"  value="Editar Datos">
                     </a>

                     <a href="/membresias/usuario/plan">
                         <input type="button"  value="Mi Plan">
                     </a>

                     <?php //   <input type="button"  value="Newsletters">  ?>

                     <a href="/membresias/usuario/logout">
                         <input type="button"  value="Cerrar Sessión">
                     </a>
                  </p>
               </div>
            </div>
         </div>
      </div>
   </main>
</div>














<?php
// Load the footer
get_footer();
