<?php
    global $wpdb;

    include_once(plugin_dir_path(__FILE__) . '../../vendor/autoload.php');

    if( get_option( 'suscripcion_plugin_mp_sandbox_mode' ) ){
        MercadoPago\SDK::setAccessToken( get_option( 'suscripcion_plugin_mp_sandbox_access_token' ) );
    }else{
        MercadoPago\SDK::setAccessToken( get_option( 'suscripcion_plugin_mp_production_access_token' ) );
    }

    $data = [
        'get'  => $_GET,
        'post' => $_POST,
    ];

    $json = json_encode($data);

    $mp_logs = [
      'external_id' => '',
      'user_id'     => null,
      'result'      => '',
      'data'        => $json,
      'fecha'       => date("Y-m-d H:i:s"),
    ];

    $wpdb->insert( $wpdb->prefix . 'users_mp_logs', $mp_logs );

    try {

        $topic = isset($_GET["topic"]) ? $_GET["topic"]: null;
        $id    = isset($_GET["id"   ]) ? $_GET["id"   ]: null;

        switch($topic) {
            case "payment":
                $payment = MercadoPago\Payment::find_by_id($id);
                break;
            case "merchant_order":
                // retorno 200
                header("HTTP/1.1 200 OK");
                exit();
                break;
        }

    } catch(Exception $e) {
         // retorno 503
        header($_SERVER["SERVER_PROTOCOL"]." 503 Service Temporarily Unavailable", true, 503);
        exit();
    }

    if(is_null($payment)){
        // retorno 200
        header("HTTP/1.1 200 OK");
        exit();
    }

    $preapproval_id     =  isset($payment->metadata->preapproval_id) ? $payment->metadata->preapproval_id : null;
    $status             =  isset($payment->status) ? $payment->status : null;
    $external_reference =  isset($payment->external_reference) ? $payment->external_reference : null;
    $description        =  isset($payment->description) ? $payment->description : null;
    $transaction_amount =  isset($payment->transaction_amount) ? $payment->transaction_amount : null;

    $transaccionAprobada = $status == 'approved';

    // busco al usuario by el $preapproval_id
    $userTemp = Usuario_Temp::getUsuarioTempByPreapprovalId($preapproval_id);


    if($transaccionAprobada){

        if(is_null($userTemp)){
            // no se encontro al usuario

            // no aprobada
            // guado en el log
            $json = json_encode($payment->toArray());

            $mp_logs = [
              'external_id' => $external_reference,
              'user_id'     => is_null($userTemp) ? null : $userTemp->user_id ,
              'result'      => $status,
              'data'        => $json,
              'fecha'       => date("Y-m-d H:i:s"),
            ];

            $wpdb->insert( $wpdb->prefix . 'users_mp_logs', $mp_logs );

        }else{
            // Marco el usuario
            $dataUserTemp = [
              'paso3'          => 1,
              'fecha'          => date("Y-m-d H:i:s")
            ];

            $where = [ 'utemp_id' => $userTemp->utemp_id];
            $wpdb->update( $wpdb->prefix . 'users_temp', $dataUserTemp, $where );


            // el usaurio si existe
            // guado en el log
            $json = json_encode($payment->toArray());

            $mp_logs = [
              'external_id' => $external_reference,
              'user_id'     => is_null($userTemp) ? null : $userTemp->user_id ,
              'result'      => $status,
              'data'        => $json,
              'fecha'       => date("Y-m-d H:i:s"),
            ];

            $wpdb->insert( $wpdb->prefix . 'users_mp_logs', $mp_logs );

            // guardo el pago

            $plan_id  =  (int)$userTemp->plan_id;

            $userPlan = Usuario_Plan::getUsuarioPlanById($plan_id);

            if(is_numeric($transaction_amount)){
                $transaction_amount = (float)$transaction_amount;
            }else{
                $transaction_amount = null;
            }

            //log de pagos
            $users_payment = [
              'external_id' => $external_reference,
              'user_id'     => $userTemp->user_id,
              'utemp_id'    => $userTemp->utemp_id,
              'plan_id'     => $userTemp->plan_id,
              'status'      => 'payment',
              'monto'       => $transaction_amount,
              'data'        => $json,
              'fecha'       => date("Y-m-d H:i:s"),
            ];

            $wpdb->insert( $wpdb->prefix . 'users_payments', $users_payment );


            if($userTemp->pago_pendiente && !$userTemp->pago_aprobado){
                // actualizo el estado
                $dataUserTemp = [
                   'pago_aprobado' => 0,
                   'pago_aprobado' => 1,
                   'fecha'         => date("Y-m-d H:i:s")
                ];

                $where = [ 'utemp_id' => $userTemp->utemp_id];
                $wpdb->update( $wpdb->prefix . 'users_temp', $dataUserTemp, $where );

                $hash = $userTemp->utemp_id . '-'. md5($userTemp->utemp_id.'-redaccion-MP-pago-aprobado');

                //*********************************************************************/
                // Envio el email para seguir con el registro
                //*********************************************************************/
                $to       = $dataUserTemp->email;
                $subject  = 'Redacción: Pago de Membresía aprobado';
                $headers  = [];
                $headers[]= "From: Redaccion <info@redaccion.com.ar>";
                $headers[]= "Content-Type: text/html; charset=UTF-8";

                $message = "
                    Hola, El pago de tu suscripcion fue aprobado.
                    <br/>
                    <br/>
                    Sigo con el registro en <a href=\"/membresias/pago-aprobado/{$hash}\">link</a>
                    <br/>
                    <br/>
                    Saludos!
                ";

                wp_mail( $to, $subject, $message, $headers );
            }
        }

    }else{
        // no aprobada
        // guado en el log
        $json = json_encode($payment->toArray());

        $mp_logs = [
          'external_id' => $external_reference,
          'user_id'     => is_null($userTemp) ? null : $userTemp->user_id ,
          'result'      => $status,
          'data'        => $json,
          'fecha'       => date("Y-m-d H:i:s"),
        ];

        $wpdb->insert( $wpdb->prefix . 'users_mp_logs', $mp_logs );
    }

    // retorno 200
    header("HTTP/1.1 200 OK");
    exit();

