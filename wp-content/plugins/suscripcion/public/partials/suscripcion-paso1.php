<?php

global $wp_query;
global $wp_session;
global $wpdb;

$wp_query->is_home = false;

if(!session_id()) {session_start(); }


// Lets load the header of the site
get_header();
?>

<div id="content" class="site-content-full">
   <main id="main" class="site-main">
      <div class="site-content" style="display: block;">

      <div class="membresiasRow">
         <div class="membresiasBreadcrumb">
             <ul>
                <li class="activo">Elegí Monto</li>
                <li class="">Pagar</li>
                <li class="">Crear Cuenta</li>
             </ul>
         </div>
      </div>
      <div class="membresiasRow">

         <div class="membresiasColumn6">
            <?php   require_once( plugin_dir_path( __FILE__ ) . 'templates/home.php'); ?>
         </div>

         <div class="membresiasColumn6 membresiasUnirme">

            <form id="paso1-email-form" class="wpforms-validate wpforms-form" method="post" enctype="multipart/form-data" action="/membresias" novalidate="novalidate">

                <?php   require_once( plugin_dir_path( __FILE__ ) . 'templates/paso_1.php'); ?>

                <?php   require_once( plugin_dir_path( __FILE__ ) . 'templates/planes.php'); ?>

               <div class="membresiasError" style="display: none;">
                   <span id="form-error"  ></span>
               </div>

               <div class="wp-block-group BloquePagos Paso1">
                   <div class="wp-block-group__inner-container">

                      <div class="wpforms-container wpforms-container-full" id="wpforms-99552">
                            <div class="wpforms-submit-container">
                                <button type="submit" name="" class="wpforms-submit " id="" value="wpforms-submit" aria-live="assertive" data-alt-text="cargando..." data-submit-text="Siguiente">Siguiente</button>
                                <span id="membresiasProcesando" style="display: none;">
                                    <img src="<?php echo  plugin_dir_url( __DIR__ ) . 'css/ajax-loading-icon.gif'?>"   style="height: 40px;vertical-align: middle;" /> Procesando...
                                </span>
                            </div>
                      </div>
                   </div>
               </div>

               <input type="hidden" id="plan_id"    name="plan_id"    value="" >

            </form>

            <script type="text/javascript">

            function showError(text){
               jQuery("#form-error").html(text);
               jQuery(".membresiasError").show();
               return false;
            }
            function clearError(){
               jQuery("#form-error").html('');
               jQuery(".membresiasError").hide();
               return false;
            }

            var procesingAjax = false;

            $(document).ready(function() {

                jQuery("#paso1-email-form").validate();

                jQuery("#paso1-email-form").submit(function( event ) {
                     event.preventDefault();

                     if(procesingAjax){
                         return false;
                     }

                     montoMensual = '';
                     montoAnual   = '';
                     email        = '';

                     email = jQuery("#email").val();
                     if(email == ''){
                         showError('Debe ingresar un Email');
                         return false;
                     }

                     plan_id = jQuery("#plan_id").val();
                     if(plan_id == ''){
                         showError('Debe seleccionar un Plan Mensual o Anual');
                         return false;
                     }

                     if(plan_id == 'CM'){
                         montoMensual = jQuery("#montoMensual").val();
                         if(montoMensual == ''){
                            showError('Debe seleccionar un monto para el Plan Mensual');
                            return false;
                         }
                     }

                     if(plan_id == 'CA'){
                         montoAnual = jQuery("#montoAnual").val();
                         if(montoAnual == ''){
                            showError('Debe seleccionar un monto para el Plan Anual');
                            return false;
                         }
                     }

                     // envio el form
                     procesingAjax = true;

                     jQuery("#membresiasProcesando").show();

                     jQuery.ajax({
                        type: 'POST',
                        url: '/membresias/paso1_process',
                        data: {
                            email       : email,
                            plan_id     : plan_id,
                            montoMensual: montoMensual,
                            montoAnual  : montoAnual,
                        },
                        success: function (response) {
                                if(response.error){
                                // Error!
                                if(response.msgError != ''){
                                    showError(response.msgError);
                                }
                                procesingAjax = false;
                                jQuery("#membresiasProcesando").hide();
                            }else{
                                 if(response.urlRedirect != ''){
                                     window.location.href = response.urlRedirect;
                                 }
                                 procesingAjax = false;
                                 jQuery("#membresiasProcesando").hide();
                            }

                        },
                        error: function (response) {
                            alert("Ocurrio un error. Intentelo nuevamente.");
                        }
                     });

                     return false;

                });

            });

            </script>

         </div>
      </div>

      </div>
   </main>
</div>




<?php
// Load the footer
get_footer();
