<?php

global $wp_query;
global $wp_session;
global $wpdb;

$wp_query->is_home = false;

if(!session_id()) {session_start(); }

include_once(plugin_dir_path(__FILE__) . '../../../vendor/autoload.php');


// veo se hay un usuario logueado
if(!is_user_logged_in()){
    // lo saco al login
    wp_redirect('/membresias/login');
    exit();
}

$userLogged = wp_get_current_user();

$user_id = $userLogged->ID;

// busco la infromacion del usaurio
$usuarioInfo = Usuario_Information::getUsuarioInfoByUsuerId($user_id);





if(!empty($_POST['action']) && $_POST['action'] =='cancelar'){

    $hash = $_POST['hash'];

    $hashOK = md5($user_id . 'cancelar');

    if($hash == $hashOK){
        // procedo con la desuscripcion

        include_once(plugin_dir_path(__FILE__) . '../../../vendor/autoload.php');

        if( get_option( 'suscripcion_plugin_mp_sandbox_mode' ) ){
            $access_token = get_option( 'suscripcion_plugin_mp_sandbox_access_token' );
        }else{
            $access_token = get_option( 'suscripcion_plugin_mp_production_access_token' );
        }

        $ch = curl_init();
        $headers  = [
                    'Authorization: Bearer '. $access_token,
                    'Content-Type: application/json'
                ];
        $postData = [
            'status' => "cancelled",
        ];
        curl_setopt($ch, CURLOPT_URL,"https://api.mercadopago.com/preapproval/" . $usuarioInfo->sucripcion_id);
        //curl_setopt($ch, CURLOPT_PUT, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result     = curl_exec ($ch);
        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);


        if($statusCode != 200 && $statusCode != 201){
            // Error
            $_SESSION['errorText'] = 'Ocurrio un error al crear el Plan Mensual Personalizado';

            wp_redirect('/membresias/usuario/plan');
            exit;
        }

        // Todo OK   => se desuscribio al usuario

        //actualizo la usuario temporal
        $dataUserTemp = [
           'eliminado' => 1,
        ];

        $where = [ 'utemp_id' => $usuarioInfo->utemp_id];
        $wpdb->update( $wpdb->prefix . 'users_temp', $dataUserTemp, $where );

        //actualizo la informacion del uasurio

        $dataUserInformation = [
           'eliminado'               => 1,
           'plan_unsuscription_date' => date("Y-m-d H:i:s"),
        ];
        $where = [ 'uinfo_id' => $usuarioInfo->uinfo_id];

        $wpdb->update( $wpdb->prefix . 'users_information', $dataUserInformation , $where);

        // borro el usuario de WP
        require_once(ABSPATH.'wp-admin/includes/user.php' );
        wp_delete_user($user_id);


        //desslogueo al usuario
        wp_logout();


        // pagina de desuscripcion
        wp_redirect('/membresias/logout');
        exit;
    }


}


// Lets load the header of the site
get_header();
?>



<div id="content" class="site-content-full">
   <main id="main" class="site-main">
      <div  class="page type-page status-publish hentry">

         <div class="">
            <div class="wp-block-group PagosHeader">
               <div class="wp-block-group__inner-container">
                  <h3 class="has-text-align-center Miembrosx1Col PFTittlePagos wpsm_panelTT"><?php echo $usuarioInfo->nombre.' '. $usuarioInfo->apellido; ?></h3>
                  <p class="has-text-align-center">
                     <?php if($usuarioInfo->profesion     != '') echo     $usuarioInfo->profesion     . '.'; ?>
                     <?php if($usuarioInfo->ciudad        != '') echo ' '.$usuarioInfo->ciudad        . '.'; ?>
                     <?php if($usuarioInfo->estado_region != '') echo ' '.$usuarioInfo->estado_region . '.'; ?>
                  </p>
                  <div class="PagosHeaderSep"></div>
                  <p class="has-text-align-center">
                     <a href="/membresias/usuario">
                         <input type="button"  value="Volver">
                     </a>
                  </p>
               </div>
            </div>
         </div>

        <?php
            if(isset($_SESSION['errorText']) && $_SESSION['errorText'] != ''){
        ?>
        <div class="wp-block-group BloquePagos" style="color: red;">
           <div class="wp-block-group__inner-container">
              <div class="wp-block-columns">
                  <?php echo  $_SESSION['errorText'];
                        unset($_SESSION['errorText']);
                  ?>
              </div>
           </div>
        </div>
        <?php
            }
        ?>


               <div class="MiembrosWBox">
                  <h3 class="has-text-align-center Miembrosx1Col Pad10">Datos de la Membresía</h3>

                  <div class="MiembrosWBox">
                     <div class="wpforms-container wpforms-container-full" id="wpforms-64938">

                     <script type="text/javascript">
                          function cancelarSuscripcion(){

                              if(confirm('Realmente desea cancalar sus sucripcion ?')){
                                   jQuery('#cancelarForm').submit();
                              }

                              return false;
                          }

                     </script>

                     <form id="cancelarForm" method="post">
                        <input type="hidden" name="action" value="cancelar" >
                        <input type="hidden" name="hash"   value="<?php echo md5($user_id . 'cancelar');?>" >
                     </form>

                     <table>
                        <tr>
                            <th>Membresia</th>
                            <th>Tipo</th>
                            <th>Valor</th>
                            <th>Creado</th>
                        </tr>
                        <tr>
                            <td><?php echo $usuarioInfo->plan_nombre; ?></td>
                            <td><?php echo ucfirst($usuarioInfo->plan_tipo);?></td>
                            <td><?php echo '$ '.$usuarioInfo->plan_valor; ?></td>
                            <td><?php echo $usuarioInfo->plan_suscripcion_date; ?></td>
                        </tr>

                     </table>

                  <p class="has-text-align-center">

                         <input type="button" onclick="return cancelarSuscripcion();"  value="Cancelar Membresía">

                  </p>


                     </div>
                     <div class="MiembrosWBox"></div>
                  </div>
               </div>







      </div>
   </main>
</div>














<?php
// Load the footer
get_footer();
