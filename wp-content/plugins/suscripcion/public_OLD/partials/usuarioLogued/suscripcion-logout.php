<?php

global $wp_query;
global $wp_session;
global $wpdb;
       
$wp_query->is_home = false;

if(!session_id()) {session_start(); }

//deslogueo al usuario
wp_logout();

// voy al home
wp_redirect('/membresias/login');
exit;    
