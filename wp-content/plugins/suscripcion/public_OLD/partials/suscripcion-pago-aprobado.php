<?php

global $wp_query;
global $wp_session;

$wp_query->is_home = false;

if(!session_id()) {session_start(); }


// recreo la session para continuar con el proceso de registro.

// $hash = $userTemp->utemp_id . '-'. md5($userTemp->utemp_id.'-redaccion-MP-pago-aprobado');
// 123456789-ada3s2d1a3s21d32as

$hash = $_GET['hash'];

$referenceArray = explode('-', $hash);
if(count($referenceArray) != 2){
    wp_redirect('/');
    exit;
}

$utemp_id  =  $referenceArray[0];
$hashCheck =  $referenceArray[1];

//chekeo

$hashOK =  md5($utemp_id.'-redaccion-MP-pago-aprobado');

if($hashCheck != $hashOK){
   wp_redirect('/');
   exit;
}

// Todo OK recreo la session

$_SESSION['suscripcion_user_temp_id'] = $utemp_id;

// voy a la pagina de registro -  paso 4
wp_redirect('/membresias/crearCuenta');
exit;
