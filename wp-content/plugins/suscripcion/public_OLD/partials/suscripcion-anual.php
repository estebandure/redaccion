<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://christophercasper.com/
 * @since      1.0.0
 *
 * @package    Suscripcion_Plugin
 * @subpackage Suscripcion_Plugin/public/partials
 */


global $wp_query;
global $wp_session;
global $wpdb;

if(!session_id()) {session_start(); }

$wp_query->is_home = false;


// veo si existe como usuario temporal - registro en curso
$userTemp = null;

$user_temp_id = isset($_SESSION['suscripcion_user_temp_id']) ? $_SESSION['suscripcion_user_temp_id']: null;
if(! empty($user_temp_id )  && is_numeric($user_temp_id)){
   $userTemp = Usuario_Temp::getUsuarioTempById($user_temp_id);
}


if(is_null($userTemp)){
      // voy al paso 1
   wp_redirect('/membresias');
   exit;
}

//el usuario Temp existe

if($userTemp->paso4){
   // ya esta registrado!!

   // borro la session
   $_SESSION['suscripcion_user_temp_id'] = null;
   unset($_SESSION['suscripcion_user_temp_id']);  // ??
   // guardo la session....


   // voy al login
   wp_redirect('/membresias/login');
   exit;
}

if($userTemp->paso3 && !$userTemp->paso4 ){
   // falta cargar los datos
   // voy al paso 4
   wp_redirect('/membresias/paso4');
   exit;

}

if(! empty( $_POST['monto'])){
    // proceso

    $monto =  (int)$_POST['monto'];

    $userPlan = Usuario_Plan::getUsuarioPlanAnualByValor($monto);

    if(is_null($userPlan)){
        // no existe el plan!!
        // lo creo...
        include_once(plugin_dir_path(__FILE__) . '../../vendor/autoload.php');

        if( get_option( 'suscripcion_plugin_mp_sandbox_mode' ) ){
            $access_token = get_option( 'suscripcion_plugin_mp_sandbox_access_token' );
        }else{
           $access_token = get_option( 'suscripcion_plugin_mp_production_access_token' );
        }

        $urlparts = parse_url(home_url());

        $ch = curl_init();
        $headers  = [
                    'Authorization: Bearer '. $access_token,
                    'Content-Type: application/json'
                ];
        $postData = [
            "back_url"  => $urlparts['scheme'].'://'. $urlparts['host'] . "/membresias/procesandoPago",
            'auto_recurring' =>  [
                    "currency_id"        => "ARS",
                    "transaction_amount" => (int)$monto,
                    "frequency"          => 12,
                    "frequency_type"     => "months",
                ],
            'reason' => "Plan Anual Personalizado $" . $monto,
        ];
        curl_setopt($ch, CURLOPT_URL,"https://api.mercadopago.com/preapproval_plan");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result     = curl_exec ($ch);
        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);


        if($statusCode != 200 && $statusCode != 201){
            // Error
            $_SESSION['errorText'] = 'Ocurrio un error al crear el Plan Anual Personalizado';

            wp_redirect('/membresias');
            exit;
        }

        // Todo OK   => se creo el plan
        // Lo agrego a planes

        $plan =  json_decode($result, false);

        $preapproval_id = $plan->id;

        $dataUsersPlan = [
            'plan_mp_id' => $plan->id,
            'nombre'     => $plan->reason,
            'tipo'       => 'anual',
            'valor'      => (int)$monto,
            'activo'     => 0,
            'data'       => json_encode($plan),
            'fecha'      => date("Y-m-d H:i:s")
        ];

        $wpdb->insert( $wpdb->prefix . 'users_plans', $dataUsersPlan );;

        $plan_id = $wpdb->insert_id;

        // lo agrego al usuario temporal
        $dataUserTemp = [
          'paso2'          => 1,
          'plan_id'        => $plan_id,
          'preapproval_id' => $preapproval_id,
          'fecha'          => date("Y-m-d H:i:s")
        ];

        $where = [ 'utemp_id' => $userTemp->utemp_id];
        $wpdb->update( $wpdb->prefix . 'users_temp', $dataUserTemp, $where );


        // voy a pagar con la tarjeta - paso 3
        wp_redirect('/membresias/pagar');
        exit;

    }else{
        // el plan ya existe!!!
        // lo selecciono
        // lo agrego al usuario temporal
        $dataUserTemp = [
          'paso2'          => 1,
          'plan_id'        => $userPlan->plan_id,
          'preapproval_id' => $userPlan->plan_mp_id,
          'fecha'          => date("Y-m-d H:i:s")
        ];

        $where = [ 'utemp_id' => $userTemp->utemp_id];
        $wpdb->update( $wpdb->prefix . 'users_temp', $dataUserTemp, $where );

        // voy a pagar con la tarjeta - paso 3
        wp_redirect('/membresias/pagar');
        exit;

    }
}

 wp_enqueue_script("jquery");

// Lets load the header of the site
get_header();
?>


<div id="content" class="site-content-full">
   <main id="main" class="site-main">
      <div id="post-92841" class="post-92841 page type-page status-publish hentry">
         <div class="">

            <div class="wp-block-group BloquePagos Paso1">
               <div class="wp-block-group__inner-container">
                  <div style="height:20px" aria-hidden="true" class="wp-block-spacer"></div>
                  <p><strong>Elige el monto de tu Membresía Mensual </strong></p>
                  <div class="wpforms-confirmation-container-full wpforms-confirmation-scroll" id="wpforms-confirmation-99552">
                     <p>
                        Selecciona el monto de tu Membresía Anual para continuar.
                     </p>
                     <form method="post" id="formMonto" action="">
                         <input type="text" value="0" id="monto" name="monto" />
                         <br />
                         <br />
                         <input type='submit' value='Seleccionar Monto' />
                          <br />
                         <br />
                         <a href="/membresias" >Volver a elegir planes de Membresía</a>
                     </form>
                  </div>
                  <div style="height:20px" aria-hidden="true" class="wp-block-spacer"></div>
               </div>
            </div>

            <div class="wp-block-group BloquePagos">
               <div class="wp-block-group__inner-container">
                  <div style="height:36px" aria-hidden="true" class="wp-block-spacer"></div>
                  <p>Podés cancelar tu membresía en cualquier momento. Si necesitás ayuda para activarla o darla de baja, por favor mandanos un mail a <a href="mailto:miembros@redaccion.com.ar">miembros@redaccion.com.ar</a> o llamanos al +54 1149171763 </p>
                  <div style="height:36px" aria-hidden="true" class="wp-block-spacer"></div>
               </div>
            </div>
         </div>
      </div>
   </main>
</div>




<?php
// Load the footer
get_footer();
