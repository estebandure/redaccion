<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://christophercasper.com/
 * @since      1.0.0
 *
 * @package    Suscripcion_Plugin
 * @subpackage Suscripcion_Plugin/public/partials
 */

global $wp_query;
global $wp_session;

$wp_query->is_home = false;

if(!session_id()) {session_start(); }

include_once(plugin_dir_path(__FILE__) . '../../vendor/autoload.php');

$recaptchaSiteKey = get_option( 'suscripcion_plugin_recaptcha_clave_sitio' ) ;

// veo si existe como usuario temporal - registro en curso
$userTemp = null;

$user_temp_id = isset($_SESSION['suscripcion_user_temp_id']) ? $_SESSION['suscripcion_user_temp_id']: null;
if(! empty($user_temp_id )  && is_numeric($user_temp_id)){
   $userTemp = Usuario_Temp::getUsuarioTempById($user_temp_id);
}

if(is_null($userTemp)){
   // voy al paso 1
   wp_redirect('/membresias');
   exit;
}


if(!is_null($userTemp)){

   $existsUserTemp = true;

   if($userTemp->paso4){
       // ya esta registrado!!

       // borro la session
       //$_SESSION['suscripcion_user_temp_id'] = null;
       //unset($_SESSION['suscripcion_user_temp_id']);  // ??
       // guardo la session....


       // voy al login
       wp_redirect('/membresias/login');
       exit;
   }


   if(!$userTemp->paso3){
       // voy al paso 3
       wp_redirect('/membresias/pagar');
       exit;
   }

   if(!$userTemp->paso2){
       // voy al paso 2
       wp_redirect('/membresias');
       exit;
   }

   if(!$userTemp->paso1){
       // voy al paso 2
       wp_redirect('/membresias');
       exit;
   }

}

// muestro el formulario de pago
// busco el plan
 $plan_id =  (int)$userTemp->plan_id;

 $userPlan = Usuario_Plan::getUsuarioPlanById($plan_id);


if(! empty( $_POST['wpforms']['fields'][6])){   // Nombre

    // verifico Google recaptcha
    $recaptcha = new \ReCaptcha\ReCaptcha(get_option( 'suscripcion_plugin_recaptcha_clave_secreta' ) );

    //$resp = $recaptcha->setExpectedHostname($_SERVER['SERVER_NAME'])
    $resp = $recaptcha->setExpectedHostname('www.redaccion.com.ar')
                      ->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
    if (!$resp->isSuccess()){
        // Error!
        $_SESSION['errorText'] = 'Recaptcha Incorrecto.';
        // voy a la pagina de registro -  paso 4
        wp_redirect('/membresias/crearCuenta');
        exit;
    }


    // Proceso el formulario
    $nombre           = $_POST['wpforms']['fields'][6];
    $apellido         = $_POST['wpforms']['fields'][7];

    // ver si se usa...!
    //$email            = $_POST['wpforms']['fields'][8];
    $email            = $userTemp->email;

    $fecha_nac        = $_POST['wpforms']['fields'][10];  //date

    try{
        $fechaCarbon = Carbon\Carbon::createFromFormat("d/m/Y", $fecha_nac);
        $fecha_nac =  $fechaCarbon->format("Y-m-d");
    }catch(\Exception $e){
        $fecha_nac = null;
    }



    $documento_tipo   = $_POST['wpforms']['fields'][63];  // 'Pasaporte', 'DNI'
    $numero_documento = $_POST['wpforms']['fields'][66];

    $telefono         = $_POST['wpforms']['fields'][9];
    $direccion1       = $_POST['wpforms']['fields'][72]['address1'];
    $direccion2       = $_POST['wpforms']['fields'][72]['address2'];
    $ciudad           = $_POST['wpforms']['fields'][72]['city'];
    $estado_region    = $_POST['wpforms']['fields'][72]['state'];
    $codigo_postal    = $_POST['wpforms']['fields'][72]['postal'];

    //value : Vivo fuera de la Argentina
    $fuera_de_argentina_arr = isset($_POST['wpforms']['fields'][74]) ?  $_POST['wpforms']['fields'][74] : null;
    $fuera_de_argentina_arr = is_array($fuera_de_argentina_arr) ?  $fuera_de_argentina_arr : [];
    $fuera_de_argentina     = in_array('Vivo fuera de la Argentina', $fuera_de_argentina_arr)? 1 : 0;

    $profesion          = $_POST['wpforms']['fields'][11];

    $contacto_por     = $_POST['wpforms']['fields'][70]; // Array
    $contacto_por     = is_array($contacto_por) ?  $contacto_por : [];

    $twitter          = in_array('Twitter'    , $contacto_por) ? 1 : 0; //Twitter
    $facebook         = in_array('Facebook'   , $contacto_por) ? 1 : 0; //Facebook
    $instagram        = in_array('Instagram'  , $contacto_por) ? 1 : 0; //Instagram
    $newsletters      = in_array('Newsletters', $contacto_por) ? 1 : 0; //Newsletters
    $coResponsable    = in_array('Co-Responsable de RED/ACCIÓN', $contacto_por) ? 1 : 0; //Co-Responsable de RED/ACCIÓN
    $unEvento         = in_array('Un evento'  , $contacto_por) ? 1 : 0; //Un evento
    $opcionOtro       = in_array('Otro'       , $contacto_por) ? 1 : 0; //Otro

    $contacto_otro    = $_POST['wpforms']['fields'][71];   //  ¿Nos contás cómo llegaste?


    // veo si existe como usuario registrado
    $usaurioRegistrado = get_user_by( 'email', $userTemp->email );
    if($usaurioRegistrado !== false){
        // existe
        $user_id         =  $usaurioRegistrado->ID;
        $random_password = '';
    }else{
        // no existe!

        // Creo un password  para el usuario
        $random_password = wp_generate_password( 12, false );

        $user_name = trim($nombre).'_'. trim($apellido);
        $exist_user_id   = username_exists( $user_name );

        if( $exist_user_id ){
            // busco un nombre de usuario que no exista....
            $i = 1;
            while( $exist_user_id ){
                $user_name = trim($nombre) . '_' . trim($apellido) . "_$i";
                $exist_user_id   = username_exists( $user_name );
                $i++;
            }
        }

        //*********************************************************************/
        // Creo el usuario WP
        $user_id = wp_create_user(
            $user_name,
            $random_password,
            $userTemp->email
        );

    }

    //*********************************************************************/
    // agrego la info
    $dataUserInformation = [
         'utemp_id'       => $userTemp->utemp_id,
         'user_id'        => $user_id,

         'plan_id'        => $userPlan->plan_id,
         'plan_suscripto' => 1,
         'plan_suscripcion_date'   => date("Y-m-d H:i:s"),
         'plan_unsuscription_date' => null,
         'plan_nombre'    => $userPlan->nombre,
         'plan_tipo'      => $userPlan->tipo,
         'plan_valor'     => $userPlan->valor,
         'plan_mp_id'     => $userPlan->plan_mp_id,
         'sucripcion_id'  => $userTemp->preapproval_id,  //ID de la suscripcion - Para cancelarlo...

         'nombre'           => $nombre,
         'apellido'         => $apellido,
         'email'            => $email,
         'fecha_nac'        => $fecha_nac,
         'documento_tipo'   => $documento_tipo,
         'numero_documento' => $numero_documento,
         'telefono'         => $telefono,

         'direccion1'       => $direccion1,
         'direccion2'       => $direccion2,
         'ciudad'           => $ciudad,
         'estado_region'    => $estado_region,
         'codigo_postal'    => $codigo_postal,

         'fuera_de_argentina' => $fuera_de_argentina,
         'profesion'          => $profesion,
         'contacto_otro'      => $contacto_otro,

         'twitter'       => $twitter,
         'facebook'      => $facebook,
         'instagram'     => $instagram,
         'newsletters'   => $newsletters,
         'coResponsable' => $coResponsable,
         'unEvento'      => $unEvento,
         'opcionOtro'    => $opcionOtro,

         'fecha'         => date("Y-m-d H:i:s")
    ];

    $wpdb->insert( $wpdb->prefix . 'users_information', $dataUserInformation );
    $uinfo_id  = $wpdb->insert_id;

    //********************************************************************/
    // Sincronizo el usuario temporal y el usuario WP
    $dataUserTemp = [
      'user_id'  => $user_id,
      'paso4'    => 1,
      'user_name'=> $user_name,            // nombre del usaurio creado
      'password' => $random_password,
      'fecha'    => date("Y-m-d H:i:s")
    ];

    $where = [ 'utemp_id' => $userTemp->utemp_id];
    $wpdb->update( $wpdb->prefix . 'users_temp', $dataUserTemp, $where );

    /***********************************************************************/
    // Sincronizo el usuario ID en la tabla de pagos
    $dataUserTemp = [
      'user_id'  => $user_id,
    ];

    $where = [ 'utemp_id' => $userTemp->utemp_id];
    $wpdb->update( $wpdb->prefix . 'users_payments', $dataUserTemp, $where );


    /***********************************************************************/
    // Agrego el usuario a la lista de Mailchimp  37bc523af8 (405477)  Miembros pagos

    $curl = curl_init();

    $postData = [
        "email_address" => $userTemp->email,
        "email_type"    => "html",
        "status"        => "subscribed"
    ];

    $apikey = get_option( 'suscripcion_plugin_mailchip_public_Key' ) ;

    curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://us12.api.mailchimp.com/3.0/lists/37bc523af8/members',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_MAXREDIRS      => 10,
          CURLOPT_TIMEOUT        => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST  => 'POST',
          CURLOPT_POSTFIELDS     => json_encode($postData),
          CURLOPT_HTTPHEADER     => array(
            'Content-Type: application/json',
            'authorization: apikey '. $apikey
          ),
    ));

    $result     = curl_exec ($curl);
    $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    if($statusCode != 200 && $statusCode != 201){
        // Error
    }
    curl_close($curl);

    //*********************************************************************/
    // Envio el nuevo password al usuario
    //*********************************************************************/
    $to       = $userTemp->email;
    $subject  = 'Redacción: Tu Membresía esta completa';
    $headers  = [];
    $headers[]= "From: Redaccion <info@redaccion.com.ar>";
    $headers[]= "Content-Type: text/html; charset=UTF-8";

    $dominio = suscripcion_url();

    $message = "
        Hola, Tu Membresía esta completa.
        <br/>
        Estos son tus datos de acceso:
        <br/>
        Usuario: {$userTemp->email}
        <br/>
        Password:  {$random_password}
        <br/>
        <br/>

        Podes loguearte en <a href=\"{$dominio}/membresias/login\">link</a>
        <br/>
        <br/>
        Saludos!
    ";

    wp_mail( $to, $subject, $message, $headers );


    //*********************************************************************/

    // borro la session
    $_SESSION['suscripcion_user_temp_id'] = null;
    unset($_SESSION['suscripcion_user_temp_id']);

    // voy a la pagina de Bienvenido
    wp_redirect('/membresias/bienvenido');
    exit;
}


// Lets load the header of the site
get_header();
?>

<div id="content" class="site-content-full">
   <div class="content-area-full">
      <main id="main" class="site-main">
         <div class="pageFull" id="post-65011">
            <header class="pageFull-header">
               <h1 class="entry-title">Miembros Pagos</h1>
            </header>
            <div class="Page-full-content">
               <div class="headerFixedMiembros">
                  <div class="MiembrosHeader preguntasFrecuentes">
                     <a href="/miembros/">&lt; volver</a>
                     <img class="redaccionMI" src="/wp-content/uploads/2018/11/redaccion.png" srcset="/wp-content/uploads/2018/11/redaccion.png 480w, /wp-content/uploads/2018/11/redaccion.png 2x" sizes="(max-width: 480px) 200px " alt="Redaccion - MEJOR STARTUP DIGITAL DE NOTICIAS LATAeM">
                     <span> &nbsp;</span>
                  </div>
               </div>
               <h2>.</h2>
               <div class="MiembrosWBox">
                  <h3 class="has-text-align-center Miembrosx1Col Pad10">¡Gracias por ser co-responsable! <img draggable="false" role="img" class="emoji" alt="🤗" src="https://s.w.org/images/core/emoji/13.0.0/svg/1f917.svg"></h3>
                  <p class="has-text-align-center">(Los últimos datos <img draggable="false" role="img" class="emoji" alt="😬" src="https://s.w.org/images/core/emoji/13.0.0/svg/1f62c.svg"> )</p>
                  <div class="MiembrosWBox">
                     <div class="wpforms-container wpforms-container-full" id="wpforms-64938">


            <?php
                if(isset($_SESSION['errorText']) && $_SESSION['errorText'] != ''){
            ?>
            <div class="wp-block-group BloquePagos" style="color: red;">
               <div class="wp-block-group__inner-container">
                  <div class="wp-block-columns">
                      <?php echo  $_SESSION['errorText'];
                            unset($_SESSION['errorText']);
                      ?>
                  </div>
               </div>
            </div>
            <?php
                }
            ?>

                        <form id="paso4-info-form" class="wpforms-validate wpforms-form" <?php /* data-formid="64938" */?> method="post" enctype="multipart/form-data" action="" novalidate="novalidate">
                           <noscript class="wpforms-error-noscript">Por favor, activa JavaScript en tu navegador para completar este formulario.</noscript>
                           <div class="wpforms-field-container">
                              <div id="wpforms-64938-field_6-container" class="wpforms-field wpforms-field-name wpforms-one-half wpforms-first" data-field-id="6"><label class="wpforms-field-label" for="wpforms-64938-field_6">Nombre <span class="wpforms-required-label">*</span></label><input type="text" id="wpforms-64938-field_6" class="wpforms-field-large wpforms-field-required" name="wpforms[fields][6]" placeholder="Nombre" required=""></div>
                              <div id="wpforms-64938-field_7-container" class="wpforms-field wpforms-field-name wpforms-one-half" data-field-id="7"><label class="wpforms-field-label" for="wpforms-64938-field_7">Apellido <span class="wpforms-required-label">*</span></label><input type="text" id="wpforms-64938-field_7" class="wpforms-field-large wpforms-field-required" name="wpforms[fields][7]" placeholder="Apellido" required=""></div>
                              <div id="wpforms-64938-field_8-container" class="wpforms-field wpforms-field-email wpforms-one-half wpforms-first" data-field-id="8"><label class="wpforms-field-label" for="wpforms-64938-field_8">Dirección de correo electrónico <span class="wpforms-required-label">*</span></label><input type="email" id="wpforms-64938-field_8" class="wpforms-field-large wpforms-field-required" name="wpforms[fields][8]" value="<?php echo $userTemp->email;?>" readonly="readonly" placeholder="Ej: miembros@redaccion.com.ar" required=""></div>
                              <div id="wpforms-64938-field_10-container" class="wpforms-field wpforms-field-text wpforms-one-half" data-field-id="10"><label class="wpforms-field-label" for="wpforms-64938-field_10">Fecha de nacimiento <span class="wpforms-required-label">*</span></label><input type="text" id="wpforms-64938-field_10" class="wpforms-field-large wpforms-field-required date" name="wpforms[fields][10]" placeholder="DD/MM/AAAA" required=""></div>
                              <div id="wpforms-64938-field_63-container" class="wpforms-field wpforms-field-select" data-field-id="63">
                                 <label class="wpforms-field-label" for="wpforms-64938-field_63">Tipo de documento <span class="wpforms-required-label">*</span></label>
                                 <select id="wpforms-64938-field_63" class="wpforms-field-medium wpforms-field-required" name="wpforms[fields][63]" required="required">
                                    <option value="DNI">DNI</option>
                                    <option value="Pasaporte">Pasaporte</option>
                                 </select>
                              </div>
                              <div id="wpforms-64938-field_66-container" class="wpforms-field wpforms-field-text" data-field-id="66"><label class="wpforms-field-label" for="wpforms-64938-field_66">Número de documento <span class="wpforms-required-label">*</span></label><input type="text" id="wpforms-64938-field_66" class="wpforms-field-large wpforms-field-required" name="wpforms[fields][66]" placeholder="Número de documento" required=""></div>
                              <div id="wpforms-64938-field_9-container" class="wpforms-field wpforms-field-text" data-field-id="9"><label class="wpforms-field-label" for="wpforms-64938-field_9">Teléfono de contacto <span class="wpforms-required-label">*</span></label><input type="text" id="wpforms-64938-field_9" class="wpforms-field-large wpforms-field-required" name="wpforms[fields][9]" placeholder="___ ____ _____" required=""></div>
                              <div id="wpforms-64938-field_72-container" class="wpforms-field wpforms-field-address" data-field-id="72">
                                 <label class="wpforms-field-label" for="wpforms-64938-field_72">Dirección (donde querés recibir el libro de regalo) <span class="wpforms-required-label">*</span></label>
                                 <div class="wpforms-field-row wpforms-field-medium">
                                    <div><input type="text" id="wpforms-64938-field_72" class="wpforms-field-address-address1 wpforms-field-required" name="wpforms[fields][72][address1]" required=""><label for="wpforms-64938-field_72" class="wpforms-field-sublabel after ">Dirección (línea 1)</label></div>
                                 </div>
                                 <div class="wpforms-field-row wpforms-field-medium">
                                    <div><input type="text" id="wpforms-64938-field_72-address2" class="wpforms-field-address-address2" name="wpforms[fields][72][address2]"><label for="wpforms-64938-field_72-address2" class="wpforms-field-sublabel after ">Dirección 2</label></div>
                                 </div>
                                 <div class="wpforms-field-row wpforms-field-medium">
                                    <div class="wpforms-field-row-block wpforms-one-half wpforms-first"><input type="text" id="wpforms-64938-field_72-city" class="wpforms-field-address-city wpforms-field-required" name="wpforms[fields][72][city]" required=""><label for="wpforms-64938-field_72-city" class="wpforms-field-sublabel after ">Ciudad</label></div>
                                    <div class="wpforms-field-row-block wpforms-one-half"><input type="text" id="wpforms-64938-field_72-state" class="wpforms-field-address-state wpforms-field-required" name="wpforms[fields][72][state]" required=""><label for="wpforms-64938-field_72-state" class="wpforms-field-sublabel after ">Estado / Provincia / Región</label></div>
                                 </div>
                                 <div class="wpforms-field-row wpforms-field-medium">
                                    <div class="wpforms-field-row-block wpforms-one-half wpforms-first"><input type="text" id="wpforms-64938-field_72-postal" class="wpforms-field-address-postal wpforms-field-required" name="wpforms[fields][72][postal]" required=""><label for="wpforms-64938-field_72-postal" class="wpforms-field-sublabel after ">Código postal</label></div>
                                 </div>
                              </div>
                              <div id="wpforms-64938-field_74-container" class="wpforms-field wpforms-field-checkbox" data-field-id="74">
                                 <label class="wpforms-field-label" for="wpforms-64938-field_74">Si no vivís en la Argentina, marcá esta opción y te enviamos el PDF</label>
                                 <ul id="wpforms-64938-field_74">
                                    <li class="choice-1 depth-1"><input type="checkbox" id="wpforms-64938-field_74_1" name="wpforms[fields][74][]" value="Vivo fuera de la Argentina"><label class="wpforms-field-label-inline" for="wpforms-64938-field_74_1">Vivo fuera de la Argentina</label></li>
                                 </ul>
                              </div>
                              <div id="wpforms-64938-field_11-container" class="wpforms-field wpforms-field-text" data-field-id="11"><label class="wpforms-field-label" for="wpforms-64938-field_11">Profesión</label><input type="text" id="wpforms-64938-field_11" class="wpforms-field-large" name="wpforms[fields][11]"></div>
                              <div id="wpforms-64938-field_70-container" class="wpforms-field wpforms-field-checkbox wpforms-list-2-columns wpforms-conditional-trigger" data-field-id="70">
                                 <label class="wpforms-field-label" for="wpforms-64938-field_70">¿Cómo llegaste a RED/ACCIÓN?</label>
                                 <ul id="wpforms-64938-field_70">
                                    <li class="choice-1 depth-1"><input type="checkbox" id="wpforms-64938-field_70_1" name="wpforms[fields][70][]" value="Twitter"><label class="wpforms-field-label-inline" for="wpforms-64938-field_70_1">Twitter</label></li>
                                    <li class="choice-2 depth-1"><input type="checkbox" id="wpforms-64938-field_70_2" name="wpforms[fields][70][]" value="Facebook"><label class="wpforms-field-label-inline" for="wpforms-64938-field_70_2">Facebook</label></li>
                                    <li class="choice-3 depth-1"><input type="checkbox" id="wpforms-64938-field_70_3" name="wpforms[fields][70][]" value="Instagram"><label class="wpforms-field-label-inline" for="wpforms-64938-field_70_3">Instagram</label></li>
                                    <li class="choice-8 depth-1"><input type="checkbox" id="wpforms-64938-field_70_8" name="wpforms[fields][70][]" value="Newsletters"><label class="wpforms-field-label-inline" for="wpforms-64938-field_70_8">Newsletters</label></li>
                                    <li class="choice-4 depth-1"><input type="checkbox" id="wpforms-64938-field_70_4" name="wpforms[fields][70][]" value="Co-Responsable de RED/ACCIÓN"><label class="wpforms-field-label-inline" for="wpforms-64938-field_70_4">Co-Responsable de RED/ACCIÓN</label></li>
                                    <li class="choice-5 depth-1"><input type="checkbox" id="wpforms-64938-field_70_5" name="wpforms[fields][70][]" value="Un evento"><label class="wpforms-field-label-inline" for="wpforms-64938-field_70_5">Un evento</label></li>
                                    <li class="choice-6 depth-1"><input type="checkbox" id="wpforms-64938-field_70_6" name="wpforms[fields][70][]" value="Otro"><label class="wpforms-field-label-inline" for="wpforms-64938-field_70_6">Otro</label></li>
                                 </ul>
                              </div>
                              <div id="wpforms-64938-field_71-container" class="wpforms-field wpforms-field-text wpforms-conditional-field wpforms-conditional-hide" data-field-id="71" style="display:no ne;">
                                 <label class="wpforms-field-label" for="wpforms-64938-field_71">¿Nos contás cómo llegaste?</label>
                                 <input type="text" id="wpforms-64938-field_71" class="wpforms-field-medium" name="wpforms[fields][71]">
                              </div>
                           </div>
                           <div class="wpforms-field wpforms-field-hp">
                               <label for="wpforms-64938-field-hp" class="wpforms-field-label">Message</label>
                               <input type="text" name="wpforms[hp]" id="wpforms-64938-field-hp" class="wpforms-field-medium">
                           </div>

                           <div class="wpforms-recaptcha-container">
                              <div class="g-recaptcha form-field" data-sitekey="<?php echo $recaptchaSiteKey; ?>"></div>
                              <span id="recaptcha-state-error" class="msg-error" style="display: none; color:red;">Este campo es obligatorio.</span>
                           </div>

                           <div class="wpforms-submit-container">
                              <button type="submit" name="wpforms[submit]" class="wpforms-submit " id="wpforms-submit-64938" value="wpforms-submit" aria-live="assertive">Finalizar registro</button>
                           </div>
                        </form>
                        <script type="text/javascript">
                            jQuery(document).ready(function(){

                                jQuery("#paso4-info-form").validate({
                                    rules: {
                                        dateITA: true
                                    }
                                });

                                jQuery('#paso4-info-form').on('submit', function(e) {
                                  if(grecaptcha.getResponse() == "") {
                                      e.preventDefault();
                                      jQuery('#recaptcha-state-error').show();
                                  } else {
                                      jQuery('#recaptcha-state-error').hide();
                                  }
                                });
                            });
                        </script>

                        <script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=es"></script>

                     </div>
                     <div class="MiembrosWBox"></div>
                  </div>
               </div>
            </div>
         </div>
      </main>
   </div>

</div>



<?php
// Load the footer
get_footer();
