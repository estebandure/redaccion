<?php

global $wp_query;
global $wp_session;
global $wpdb;

$wp_query->is_home = false;

if(!session_id()) {session_start(); }


if(! empty( $_POST['email'])){
    // proceso

    $email =  $_POST['email'];

    // veo se hay un usuario logueado  - lo envio al area del usuario
    if(is_user_logged_in()){
       $userLogged = wp_get_current_user();
       if($userLogged->user_email == $email){
           // es el email del usaurio logueado...
           wp_redirect('/membresias/usuario');
           exit;
       }
    }

    // veo si existe como usuario registrado
    $usaurioRegistrado = get_user_by( 'email', $email );
    if($usaurioRegistrado !== false){
       wp_redirect('/membresias/login');
       exit;
    }

    // veo si existe como usuario temporal - registro en curso
    $userTemp = null;

    $user_temp_id = isset($_SESSION['suscripcion_user_temp_id']) ? $_SESSION['suscripcion_user_temp_id']: null;
    if(! empty($user_temp_id )  && is_numeric($user_temp_id)){
        $userTemp = Usuario_Temp::getUsuarioTempById($user_temp_id);
    }

    $existsUserTemp  = false;

    if(!is_null($userTemp)){

       if($userTemp->email != $email){
           // es otro usuario
           $existsUserTemp  = false;
       }else{
           // es el mismo usuario
           $existsUserTemp = true;
       }
    }else{
        // No existe
        $existsUserTemp  = false;
    }

    if(!$existsUserTemp){
        // lo busco por email entre los que no completaron el registro
        $userTemp = Usuario_Temp::getUsuarioTempByEmail($email);
        if(!is_null($userTemp)){
            $existsUserTemp = true;
            // lo guardo en el session
            $_SESSION['suscripcion_user_temp_id'] = $userTemp->utemp_id;
        }
    }


    if(!$existsUserTemp){
        // creo el usuario
        $dataUserTemp = [
          'user_id' => null,
          'email'   => $email,
          'paso1'   => 1,
          'paso2'   => 0,
          'paso3'   => 0,
          'paso4'   => 0,
          'plan_id' => null,   //todavia no eligio el plan
          'fecha'   => date("Y-m-d H:i:s")
        ];

        $wpdb->insert( $wpdb->prefix . 'users_temp', $dataUserTemp );
        $user_temp_id  = $wpdb->insert_id;

        // lo guardo en el session
        $_SESSION['suscripcion_user_temp_id'] = $user_temp_id;

        //recargo
        $userTemp = Usuario_Temp::getUsuarioTempById($user_temp_id);
    }

    if($userTemp->paso3 && $userTemp->pago_aprobado && !$userTemp->paso4){
       // faltael registro
       // voy al paso 4
       wp_redirect('/membresias/crearCuenta');
       exit;
    }

    if($userTemp->paso3 && $userTemp->pago_pendiente && !$userTemp->paso4){
        // falta confirmacion del pago
        // Pagina de retomar el pago
        // voy a la pagina de procesando...
        wp_redirect('/membresias/procesandoPago');
        exit;
    }

    if($userTemp->paso3){
       // ya esta registrado!!

       // borro la session
       //$_SESSION['suscripcion_user_temp_id'] = null;
       //unset($_SESSION['suscripcion_user_temp_id']);  // ??

       // voy al login
       wp_redirect('/membresias/login');
       exit;
    }

    if($userTemp->paso2 && !$userTemp->paso3 ){
       // falta cargar los datos
       // voy al paso 3
       wp_redirect('/membresias/pagar');
       exit;

    }


   // voy al paso 2
   wp_redirect('/membresias');
   exit;
}



// Lets load the header of the site
get_header();
?>

<div id="content" class="site-content-full">
   <main id="main" class="site-main">
      <div id="post-92841" class="post-92841 page type-page status-publish hentry">
         <div class="">
            <div class="wp-block-group PagosHeader">
               <div class="wp-block-group__inner-container">
                  <h3 class="has-text-align-center Miembrosx1Col PFTittlePagos wpsm_panelTT">¡Gracias por sumarte a nuestra comunidad de co-responsables!</h3>
                  <p class="has-text-align-center"><em>Tu aporte es fundamental para que nuestro periodismo sea abierto, sume más voces y logre un mayor impacto.</em></p>
                  <div class="PagosHeaderSep"></div>
               </div>
            </div>
            <div class="wp-block-group BloquePagos">
               <div class="wp-block-group__inner-container">
                  <p><strong>Como co-responsable vas a acceder a:</strong></p>
                  <ul class="BeneficiosCo-responsables">
                     <li>El Reporte de los Lunes <em>(El panorama semanal de Chani Guyot)</em></li>
                     <li>Acceso a servicios exclusivos <em>(recibí nuestros contenidos por WhatsApp)</em></li>
                     <li>Invitaciones exclusivas a los eventos de RED/ACCIÓN</li>
                     <li>La posibilidad de participar de nuestro periodismo sugiriendo temas y enfoques</li>
                     <li>La newsletter semanal para miembros</li>
                     <li>Lo más importante: vas a ayudar a que este periodismo con impacto siga siendo abierto para todos</li>
                  </ul>
                  <p class="has-text-color has-vivid-red-color"><br><strong>Como agradecimiento, tenemos un regalo de bienvenida</strong>...</p>
                  <div class="wp-block-columns">
                     <div class="wp-block-column" style="flex-basis:18%">
                        <div class="wp-block-image BloquesPagoLibro">
                           <figure class="aligncenter size-medium"><img data-attachment-id="56919" data-permalink="https://www.redaccion.com.ar/100-libros-para-entender-el-mundo/redaccion_100libros/" data-orig-file="https://i0.wp.com/www.redaccion.com.ar/wp-content/uploads/2019/11/Redaccion_100libros.png?fit=600%2C600&amp;ssl=1" data-orig-size="600,600" data-comments-opened="0" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="Redaccion_100libros" data-image-description="" data-medium-file="https://i0.wp.com/www.redaccion.com.ar/wp-content/uploads/2019/11/Redaccion_100libros.png?fit=238%2C238&amp;ssl=1" data-large-file="https://i0.wp.com/www.redaccion.com.ar/wp-content/uploads/2019/11/Redaccion_100libros.png?fit=600%2C600&amp;ssl=1" loading="lazy" src="https://i0.wp.com/www.redaccion.com.ar/wp-content/uploads/2019/11/Redaccion_100libros.png?resize=238%2C238&amp;ssl=1" alt="" class="wp-image-56919" srcset="https://i0.wp.com/www.redaccion.com.ar/wp-content/uploads/2019/11/Redaccion_100libros.png?resize=238%2C238&amp;ssl=1 238w, https://i0.wp.com/www.redaccion.com.ar/wp-content/uploads/2019/11/Redaccion_100libros.png?resize=300%2C300&amp;ssl=1 300w, https://i0.wp.com/www.redaccion.com.ar/wp-content/uploads/2019/11/Redaccion_100libros.png?w=600&amp;ssl=1 600w, https://i0.wp.com/www.redaccion.com.ar/wp-content/uploads/2019/11/Redaccion_100libros.png?resize=100%2C100&amp;ssl=1 100w" sizes="(max-width: 238px) 100vw, 238px" data-recalc-dims="1" width="238" height="238"></figure>
                        </div>
                        <p></p>
                     </div>
                     <div class="wp-block-column BloquesPagoLibroCol" style="flex-basis:82%">
                        <p><br><strong>“100 libros para entender el mundo”</strong>. Te enviamos a tu casa un ejemplar del libro en el que escritores, editores, pensadores y periodistas recomiendan sus libros favoritos. Con texto del María O'Donell, Andrés Malamud, Claudia Piñeiro, Juan Llach, Graciela Fernández Meijide y Marcos Novaro, entre muchos otros. (Envíos dentro de la Argentina. PDF para el exterior)</p>
                     </div>
                  </div>
                  <div style="height:20px" aria-hidden="true" class="wp-block-spacer"></div>
               </div>
            </div>
            <div class="wp-block-group BloquePagos Paso1">
               <div class="wp-block-group__inner-container">
                  <div style="height:20px" aria-hidden="true" class="wp-block-spacer"></div>
                  <p><strong>Tu email: </strong></p>
                  <div class="wpforms-container wpforms-container-full" id="wpforms-99552">

                     <form id="paso1-email-form" class="wpforms-validate wpforms-form" data-formid="99552" method="post" enctype="multipart/form-data" action="/membresias" novalidate="novalidate">
                        <noscript class="wpforms-error-noscript">Por favor, activa JavaScript en tu navegador para completar este formulario.</noscript>
                        <div class="wpforms-field-container">
                           <div id="wpforms-99552-field_2-container" class="wpforms-field wpforms-field-email correo-miembros-pagos wpforms-one-half wpforms-first" data-field-id="2">
                               <label class="wpforms-field-label wpforms-label-hide" for="wpforms-99552-field_2">Correo electrónico <span class="wpforms-required-label">*</span></label>
                               <input type="email" id="wpforms-99552-field_2" class="wpforms-field-medium wpforms-field-required" name="email" placeholder="Ingresá tu e-mail" style="width: 50%;" required>
                           </div>
                        </div>
<?php /*
                        <div class="wpforms-field wpforms-field-hp">
                            <label for="wpforms-99552-field-hp" class="wpforms-field-label">Phone</label>
                            <input type="text" name="wpforms[hp]" id="wpforms-99552-field-hp" class="wpforms-field-medium">
                        </div>
*/ ?>
                        <div class="wpforms-submit-container">

                            <button type="submit" name="" class="wpforms-submit " id="" value="wpforms-submit" aria-live="assertive" data-alt-text="cargando..." data-submit-text="Siguiente">Siguiente</button>
                        </div>
                     </form>
                     <script type="text/javascript">
                        jQuery("#paso1-email-form").validate();
                     </script>

                  </div>
                  <div style="height:20px" aria-hidden="true" class="wp-block-spacer"></div>
               </div>
            </div>

            <div class="wp-block-group BloquePagos">
               <div class="wp-block-group__inner-container">
                  <div style="height:36px" aria-hidden="true" class="wp-block-spacer"></div>
                  <p>Podés cancelar tu membresía en cualquier momento. Si necesitás ayuda para activarla o darla de baja, por favor mandanos un mail a <a href="mailto:miembros@redaccion.com.ar">miembros@redaccion.com.ar</a> o llamanos al +54 1149171763 </p>
                  <div style="height:36px" aria-hidden="true" class="wp-block-spacer"></div>
               </div>
            </div>
         </div>
      </div>
   </main>
</div>




<?php
// Load the footer
get_footer();
