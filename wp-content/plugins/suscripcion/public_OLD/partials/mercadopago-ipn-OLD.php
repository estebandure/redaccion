<?php
    global $wpdb;

    include_once(plugin_dir_path(__FILE__) . '../../vendor/autoload.php');   
    
    if( get_option( 'suscripcion_plugin_mp_sandbox_mode' ) ){
        MercadoPago\SDK::setAccessToken( get_option( 'suscripcion_plugin_mp_sandbox_access_token' ) );
    }else{
        MercadoPago\SDK::setAccessToken( get_option( 'suscripcion_plugin_mp_production_access_token' ) );
    }
   
    $data = [
        'get'  => $_GET,
        'post' => $_POST,
    ];
    
    $json = json_encode($data);    
    
    $mp_logs = [
      'external_id' => '',
      'user_id'     => null,
      'result'      => '',
      'data'        => $json,
      'fecha'       => date("Y-m-d H:i:s"),
    ];

    $wpdb->insert( $wpdb->prefix . 'users_mp_logs', $mp_logs );       
    
    try {

        $merchant_order = null;
        
        $topic = isset($_GET["topic"]) ? $_GET["topic"]: null;
        $id    = isset($_GET["id"   ]) ? $_GET["id"   ]: null;
          
        switch($topic) {
            case "payment":
                $payment = MercadoPago\Payment::find_by_id($id);
                if(!is_null($payment)){
                    // Get the payment and the corresponding merchant_order reported by the IPN.
                    $merchant_order = MercadoPago\MerchantOrder::find_by_id($payment->order->id);
                }                
                break;
            case "merchant_order":
                $merchant_order = MercadoPago\MerchantOrder::find_by_id($id);
                break;
        }
    
    } catch(Exception $e) {
         // retorno 503
        header($_SERVER["SERVER_PROTOCOL"]." 503 Service Temporarily Unavailable", true, 503);
        exit();    
    }
    if(is_null($payment)){
        // retorno 200
        header("HTTP/1.1 200 OK");
        exit();         
    }       
       

    $paid_amount = 0;
    foreach ($merchant_order->payments as $payment) {
        if ($payment['status'] == 'approved'){
            $paid_amount += $payment['transaction_amount'];
        }
    }
    
    $transaccionAprobada = false;

    // If the payment's transaction amount is equal (or bigger) than the merchant_order's amount you can release your items
    if($paid_amount >= $merchant_order->total_amount){
        if (count($merchant_order->shipments)>0) { // The merchant_order has shipments
            if($merchant_order->shipments[0]->status == "ready_to_ship") {
                //print_r("Totally paid. Print the label and release your item.");
                $transaccionAprobada = true;
            }
        } else { // The merchant_order don't has any shipments
            //print_r("Totally paid. Release your item.");
            $transaccionAprobada = true;
        }
    } else {
        $transaccionAprobada = false;
        //print_r("Not paid yet. Do not release your item.");
    }
    
    $external_reference  = $merchant_order->external_reference;
    
    if($transaccionAprobada){
        // busco el external reference
        
        // verifico el formato  I =>  12356456-temp-1-ABCDE
        // verifico el formato II =>  12356456-user-2-ABCDE
        $referenceArray = explode('-',$external_reference);
        if(count($referenceArray) != 4){
            // Es un pago de otra cosa....
            // guardo en el log
            $json = json_encode($merchant_order);    
            
            $mp_logs = [
              'external_id' => $external_reference,
              'user_id'     => null,
              'result'      => 'Sin referencia',
              'data'        => $json,
              'fecha'       => date("Y-m-d H:i:s"),
            ];

            $wpdb->insert( $wpdb->prefix . 'users_mp_logs', $mp_logs );              
          
            // retorno 200
            header("HTTP/1.1 200 OK");
            exit();
            
        }
        
        //tien 4 partes...
        $ID_USUARIO = $referenceArray[0];
        $tipo       = $referenceArray[1];
        $plan_id    = $referenceArray[2];
        $hash       = $referenceArray[3];
        
        // chekeo....
        $reference = $ID_USUARIO.'-'.$tipo.'-'.$plan_id;
        $hash_reference =  substr(md5($reference.'-redaccion-MP'),0,5);        
        if($hash_reference != $hash){
            // Error en la external reference
            // guardo en el log
            $json = json_encode($merchant_order);    
            
            $mp_logs = [
              'external_id' => $external_reference,
              'user_id'     => null,
              'result'      => 'bad hash!',
              'data'        => $json,
              'fecha'       => date("Y-m-d H:i:s"),
            ];

            $wpdb->insert( $wpdb->prefix . 'users_mp_logs', $mp_logs );              
          
            // retorno 200
            header("HTTP/1.1 200 OK");
            exit();            
        }
        
        // reference OK
        if($tipo == 'temp'){
            //Usuario Temporal
            $userTemp = Usuario_Temp::getUsuarioTempById($user_temp_id);
            if(is_null($userTemp)){
                $json = json_encode($merchant_order);    
                
                $mp_logs = [
                  'external_id' => $external_reference,
                  'user_id'     => null,
                  'result'      => 'usuario Temp no encontrado',
                  'data'        => $json,
                  'fecha'       => date("Y-m-d H:i:s"),
                ];

                $wpdb->insert( $wpdb->prefix . 'users_mp_logs', $mp_logs );              
              
                // retorno 200
                header("HTTP/1.1 200 OK");
                exit();                  
                
            }
            // busco el usuario
            $user_id  = $userTemp->user_id;
            //$plan_id  =  (int)$userTemp->plan_id;
            $plan_id  =  (int)$plan_id;
                
            $userPlan = Usuario_Plan::getUsuarioPlanById($plan_id);
            
            //log de pagos
            $users_payment = [
              'external_id' => $external_reference,
              'user_id'     => $user_id,
              'utemp_id'    => $userTemp->utemp_id,
              'plan_id'     => $userPlan->plan_id,
              'status'      => 'payment',
              'data'        => $json,
              'fecha'       => date("Y-m-d H:i:s"),
            ];

            $wpdb->insert( $wpdb->prefix . 'users_payments', $users_payment );                 
            
            
            if($userTemp->pago_pendiente && !$userTemp->pago_aprobado){
                // actualizo el estado
                $dataUserTemp = [
                   'pago_aprobado' => 0,
                   'pago_aprobado' => 1,
                   'fecha'         => date("Y-m-d H:i:s")
                ];
                   
                $where = [ 'utemp_id' => $userTemp->utemp_id];
                $wpdb->update( $wpdb->prefix . 'users_temp', $dataUserTemp, $where );
                
                $hash = $userTemp->utemp_id. md5($userTemp->utemp_id.'-redaccion-MP-pago-aprobado');
                
                //*********************************************************************/
                // Envio el email para seguir con el registro
                //*********************************************************************/
                $to       = $dataUserTemp->email;
                $subject  = 'Redacción: Pago aprobado';
                $headers  = [];
                $headers[]= "From: Redaccion <info@redaccion.com.ar>";
                $headers[]= "Content-Type: text/html; charset=UTF-8";
                
                $message = "                                
                    Hola, El pago de tu suscripcion fue aprobado.
                    <br/>
                    <br/>
                    Sigo con el registro en <a href=\"/suscripcion/pago-aprobado/{$hash}\">link</a>
                    <br/>
                    <br/>
                    Saludos!    
                ";
                
                wp_mail( $to, $subject, $message, $headers );
            }
            
        }elseif($tipo == 'user'){
            // Usuario registrado
           
            // veo si existe como usuario registrado
            $usuarioRegistrado = get_user_by( 'ID', $ID_USUARIO );   
            if($usuarioRegistrado !== false){
                // existe
                
                //$plan_id  =  (int)$userTemp->plan_id;
                $plan_id  =  (int)$plan_id;
                        
                $userPlan = Usuario_Plan::getUsuarioPlanById($plan_id);                 
                
                $usuarioInfo = Usuario_Information::getUsuarioInfoByUsuerId($ID_USUARIO);
                
                //log de pagos
                $users_payment = [
                  'external_id' => $external_reference,
                  'user_id'     => $ID_USUARIO,              // usuario registrado 
                  'utemp_id'    => $usuarioInfo->utemp_id,   // usuario temporal
                  'plan_id'     => $userPlan->plan_id,
                  'status'      => 'payment',
                  'data'        => $json,
                  'fecha'       => date("Y-m-d H:i:s"),
                ];

                $wpdb->insert( $wpdb->prefix . 'users_payments', $users_payment );                   
                
            }else{
                // el usuario no existe!
                // guado en el log
                $json = json_encode($merchant_order);    
                
                $mp_logs = [
                  'external_id' => $external_reference,
                  'user_id'     => $ID_USUARIO,
                  'result'      => '',
                  'data'        => $json,
                  'fecha'       => date("Y-m-d H:i:s"),
                ];

                $wpdb->insert( $wpdb->prefix . 'users_mp_logs', $mp_logs );                  
            } 
        }else{  
           // Error! 
        }
    }else{
        // no aprobada 
        // guado en el log
        $json = json_encode($merchant_order);    
        
        $mp_logs = [
          'external_id' => $external_reference,
          'user_id'     => null,
          'result'      => '',
          'data'        => $json,
          'fecha'       => date("Y-m-d H:i:s"),
        ];

        $wpdb->insert( $wpdb->prefix . 'users_mp_logs', $mp_logs );        
    }

    // retorno 200
    header("HTTP/1.1 200 OK");
    exit();
            
