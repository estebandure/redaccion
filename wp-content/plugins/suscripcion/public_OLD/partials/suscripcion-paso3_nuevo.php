<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://christophercasper.com/
 * @since      1.0.0
 *
 * @package    Suscripcion_Plugin
 * @subpackage Suscripcion_Plugin/public/partials
 */


global $wp_query;
global $wp_session;

$wp_query->is_home = false;

if(!session_id()) {session_start(); }

// veo si existe como usuario temporal - registro en curso
$userTemp = null;

$user_temp_id = isset($_SESSION['suscripcion_user_temp_id']) ? $_SESSION['suscripcion_user_temp_id']: null;
if(! empty($user_temp_id )  && is_numeric($user_temp_id)){
   $userTemp = Usuario_Temp::getUsuarioTempById($user_temp_id);
}

if(is_null($userTemp)){
   // voy al paso 1
   wp_redirect('/suscripcion');
   exit;
}


if(!is_null($userTemp)){

   $existsUserTemp = true;

   if($userTemp->paso4){
       // ya esta registrado!!

       // borro la session
       $_SESSION['suscripcion_user_temp_id'] = null;
       unset($_SESSION['suscripcion_user_temp_id']);  // ??
       // guardo la session....


       // voy al login
       wp_redirect('/suscripcion/login');
       exit;
   }

   if($userTemp->paso3 && !$userTemp->paso4 ){
       // falta cargar los datos
       // voy al paso 3
       wp_redirect('/suscripcion/paso4');
       exit;

   }

   if(!$userTemp->paso3){
      // no hago nada...
   }
}

// muestro el formulario de pago
// busco el plan
 $plan_id =  (int)$userTemp->plan_id;

 $userPlan = Usuario_Plan::getUsuarioPlanById($plan_id);

// Lets load the header of the site
get_header();
?>


<div id="content" class="site-content-full">
   <main id="main" class="site-main">
      <div id="post-92841" class="post-92841 page type-page status-publish hentry">
         <div class="">
            <div class="wp-block-group PagosHeader">
               <div class="wp-block-group__inner-container">
                  <h3 class="has-text-align-center Miembrosx1Col PFTittlePagos wpsm_panelTT">¡Gracias por sumarte a nuestra comunidad de co-responsables!</h3>
                  <p class="has-text-align-center"><em>Tu aporte es fundamental para que nuestro periodismo sea abierto, sume más voces y logre un mayor impacto.</em></p>
                  <div class="PagosHeaderSep"></div>
               </div>
            </div>
            <div class="wp-block-group BloquePagos">
               <div class="wp-block-group__inner-container">
                  <div class="wp-block-columns">
                       Hola, tu Membresía para "<?php echo $userPlan->nombre ?>" será de $<?php echo $userPlan->valor; ?>

                  </div>
                  <div class="wp-block-columns">
                        <?php //echo  $userTemp->init_point ?>

                        <a href="<?php echo  $userTemp->init_point; ?>">
                           <button type="submit" class="mercadopago-button" >Pagar Plan Mensual $25</button>
                        </a>

                       <?php /* include_once('pago_con_tarjeta.php');  */?>

                  </div>
                  <div style="height:20px" aria-hidden="true" class="wp-block-spacer"></div>
               </div>
            </div>

            <div class="wp-block-group BloquePagos">
               <div class="wp-block-group__inner-container">
                  <div style="height:36px" aria-hidden="true" class="wp-block-spacer"></div>
                  <p>Podés cancelar tu membresía en cualquier momento. Si necesitás ayuda para activarla o darla de baja, por favor mandanos un mail a <a href="mailto:miembros@redaccion.com.ar">miembros@redaccion.com.ar</a> o llamanos al +54 1149171763 </p>
                  <div style="height:36px" aria-hidden="true" class="wp-block-spacer"></div>
               </div>
            </div>
         </div>
      </div>
   </main>
</div>




<?php
// Load the footer
get_footer();
