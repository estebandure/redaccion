<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://christophercasper.com/
 * @since      1.0.0
 *
 * @package    Suscripcion_Plugin
 * @subpackage Suscripcion_Plugin/public/partials
 */


global $wp_query;
global $wp_session;

$wp_query->is_home = false;


// Lets load the header of the site
get_header();
?>


<div id="content" class="site-content-full">
   <main id="main" class="site-main">
      <div class=" page type-page status-publish hentry">
         <div class="">
            <div class="wp-block-group PagosHeader">
               <div class="wp-block-group__inner-container">
                  <h3 class="has-text-align-center Miembrosx1Col PFTittlePagos wpsm_panelTT">Logout!</h3>
                  <p class="has-text-align-center">Tu Membresía a sido dada de baja</p>
                  <div class="PagosHeaderSep"></div>
                  <p class="has-text-align-center">
                    <a href="/">
                        <input type="button"  value="Home">
                    </a>
                  </p>
               </div>
            </div>


         </div>
      </div>
   </main>
</div>


<?php
// Load the footer
get_footer();
