<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://christophercasper.com/
 * @since      1.0.0
 *
 * @package    Suscripcion_Plugin
 * @subpackage Suscripcion_Plugin/public/partials
 */


global $wp_query;
global $wp_session;

$wp_query->is_home = false;

if(!session_id()) {session_start(); }

// veo si existe como usuario temporal - registro en curso
$userTemp = null;

$user_temp_id = $_SESSION['suscripcion_user_temp_id'];
if(! empty($user_temp_id )  && is_numeric($user_temp_id)){
   $userTemp = Usuario_Temp::getUsuarioTempById($user_temp_id);
}

if(is_null($userTemp)){
   // voy al paso 1
   wp_redirect('/membresias');
   exit;
}


if(!is_null($userTemp)){

   $existsUserTemp = true;

   if($userTemp->paso4){
       // ya esta registrado!!

       // borro la session
       $_SESSION['suscripcion_user_temp_id'] = null;
       unset($_SESSION['suscripcion_user_temp_id']);  // ??
       // guardo la session....


       // voy al login
       wp_redirect('/membresias/login');
       exit;
   }

   if(!$userTemp->paso3){
        // veo si esta el external_reference y el preapproval_id

        if( isset($_GET['preapproval_id']) ){
            // verifico....

            include_once(plugin_dir_path(__FILE__) . '../../vendor/autoload.php');

            if( get_option( 'suscripcion_plugin_mp_sandbox_mode' ) ){
                MercadoPago\SDK::setAccessToken( get_option( 'suscripcion_plugin_mp_sandbox_access_token' ) );
            }else{
                MercadoPago\SDK::setAccessToken( get_option( 'suscripcion_plugin_mp_production_access_token' ) );
            }

            $preapproval_id = $_GET['preapproval_id'];

            $preapproval =  MercadoPago\Preapproval()->get($preapproval_id);

            if($preapproval->status == 'authorized'){

                //Pago aprobado   => OK
                // busco el plan
                $plan_id =  (int)$userTemp->plan_id;

                $userPlan = Usuario_Plan::getUsuarioPlanById($plan_id);

                $external_reference = $preapproval->external_reference;


                // guado en el log
                $json = json_encode($preapproval->toArray());

                //log de pagos
                $users_payment = [
                  'external_id' => $external_reference,
                  'user_id'     => null,
                  'utemp_id'    => $userTemp->utemp_id,
                  'plan_id'     => $userPlan->plan_id,
                  'monto'       => $userPlan->valor,
                  'status'      => 'authorized',
                  'data'        => $json,
                  'fecha'       => date("Y-m-d H:i:s"),
                ];

                $wpdb->insert( $wpdb->prefix . 'users_payments', $users_payment );


                // Marco el usuario
                $dataUserTemp = [
                  'paso3'          => 1,
                  'pago_aprobado'  => 1,
                  'preapproval_id' => $preapproval->id,
                  'fecha'          => date("Y-m-d H:i:s")
                ];

                $where = [ 'utemp_id' => $userTemp->utemp_id];
                $wpdb->update( $wpdb->prefix . 'users_temp', $dataUserTemp, $where );

                // voy a la pagina de registro -  paso 4
                wp_redirect('/membresias/crearCuenta');
                exit;
            }else{
                // otro estado...!

                // Si se aprueba el pago se avisa por IPN
                // y el usuario recibe un email con el link para seguir el registro

            }
        }

        // voy al paso 3
        wp_redirect('/membresias/pagar');
        exit;
   }

   if(!$userTemp->paso2){
       // voy al paso 2
       wp_redirect('/membresias');
       exit;
   }

   if(!$userTemp->paso1){
       // voy al paso 2
       wp_redirect('/membresias');
       exit;
   }

   if($userTemp->paso3 && $userTemp->pago_aprobado ){
       // falta cargar los datos
       // voy al paso 3
       wp_redirect('/membresias/crearCuenta');
       exit;

   }

}

// muestro el formulario de pago
// busco el plan
 $plan_id =  (int)$userTemp->plan_id;

 $userPlan = Usuario_Plan::getUsuarioPlanById($plan_id);


// Lets load the header of the site
get_header();
?>


<div id="content" class="site-content-full">
   <main id="main" class="site-main">
      <div id="post-92841" class="post-92841 page type-page status-publish hentry">
         <div class="">
            <div class="wp-block-group PagosHeader">
               <div class="wp-block-group__inner-container">
                  <h3 class="has-text-align-center Miembrosx1Col PFTittlePagos wpsm_panelTT">¡Gracias por sumarte a nuestra comunidad de co-responsables!</h3>
                  <p class="has-text-align-center"><em>Tu aporte es fundamental para que nuestro periodismo sea abierto, sume más voces y logre un mayor impacto.</em></p>
                  <div class="PagosHeaderSep"></div>
               </div>
            </div>
            <div class="wp-block-group BloquePagos">
               <div class="wp-block-group__inner-container">
                  <div class="wp-block-columns">
                       Hola, estamos procesando tu pago par tu Membresía para "<?php echo $userPlan->nombre ?>" será de $<?php echo $userPlan->valor; ?>

                  </div>
                  <div class="wp-block-columns">
                       Espera unos momentos y consulta tu casilla de email para continuar con el registro.
                  </div>
                  <div class="wp-block-columns">



                  </div>
                  <div style="height:20px" aria-hidden="true" class="wp-block-spacer"></div>
               </div>
            </div>

            <div class="wp-block-group BloquePagos">
               <div class="wp-block-group__inner-container">
                  <div style="height:36px" aria-hidden="true" class="wp-block-spacer"></div>
                  <p>Podés cancelar tu membresía en cualquier momento. Si necesitás ayuda para activarla o darla de baja, por favor mandanos un mail a <a href="mailto:miembros@redaccion.com.ar">miembros@redaccion.com.ar</a> o llamanos al +54 1149171763 </p>
                  <div style="height:36px" aria-hidden="true" class="wp-block-spacer"></div>
               </div>
            </div>
         </div>
      </div>
   </main>
</div>




<?php
// Load the footer
get_footer();
