<?php

global $wp_query;
global $wp_session;
global $wpdb;
       
$wp_query->is_home = false;

if(!session_id()) {session_start(); }
        
// veo se hay un usuario logueado  
if(is_user_logged_in()){
    // lo saco al login
    wp_redirect('/membresias/usuario');
    exit();
}

     
if(! empty( $_POST['log'])){
  // proceso
  $log = $_POST['log'];
  $pwd = $_POST['pwd'];

  // logueo al usuario       
  $user = wp_signon();
 
  if ( ! is_wp_error($user) ){
        //OK
        wp_set_current_user($user->ID);
        wp_redirect('/miembros-corresponsables/');
       //wp_redirect('/membresias/usuario');
        exit();   
  }
  
  $error = $user->get_error_message();      
  
    
}


// Lets load the header of the site
get_header();
?>


<div id="content" class="site-content-full">
   <main id="main" class="site-main">
      <div id="post-92841" class="post-92841 page type-page status-publish hentry">
         <div class="">
            <div class="wp-block-group PagosHeader">
               <div class="wp-block-group__inner-container">
                  <h3 class="has-text-align-center Miembrosx1Col PFTittlePagos wpsm_panelTT">Login</h3>
                  <p class="has-text-align-center">Ingresá tu usuario y contraseña.</p>
                  <div class="PagosHeaderSep"></div>
                  <p class="has-text-align-center">
       
         <?php
         
         if(!empty($error)){
         ?>    
            <span><?php echo $error;?></span>
         <?php   
         }
         ?>         

         <div class="Login Miembros">
            <div class="mp_wrapper mp_login_form">
               <form name="mepr_loginform" id="mepr_loginform" class="mepr-form" action="" method="post">
                  <div class="mp-form-row mepr_username">
                     <div class="mp-form-label">
                        <label for="log">Nombre de Usuario</label>
                     </div>
                     <input type="text" name="log" id="user_login" value="" placeholder="Mail o nombre de usuario" required>
                  </div>
                  <div class="mp-form-row mepr_password">
                     <div class="mp-form-label">
                        <label for="pwd">Contraseña</label>
                     </div>
                     <input type="password" name="pwd" id="user_pass" value="" placeholder="Contraseña" required>
                  </div>

                  <div class="mp-spacer">&nbsp;</div>
                  <div class="submit">
                     <input type="submit" name="wp-submit" id="wp-submit" class="button-primary mepr-share-button " value="Ingresar">
                  </div>
               </form>
           
                 <script type="text/javascript">
                    jQuery("#mepr_loginform").validate();
                 </script>               
               
               <div class="mp-spacer">&nbsp;</div>

               <div class="olvidePass"><a href="https://www.redaccion.com.ar/wp-login.php?action=lostpassword" tilte="Recuperar contraseña">Olvidé mi contraseña</a></div>

            </div>
         </div>

                  
                  
                  </p>
               </div>
            </div>
         </div>
      </div>
   </main>
</div>




<?php
// Load the footer
get_footer();

