<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://www.mywebsite.com
 * @since      1.0.0
 *
 * @package    Suscripcion
 * @subpackage Suscripcion/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Suscripcion
 * @subpackage Suscripcion/public
 * @author     Esteban Dure <estebandure@gmail.com>
 */
class Suscripcion_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}
    

    /**
     * Custom Plugin Rewrites
     *
     * @since    1.0.0
     */
    public function register_suscripcion_plugin_redirect()
    {         
        
        if (get_query_var('suscripcion_plugin')&& get_query_var('suscripcion_plugin') == 'index') {
            add_filter('template_include', function () {
                
                wp_enqueue_script( 'jquery-validation', plugin_dir_url( __FILE__ ) . 'js/jquery-validation/jquery.validate.min.js'            , array( 'jquery' ), $this->version, false );
                wp_enqueue_script( 'jquery-validation-localization', plugin_dir_url( __FILE__ ) . 'js/jquery-validation/localization/messages_es_AR.min.js', null            , $this->version, false );
                
                
                return plugin_dir_path(__FILE__) . 'partials/suscripcion-paso1.php';
            });
        }
        
        if (get_query_var('suscripcion_plugin') && get_query_var('suscripcion_plugin') == 'paso2') {
            add_filter('template_include', function () {
                return plugin_dir_path(__FILE__) . 'partials/suscripcion-paso2.php';
            });
        }
        if (get_query_var('suscripcion_plugin') && get_query_var('suscripcion_plugin') == 'paso3') {
            add_filter('template_include', function () {
                
                wp_enqueue_script( $this->plugin_name, "https://secure.mlstatic.com/sdk/javascript/v1/mercadopago.js" , false );
                
                return plugin_dir_path(__FILE__) . 'partials/suscripcion-paso3.php';
            });
        }
        if (get_query_var('suscripcion_plugin') && get_query_var('suscripcion_plugin') == 'paso4') {
            add_filter('template_include', function () {
                
                wp_enqueue_script( 'jquery-validation', plugin_dir_url( __FILE__ ) . 'js/jquery-validation/jquery.validate.min.js'            , array( 'jquery' ), $this->version, false );
                wp_enqueue_script( 'jquery-validation-localization', plugin_dir_url( __FILE__ ) . 'js/jquery-validation/localization/messages_es_AR.min.js', null            , $this->version, false );
                wp_enqueue_script( 'jquery-validation-additional-methods', plugin_dir_url( __FILE__ ) . 'js/jquery-validation/additional-methods.min.js'            , array( 'jquery' ), $this->version, false );
                
                wp_enqueue_style('', "/wp-content/plugins/wpforms/assets/css/wpforms-full.css"  );  //Todo: sacar!!!
                return plugin_dir_path(__FILE__) . 'partials/suscripcion-paso4.php';
            });
        }
        if (get_query_var('suscripcion_plugin') && get_query_var('suscripcion_plugin') == 'procesandoPago') {
            add_filter('template_include', function () {
                return plugin_dir_path(__FILE__) . 'partials/suscripcion-procesando.php';
            });
        }
        if (get_query_var('suscripcion_plugin') && get_query_var('suscripcion_plugin') == 'pago-aprobado') {
            add_filter('template_include', function () {
                return plugin_dir_path(__FILE__) . 'partials/suscripcion/pago-aprobado.php';
            });
        }
        if (get_query_var('suscripcion_plugin') && get_query_var('suscripcion_plugin') == 'bienvenido') {
            add_filter('template_include', function () {
                return plugin_dir_path(__FILE__) . 'partials/suscripcion-bienvenido.php';
            });
        }
        
        if (get_query_var('suscripcion_plugin') && get_query_var('suscripcion_plugin') == 'mensual') {
            add_filter('template_include', function () {
                return plugin_dir_path(__FILE__) . 'partials/suscripcion-mensual.php';
            });
        }
        if (get_query_var('suscripcion_plugin') && get_query_var('suscripcion_plugin') == 'anual') {
            add_filter('template_include', function () {
                return plugin_dir_path(__FILE__) . 'partials/suscripcion-anual.php';
            });
        }
        
        if (get_query_var('suscripcion_plugin') && get_query_var('suscripcion_plugin') == 'ipn') {
            add_filter('template_include', function () {
                return plugin_dir_path(__FILE__) . 'partials/mercadopago-ipn.php';
            });
        }
        
        if (get_query_var('suscripcion_plugin') && get_query_var('suscripcion_plugin') == 'login') {
            add_filter('template_include', function () {
                wp_enqueue_script( 'jquery-validation', plugin_dir_url( __FILE__ ) . 'js/jquery-validation/jquery.validate.min.js'            , array( 'jquery' ), $this->version, false );
                wp_enqueue_script( 'jquery-validation-localization', plugin_dir_url( __FILE__ ) . 'js/jquery-validation/localization/messages_es_AR.min.js', null            , $this->version, false );
                
                return plugin_dir_path(__FILE__) . 'partials/suscripcion-login.php';
            });
        }
        
        // Pagina index del usuario logueado
        if (get_query_var('suscripcion_plugin') && get_query_var('suscripcion_plugin') == 'usuario') {
            add_filter('template_include', function () {
                
                return plugin_dir_path(__FILE__) . 'partials/usuarioLogued/suscripcion-index.php';
            });
        }        
        
        if (get_query_var('suscripcion_plugin') && get_query_var('suscripcion_plugin') == 'logout') {
            add_filter('template_include', function () {
                
                return plugin_dir_path(__FILE__) . 'partials/suscripcion-logout.php';
            });
        }        
        
        if (get_query_var('suscripcion_plugin') && get_query_var('suscripcion_plugin') == 'usuariologout') {
            add_filter('template_include', function () {
                
                return plugin_dir_path(__FILE__) . 'partials/usuarioLogued/suscripcion-logout.php';
            });
        }        
        
        if (get_query_var('suscripcion_plugin') && get_query_var('suscripcion_plugin') == 'usuariodatos') {
            add_filter('template_include', function () {
                
                
                wp_enqueue_script( 'jquery-validation', plugin_dir_url( __FILE__ ) . 'js/jquery-validation/jquery.validate.min.js'            , array( 'jquery' ), $this->version, false );
                wp_enqueue_script( 'jquery-validation-localization', plugin_dir_url( __FILE__ ) . 'js/jquery-validation/localization/messages_es_AR.min.js', null            , $this->version, false );
                wp_enqueue_script( 'jquery-validation-additional-methods', plugin_dir_url( __FILE__ ) . 'js/jquery-validation/additional-methods.min.js'            , array( 'jquery' ), $this->version, false );
                
                wp_enqueue_style('', "/wp-content/plugins/wpforms/assets/css/wpforms-full.css"  );  //Todo: sacar!!!
                  
                
                return plugin_dir_path(__FILE__) . 'partials/usuarioLogued/suscripcion-datos.php';
            });
        } 
         
        
        if (get_query_var('suscripcion_plugin') && get_query_var('suscripcion_plugin') == 'usuarioplan') {
            add_filter('template_include', function () {
                
                return plugin_dir_path(__FILE__) . 'partials/usuarioLogued/suscripcion-plan.php';
            });
        } 
        
    }

    /**
     * Register Query Values for Custom Plugin
     *
     * Filters that are needed for rendering the custom plugin page
     *
     * @since    1.0.0
     */
    public function register_query_values($vars)
    {
        // Equivalent to array_push($vars, 'suscripcion_plugin', ..)
        $vars[] = 'suscripcion_plugin';
        
        
        $vars[] = 'suscripcion_plugin_paged';     //
        $vars[] = 'suscripcion_plugin_vehicle';   //

        return $vars;
    }    

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Suscripcion_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Suscripcion_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/suscripcion-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Suscripcion_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Suscripcion_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/suscripcion-public.js', array( 'jquery' ), $this->version, false );
        

	}

}
