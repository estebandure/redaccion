<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.mywebsite.com
 * @since      1.0.0
 *
 * @package    Suscripcion
 * @subpackage Suscripcion/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Suscripcion
 * @subpackage Suscripcion/includes
 * @author     Esteban Dure <estebandure@gmail.com>
 */
class Suscripcion_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
