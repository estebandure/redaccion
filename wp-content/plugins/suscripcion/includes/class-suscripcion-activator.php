<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.mywebsite.com
 * @since      1.0.0
 *
 * @package    Suscripcion
 * @subpackage Suscripcion/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Suscripcion
 * @subpackage Suscripcion/includes
 * @author     Esteban Dure <estebandure@gmail.com>
 */
class Suscripcion_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
