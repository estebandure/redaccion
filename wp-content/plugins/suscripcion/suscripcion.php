<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.mywebsite.com
 * @since             1.0.0
 * @package           Suscripcion
 *
 * @wordpress-plugin
 * Plugin Name:       Suscripciones
 * Plugin URI:        http://www.mywebsite.com/membresias
 * Description:       Sistema de suscripcion de usuarios y Pagos por MercadoPago
 * Version:           1.1.0
 * Author:            Esteban Dure
 * Author URI:        http://www.mywebsite.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       suscripcion
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'SUSCRIPCION_VERSION', '1.1.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-suscripcion-activator.php
 */
function activate_suscripcion() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-suscripcion-activator.php';
	Suscripcion_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-suscripcion-deactivator.php
 */
function deactivate_suscripcion() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-suscripcion-deactivator.php';
	Suscripcion_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_suscripcion' );
register_deactivation_hook( __FILE__, 'deactivate_suscripcion' );

register_activation_hook( __FILE__, 'suscripcion_create_db' );

function suscripcion_create_db() {
    global $wpdb;

    $version = get_option( 'suscripcion_version', '1.1' );


    $charset_collate = $wpdb->get_charset_collate();

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );


    // Tabla de usarios Temporales - antes de la suscripcion
    $table_name = $wpdb->prefix . 'users_temp';
    $sql = "CREATE TABLE $table_name (
        `utemp_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
        `user_id` int(11) DEFAULT NULL,
        `user_name` varchar(255)  NULL,
        `email` varchar(100) DEFAULT NULL,
        `paso1` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
        `paso2` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
        `paso3` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
        `paso4` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
        `pago_pendiente` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
        `pago_aprobado`  tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
        `plan_id` int(11) UNSIGNED DEFAULT NULL,
        `preapproval_id` varchar(255)  NULL,
        `password` varchar(255)  NULL,
        `eliminado` tinyint(3) UNSIGNED DEFAULT 0,
        `fecha` datetime NULL DEFAULT NULL,
        PRIMARY KEY  (utemp_id)
    ) $charset_collate;";
    dbDelta( $sql );

    // Tabla de Informacion de los usarios suscriptos
    $table_name = $wpdb->prefix . 'users_information';
    $sql = "CREATE TABLE $table_name (
          `uinfo_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
          `utemp_id` int(11) UNSIGNED NOT NULL,
          `user_id`  int(11) UNSIGNED NOT NULL,
          `user_name` varchar(255)  NULL,
          `plan_id` int(11) UNSIGNED NOT NULL,
          `plan_suscripto` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
          `plan_suscripcion_date` date DEFAULT NULL,
          `plan_unsuscription_date` date DEFAULT NULL,
          `plan_suscripcion_end` date DEFAULT NULL,
          `plan_preapproval_id` varchar(255) DEFAULT NULL,
          `plan_nombre` varchar(255) NOT NULL,
          `plan_tipo` enum('mensual','anual') NOT NULL DEFAULT 'mensual',
          `plan_valor` decimal(20,2) NOT NULL DEFAULT 0.00,
          `plan_mp_id` varchar(255) NOT NULL,
          `sucripcion_id`  varchar(255) NOT NULL,
          `nombre` varchar(255) DEFAULT NULL,
          `apellido` varchar(255) NOT NULL,
          `email` varchar(255) NOT NULL,       /* ToDo: Ver si hay que sacarlo..!? */
          `fecha_nac` date DEFAULT NULL,
          `documento_tipo` enum('Pasaporte', 'DNI') NOT NULL DEFAULT 'DNI',
          `numero_documento` varchar(50) DEFAULT NULL,
          `telefono` varchar(255) NOT NULL,
          `direccion1` varchar(255) NOT NULL,
          `direccion2` varchar(255) NOT NULL,
          `ciudad` varchar(255) NOT NULL,
          `estado_region` varchar(255) NOT NULL,
          `codigo_postal` varchar(20) NOT NULL,
          `fuera_de_argentina` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
          `profesion` varchar(255) NOT NULL,
          `contacto_por` varchar(255),
          `twitter`       tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
          `facebook`      tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
          `instagram`     tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
          `newsletters`   tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
          `coResponsable` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
          `unEvento`      tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
          `opcionOtro`    tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
          `contacto_otro` varchar(255),
          `eliminado` tinyint(3) UNSIGNED DEFAULT 0,
          `fecha` datetime NOT NULL,
          `pais` varchar(255)  NULL,
          `dato_curioso` text  NULL,
           PRIMARY KEY  (uinfo_id)
    ) $charset_collate;";

    dbDelta( $sql );

    // Tabla de pagos de los usarios suscriptos
    $table_name = $wpdb->prefix . 'users_payments';
    $sql = "CREATE TABLE $table_name (
        `upay_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
        `user_id` int(11) UNSIGNED ,
        `utemp_id` int(11) UNSIGNED,
        `external_id` varchar(255) DEFAULT NULL,
        `plan_id` int(11) UNSIGNED NOT NULL,
        `status` varchar(255) NOT NULL,
        `monto` decimal(20,2) DEFAULT NULL,
        `data` text DEFAULT NULL,
        `fecha` datetime NOT NULL,
        PRIMARY KEY  (upay_id)
    ) $charset_collate;";
    dbDelta( $sql );

    // Tabla de planes disponibles
    $table_name = $wpdb->prefix . 'users_plans';
    $sql = "CREATE TABLE $table_name (
          `plan_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
          `plan_mp_id` varchar(255) NOT NULL,
          `nombre` varchar(255) NOT NULL,
          `tipo` enum('mensual','anual') NOT NULL DEFAULT 'mensual',
          `valor` decimal(20,2) NOT NULL DEFAULT 0.00,
          `activo` tinyint(3) UNSIGNED DEFAULT 0,
          `data` text NOT NULL,
          `fecha` datetime NOT NULL,
           PRIMARY KEY  (plan_id)
    ) $charset_collate;";
    dbDelta( $sql );

    // Tabla de los Logs de MercadoPago - para debug
    $table_name = $wpdb->prefix . 'users_mp_logs';
    $sql = "CREATE TABLE $table_name (
          `log_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
          `external_id` varchar(255) DEFAULT NULL,
          `user_id` int(11) UNSIGNED DEFAULT NULL,
          `result` varchar(255) NOT NULL,
          `data` text NOT NULL,
          `fecha` datetime NOT NULL,
          PRIMARY KEY  (log_id)
    ) $charset_collate;";
    dbDelta( $sql );

}

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-suscripcion.php';

function suscripcion_url(){
    if(isset($_SERVER['HTTPS'])){
        $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
    }
    else{
        $protocol = 'http';
    }
    return $protocol . "://" . $_SERVER['HTTP_HOST'];
}


/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_suscripcion() {

	$plugin = new Suscripcion();
	$plugin->run();

}
run_suscripcion();
