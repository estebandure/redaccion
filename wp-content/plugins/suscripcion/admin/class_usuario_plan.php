<?php

class Usuario_Plan{

    public static function getUsuarioPlanById($plan_id) {

        global $wpdb;

        $userPlan = null;
        $sql = sprintf( 'SELECT * FROM %s WHERE %s = %s limit 1', $wpdb->prefix . 'users_plans', 'plan_id', $plan_id);

        $userPlanList = $wpdb->get_results( $sql );
        if(!is_null($userPlanList) && is_array($userPlanList)  && count($userPlanList)){
            foreach($userPlanList as $row){  $userPlan = $row; break; }
        }

        return $userPlan;
    }
    
    public static function getUsuarioPlanMesuales() {

        global $wpdb;

        $userPlan = null;
        $sql = sprintf( 'SELECT * 
                         FROM %s 
                         WHERE tipo = "mensual" 
                         AND   activo = 1 
                         ORDER BY valor asc', $wpdb->prefix . 'users_plans');

        $userPlanList = $wpdb->get_results( $sql );

        return $userPlanList;
    }
    
    public static function getUsuarioPlanAnuales() {

        global $wpdb;

        $userPlan = null;
        $sql = sprintf( 'SELECT * 
                         FROM %s 
                         WHERE tipo = "anual" 
                         AND activo = 1 
                         ORDER BY valor asc', $wpdb->prefix . 'users_plans');

        $userPlanList = $wpdb->get_results( $sql );

        return $userPlanList;
    }
    
    public static function getUsuarioPlanMensualByValor($valor){
    
        global $wpdb;

        $userPlan = null;
        $sql = sprintf( 'SELECT * 
                         FROM %s 
                         WHERE tipo = "mensual" 
                         AND   valor = %d 
                         AND   legacy  = 0
                         limit 1 ', $wpdb->prefix . 'users_plans', $valor);

        $userPlanList = $wpdb->get_results( $sql );
        if(!is_null($userPlanList) && is_array($userPlanList)  && count($userPlanList)){
            foreach($userPlanList as $row){  $userPlan = $row; break; }
        }

        return $userPlan;
    }    

        public static function getUsuarioPlanAnualByValor($valor){
    
        global $wpdb;

        $userPlan = null;
        $sql = sprintf( 'SELECT * 
                         FROM %s 
                         WHERE tipo = "anual" 
                         AND   valor = %d 
                         AND   legacy  = 0
                         limit 1 ', $wpdb->prefix . 'users_plans', $valor);

        $userPlanList = $wpdb->get_results( $sql );
        if(!is_null($userPlanList) && is_array($userPlanList)  && count($userPlanList)){
            foreach($userPlanList as $row){  $userPlan = $row; break; }
        }

        return $userPlan;
    }  
      
}

