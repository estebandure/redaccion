<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.mywebsite.com
 * @since      1.0.0
 *
 * @package    Suscripcion
 * @subpackage Suscripcion/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Suscripcion
 * @subpackage Suscripcion/admin
 * @author     Esteban Dure <estebandure@gmail.com>
 */
class Suscripcion_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		// Lets add an action to setup the admin menu in the left nav
		add_action( 'admin_menu', array($this, 'add_admin_menu') );
		// Add some actions to setup the settings we want on the wp admin page
		add_action('admin_init', array($this, 'setup_sections'));
		add_action('admin_init', array($this, 'setup_fields'));

	}

	public function add_admin_menu() {

		// Main Menu Item
	  	add_menu_page(
			'Suscripcion de Usuarios',
			'Suscripcion',
			'manage_options',
			'suscripcion-plugin',
            '',
			'dashicons-admin-users', 65);

        add_submenu_page(
            'suscripcion-plugin',
            'Usuarios Suscriptos',
            'Usuarios Suscriptos',
            'manage_options',
            'suscripcion-plugin',
            array($this, 'display_suscripcion_plugin_admin_users_page')
        );

        add_submenu_page(
            'suscripcion-plugin',
            'Pagos de los usuarios',
            'Pagos de los usuarios',
            'manage_options',
            'suscripcion-plugin/payments',
            array($this, 'display_suscripcion_plugin_admin_payments_page')
        );

        add_submenu_page(
            'suscripcion-plugin',
            'Configuración',
            'Configuración',
            'manage_options',
            'suscripcion-plugin/config',
            array($this, 'display_suscripcion_plugin_admin_config_page')
        );
	}

    /**
     * Callback function for displaying the admin settings page.
     *
     * @since    1.0.0
     */
    public function display_suscripcion_plugin_admin_info_page(){
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/suscripcion-plugin-admin-info-page.php';
    }

    /**
     * Callback function for displaying the admin settings page.
     *
     * @since    1.0.0
     */
    public function display_suscripcion_plugin_admin_users_page(){
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/suscripcion-plugin-admin-users-page.php';
    }

    /**
     * Callback function for displaying the admin settings page.
     *
     * @since    1.0.0
     */
    public function display_suscripcion_plugin_admin_payments_page(){
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/suscripcion-plugin-admin-payments-page.php';
    }

	/**
	 * Callback function for displaying the admin settings page.
	 *
	 * @since    1.0.0
	 */
	public function display_suscripcion_plugin_admin_config_page(){
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/suscripcion-plugin-admin-config-page.php';
	}


//	/**
//	 * Callback function for displaying the second sub menu item page.
//	 *
//	 * @since    1.0.0
//	 */
//	public function display_suscripcion_plugin_admin_page_two(){
//		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/suscripcion-plugin-admin-two-display.php';
//	}

	/**
	 * Setup sections in the settings
	 *
	 * @since    1.0.0
	 */
	public function setup_sections() {
		add_settings_section( 'section_one'  , 'MercadoPago Sandbox'   , array($this, 'section_callback'), 'suscripcion-plugin-options' );
        add_settings_section( 'section_two'  , 'MercadoPago Producción', array($this, 'section_callback'), 'suscripcion-plugin-options' );
        add_settings_section( 'section_three', 'Mailchip'              , array($this, 'section_callback'), 'suscripcion-plugin-options' );
        add_settings_section( 'section_four' , 'Google reCaptcha'      , array($this, 'section_callback'), 'suscripcion-plugin-options' );
		add_settings_section( 'section_five' , 'Email de Alerta de un Nuevo Miembro'  , array($this, 'section_callback'), 'suscripcion-plugin-options' );
	}

	/**
	 * Callback for each section
	 *
	 * @since    1.0.0
	 */
	public function section_callback( $arguments ) {
		switch( $arguments['id'] ){
			case 'section_one':
				echo '<p>Configuración de MecadoPago en modo Sandbox para pruebas</p>';
				break;
            case 'section_two':
                echo '<p>Configuración de MecadoPago en modo Producción para el sitio en vivo</p>';
                break;
            case 'section_three':
                echo '<p>Configuración de la API de Mailchip.</p>';
                break;
            case 'section_four':
                echo '<p>Configuración de Google reCaptcha.</p>';
                break;
			case 'section_five':
				echo '<p>Emails a los que le llegara el alerta. Email separados por comas. </p>';
				break;
		}
	}

	/**
	 * Field Configuration, each item in this array is one field/setting we want to capture
	 *
	 * @since    1.0.0
	 */
	public function setup_fields() {
		$fields = array(
            array(
                'uid'         => 'suscripcion_plugin_mp_sandbox_mode',
                'label'       => 'MercadoPago Modo Sandbox',
                'section'     => 'section_one',
                'type'        => 'checkbox',
                'placeholder' => '',
                'default'     => ['0'],
                'options'     => [ '1' => 'Modo Sandbox']
            ),
            array(
                'uid' => 'suscripcion_plugin_mp_sandbox_public_Key',
                'label' => 'Sandbox Public Key',
                'section' => 'section_one',
                'type' => 'text',
                'placeholder' => '',
                'default' => "",
            ),
            array(
                'uid' => 'suscripcion_plugin_mp_sandbox_access_token',
                'label' => 'Sandbox Access Token',
                'section' => 'section_one',
                'type' => 'text',
                'placeholder' => '',
                'default' => "",
            ),

			array(
				'uid' => 'suscripcion_plugin_mp_production_public_Key',
				'label' => 'Producción Public Key',
				'section' => 'section_two',
				'type' => 'text',
				'placeholder' => '',
				'default' => "",
			),
            array(
                'uid' => 'suscripcion_plugin_mp_production_access_token',
                'label' => 'Producción Access Token',
                'section' => 'section_two',
                'type' => 'text',
                'placeholder' => '',
                'default' => "",
            ),

            array(
                'uid' => 'suscripcion_plugin_mailchip_public_Key',
                'label' => 'API Key',
                'section' => 'section_three',
                'type' => 'text',
                'placeholder' => '',
                'default' => "",
            ),

            array(
                'uid' => 'suscripcion_plugin_recaptcha_clave_sitio',
                'label' => 'reCaptcha Clave del Sitio',
                'section' => 'section_four',
                'type' => 'text',
                'placeholder' => '',
                'default' => "",
            ),

            array(
                'uid' => 'suscripcion_plugin_recaptcha_clave_secreta',
                'label' => 'reCaptcha Clave Secreta',
                'section' => 'section_four',
                'type' => 'text',
                'placeholder' => '',
                'default' => "",
            ),

            array(
                'uid' => 'suscripcion_plugin_emails_alerta_nuevo_miembro',
                'label' => 'Emails',
                'section' => 'section_five',
                'type' => 'text',
                'placeholder' => '',
                'default' => "",
            ),

		);
		// Lets go through each field in the array and set it up
		foreach( $fields as $field ){
			add_settings_field( $field['uid'], $field['label'], array($this, 'field_callback'), 'suscripcion-plugin-options', $field['section'], $field );
			register_setting( 'suscripcion-plugin-options', $field['uid'] );
		}
	}

	/**
	 * This handles all types of fields for the settings
	 *
	 * @since    1.0.0
	 */
	public function field_callback($arguments) {
		// Set our $value to that of whats in the DB
		$value = get_option( $arguments['uid'] );
		// Only set it to default if we get no value from the DB and a default for the field has been set
		if(!$value) {
			$value = $arguments['default'];
		}
		// Lets do some setup based ont he type of element we are trying to display.
		switch( $arguments['type'] ){
			case 'text':
			case 'password':
			case 'number':
				printf( '<input name="%1$s" id="%1$s" type="%2$s" placeholder="%3$s" value="%4$s" style="width: 65%%" />', $arguments['uid'], $arguments['type'], $arguments['placeholder'], $value );
				break;
			case 'textarea':
				printf( '<textarea name="%1$s" id="%1$s" placeholder="%2$s" rows="5" cols="50">%3$s</textarea>', $arguments['uid'], $arguments['placeholder'], $value );
				break;
			case 'select':
			case 'multiselect':
				if( ! empty ( $arguments['options'] ) && is_array( $arguments['options'] ) ){
					$attributes = '';
					$options_markup = '';
					foreach( $arguments['options'] as $key => $label ){
						$options_markup .= sprintf( '<option value="%s" %s>%s</option>', $key, selected( $value[ array_search( $key, $value, true ) ], $key, false ), $label );
					}
					if( $arguments['type'] === 'multiselect' ){
						$attributes = ' multiple="multiple" ';
					}
					printf( '<select name="%1$s[]" id="%1$s" %2$s>%3$s</select>', $arguments['uid'], $attributes, $options_markup );
				}
				break;
			case 'radio':
			case 'checkbox':
				if( ! empty ( $arguments['options'] ) && is_array( $arguments['options'] ) ){

					$options_markup = '';
					$iterator = 0;
					foreach( $arguments['options'] as $key => $label ){
						$iterator++;
						$is_checked = '';
						// This case handles if there is only one checkbox and we don't have anything saved yet.
						if(isset($value[ array_search( $key, $value, true ) ])) {
							$is_checked = checked( $value[ array_search( $key, $value, true ) ], $key, false );
						} else {
							$is_checked = "";
						}
						// Lets build out the checkbox
						$options_markup .= sprintf( '<label for="%1$s_%6$s"><input id="%1$s_%6$s" name="%1$s[]" type="%2$s" value="%3$s" %4$s /> %5$s</label><br/>', $arguments['uid'], $arguments['type'], $key, $is_checked, $label, $iterator );
					}
					printf( '<fieldset>%s</fieldset>', $options_markup );
				}
				break;
			case 'image':
				// Some code borrowed from: https://mycyberuniverse.com/integration-wordpress-media-uploader-plugin-options-page.html
				$options_markup = '';
				$image = [];
				$image['id'] = '';
				$image['src'] = '';

				// Setting the width and height of the header iamge here
				$width = '1800';
				$height = '1068';

				// Lets get the image src
				$image_attributes = wp_get_attachment_image_src( $value, array( $width, $height ) );
				// Lets check if we have a valid image
				if ( !empty( $image_attributes ) ) {
					// We have a valid option saved
					$image['id'] = $value;
					$image['src'] = $image_attributes[0];
				} else {
					// Default
					$image['id'] = '';
					$image['src'] = $value;
				}

				// Lets build our html for the image upload option
				$options_markup .= '
				<img data-src="' . $image['src'] . '" src="' . $image['src'] . '" width="180px" height="107px" />
				<div>
					<input type="hidden" name="' . $arguments['uid'] . '" id="' . $arguments['uid'] . '" value="' . $image['id'] . '" />
					<button type="submit" class="upload_image_button button">Upload</button>
					<button type="submit" class="remove_image_button button">&times; Delete</button>
				</div>';
				printf('<div class="upload">%s</div>',$options_markup);
				break;
		}
		// If there is helper text, lets show it.
		if( array_key_exists('helper',$arguments) && $helper = $arguments['helper']) {
			printf( '<span class="helper"> %s</span>', $helper );
		}
		// If there is supplemental text lets show it.
		if( array_key_exists('supplemental',$arguments) && $supplemental = $arguments['supplemental'] ){
			printf( '<p class="description">%s</p>', $supplemental );
		}
	}

	/**
	 * Admin Notice
	 *
	 * This displays the notice in the admin page for the user
	 *
	 * @since    1.0.0
	 */
	public function admin_notice($message) { ?>
		<div class="notice notice-success is-dismissible">
			<p><?php echo($message); ?></p>
		</div><?php
	}

	/**
	 * This handles setting up the rewrite rules for Past Sales
	 *
	 * @since    1.0.0
	 */
	public function setup_rewrites() {
		//
		$url_slug = 'membresias';
		// Lets setup our rewrite rules
        add_rewrite_rule( $url_slug . '/?$'        , 'index.php?suscripcion_plugin=index'  , 'top' );
        add_rewrite_rule( $url_slug . '/paso1/?$'  , 'index.php?suscripcion_plugin=paso1'  , 'top' );
        add_rewrite_rule( $url_slug . '/paso2/?$'  , 'index.php?suscripcion_plugin=paso2'  , 'top' );
        // PAgar
        add_rewrite_rule( $url_slug . '/paso3/?$'  , 'index.php?suscripcion_plugin=paso3'  , 'top' );
        add_rewrite_rule( $url_slug . '/pagar/?$'  , 'index.php?suscripcion_plugin=paso3'  , 'top' );

        //Crear Cuenta
        add_rewrite_rule( $url_slug . '/paso4/?$'  , 'index.php?suscripcion_plugin=paso4'  , 'top' );
        add_rewrite_rule( $url_slug . '/crearCuenta/?$'  , 'index.php?suscripcion_plugin=paso4'  , 'top' );

        add_rewrite_rule( $url_slug . '/paso1_process/?$'  , 'index.php?suscripcion_plugin=paso1_process'  , 'top' );
        add_rewrite_rule( $url_slug . '/procesandoPago/?$'  , 'index.php?suscripcion_plugin=procesandoPago'  , 'top' );

        add_rewrite_rule( $url_slug . '/pago-aprobado/(.+)/?$'  , 'index.php?suscripcion_plugin=pago-aprobado&hash=$matches[1]'  , 'top' );
        add_rewrite_rule( $url_slug . '/bienvenido/*?$'  , 'index.php?suscripcion_plugin=bienvenido'  , 'top' );

        add_rewrite_rule( $url_slug . '/mensual/*?$'  , 'index.php?suscripcion_plugin=mensual'  , 'top' );
        add_rewrite_rule( $url_slug . '/anual/*?$'    , 'index.php?suscripcion_plugin=anual'  , 'top' );

        //Pago con la tarjeta
        add_rewrite_rule( $url_slug . '/chekout/?$', 'index.php?suscripcion_plugin=chekout', 'top' );

        // Registro luego de pagar
        add_rewrite_rule( $url_slug . '/registro/?$', 'index.php?suscripcion_plugin=chekout', 'top' );

        //recepcion del pago de MercadoPago
        add_rewrite_rule( $url_slug . '/mp/ipn/?$'    , 'index.php?suscripcion_plugin=ipn'    , 'top' );
        // sacar..!
        add_rewrite_rule( 'suscripcion/mp/ipn/?$'    , 'index.php?suscripcion_plugin=ipn'    , 'top' );

        //login del usuario
        add_rewrite_rule( $url_slug . '/login/?$'    , 'index.php?suscripcion_plugin=login'    , 'top' );
        //logout de la suscripcion
        add_rewrite_rule( $url_slug . '/logout/?$'    , 'index.php?suscripcion_plugin=logout'    , 'top' );

        add_rewrite_rule( $url_slug . '/usuario/?$'    , 'index.php?suscripcion_plugin=usuario'    , 'top' );

        //Area del usuario en el Front
        add_rewrite_rule( $url_slug . '/usuario/logout?$' , 'index.php?suscripcion_plugin=usuariologout' , 'top' );
        add_rewrite_rule( $url_slug . '/usuario/datos/?$' , 'index.php?suscripcion_plugin=usuariodatos' , 'top' );
        add_rewrite_rule( $url_slug . '/usuario/plan/?$'  , 'index.php?suscripcion_plugin=usuarioplan'  , 'top' );
	//	add_rewrite_rule( $url_slug . '/usuario/pagos/?$' , 'index.php?suscripcion_plugin=usuario&suscripcion_option=pagos'   , 'top' );

		// Lets flush rewrite rules on activation
		flush_rewrite_rules();
	}


	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Suscripcion_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Suscripcion_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/suscripcion-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Suscripcion_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Suscripcion_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/suscripcion-admin.js', array( 'jquery' ), $this->version, false );

	}

}
