<?php

class Usuario_Information{

    public static function getUsuarioInfoByUsuerId($user_id) {

        global $wpdb;

        $userPlan = null;
        $sql = sprintf( 'SELECT * FROM %s WHERE %s = %s limit 1', $wpdb->prefix . 'users_information', 'user_id', $user_id);

        $userInformationList = $wpdb->get_results( $sql );
        if(!is_null($userInformationList) && is_array($userInformationList)  && count($userInformationList)){
            foreach($userInformationList as $row){  $userInformation = $row; break; }
        }

        return $userInformation;
    }
    
    public static function getUsuarioPlanByUsuerTempId($utemp_id) {

        global $wpdb;

        $userPlan = null;
        $sql = sprintf( 'SELECT * FROM %s WHERE %s = %s limit 1', $wpdb->prefix . 'users_information', 'utemp_id', $utemp_id);

        $userInformationList = $wpdb->get_results( $sql );
        if(!is_null($userInformationList) && is_array($userInformationList)  && count($userInformationList)){
            foreach($userInformationList as $row){  $userInformation = $row; break; }
        }

        return $userInformation;
    }
    

      
}

