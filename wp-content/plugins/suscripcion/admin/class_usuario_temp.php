<?php

  
class Usuario_Temp{

    public static function getUsuarioTempById($utemp_id) {

        global $wpdb;

        $userTemp = null;
        $sql = sprintf( 'SELECT * FROM %s WHERE %s = "%s" limit 1', $wpdb->prefix . 'users_temp', 'utemp_id', $utemp_id);

        $userTempList = $wpdb->get_results( $sql );
        if(!is_null($userTempList) && is_array($userTempList)  && count($userTempList)){
            foreach($userTempList as $row){  $userTemp = $row; break; }
        }

        return $userTemp;
    }
    

    public static function getUsuarioTempByEmail($email) {

        global $wpdb;

        $userTemp = null;
        $sql = sprintf( 'SELECT * FROM %s WHERE %s = "%s" and paso4 = 0 order by utemp_id asc limit 1', $wpdb->prefix . 'users_temp', 'email', $email);

        $userTempList = $wpdb->get_results( $sql );
        if(!is_null($userTempList) && is_array($userTempList)  && count($userTempList)){
            foreach($userTempList as $row){  $userTemp = $row; break; }
        }

        return $userTemp;
    }
    
    

    public static function getUsuarioTempByPreapprovalId($preapproval_id) {

        global $wpdb;
        
        if(is_null($preapproval_id))  return null;

        $userTemp = null;
        $sql = sprintf( 'SELECT * FROM %s WHERE %s = "%s" and preapproval_id != ""  limit 1', $wpdb->prefix . 'users_temp', 'preapproval_id', $preapproval_id);

        $userTempList = $wpdb->get_results( $sql );
        if(!is_null($userTempList) && is_array($userTempList)  && count($userTempList)){
            foreach($userTempList as $row){  $userTemp = $row; break; }
        }

        return $userTemp;
    }
        

      
}
