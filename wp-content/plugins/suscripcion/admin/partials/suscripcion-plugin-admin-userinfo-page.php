<?php
  $user_id = isset($_GET['usuario'])?$_GET['usuario']:null;

  // busco al usuario
  $user =  get_user_by('ID', $user_id);

  if(empty($user)){
      wp_redirect(admin_url('admin.php?page=suscripcion-plugin'));
      exit;
  }

  // busco la informacion del usuario
  $usuarioInfo = Usuario_Information::getUsuarioInfoByUsuerId($user_id);

  // Plan del usuario
  $usuarioPlan = Usuario_Plan::getUsuarioPlanById($usuarioInfo->plan_id);

  // Pagos del Usuario



?>


<div class="wrap suscripcion">

    <div id="icon-users" class="icon32"><br/></div>
    <h2>Datos del Usuario: </h2>

    <table>
        <tr><th>Usuario</th><td><?php echo $user->display_name;?></td></tr>
        <tr><th>Email</th><td><?php echo $user->user_email;?></td></tr>

    </table>

    <h2>Información de la Membresía: </h2>

    <table>
        <tr><th>Plan ID</th><td><?php echo $usuarioInfo->plan_id;?></td></tr>
        <tr><th>Fecha de Suscripción</th><td><?php echo $usuarioInfo->plan_suscripcion_date;?></td></tr>
        <tr><th>Fecha de Desuscripción</th><td><?php echo $usuarioInfo->plan_unsuscription_date;?></td></tr>
        <tr><th>Fecha de Fin de Sscripción</th><td><?php echo $usuarioInfo->plan_suscripcion_end;?></td></tr>
        <tr><th>Plan Preapproval ID (MP)</th><td><?php echo $usuarioInfo->plan_mp_id;?></td></tr>
        <tr><th>Plan Nombre</th><td><?php echo $usuarioInfo->plan_nombre;?></td></tr>
        <tr><th>Plan Tipo</th><td><?php echo $usuarioInfo->plan_tipo;?></td></tr>
        <tr><th>Plan Valor</th><td>$ <?php echo $usuarioInfo->plan_valor;?></td></tr>

    </table>

    <h2>Información del Usuario: </h2>

    <table>
        <tr><th>Nombre</th><td><?php echo $usuarioInfo->nombre;?></td></tr>
        <tr><th>Apellido</th><td><?php echo $usuarioInfo->apellido;?></td></tr>
        <tr><th>Email</th><td><?php echo $usuarioInfo->email;?></td></tr>
        <tr><th>Fecha de Nacimiento</th><td><?php echo $usuarioInfo->fecha_nac;?></td></tr>
        <tr><th>Documento Tipo</th><td><?php echo $usuarioInfo->documento_tipo;?></td></tr>
        <tr><th>Documento Numero</th><td><?php echo $usuarioInfo->numero_documento;?></td></tr>
        <tr><th>Telefono</th><td><?php echo $usuarioInfo->telefono;?></td></tr>
        <tr><th>Dirección</th><td><?php echo $usuarioInfo->direccion1;?></td></tr>
        <tr><th>Dirección</th><td><?php echo $usuarioInfo->direccion2;?></td></tr>
        <tr><th>Ciudad</th><td><?php echo $usuarioInfo->ciudad;?></td></tr>
        <tr><th>Estado/Region</th><td><?php echo $usuarioInfo->estado_region;?></td></tr>
        <tr><th>Codigo Postal</th><td><?php echo $usuarioInfo->codigo_postal;?></td></tr>
        <tr><th>Pais</th><td><?php echo $usuarioInfo->pais;?></td></tr>
        <tr><th>&nbsp;</th><td>&nbsp;</td></tr>
        <tr><th>Fuera de Argentina</th><td><?php echo $usuarioInfo->fuera_de_argentina ? 'Si':'No';?></td></tr>
        <tr><th>Profesion</th><td><?php echo $usuarioInfo->profesion;?></td></tr>
        <tr>
            <th>Contacto Por</th>
            <td>
                <table>
                    <tr><th>Twitter</th><td><?php echo $usuarioInfo->twitter ? 'Si':'No';?></td></tr>
                    <tr><th>Facebook</th><td><?php echo $usuarioInfo->facebook ? 'Si':'No';?></td></tr>
                    <tr><th>Instagram</th><td><?php echo $usuarioInfo->instagram ? 'Si':'No';?></td></tr>
                    <tr><th>Newsletters</th><td><?php echo $usuarioInfo->newsletters ? 'Si':'No';?></td></tr>
                    <tr><th>Co-Responsable</th><td><?php echo $usuarioInfo->coResponsable ? 'Si':'No';?></td></tr>
                    <tr><th>Un Evento</th><td><?php echo $usuarioInfo->unEvento ? 'Si':'No';?></td></tr>
                    <tr><th>Otro</th><td><?php echo $usuarioInfo->opcionOtro ? 'Si':'No';?></td></tr>
                    <?php
                        if($usuarioInfo->opcionOtro){
                    ?>
                    <tr><th>Otro:</th><td><?php echo $usuarioInfo->contacto_otro; ?></td></tr>
                    <?php
                    }
                    ?>
                </table>
            </td>
        </tr>
        <tr><th>Dato Curioso</th><td><?php echo $usuarioInfo->dato_curioso;?></td></tr>

    </table>

    <?php /*
    <h2>Pagos del Usuario: </h2>


    */ ?>


</div>
