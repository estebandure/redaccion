<?php

if(isset($_GET['action']) && wp_verify_nonce($_GET['_wpnonce'], 'suscripcion_view_usuario')) { 
    switch($_GET['action']) {
        case 'ver' :
            require_once plugin_dir_path( dirname( __FILE__ ) ) . 'partials/suscripcion-plugin-admin-userinfo-page.php';
            exit();
            break;
        default:{
           // sigo... 
        }
    }
}

//Prepare Table of elements
$optionsList = array(
            'singular'  => 'ususario',     //singular name of the listed records
            'plural'    => 'ususarios',    //plural name of the listed records
            'ajax'      => false          //does this table support ajax?
        );
$wp_list_table = new Usuarios_List_Table($optionsList);
$wp_list_table->prepare_items();


?>
    <div class="wrap">
        
        <div id="icon-users" class="icon32"><br/></div>
        <h2>Usuarios Suscriptos</h2>
        

        <?php $wp_list_table->display() ?>
        
    </div>