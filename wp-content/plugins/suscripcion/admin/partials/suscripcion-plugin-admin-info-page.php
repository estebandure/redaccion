<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://www.mywebsite.com
 * @since      1.0.0
 *
 * @package    Suscripcion
 * @subpackage Suscripcion/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<h2>Información del Plugin</h2>
