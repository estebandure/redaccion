<?php

//Prepare Table of elements
$optionsList = array(
            'singular'  => 'pago',     //singular name of the listed records
            'plural'    => 'pagos',    //plural name of the listed records
            'ajax'      => false          //does this table support ajax?
        );
$wp_list_table = new Pagos_List_Table($optionsList);
$wp_list_table->prepare_items();


?>
    <div class="wrap">
        
        <div id="icon-users" class="icon32"><br/></div>
        <h2>Pagos de los usuarios</h2>
        

        <?php $wp_list_table->display() ?>
        
    </div>