<?php
/*
 * Load the base class
 */
if(!class_exists('WP_List_Table')){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}


 class Usuarios_List_Table extends WP_List_Table{
    
    private $per_page = 25;
    
    function __construct($options, $per_page = NULL) {
        global $status, $page;
        
        if ($per_page) {
            $this->per_page = $per_page;
        }          

        parent::__construct($options, $this->per_page);   
        
   
    }
    
    function column_nombre( $item ) {

      // create a nonce
      $ver_nonce = wp_create_nonce( 'suscripcion_view_usuario' );

      $title = '<strong>' . $item['nombre'] . '</strong>';

      $actions = [
        'ver' => sprintf( '<a href="?page=%s&action=%s&usuario=%s&_wpnonce=%s">Ver</a>', esc_attr( $_REQUEST['page'] ), 'ver', absint( $item['user_id'] ), $ver_nonce )
      ];

      return $title . $this->row_actions( $actions );
    }    

    function column_default($item, $column_name){
     
        return $item[$column_name];
    }
    
    function column_eliminado($item){
        
        if($item['eliminado']){
            return "No";
        }else{
            return "Si";
        }
    }
    
    function column_cb($item){
        return '<input type="checkbox" name="'.$this->_args['singular'].'" value="'.$item['user_id'].'" />';
    }

    function prepare_items() {
        global $wpdb, $_wp_column_headers;
        
        $screen = get_current_screen();
        
        $table_users_information = $wpdb->prefix . 'users_information';
        $table_users             = $wpdb->prefix . 'users';
        
        $user_search_key = isset( $_REQUEST['s'] ) ? wp_unslash( trim( $_REQUEST['s'] ) ) : '';
            

        /* -- Preparing your query -- */
        $query = "SELECT * 
                  FROM {$table_users_information} ui, 
                       {$table_users} u 
                  WHERE u.ID = ui.user_id     
                  ";
 
        if($user_search_key){
            $query .= "  AND (";
            $query .= "     nombre      like '%$user_search_key%' ";
            $query .= "  OR apellido    like '%$user_search_key%' ";
            $query .= "  OR email       like '%$user_search_key%' ";
            $query .= "  OR plan_nombre like '%$user_search_key%' ";
            $query .= "  OR fecha       like '%$user_search_key%' ";
            $query .= "  ) ";
        }

        /* -- Ordering parameters -- */
        $orderby = !empty($_GET["orderby"]) ? esc_sql($_GET["orderby"]) : 'ASC';
        $order   = !empty($_GET["order"  ]) ? esc_sql($_GET["order"]) : '';
        if(!empty($orderby) & !empty($order)){ $query.=' ORDER BY '.$orderby.' '.$order; }

        /* -- Pagination parameters -- */
        //Number of elements in your table?
        $total_items = $wpdb->query($query); //return the total number of affected rows
        //Which page is this?
        $paged = !empty($_GET["paged"]) ? esc_sql($_GET["paged"]) : '';
        //Page Number
        if(empty($paged) || !is_numeric($paged) || $paged<=0 ){ $paged=1; }
        //adjust the query to take pagination into account
        if(!empty($paged)){
            $offset=($paged-1)*$this->per_page;
            $query.=' LIMIT '.(int)$offset.','.(int)$this->per_page;
        }
            
        // Register the pagination
        $current_page = $this->get_pagenum();
        $this->set_pagination_args( array(
            'total_items' => $total_items,
            'total_pages' => ceil($total_items/$this->per_page),
            'per_page' => $this->per_page,
        ) );


        // Register the columns
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array($columns, $hidden, $sortable);           
        
        /* -- Register the Columns -- */
        $columns = $this->get_columns();
        $_wp_column_headers[$screen->id]=$columns;
        

        // Bulk action
        $this->process_bulk_action();
        
        // Fetch item
        $this->items = $wpdb->get_results($query, 'ARRAY_A');
    }
    
    function display() {
        ?>
        <form method="get">
            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
            <?php
            $this->search_box( __( 'Find', $this->plugin_text_domain ), 'nds-user-find');
            parent::display();
            ?>
        </form>
        <?php
    }


    /**
     * Define the columns that are going to be used in the table
     * @return array $columns, the array of columns to use with the table
     */
    function get_columns() {
        return array(
            'cb'          => '<input type="checkbox" />', //Render a checkbox instead of text
            'nombre'      => __('Nombre'),
            'apellido'    => __('Apellido'),           
            'email'       => __('Email'),
            'plan_nombre' => __('Plan'),
            'eliminado'   => __('Activo'),
            'fecha'       => __('Registro')
        );
    }

    /**
     * Decide which columns to activate the sorting functionality on
     * @return array $sortable, the array of columns that can be sorted by the user
     */
    public function get_sortable_columns() {
        return array(
            'nombre'          => array( 'nombre'     , 'asc'),
            'apellido'        => array( 'apellido'   , 'asc'),
            'email'           => array( 'email'      , 'asc'),
            'plan_nombre'     => array( 'plan_nombre', 'asc'),
            'fecha'           => array( 'fecha'      , 'asc')
        );
    }
    
    function column_name($item){

        //Build row actions
        $actions = array(
            'edit'      => '<a href="?page='.$_REQUEST['page'].'&action=edit&gallery='.$item['user_id'].'">Edit</a>',
            'delete'    => '<a href="?page='.$_REQUEST['page'].'&action=delete&gallery='.$item['user_id'].'">Delete</a>'
        );

        //Return the title contents
        return $item['name'].' '.$this->row_actions($actions);
    }  
    
    public function no_items() {
        _e( 'No hay usuarios registrados.' );
    }        
      
/*    
    function get_bulk_actions() {
        $actions = array(
            'delete'    => 'Delete'
        );
        return $actions;
    }    
    
    function process_bulk_action() {

        //Detect when a bulk action is being triggered...
        if( 'delete'===$this->current_action() ) {
            wp_die('Items deleted (or they would be if we had items to delete)!');
        }
    }
*/    

  
}
  