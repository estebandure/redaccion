<?php
/*
 * Load the base class
 */
if(!class_exists('WP_List_Table')){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}


 class Pagos_List_Table extends WP_List_Table{
    
    private $per_page = 25;
    
    function __construct($options, $per_page = NULL) {
        global $status, $page;
        
        if ($per_page) {
            $this->per_page = $per_page;
        }          

        parent::__construct($options, $this->per_page);   
        
   
    }

    function column_default($item, $column_name){
     
        return $item[$column_name];
    }
    
    function column_cb($item){
        return '<input type="checkbox" name="'.$this->_args['singular'].'" value="'.$item['user_id'].'" />';
    }

    function prepare_items() {
        global $wpdb;
        
        $table_users_payments = $wpdb->prefix . 'users_payments';
        $table_users          = $wpdb->prefix . 'users';
        $table_plans          = $wpdb->prefix . 'users_plans';

        /* -- Preparing your query -- */
        $query = "SELECT up.*, u.display_name, u.user_email, pl.nombre as plan_nombre
                  FROM {$table_users_payments} up
                  LEFT JOIN  {$table_users}  u ON u.ID = up.user_id 
                  LEFT JOIN  {$table_plans} pl ON pl.plan_id = up.plan_id 
                  WHERE 1 = 1    
                  ";

        /* -- Ordering parameters -- */
        $orderby = !empty($_GET["orderby"]) ? esc_sql($_GET["orderby"]) : 'ASC';
        $order   = !empty($_GET["order"  ]) ? esc_sql($_GET["order"]) : '';
        if(!empty($orderby) & !empty($order)){ $query.=' ORDER BY '.$orderby.' '.$order; }

        /* -- Pagination parameters -- */
        //Number of elements in your table?
        $total_items = $wpdb->query($query); //return the total number of affected rows
        //Which page is this?
        $paged = !empty($_GET["paged"]) ? esc_sql($_GET["paged"]) : '';
        //Page Number
        if(empty($paged) || !is_numeric($paged) || $paged<=0 ){ $paged=1; }
        //adjust the query to take pagination into account
        if(!empty($paged)){
            $offset=($paged-1)*$this->per_page;
            $query.=' LIMIT '.(int)$offset.','.(int)$this->per_page;
        }
            
        // Register the pagination
        $current_page = $this->get_pagenum();
        $this->set_pagination_args( array(
            'total_items' => $total_items,
            'total_pages' => ceil($total_items/$this->per_page),
            'per_page' => $this->per_page,
        ) );

        // Register the columns
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array($columns, $hidden, $sortable);           

        // Bulk action
        $this->process_bulk_action();
        
        // Fetch item
        $this->items = $wpdb->get_results($query, 'ARRAY_A');
    }
    
    function display() {
        ?>
        <form method="get">
            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
            <?php parent::display(); ?>
        </form>
        <?php
    }


    /**
     * Define the columns that are going to be used in the table
     * @return array $columns, the array of columns to use with the table
     */
    function get_columns() {
        return array(
            'cb'           => '<input type="checkbox" />', //Render a checkbox instead of text
            'display_name' => __('Nombre'),
            'user_email'   => __('Email'),
            'plan_nombre'  => __('Plan'),
            'status'       => __('Estado'),
            'monto'        => __('Monto'),
            'fecha'        => __('Fecha')
        );
    }

    /**
     * Decide which columns to activate the sorting functionality on
     * @return array $sortable, the array of columns that can be sorted by the user
     */
    public function get_sortable_columns() {
        return array(
            'nombre'          => array( 'nombre'     , 'asc'),
            'apellido'        => array( 'apellido'   , 'asc'),
            'email'           => array( 'email'      , 'asc'),
            'plan_nombre'     => array( 'plan_nombre', 'asc'),
            'fecha'           => array( 'fecha'      , 'asc')
        );
    }
    
    function column_name($item){

        //Build row actions
        $actions = array(
            'edit'      => '<a href="?page='.$_REQUEST['page'].'&action=edit&gallery='.$item['user_id'].'">Edit</a>',
            'delete'    => '<a href="?page='.$_REQUEST['page'].'&action=delete&gallery='.$item['user_id'].'">Delete</a>'
        );

        //Return the title contents
        return $item['name'].' '.$this->row_actions($actions);
    }    
/*    
    function get_bulk_actions() {
        $actions = array(
            'delete'    => 'Delete'
        );
        return $actions;
    }    
    
    function process_bulk_action() {

        //Detect when a bulk action is being triggered...
        if( 'delete'===$this->current_action() ) {
            wp_die('Items deleted (or they would be if we had items to delete)!');
        }
    }
*/    

  
}
  