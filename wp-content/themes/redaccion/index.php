<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

      <div class="site-date">
          <div class="" id="fecha1"></div>
          <script>
          var f = new Date();
          var weekday = new Array(7);
          weekday[0] =  "Domingo";
          weekday[1] = "Lunes";
          weekday[2] = "Martes";
          weekday[3] = "Miércoles";
          weekday[4] = "Jueves";
          weekday[5] = "Viernes";
          weekday[6] = "Sábado";
          var n = weekday[f.getDay()];

          document.getElementById("fecha1").innerHTML = "<strong>"+n+"</strong> "+f.getDate()+"."+(f.getMonth()+1)+"."+(f.getYear()-100)+" | "+f.getHours()+":"+f.getMinutes()+" hs";
          </script>
        </div><!-- .site-date -->

      <p class="site-description"><?php echo get_bloginfo('description'); /* WPCS: xss ok. */ ?></p>


		<?php
		if ( have_posts() ) :

			if ( is_home() && ! is_front_page() ) :
				?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>
				<?php
			endif;

			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_type() );

			endwhile;



		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
