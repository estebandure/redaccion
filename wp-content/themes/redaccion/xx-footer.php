<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package redaccion
 */

?>



</div><!-- #content -->
</div><!-- #page -->


<footer class="dock" id="tab-bar">


<a class="rv4dkl2 pie_actualidad" title="Actualidad" href="/">

  <span>Actualidad</span>
</a>


<a class="rv4dkl2 pie_en-profundidad" title="En Profundidad" href="/secciones/en-profundidad/">

  <span>En Profundidad</span>
</a>


<a class="rv4dkl2 pie_newsletters" title="Newletters" href="/newsletters/">

  <span>Newsletters</span>
</a>


<a class="rv4dkl2 pie_miembros" title="Miembros" href="/miembros/"  >

  <span>Co-Responsables</span>
</a>


</footer>



<div class="footer">



<div id="subfooter">
	<div class="widgetBox">
	<div class="inner">


<?php dynamic_sidebar( 'subfooterleft' ); ?>



</div></div>
<div class="widgetBox">
	<div class="inner">

<?php dynamic_sidebar( 'subfootercenter' ); ?>


</div></div>

<div class="widgetBox">
	<div class="inner">
<?php dynamic_sidebar( 'subfootercenter1' ); ?>
</div></div>
<div class="widgetBox">
	<div class="inner">
<?php dynamic_sidebar( 'subfooterright' ); ?>
</div></div>


</div>



</div>


<?php wp_footer(); ?>




<!-- Twitter universal website tag code -->
<script>
!function(e,t,n,s,u,a){e.twq||(s=e.twq=function(){s.exe?s.exe.apply(s,arguments):s.queue.push(arguments);
},s.version='1.1',s.queue=[],u=t.createElement(n),u.async=!0,u.src='//static.ads-twitter.com/uwt.js',
a=t.getElementsByTagName(n)[0],a.parentNode.insertBefore(u,a))}(window,document,'script');
// Insert Twitter Pixel ID and Standard Event data below
twq('init','o228p');
twq('track','PageView');
</script>
<!-- End Twitter universal website tag code -->
<script>
window.rleParticipation={api:"https://www.redaccion.com.ar/api/v1/",isLastNodeCookie:!1,getCookieId:function(){return localStorage.getItem("rle_ci")},allowCookies:function(){rleParticipation.isLastNodeCookie&&localStorage.setItem("rle_ac","1")},nodeCallback:function(e){var t=document.createElement("script");t.setAttribute("src",rleParticipation.api+"node/script/"+e),document.body.appendChild(t)},getNode:function(){var e=new XMLHttpRequest;e.onreadystatechange=function(){if(4==this.readyState&&200==this.status&&this.response){var e=JSON.parse(this.response);rleParticipation.isLastNodeCookie=e.cookie,e.nodeId&&rleParticipation.nodeCallback(e.nodeId)}},e.open("GET",rleParticipation.api+"node/next/"+localStorage.getItem("rle_ci")),e.send()},logEvent:function(e,t,i){var o=localStorage,a="rle_ac",n="rle_ci",r=Math.random().toString(36).substring(8)+(new Date).getTime()+Math.random().toString(36).substring(8);if(o.getItem(n)||o.setItem(n,r),o.getItem(a)||o.setItem(a,"0"),1===parseInt(o.getItem(a))){var s=new XMLHttpRequest;s.open("POST",rleParticipation.api+"event"),s.setRequestHeader("Content-Type","application/json;charset=UTF-8"),s.send(JSON.stringify({cookieId:o.getItem(n),key:e,value:JSON.stringify(t),nodeId:i}))}}};
<?php $c = get_the_category();echo 'window.rleParticipation.logEvent("pageView", {"page":location.pathname + location.search'.(count($c) >0 ? ',category:{name:"'.$c[0]->name.'",id:"'.$c[0]->term_id.'"}' : '').'});';  ?>
setTimeout(function() { window.rleParticipation.getNode(); }, 500);
window.addEventListener("message",function(e){if("removeFrame"===e.data[0]){var t=document.querySelectorAll("iframe");for(var r of t)if(r.getAttribute("src")&&r.getAttribute("src").endsWith("/"+e.data[1]+"/1"))return r.parentNode.removeChild(r)}},!1);
</script>




<script type='text/javascript'>
console.log ("categoria " + jQuery('.category a').html());
console.log ("autor " + jQuery('.author a').html());


    (function() {
        /** CONFIGURATION START **/
        var _sf_async_config = window._sf_async_config = (window._sf_async_config || {});

        _sf_async_config.uid = 66074; //CHANGE THIS
        _sf_async_config.domain = 'redaccion.com.ar'; //CHANGE THIS
        _sf_async_config.flickerControl = false;
        _sf_async_config.useCanonical = true;
        _sf_async_config.useCanonicalDomain = true;



        /**CHART BEAT**/
 _sf_async_config.sections = jQuery('.single .category a').html();

_sf_async_config.authors = jQuery('.single .author a').html();

/** CONFIGURATION END **/

        function loadChartbeat() {
            var e = document.createElement('script');
            var n = document.getElementsByTagName('script')[0];
            e.type = 'text/javascript';
            e.async = true;
            e.src = '//static.chartbeat.com/js/chartbeat.js';
            n.parentNode.insertBefore(e, n);
        }
        loadChartbeat();
     })();
</script>
<script>
            $(document).ready(function() {





				jQuery(".principal a").attr("target", "_blank");



jQuery(".sub-menu a .items_txt").html(function(buscayreemplaza, reemplaza) {
return reemplaza.replace('h2', 'h3');
});

jQuery( ".items_ul li" ).removeClass( "menu-item menu-item-type-taxonomy menu-item-object-category " ).addClass( "items_li" );
jQuery('li.menu-item-has-children > a >.items_txt').each(function() {
	jQuery(this).parent().append('<svg aria-hidden="true" class="items_arrow" width="8" height="12" viewBox="0 0 8 12" xmlns="http://www.w3.org/2000/svg"><path d="M1.777 12L8 6.022l-.023-.023L8 5.977 1.779 0 0 1.708l4.466 4.29L0 10.292z" fill-rule="evenodd" fill="var(--color,#4c4c4c)"></path></svg>');
});
jQuery( ".sub-menu li  .items_icon" ).remove();



              var first = true;

			  var width = $(window).width();
			  if (width > 769) {
					jQuery( " .menu .menu_items" ).prependTo( ".aside" );
              }

              if (width <= 768) {
                $(".menu").hide();
              }
              var $drillDown = $("#drilldown");

              // Hide menu once we know its width
              $('#showmenu').click(function() {
                var $menu = $('.menu');
                if ($menu.is(':visible')) {
                  // Slide away
                  $menu.animate({left: -($menu.outerWidth() + 10)}, function() {
                    $menu.hide();
                  });
                }
                else {
                  // Slide in
                  $menu.show().css("left", -($menu.outerWidth() + 10)).animate({left: 0});
                }
			  });


//drop down menu items//

var accordion_head = jQuery('.items_ul > li.menu-item-has-children > a'),
				accordion_body = jQuery('.items_ul li.menu-item-has-children > .sub-menu');
				// Open the first tab on load
		    //accordion_head.first().addClass('active').next().slideDown('normal');
				// Click function
				accordion_head.on('click', function(event) {
					// Disable header links
				//	console.log(this );
					event.preventDefault();
					// Show and hide the tabs on click
					if (jQuery(this).attr('class') != 'item-active'){
						accordion_body.slideUp('normal');
						jQuery(this).next().stop(true,true).slideToggle('normal');
						accordion_head.removeClass('item-active');
						jQuery(this).addClass('item-active');

					}else{
						accordion_body.slideUp('normal');
						jQuery(this).removeClass('item-active');
						accordion_head.removeClass('item-active');
					}
						});
//termina drop dowm menu itemas///


            });



            </script>


</body>
</html>
