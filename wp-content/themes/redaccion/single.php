<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package redaccion
 */

get_header();
?>

<?php require("AutorCoautor.php");?>

   

<div class="rv4scb">
<aside class="aside"></aside>
<div id="content" class="site-content single-Content">
  
	<div id="primary" class="content-area">
		<main id="mainPost" class="site-main">






    <!--POSTEO DESARROLLADO--->


		<?php
      $notID=[];
        $elposteoID=0;
		while ( have_posts() ) :
      the_post();
      $categoryThis = get_the_category(); 
    
$terms = get_the_terms( $post->ID , 'category');
if($terms) {
	foreach( $terms as $term ) {
		$cat_obj = get_term($term->term_id, 'category');
		$categoryThisslug = $cat_obj->slug;
	}
}


      
      $elposteoID=get_the_ID();
      get_template_part( 'template-parts/content-NotasV3', get_post_type() );
      
      //para categoria especial de news
      if ( get_post_meta( $post->ID, 'NewsBox', true ) ) {
        $CatNewsBox = get_post_meta($post->ID, "NewsBox", true);
       }else{
        $CatNewsBox ="NewsDefault";
       }

    
     
      array_push($notID,get_the_ID());
      wp_reset_postdata();
		endwhile; // End of the loop.
		?>



<!--FIN POSTEO DESSARROLLADO-->
<!-- 3 notas al pie del singl-->


<p class="SubTMenu">EN PROFUNDIDAD</p>
  <DIV class="NotasPieSingle">

<?php


$html="";

$NotaID = get_the_ID($post->ID);
$query1 =  new WP_Query( ['category__in'=>[4619],  'post_type' => ['post'], 'post__not_in' =>$notID,  'posts_per_page' => 3 ] );

//echo $query3->request;

while ( $query1->have_posts()) {

   $html.="<div class='' id='Note$NotaID'>";
   $query1->the_post();
   $tituloDesc= get_the_title();

//$categories = get_the_category();

      $html.= '<a href="' . get_permalink( ) . '" title="' .  $tituloDesc . ' " >';
      $html.=get_the_post_thumbnail( );
      $html.='</a>';



   $html.='<div class="aside_tit">';
   if ( get_post_meta( $post->ID, 'TituloHome', true ) ) {
  $TituloHome = get_post_meta($post->ID, "TituloHome", true);
  $html.="<a href='" . get_permalink()  . "' > $TituloHome </a> ";
 }else {
   $html.=the_title( '<a href="' . esc_url( get_permalink() ) . '" >', '</a>' , FALSE);
 }


 $html.="</div>";
 $html.="<div class='aside_autor'>";
   $html.=AutorCoautor(1);
   $html.="</div>";
   $html.="</div>";
   wp_reset_postdata();

}

echo $html;
?>


  </div>
<!-- fin de las 3 notas al pi del single-->
   </main><!-- #main -->

   



  </div><!-- #primary -->
</div><!-- /content -->


<aside class="asideb AsidebPost"><!-- col derecha-->
   
            <p>Cada día RED/ACCIÓN aborda un tema en profundidad para ayudarte a comprender mejor el mundo y ofrecerte soluciones para cambiarlo. </p>

<!-- aside story -->

<a style="display: block;margin: 16px 0 0 0;font-size: 14.4px;padding: 0.8em;" href="/" class="profundizar">Seguir navegando por Red/Acción</a>

<br>
<br>


<?php

 if ( class_exists( 'Jetpack_RelatedPosts' ) ) {
  echo do_shortcode( '[jetpack-related-posts]' );
}
     ?>

<?php

$notID=[];
$html="";
$NotaID = get_the_ID($post->ID);
$query1 =  new WP_Query( ['category__in'=>[4619], 'post__not_in' =>$notID,  'post_type' => ['post'], 'posts_per_page' => 4 ] );

//echo $query3->request;

while ( $query1->have_posts()) {

   $html.="<div class='aside_story' id='Note$NotaID'>";
   $query1->the_post();
   $tituloDesc1= get_the_title();
//$categories = get_the_category();

      $html.= '<a href="' . get_permalink() . '" title="' . $tituloDesc1. ' " >';
      $html.=get_the_post_thumbnail( );
      $html.='</a>';



   $html.='<div class="aside_tit">';
   if ( get_post_meta( $post->ID, 'TituloHome', true ) ) {
  $TituloHome = get_post_meta($post->ID, "TituloHome", true);
  $html.="<a href='" . get_permalink()  . "' > $TituloHome </a> ";
 }else {
   $html.=the_title( '<a href="' . esc_url( get_permalink() ) . '" >', '</a>' , FALSE);
 }
 $html.="</div>";
 $html.="<div class='aside_autor'>";
   $html.=AutorCoautor(1);
   $html.="</div>";
   $html.="</div>";
   wp_reset_postdata();
  array_push($notID,get_the_ID());

}


?>
 <!-- fin aside story -->


</aside><!-- fin col derecha-->
  </div><!---/rv4scb-->






<?php

get_footer();

function tiene_category($cats, $catid) {
  
  foreach ($cats as $cat) {
    if ($cat->term_id == $catid) {
      return true;
    }
    
  }
  return false;
}