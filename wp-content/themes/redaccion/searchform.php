
	

    <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
    <div class="searchContainer">
    <button class="searchIcon" type="submit" name="header_search_submit" class="button-reset color-inherit db o-60 absolute center-v right-1 hover-primary6">
    <svg  width="15" height="15" viewBox="0 0 15 15" xmlns="http://www.w3.org/2000/svg"> <path d="M12.09 10.937a6.718 6.718 0 0 0 1.452-4.175C13.542 3.028 10.51 0 6.77 0 3.03 0 0 3.027 0 6.76a6.767 6.767 0 0 0 10.665 5.527l2.634 2.44c.404.373 1.046.36 1.427-.02.383-.381.361-.997-.046-1.374l-2.59-2.396zm-5.32.528a4.712 4.712 0 0 1-4.709-4.703 4.711 4.711 0 0 1 4.71-4.704 4.711 4.711 0 0 1 4.71 4.704 4.713 4.713 0 0 1-4.71 4.703z" fill="#000"></path></svg></button>


        <input type="search" class="searchBox" 
            placeholder="<?php echo esc_attr_x( 'Encontrar', 'placeholder' ) ?>"
            value="<?php echo get_search_query() ?>" name="s"
            title="<?php echo esc_attr_x( 'Búsqueda de:', 'label' ) ?>" />
</div>
</form>


