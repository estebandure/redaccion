<?php
/* Template Name:Portada miembros 21*/
/* Template Post Type:page*/
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

get_header();
?>








<?php require("AutorCoautor.php");?>

   

<div class="rv4scb">
<aside class="aside"></aside>
<div id="content" class="site-content single-Content">
  
	<div id="primary" class="content-area">
		<main id="mainPost" class="site-main">
    <?php if(current_user_can('mepr-active','rules:130686')){
    //  echo("Contenido para miembros." ) ;

       
wp_nav_menu( array(

'menu'              => 'Landing-Miembros-Co-responsables', // (int|string|WP_Term) Desired menu. Accepts a menu ID, slug, name, or object.
'menu_class'        => 'miembros-co-menu', // (string) CSS class to use for the ul element which forms the menu. Default 'menu'.
'before'            => '  ', // (string) Text before the link markup.
'after'             => '', // (string) Text after the link markup.
'link_before'       => '<div class="items_miembros">', // (string) Text before the link text.
'link_after'        => '</div> ', // (string) Text after the link text.
) );
}else{  //echo("Contenido para  otros miembros." ) ;
  echo('
  <ul id="menu-landing-miembros-co-responsables" class="miembros-co-menu"><li id="menu-item-130821" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-124445 current_page_item menu-item-130821"> <a href="https://www.redaccion.com.ar/miembros-corresponsables/" aria-current="page"><div class="items_miembros">Miembros</div> </a></li>');
  echo('<li id="menu-item-130820" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-130820"> <a href="/membresias/usuario/datos/"><div class="items_miembros">Mi perfil</div> </a></li>');
  echo('<li id="menu-item-130822" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-130822"> <a href="/membresias/usuario/plan/"><div class="items_miembros">Mi plan</div> </a></li>');
  echo('</ul>');
}
?>



    <!--POSTEO DESARROLLADO--->
    <?php
		
			the_title( '<h1 class="entry-title">', '</h1>' ); //post H1
	the_content();
            ?>
            
          


<div class="NuestrasTematicas">

  <?php      

// The Query -notas


$tax_terms = array(39,4642,33,31,4643,4641,693,34,32,1035);
$notID=[];
$PostN = 0;
foreach ( $tax_terms as $term ) {


    $args = array(
    'posts_per_page' => 1,
    'cat' => $term,
    'post__not_in' =>$notID,
    'post_type' => "post",
     );
     array_push($notID,get_the_ID());
    
    
    $post = get_posts( $args )[0];
    $NotaID = get_the_ID($post->ID); 
    $notID[]=$NotaID;
    $category = get_the_category();
    $categoryName=$category[0]->cat_name;
    $category_link = get_category_link($category[0] );
    $PostN++;
    echo'<div class="NuestrasTematicasBox">';
    echo '<div class="notaTematicasBox_TH"> ';
    echo get_the_post_thumbnail( ); 
    
    echo '</div>' ;
    echo '<div class="notaTematicas_info"> ';
    echo '<span class="category">';
   
    echo '<a class="cat'.$PostN .'" href="'.$category_link.'" title="'.$categoryName.'" style="text-transform:uppercase;">'.$categoryName .'</a>';
    echo '</span>';
    echo '<h3 ><a href="' . get_permalink( $post->ID ) . '">' . $post->post_title . '</a></h3>';
  
   //autor - fecha-tiempo
   echo ('<div class="AutorCoautor">');




//if ( $posts ):
//foreach ( $posts as $post ) : setup_postdata( $post );
// Setting up the coauthors variable
$coauthors = get_coauthors();

// Counter for the coauthors foreach loop below
$coauth = 0;

// Counting the number of objects in the array.
$len = count( $coauthors );
$Piclen = 100;

    // Meta data here...
foreach( $coauthors as $coauthor ):
        // Updating the counter.
 $Piclen--;
        // van las fotos de los autores
        $UserPic = get_avatar( $coauthor->ID );
        

endforeach;



    foreach( $coauthors as $coauthor ):


        // Getting the data for the current author
        $userdata = get_userdata( $coauthor->ID );


        // If one object in the array
        if ( $coauth == 0 ):
            // Just the authors name
            echo '<a class="url fn n" href="/author/' . $userdata->user_nicename  .'/">'. $userdata->display_name .'</a>';

// If last object in the array
        elseif ( $coauth == ($len - 1) ):

          //echo ($len);
            // Adding an "and" before the last object
            echo ' y <a class="url fn n" href="/author/' . $userdata->user_nicename  .'/">'. $userdata->display_name .'</a>';

        // If more than one object in the array
        elseif ( $coauth >= 1 ):
            // Adding a "comma" after the name
           // echo ($coauth);
          // echo ($len);
            echo ', <a class="url fn n" href="/author/' . $userdata->user_nicename  .'/">'. $userdata->display_name .'</a>';


        endif;

        // Updating the counter.
        $coauth++;
    endforeach;



echo '<span class="posted-on"> . ';
echo the_time('j M Y')  .' · ';
echo do_shortcode('[rt_reading_time]')  .' min ' ;
echo ' </span>';


    // More meta data here...

// endforeach;
//endif;



echo ("</div>");

   //fin autor - fecha -tiempo
     echo '</div></div>' ;
}
wp_reset_postdata();

array_push($notID,get_the_ID());

?>
</div> <!--- cierra temáticas --->


<!--FIN POSTEO DESSARROLLADO-->

<!-- 3 notas al pie del singl-->
<p class="SubTMenu">EN PROFUNDIDAD</p>
  <DIV class="NotasPieSingle">

<?php


$html="";

$NotaID = get_the_ID($post->ID);
$query1 =  new WP_Query( ['category__in'=>[4619],  'post_type' => ['post'], 'post__not_in' =>$notID,  'posts_per_page' => 3 ] );

//echo $query3->request;

while ( $query1->have_posts()) {

   $html.="<div class='' id='Note$NotaID'>";
   $query1->the_post();

//$categories = get_the_category();

      $html.= '<a href="' . get_permalink() . '" title="' . esc_attr( $_post->post_title ) . ' " >';
      $html.=get_the_post_thumbnail( );
      $html.='</a>';



   $html.='<div class="aside_tit">';
   if ( get_post_meta( $post->ID, 'TituloHome', true ) ) {
  $TituloHome = get_post_meta($post->ID, "TituloHome", true);
  $html.="<a href='" . get_permalink()  . "' > $TituloHome </a> ";
 }else {
   $html.=the_title( '<a href="' . esc_url( get_permalink() ) . '" >', '</a>' , FALSE);
 }


 $html.="</div>";
 $html.="<div class='aside_autor'>";
   $html.=AutorCoautor(1);
   $html.="</div>";
   $html.="</div>";
 
 
}
 
echo $html;
?>


  </div>
  
<!-- fin de las 3 notas al pi del single-->

   </main><!-- #main -->
  </div><!-- #primary -->
</div><!-- /content -->
  </div><!---/rv4scb-->




  <script type="text/javascript">
//<![CDATA[
if(document.createStyleSheet) {
  document.createStyleSheet('https://www.redaccion.com.ar/wp-content/themes/redaccion/css/pageMiembros-co.css');
}
else {
  var styles = "@import url(' https://www.redaccion.com.ar/wp-content/themes/redaccion/css/pageMiembros-co.css');";
  var newSS=document.createElement('link');
  newSS.rel='stylesheet';
  newSS.href='data:text/css,'+escape(styles);
  document.getElementsByTagName("head")[0].appendChild(newSS);
}
//]]>
</script>
<script>

jQuery(document).ready(function(){
  jQuery('.cat1').html('AMBIENTE').attr('href', '/secciones/ambiente/');
  jQuery('.cat2').html('COMUNIDADES').attr('href', '/secciones/comunidades/');
  jQuery('.cat3').html('CULTURA Y DIVERSION').attr('href', '/secciones/cultura-y-diversion/');
  jQuery('.cat4').html('EDUCACIÓN').attr('href', '/secciones/educacion/');
  jQuery('.cat5').html('GÉNERO Y DIVERSIDAD').attr('href', '/secciones/genero-y-diversidad/');
  jQuery('.cat6').html('IMPACTO SOCIAL').attr('href', '/secciones/impacto-social/');
  jQuery('.cat7').html('OPINIÓN').attr('href', '/secciones/opinion/');
  jQuery('.cat8').html('SALUD Y BIENESTAR').attr('href', '/secciones/salud-y-bienestar/');
  jQuery('.cat9').html('TECNOLOGIA').attr('href', '/secciones/tecnologia/');
  jQuery('.cat10').html('VIDA COTIDIANA').attr('href', '/secciones/vida-cotidiana/');
  



    jQuery('.smooth a').on('click', function(e) {
		e.preventDefault();
		var desiredHeight = $(window).height() - 100;
		var $link = jQuery(this);
		var anchor = $link.attr('href');
		$('html, body').stop().animate({scrollTop: $(anchor).offset().top + (-100)}, 1000);
  });
  



  if(jQuery(window).width() <= 780) {
  var BeneficiosClick = jQuery('.SectionBeneficios .BoxBeneficios');


// var BoxBeneficiosDivMobile = jQuery(this);
BeneficiosClick.click(function() {

if (!jQuery(this).hasClass('BoxBeneficioshover') ){ 
 console.log("click en cajita click");
   jQuery(this).addClass('BoxBeneficioshover');

}else{
 console.log("sacar");
   jQuery(this).removeClass('BoxBeneficioshover');
 
}
});

}else{


  jQuery('.SectionBeneficios .BoxBeneficios').each(function()  { 
var BoxBeneficiosDiv = jQuery(this);

jQuery(BoxBeneficiosDiv).hover(
    function() {
        jQuery( this ).addClass( "BoxBeneficioshover" );
    }, function() {
        jQuery (this ).removeClass( "BoxBeneficioshover" );
    }
  );

});
}


});
</script>

<?php

get_footer();

function tiene_category($cats, $catid) {
  
  foreach ($cats as $cat) {
    if ($cat->term_id == $catid) {
      return true;
    }
    
  }
  return false;
}