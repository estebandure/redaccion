<?php
/* Template Name: PageProyecto */
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

get_header();
?>

<div class="Page-full-content">
<div class="Col-Fullwidth-SiteWidth-Content CabeceraFull">
	<div class="Fullwidth1ColCenter">
		<h1>El Proyecto</h1>
         <p class="archive-description-NoBorder">Nuestro periodismo humano reconecta a las audiencias y promueve su participación para lograr un impacto positivo en la sociedad.</p>
	</div>

</div>
</div>


<div id="content" class="site-content">
	<div id="" class="content-area PageFullW">
		<main id="main" class="site-main">

    

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content-min', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->


<script>

jQuery(document).ready(function(){
 
/* Smooth scrolling para anclas */
 
  jQuery('a.smooth').on('click', function(e) {
    e.preventDefault();
    var $link = jQuery(this);
    var anchor = $link.attr('href');
    $('html, body').stop().animate({scrollTop: $(anchor).offset().top}, 1000);
});
 



if(jQuery(window).width() <= 767) {
var oldscrollP=0;
jQuery(".ColMenuProyecto").insertBefore( ".ContentMiembros" );
jQuery("#S1").insertBefore( ".ContentMiembros" );
//jQuery("a.smooth:nth-child(2)").text('Membresía');
//jQuery("a.smooth:nth-child(3)").text('Equipo');
//jQuery("a.smooth:nth-child(4)").text('Principios');

jQuery(window).on("scroll", function() {
    
   
    
    if(jQuery(window).scrollTop() > 42) {
        //console.log("voy bajando"); 
        
         if (jQuery(window).scrollTop()>oldscrollP) {
        console.log("abajo pagina proyect"); 
        jQuery(".ColMenuProyecto").addClass("navPsticky");
        jQuery(".navigationProyect").removeClass("navigationProyectsticky");
        
        } else {
        //console.log("arriba");
        jQuery(".navigationProyect").addClass("navigationProyectsticky");
        
        }
    }     else {
        console.log("CERO");
         jQuery(".navigationProyect").removeClass("navigationProyectsticky");
        jQuery(".ColMenuProyecto").removeClass("navPsticky");
        
        }
    oldscrollP = jQuery(window).scrollTop();
});

}








});
</script>
    

<?php
get_footer();


