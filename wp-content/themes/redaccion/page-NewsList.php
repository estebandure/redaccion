<?php
/* Template Name: Page Todas-News */
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

get_header();
?>

<div class="rv4scb">
<aside class="aside"></aside>
<div id="content" class="site-content-full">
	<div class="content-area-full">
		<main id="main" class="site-main">






     








		<?php
		while ( have_posts() ) :
      the_post();
     echo(' <header class="page-header authorHeader">');
      the_title( '<h1 >', '</h1>' ); //post H1
echo ('</header>');
			get_template_part( 'template-parts/content-min-full', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
  </div><!-- #primary -->
  
  </div><!-- /content -->
  </div><!---/rv4scb-->



<?php

get_footer();
