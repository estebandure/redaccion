<?php
/* Template Name: Pagesimple sin menu */
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

get_header();
?>

<div id="content" class="site-content-full">
	
		<main id="main" class="site-main">



		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'page' );

			

		endwhile; // End of the loop.
		?>

</main><!-- #main -->

  
  </div><!-- /content -->


<script>
	jQuery(document).ready(function(){
if(jQuery(window).width() <= 780) {
  var BeneficiosClick = jQuery('.SectionBeneficios .BoxBeneficios');


// var BoxBeneficiosDivMobile = jQuery(this);
BeneficiosClick.click(function() {

if (!jQuery(this).hasClass('BoxBeneficioshover') ){ 
 console.log("click en cajita click");
   jQuery(this).addClass('BoxBeneficioshover');

}else{
 console.log("sacar");
   jQuery(this).removeClass('BoxBeneficioshover');
 
}
});

}else{


  jQuery('.SectionBeneficios .BoxBeneficios').each(function()  { 
var BoxBeneficiosDiv = jQuery(this);

jQuery(BoxBeneficiosDiv).hover(
    function() {
        jQuery( this ).addClass( "BoxBeneficioshover" );
    }, function() {
        jQuery (this ).removeClass( "BoxBeneficioshover" );
    }
  );

});



}
	});
</script>

<script>
if ( window.history.replaceState ) {
  window.history.replaceState( null, null, window.location.href );
}
</script>

<?php

get_footer();
