<?php
/* Template Name: PageMono */
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

get_header();
?>


<div class="ContentMono">
    <div class="MonoBg">
  <div class="MonoTitle">

           <?php the_title( '<h1>', '</h1>' ); ?>

           <div class="Page-full-contentBajada">Leer es un placer que no prescribe. Por eso RED/ACCIÓN presenta MONO, una revista desplegable con periodismo de alta calidad, que se distribuye cada dos meses entre sus miembros.</div>
           <p class="btColaborar"><a href="/miembros/" target="_blank" rel="noreferrer noopener" aria-label="Hace miembro de mono">Hacete miembro</a></p>
  </div>
    </div>

</div>



<div id="content" class="site-content site-contentMono">
  <div id="" class="content-area PageFullW MonoFull">
    <main id="main" class="site-main MonoContent">

    <div class="MonoColumn" id="mainPlus">






  <?php
      $queryMono = new WP_Query( [ 'post_type' => ['MonoRevista'],'orderby'=> 'date', 'posts_per_page' => 6 ] );


while ( $queryMono->have_posts()) {
    $queryMono->the_post();
   // echo '<li>'.$n.' - ' . get_the_title() . '</li>';
    $categories = get_the_category();
     get_template_part( 'template-parts/content-mono', get_post_type() );

}


   


      
        
        ?>
  </div> 
<div class="CargaNotasHolder"><a id="CargaNotas" href="#" class="BTBlacKAction ">VER ANTERIORES</a> </div>
    </main><!-- #main -->
  </div><!-- #primary -->


<style type="text/css">
    body{background-color: #1b152d;}
</style>
    


<script type="text/javascript">
    jQuery(document).ready(function() {
      
            
   if ( jQuery('.page-template-page-mono').hasClass('logged-in') ) {
       //do something it does have the protected class!
       
   } else{
     //alert("NO tengo class logged");
     jQuery(".Mono-description a").remove();
   }

     
    

 


var win = jQuery(window);
var loadinginf=0;
var currentinf=0;
var footerheight=jQuery(".footer").height();
//console.log(footerheight);

  // Each time the user scrolls
jQuery("#CargaNotas").click(function(event) {
  event.preventDefault();
 // alert( "Handler for .click() called." );
  

   //console.log("carga scroll " + currentinf);

        if ((loadinginf==0)&&(currentinf<=14)) {
        jQuery('#loading').show();
        loadinginf=1;
        currentinf+=6;
        console.log("carga scroll " + currentinf);
        jQuery.ajax({
          url: "https://www.redaccion.com.ar/infinitphpmono/?de="+currentinf,
          dataType: 'html',
          success: function(html) {
          jQuery('#mainPlus').append(html);
          jQuery('#loading').hide();
          
          }, 
          complete: function() {
          loadinginf=0; 
          }
        });
      }
      
      });


   });  



</script>



<?php
get_footer();


