<?php
/**
 * redaccion functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package redaccion


 */




if ( ! function_exists( 'redaccion_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function redaccion_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on redaccion, use a find and replace
		 * to change 'redaccion' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'redaccion', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'redaccion' ),
		) );




		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'redaccion_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'redaccion_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function redaccion_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'redaccion_content_width', 640 );
}
add_action( 'after_setup_theme', 'redaccion_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function redaccion_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'redaccion' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'redaccion' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));


register_sidebar(array('name'=>'subfooterleft'));
register_sidebar(array('name'=>'subfootercenter'));
register_sidebar(array('name'=>'subfootercenter1'));
register_sidebar(array('name'=>'subfooterright'));

}
add_action( 'widgets_init', 'redaccion_widgets_init' );








/**
 * Enqueue scripts and styles.
 */
function redaccion_scripts() {
	//wp_enqueue_style( 'redaccion-style', get_stylesheet_uri() );
 wp_enqueue_style( 'redaccion-style', get_stylesheet_uri(), array(), ''.rand() );
	wp_enqueue_script( 'redaccion-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'redaccion-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'redaccion_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}






function codex_custom_init() {

  register_post_type(
    'MonoRevista', array(
      'name'                => _x( 'MonoRevista', 'Post Type General Name', 'redaccion' ),
      'singular_name'       => _x( 'MonoRevista', 'Post Type Singular Name', 'redaccion' ),
      'menu_name'           => __( 'MonoRevista', 'redaccion' ),
      'parent_item_colon'   => __( 'MonoRevista Principal', 'redaccion' ),
      'all_items'           => __( 'Todos los MonoRevista', 'redaccion' ),
      'view_item'           => __( 'Ver MonoMonoRevista', 'redaccion' ),
      'add_new_item'        => __( 'Añadir Nuevo MonoRevista', 'redaccion' ),
      'add_new'             => __( 'Añadir MonoRevista', 'redaccion' ),
      'edit_item'           => __( 'Editar MonoRevista', 'redaccion' ),
      'update_item'         => __( 'Actualizar MonoRevista', 'redaccion' ),
      'search_items'        => __( 'Buscar MonoRevista', 'redaccion' ),
      'not_found'           => __( 'No encontrado', 'redaccion' ),
      'not_found_in_trash'  => __( 'No encontrado en Papelera', 'redaccion' ),
      'label'               => __( 'MonoRevista', 'redaccion' ),
      'description'         => __( 'Reseñas de MonoRevista', 'redaccion' ),
      'labels'              => $labels,
      // Features this CPT supports in Post Editor
      'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
      'show_in_rest'       => true,
      // You can associate this CPT with a taxonomy or custom taxonomy.
     // 'taxonomies'          => array( 'category' ),
      /* A hierarchical CPT is like Pages and can have
      * Parent and child items. A non-hierarchical CPT
      * is like Posts.
      */
      'hierarchical'        => false,
      'public'              => true,
      'show_ui'             => true,
      'show_in_menu'        => true,
      'show_in_nav_menus'   => true,
      'show_in_admin_bar'   => true,
      'menu_position'       => 5,
      'can_export'          => true,
      'has_archive'         => true,
      'exclude_from_search' => false,
      'publicly_queryable'  => true,
      'capability_type'     => 'page',
    )
  );

  register_post_type(
    'EncuentroPost', array(
      'name'                => _x( 'Encuentros Post', 'Post Type General Name', 'redaccion' ),
      'singular_name'       => _x( 'Encuentros Post', 'Post Type Singular Name', 'redaccion' ),
      'menu_name'           => __( 'Encuentros Post', 'redaccion' ),
      'parent_item_colon'   => __( 'Encuentro Principal  Post', 'redaccion' ),
      'all_items'           => __( 'Todos los Encuentros', 'redaccion' ),
      'view_item'           => __( 'Ver Encuentro', 'redaccion' ),
      'add_new_item'        => __( 'Añadir Nuevo Encuentro Post', 'redaccion' ),
      'add_new'             => __( 'Añadir Nuevo Encuentro  Post', 'redaccion' ),
      'edit_item'           => __( 'Editar Encuentro  Post', 'redaccion' ),
      'update_item'         => __( 'Actualizar Encuentro  Post', 'redaccion' ),
      'search_items'        => __( 'Buscar Encuentro  Post', 'redaccion' ),
      'not_found'           => __( 'No encontrado', 'redaccion' ),
      'not_found_in_trash'  => __( 'No encontrado en Papelera', 'redaccion' ),
      'label'               => __( 'Encuentros Post', 'redaccion' ),
      'description'         => __( 'Reseñas de encuentros', 'redaccion' ),
      'labels'              => $labels,
      // Features this CPT supports in Post Editor
      'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
      'show_in_rest'       => true,
      // You can associate this CPT with a taxonomy or custom taxonomy.
       'taxonomies'          => array(  'category'),
     // 'taxonomies'          => array( 'category' ),
      /* A hierarchical CPT is like Pages and can have
      * Parent and child items. A non-hierarchical CPT
      * is like Posts.
      */
      'hierarchical'        => false,
      'public'              => true,
      'show_ui'             => true,
      'show_in_menu'        => true,
      'show_in_nav_menus'   => true,
      'show_in_admin_bar'   => true,
      'menu_position'       => 5,
      'can_export'          => true,
      'has_archive'         => true,
      'exclude_from_search' => false,
      'publicly_queryable'  => true,
      'capability_type'     => 'page',
    )
  );
 register_post_type(
    'equilibralacancha', array(
       'name'                => _x( 'Jugadoras', 'Post Type General Name', 'redaccion' ),
      'singular_name'       => _x( 'Jugadora', 'Post Type Singular Name', 'redaccion' ),
      'menu_name'           => __( 'Jugadoras', 'redaccion' ),
      'parent_item_colon'   => __( 'Jugadoras Principal', 'redaccion' ),
      'all_items'           => __( 'Todos los Jugadoras', 'redaccion' ),
      'view_item'           => __( 'Ver Jugadoras', 'redaccion' ),
      'add_new_item'        => __( 'Añadir Nuevo Jugadoras', 'redaccion' ),
      'add_new'             => __( 'Añadir Nuevo', 'redaccion' ),
      'edit_item'           => __( 'Editar Jugadoras', 'redaccion' ),
      'update_item'         => __( 'Actualizar Jugadoras', 'redaccion' ),
      'search_items'        => __( 'Buscar Jugadoras', 'redaccion' ),
      'not_found'           => __( 'No encontrado', 'redaccion' ),
      'not_found_in_trash'  => __( 'No encontrado en Papelera', 'redaccion' ),
      'label'               => __( 'Jugadoras', 'redaccion' ),
      'description'         => __( 'Reseñas de Jugadoras', 'redaccion' ),
      'labels'              => $labels,
      // Features this CPT supports in Post Editor
      'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
       'show_in_rest'       => true,
      // You can associate this CPT with a taxonomy or custom taxonomy.
      'taxonomies'          => array( 'puesto' ),
      /* A hierarchical CPT is like Pages and can have
      * Parent and child items. A non-hierarchical CPT
      * is like Posts.
      */
      'hierarchical'        => false,
      'public'              => true,
      'show_ui'             => true,
      'show_in_menu'        => true,
      'show_in_nav_menus'   => true,
      'show_in_admin_bar'   => true,
      'menu_position'       => 5,
      'can_export'          => true,
      'has_archive'         => true,
      'exclude_from_search' => false,
      'publicly_queryable'  => true,
      'capability_type'     => 'page',
    )
  );


register_post_type(
    'redaccion-abierta', array(
       'name'                => _x( 'Redacción Abierta', 'Post Type General Name', 'redaccion' ),
      'singular_name'       => _x( 'Preguntas', 'Post Type Singular Name', 'redaccion' ),
      'menu_name'           => __( 'Preguntas', 'redaccion' ),
      'parent_item_colon'   => __( 'Preguntas Principal', 'redaccion' ),
      'all_items'           => __( 'Todos las Preguntas', 'redaccion' ),
      'view_item'           => __( 'Ver Preguntas', 'redaccion' ),
      'add_new_item'        => __( 'Añadir Nueva Preguntas', 'redaccion' ),
      'add_new'             => __( 'Añadir Nueva', 'redaccion' ),
      'edit_item'           => __( 'Editar Preguntas', 'redaccion' ),
      'update_item'         => __( 'Actualizar Preguntas', 'redaccion' ),
      'search_items'        => __( 'Buscar Preguntas', 'redaccion' ),
      'not_found'           => __( 'No encontrado', 'redaccion' ),
      'not_found_in_trash'  => __( 'No encontrado en Papelera', 'redaccion' ),
      'label'               => __( 'Redacción Abierta', 'redaccion' ),
      'description'         => __( 'Reseñas de Preguntas', 'redaccion' ),
      'labels'              => $labels,
      // Features this CPT supports in Post Editor
      'supports'            => array( 'post-formats' ,'title', 'editor', 'excerpt', 'author', 'thumbnail','page-attributes', 'comments', 'revisions', 'custom-fields', ),
       'show_in_rest'       => true,
      // You can associate this CPT with a taxonomy or custom taxonomy.
      'taxonomies'          => array(  'category'),
      /* A hierarchical CPT is like Pages and can have
      * Parent and child items. A non-hierarchical CPT
      * is like Posts.
      */
      'hierarchical'        => false,
      'public'              => true,
      'show_ui'             => true,
      'show_in_menu'        => true,
      'show_in_nav_menus'   => true,
      'show_in_admin_bar'   => true,
      'menu_position'       => 5,
      'can_export'          => true,
      'has_archive'         => true,
      'exclude_from_search' => false,
      'publicly_queryable'  => true,
      'capability_type'     => 'page',
    )
  );




register_post_type(
  'Banners', array(
     'name'                => _x( 'Banners', 'Post Type General Name', 'redaccion' ),
    'singular_name'       => _x( 'Banners', 'Post Type Singular Name', 'redaccion' ),
    'menu_name'           => __( 'Banners', 'redaccion' ),
    'parent_item_colon'   => __( 'Banner Principal', 'redaccion' ),
    'all_items'           => __( 'Todos los Banners', 'redaccion' ),
    'view_item'           => __( 'Ver Preguntas', 'redaccion' ),
    'add_new_item'        => __( 'Añadir Nuevo Banner', 'redaccion' ),
    'add_new'             => __( 'Añadir Nuev0', 'redaccion' ),
    'edit_item'           => __( 'Editar Banner', 'redaccion' ),
    'update_item'         => __( 'Actualizar Banners', 'redaccion' ),
    'search_items'        => __( 'Buscar Banners', 'redaccion' ),
    'not_found'           => __( 'No encontrado', 'redaccion' ),
    'not_found_in_trash'  => __( 'No encontrado en Papelera', 'redaccion' ),
    'label'               => __( 'Banners', 'redaccion' ),
    'description'         => __( 'Reseñas de Banners', 'redaccion' ),
    'labels'              => $labels,
    // Features this CPT supports in Post Editor
    'supports'            => array( 'post-formats' ,'title', 'editor', 'excerpt', 'author', 'thumbnail','page-attributes', 'comments', 'revisions', 'custom-fields', ),
     'show_in_rest'       => true,
    // You can associate this CPT with a taxonomy or custom taxonomy.
    'taxonomies'          => array(  'category'),
    /* A hierarchical CPT is like Pages and can have
    * Parent and child items. A non-hierarchical CPT
    * is like Posts.
    */
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 5,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'page',
  )
);




 register_post_type(
    'acciones', array(
       'name'                => _x( 'Acciones', 'Post Type General Name', 'redaccion' ),
      'singular_name'       => _x( 'Accion', 'Post Type Singular Name', 'redaccion' ),
      'menu_name'           => __( 'Acciones', 'redaccion' ),
      'parent_item_colon'   => __( 'Acciones Principal', 'redaccion' ),
      'all_items'           => __( 'Todos las Acciones', 'redaccion' ),
      'view_item'           => __( 'Ver JAcciones', 'redaccion' ),
      'add_new_item'        => __( 'Añadir Nueva Accion', 'redaccion' ),
      'add_new'             => __( 'Añadir Nueva', 'redaccion' ),
      'edit_item'           => __( 'Editar Acciones', 'redaccion' ),
      'update_item'         => __( 'Actualizar Acciones', 'redaccion' ),
      'search_items'        => __( 'Buscar Acciones', 'redaccion' ),
      'not_found'           => __( 'No encontrado', 'redaccion' ),
      'not_found_in_trash'  => __( 'No encontrado en Papelera', 'redaccion' ),
      'label'               => __( 'Acciones', 'redaccion' ),
      'description'         => __( 'Reseñas de Acciones', 'redaccion' ),
      'labels'              => $labels,
      // Features this CPT supports in Post Editor
      'supports'            => array( 'post-formats' ,'title', 'editor', 'excerpt', 'author', 'thumbnail','page-attributes', 'comments', 'revisions', 'custom-fields', ),
       'show_in_rest'       => true,
      // You can associate this CPT with a taxonomy or custom taxonomy.
      'taxonomies'          => array( 'AccionesTipo', 'category'),
      /* A hierarchical CPT is like Pages and can have
      * Parent and child items. A non-hierarchical CPT
      * is like Posts.
      */
      'hierarchical'        => false,
      'public'              => true,
      'show_ui'             => true,
      'show_in_menu'        => true,
      'show_in_nav_menus'   => true,
      'show_in_admin_bar'   => true,
      'menu_position'       => 5,
      'can_export'          => true,
      'has_archive'         => true,
      'exclude_from_search' => false,
      'publicly_queryable'  => true,
      'capability_type'     => 'page',
    )
  );
 

}

add_action( 'init', 'codex_custom_init' );

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/

add_action( 'init', 'custom_post_type', 0 );

add_action( 'pre_get_posts', 'add_my_post_types_to_query' );

function add_my_post_types_to_query( $query ) {
    if ( is_home() && $query->is_main_query() )
        $query->set( 'post_type', array( 'post', 'MonoRevista', 'EncuentroPost' ,'acciones','redaccion-abierta', 'Banners','equilibralacancha') );
    return $query;
}






// Lo enganchamos en la acción init y llamamos a la función create_book_taxonomies() cuando arranque
add_action( 'init', 'create_player_taxonomies', 0 );
 
// Creamos dos taxonomías, posición y autor para el custom post type "equilibralacancha"
function create_player_taxonomies() {
        // Añadimos nueva taxonomía y la hacemos jerárquica (como las categorías por defecto)
        $labels = array(
        'name' => _x( 'Posiciones', 'taxonomy general name' ),
        'singular_name' => _x( 'Posición', 'taxonomy singular name' ),
        'search_items' =>  __( 'Buscar por Posición' ),
        'all_items' => __( 'Todos las Posiciones' ),
        'parent_item' => __( 'Posición padre' ),
        'parent_item_colon' => __( 'Posición padre:' ),
        'edit_item' => __( 'Editar Posición' ),
        'update_item' => __( 'Actualizar Posición' ),
        'add_new_item' => __( 'Añadir nueva Posición' ),
        'new_item_name' => __( 'Nombre de la nueva Posición' ),
);
register_taxonomy( 'puesto', array( 'equilibralacancha' ), array(
        'hierarchical' => true,
        'labels' => $labels, /* Aquí es donde se utiliza la variable $labels que hemos creado arriba*/
        'show_ui' => true,
        'query_var' => true,
        'show_in_rest' => true,
        'rewrite' => array( 'slug' => 'puesto' ),
));

}



// Lo enganchamos en la acción init y llamamos a la función create_book_taxonomies() cuando arranque
add_action( 'init', 'create_acciones_taxonomies', 0 );
 
// Creamos dos taxonomías, posición y autor para el custom post type "equilibralacancha"
function create_acciones_taxonomies() {
        // Añadimos nueva taxonomía y la hacemos jerárquica (como las categorías por defecto)
        $labels = array(
        'name' => _x( 'Tipo de Acción', 'taxonomy general name' ),
        'singular_name' => _x( 'Tipo de Acción', 'taxonomy singular name' ),
        'search_items' =>  __( 'Buscar por Acción' ),
        'all_items' => __( 'Todos las Acciones' ),
        'parent_item' => __( 'Tipo de Acción padre' ),
        'parent_item_colon' => __( 'Tipo de Acciónn padre:' ),
        'edit_item' => __( 'Editar Tipo de Acción' ),
        'update_item' => __( 'Actualizar Tipo de Acción' ),
        'add_new_item' => __( 'Añadir nueva Tipo de Acción' ),
        'new_item_name' => __( 'Nombre del nuevo Tipo de Acción' ),
);
register_taxonomy( 'AccionesTipo', array( 'acciones' ), array(
        'hierarchical' => true,
        'labels' => $labels, /* Aquí es donde se utiliza la variable $labels que hemos creado arriba*/
        'show_ui' => true,
        'query_var' => true,
        'show_in_rest' => true,
        'rewrite' => array( 'slug' => 'AccionesTipo' ),

));

}


function delete_post_type(){
    unregister_post_type( 'preguntas' );
}
add_action('init','delete_post_type');


function jptweak_remove_share() {
    remove_filter( 'the_content', 'sharing_display', 19 );
    remove_filter( 'the_excerpt', 'sharing_display', 19 );
    if ( class_exists( 'Jetpack_Likes' ) ) {
        remove_filter( 'the_content', array( Jetpack_Likes::init(), 'post_likes' ), 30, 1 );
    }
}

add_action( 'loop_start', 'jptweak_remove_share' );

function remove_img_attributes( $html ) {
$html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
return $html;
}
add_filter( 'post_thumbnail_html', 'remove_img_attributes', 10 );
add_filter( 'image_send_to_editor', 'remove_img_attributes', 10 );


/**
 * Add HTML5 theme support.
 */
function wpdocs_after_setup_theme() {
    add_theme_support( 'html5', array( 'search-form' ) );
}
add_action( 'after_setup_theme', 'wpdocs_after_setup_theme' );





function wpb_imagelink_setup() {
    $image_set = get_option( 'image_default_link_type' );
     
    if ($image_set !== 'none') {
        update_option('image_default_link_type', 'none');
    }
}
add_action('admin_init', 'wpb_imagelink_setup', 10);

//FORM FILTROS


function misha_filter_function(){
  $args = array(
    'orderby' => 'date', // we will sort posts by date
    'posts_per_page'=> 5 ,
    'order' => $_POST['date'] // ASC or DESC

  );
 
  // for taxonomies / categories
 if( isset( $_POST['categoryfilter'] ) )
    $args['tax_query'] = array(
      array(
        'taxonomy' => 'category',
        'field' => 'id',
        
        'terms' => $_POST['categoryfilter']

      )
    );
 
  
 
  $query = new WP_Query( $args );


 
  if( $query->have_posts() ) :

    while( $query->have_posts() ): 
      $query->the_post();


     // echo '<h2>' . $query->post->post_title . '</h2>';


       get_template_part( 'template-parts/content-portada', get_post_type() );

    endwhile;
    wp_reset_postdata();
  else :
    echo 'No se han encontrado resultados';
  endif;
 
  die();
}
 
 
add_action('wp_ajax_myfilter', 'misha_filter_function'); 
add_action('wp_ajax_nopriv_myfilter', 'misha_filter_function');

function sc_taglist(){
    return get_the_tag_list( ' ', ' | ' );
}
add_shortcode('tags', 'sc_taglist');

//agregar hoja de estilo


add_action( 'wp_enqueue_scripts', 'custom_enqueue_styles');

function custom_enqueue_styles() {
  wp_enqueue_style( 'custom-style', 
            get_stylesheet_directory_uri() . '/css/portadaV3.css?'.rand(), 
            array(), 
            wp_get_theme()->get('Version')
          );


  wp_enqueue_style( 'custom-style-page', 
            get_stylesheet_directory_uri() . '/css/pageExtra.css?'.rand(), 
            array(), 
            wp_get_theme()->get('Version')
          );

          wp_enqueue_style( 'custom-style-pages20', 
          get_stylesheet_directory_uri() . '/css/structure.css?'.rand(), 
          array(), 
          wp_get_theme()->get('Version')
        );

        wp_enqueue_style( 'custom-style-20', 
        get_stylesheet_directory_uri() . '/css/style-20.css?'.rand(), 
        array(), 
        wp_get_theme()->get('Version')
      );

}



// Add Shortcode
function custom_shortcode() {

$autorInf = get_the_author_meta('description');
return "<div class=boxSindicadas > $autorInf </div>";

}

add_shortcode( 'autorinfo', 'custom_shortcode' );



//ad infinite scroll


add_action( 'after_setup_theme', 'my_child_setup' );
function my_child_setup() {
  add_theme_support( 'infinite-scroll', array(
    'container'      => 'main',
    'render'         => 'my_child_infinite_scroll_render',
    'posts_per_page' => 10,
    'footer'=> 'page',   
  ) );
}

function my_child_infinite_scroll_render() {
  while ( have_posts() ) {
    the_post();
    $categories = get_the_category();
    //echo $categories ;
    get_template_part( 'template-parts/content-archivo', get_post_type() );
  }
}



function listed_change_jetpack_infinite_scroll_js_settings( $settings ) {
  $settings['text'] = __( 'Cargar más notas', 'l18n' );
  return $settings;
}
add_filter( 'infinite_scroll_js_settings', 'listed_change_jetpack_infinite_scroll_js_settings' ); 



/* Desactivar wptexturize */
add_filter( 'run_wptexturize', '__return_false' );

remove_filter('the_content', 'wptexturize');
remove_filter('the_excerpt', 'wptexturize');
remove_filter('comment_text', 'wptexturize');
remove_filter('the_title', 'wptexturize');

/*categorias qeu filtra el archive*/
function exclude_stuff($query) { 
    if ( $query->is_author) {
        $query->set('cat', '-1005,-1,-38,-788,-703,-704,-705,-706,-732,-694,-36,-988,-2272,-2642,-4358');
    }
    return $query;
}

add_filter('pre_get_posts', 'exclude_stuff');


/*mail de reset pass*/

function my_retrieve_password_subject_filter($old_subject) {
    // $old_subject is the default subject line created by WordPress.
    // (You don't have to use it.)

    $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
    $subject = sprintf( __('RED/ACCIÓN - Recuperar contraseña'), $blogname );
    // This is how WordPress creates the subject line. It looks like this:
    // [Doug's blog] Password Reset
    // You can change this to fit your own needs.

    // You have to return your new subject line:
    return $subject;
}

//* Password reset activation E-mail -> Body
add_filter( 'retrieve_password_message', 'dnlt_retrieve_password_message', 10, 2 );

function dnlt_retrieve_password_message( $message, $key ){
    $user_data = '';
    // If no value is posted, return false
    if( ! isset( $_POST['user_login'] )  ){
            return '';
    }
    // Fetch user information from user_login
    if ( strpos( $_POST['user_login'], '@' ) ) {

        $user_data = get_user_by( 'email', trim( $_POST['user_login'] ) );
    } else {
        $login = trim($_POST['user_login']);
        $user_data = get_user_by('login', $login);
    }
    if( ! $user_data  ){
        return '';
    }
    $user_login = $user_data->user_login;
    $user_email = $user_data->user_email;
    // Setting up message for retrieve password
    $message = "Alguien ha solicitado un reinicio de contraseña para:\n\n";
    $message .= network_home_url( '/' ) . "\r\n\r\n";
    $message .= sprintf(__('Usuario: %s'), $user_login) . "\r\n\r\n";  
    $message .= "Si ha sido un error, ignora este correo y no pasará nada.\n\n"; 
    $message .= "Para restaurar la contraseña, visita la siguiente dirección:\n";
    $message .= network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user_login), 'login');
    
    return $message;
}
// To get these filters up and running:
add_filter ( 'retrieve_password_title', 'my_retrieve_password_subject_filter', 10, 1 );



function change_password_mail_message( $pass_change_email, $user, $userdata ) {
    $subject   = "Your new subject";
    $message   = "<p>Test HTML email</p>";
    $headers[] = 'Content-Type: text/html';

    $pass_change_email['subject'] = $subject;
    $pass_change_email['message'] = $message;
    $pass_change_email['headers'] = $headers;

    return $pass_change_email;
}

add_filter( 'password_change_email', 'change_password_mail_message', 10, 3 );


function SearchFilter($query) {
   // If 's' request variable is set but empty
   if (isset($_GET['s']) && empty($_GET['s']) && $query->is_main_query()){
      $query->is_search = true;
      $query->is_home = false;
   }
   return $query;
}
add_filter('pre_get_posts','SearchFilter');



// customLoop() - Notas propuestas por la comunidad .
function notas_comunidad($atts, $content = null) {
    extract(shortcode_atts(array(
        'posts_per_page' => '4',
        "category" => '',
    ), $atts));
 
    global $wp_query,$paged,$post;
 
    $temp = $wp_query;
    
// Define query
  $query_args = array(
    'posts_per_page' => $posts_per_page,
     'paged' => false,
     'category_name'=> $category
  );


    $wp_query = new WP_Query($query_args);
    
 
 
    ob_start();
    ?>
 
   <div class="Participacion-Feed site-contentFullPage ">
    <?php require("AutorCoautor.php");?>
    <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
     
      <article >
     <a href="<?php the_permalink() ?>" rel="bookmark" >  <?php the_post_thumbnail( ); ?></a> 
<?php the_title('<a href="'.get_the_permalink().' " alt="'.get_the_title().'"><h2 class="Participacion-titleFeed">','</h2></a>' );?>
<div class="ParticipacionAutor"><?php  AutorCoautor();?></div> 
</article>
    <?php endwhile; ?>
   
 </div>

 
    <?php
    $wp_query = null; $wp_query = $temp;
 
    $content = ob_get_contents();
 
    ob_end_clean();
 
    return  $content;
}
 
add_shortcode("Notas-Comunidad", "notas_comunidad");






// customLoop() - Notas  ACCIONES.
function notas_acciones($atts, $content = null) {
    extract(shortcode_atts(array(
        'posts_per_page' => '4',
        "category" => '',
    ), $atts));
 
    global $wp_query,$paged,$post;
 
    $temp = $wp_query;
    
// Define query
  $query_args = array(
    'posts_per_page' => $posts_per_page,
     'paged' => false,
     'category_name'=> $category,
     'post_type' => 'acciones'
  );


    $wp_query = new WP_Query($query_args);
    
 
 
    ob_start();
    ?>
 
      <div class="site-contentFullPage">
       
   <div class="LoopAcciones">
    <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
         <article class="acciones">
          <div class="ActionButtonContent">
<?php the_content(); ?>
            </div>
       </article>
    <?php endwhile; ?>
     </div>
    </div>

 
    <?php
    $wp_query = null; $wp_query = $temp;
 
    $content = ob_get_contents();
 
    ob_end_clean();
 
    return  $content;
}
 
add_shortcode("Notas-Acciones", "notas_acciones");

//notas miembros/

// customLoop() - Notas MIEMBROS .
function notas_miembros($atts, $content = null) {
    extract(shortcode_atts(array(
        'posts_per_page' => '4',
        "category" => '',
    ), $atts));
 
    global $wp_query,$paged,$post;
 
    $temp = $wp_query;
    
// Define query
  $query_args = array(
    'posts_per_page' => $posts_per_page,
     'paged' => false,
     'category_name'=> $category
  );


    $wp_query = new WP_Query($query_args);
    
 
 
    ob_start();
    ?>
 
   <div class="MiembrosPeriodismo-Feed site-contentFullPage ">
    <div class="swiper-container">
    <div class="swiper-wrapper">
    
    <?php 
$cuentaPost=1;
    while ($wp_query->have_posts()) : $wp_query->the_post(); 

echo "<article class='swiper-slide'>";
  

 ?>
     
 <a href="<?php the_permalink() ?>" rel="bookmark" >  <?php the_post_thumbnail( ); ?></a> 
     <?php 
     echo "<div class='MiembrosPeriodismoTxt'><div class='MiembrosPeriodismoCat'>";
    //echo $category ;
     $categoriess = get_the_category();

      echo $categoriess[0]->cat_name;

the_title('</div><a href="'.get_the_permalink().' " alt="'.get_the_title().'" class="MiembrosPeriodismo-titleFeed">','</a></div>' );

   echo "</article>";

 ?>

    <?php endwhile; ?>
  </div><!--cierra swaper wrapper-->

  <div class="swiper-button-next"></div>
  <div class="swiper-button-prev"></div>
  
  </div><!--cierra swiper container-->
 </div><!--cierra periodismos content full-->

 
    <?php
    $wp_query = null; $wp_query = $temp;
 
    $content = ob_get_contents();
 
    ob_end_clean();
 
    return  $content;
}
 
add_shortcode("Notas-Miembros", "notas_miembros");

//fin notas miembros///




//notas miembros/

// customLoop() - Notas MIEMBROS Comentarios .
function notas_comentarios($atts, $content = null) {
    extract(shortcode_atts(array(
        'posts_per_page' => '10',
        "category" => '',
    ), $atts));
 
    global $wp_query,$paged,$post;
 
    $temp = $wp_query;
    
// Define query
  $query_args = array(
    'posts_per_page' => $posts_per_page,
     'paged' => false,
     'category_name'=> $category
  );


    $wp_query = new WP_Query($query_args);
    
 
 
    ob_start();
    ?>
 
   <div class="slideshow-container">
    
    <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
     
      <div class="mySlides fade BoxOpinionUsuario"> <div class="BoxOpinionUsuarioImgTxt">
    
 <div class="OpinionUsuario "><?php the_title('<q >','</q>' );?></div>
<div class="UsuarioBoxName "> <?php the_content(); ?></div>
    
    <?php if ( has_post_thumbnail() ) {
      echo ('<div class="mgUsuario">  ');
  the_post_thumbnail();
  echo ('</div>');
} 

?>
</div> </div>
    <?php endwhile; ?>
   
 </div>

<div style="text-align:center">
<?php 
$cuentaslide=1;
while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

  <span class="dot" onclick="currentSlide(<?=$cuentaslide ?>)"></span> 
 

<?php 
$cuentaslide++;
endwhile; ?>
 </div>
    <?php
    $wp_query = null; $wp_query = $temp;
 
    $content = ob_get_contents();
 
    ob_end_clean();
 
    return  $content;
}
 
add_shortcode("Notas-Comentarios", "notas_comentarios");

//fin notas miembros///



//notas miembros/

// customLoop() - Notas MIEMBROS tips.
function notas_beneficios($atts, $content = null) {
    extract(shortcode_atts(array(
        'posts_per_page' => '30',
        "category" => '',
    ), $atts));
 
    global $wp_query,$paged,$post;
 
    $temp = $wp_query;
    
// Define query
  $query_args = array(
    'posts_per_page' => $posts_per_page,
     'paged' => false,
     'category_name'=> $category
  );


    $wp_query = new WP_Query($query_args);
    
 
 
    ob_start();
    ?>
 
   <div class="SectionBeneficios">
   
    <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
     
      <div class="BoxBeneficios">
      <div class="BeneficiosThumb">
  <div class="TipsFeed">
<?php 
 the_content(); 
echo "</div>";
   the_post_thumbnail( ); 
echo "</div>";
if ( ! has_excerpt() ) {
    echo '';
} else { 
      echo"<div class='BoxBeneficiosDivHover'>"; 
      the_excerpt(); 
      echo"</div> ";
}

 ?>

</div>
    <?php endwhile; ?>
   
 </div>


    <?php
    $wp_query = null; $wp_query = $temp;
 
    $content = ob_get_contents();
 
    ob_end_clean();
 
    return  $content;
}
 
add_shortcode("Notas-Beneficios", "notas_beneficios");

//fin notas miembros///



/*** Countdown Timer Shortcode ***/
function content_countdown($atts, $content = null){
  extract(shortcode_atts(array(
     'month' => '',
     'day'   => '',
     'year'  => ''
    ), $atts));
    $remain = ceil((mktime( 0,0,0,(int)$month,(int)$day,(int)$year) - time())/86400);
    if( $remain > 1 ){
        return $daysremain = "<div class=\"eventClose\"><strong>CIERRA EN $remain DÍAS</strong></div>";
    }else if($remain == 1 ){
    return $daysremain = "<div class=\"eventClose\"><strong>CIERRA EN $remain DÍA</strong></div>";
    }else{
        return "<div class=\"eventClose\"><strong>$content</strong></div>";
    }
}
add_shortcode('cdt', 'content_countdown');



add_filter('walker_nav_menu_start_el','dcms_agregar_descripcion',10,4);

function dcms_agregar_descripcion( $item_output, $item, $depth, $args ){
    if ( ! empty($item->description)){
        return str_replace( '</h2>', '</h2><p>'.$item->description.'</p>', $item_output );        
    }
    return $item_output;
}


//remover notas relacionadas de jetpack del pie de los posteos

function jetpackme_remove_rp() {
  if ( class_exists( 'Jetpack_RelatedPosts' ) ) {
      $jprp = Jetpack_RelatedPosts::init();
      $callback = array( $jprp, 'filter_add_target_to_dom' );

      remove_filter( 'the_content', $callback, 40 );
  }
}
add_action( 'wp', 'jetpackme_remove_rp', 20 );
