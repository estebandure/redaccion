<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package redaccion
 */

get_header();
?>
<div id="content" class="Content404"> <!--ABRE TOP CONTENT--><!--cierra en el footer-->
	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php esc_html_e( 'No se ha encontrado la página que buscás.', 'redaccion' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content" id="search-nav">
					<p><?php esc_html_e( 'Realizar búsqueda por palabra clave', 'redaccion' ); ?></p>

					<?php
					get_search_form();

					
					?>

					

				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
