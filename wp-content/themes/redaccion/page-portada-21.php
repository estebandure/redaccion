<?php
/* Template Name: PageHome 2021*/
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

get_header();
?>

	<main id="primaryHome" class="site-main-home">


<?php require("AutorCoautor-portada21.php");?>
<?php require("AutorCoautor-portada-sinLink21.php");?>
    <?php
$notID=[];
$elposteoID=0;
?>
 

<section class="">
	<div class="PortadaFirstWrapper">
	<!--PRIMERA NOTA-->
	<div class="PortadaFWCol1 PortadaFW">
	<?php
			/*--- Create a sticky loop ---*/
			//$sticky = get_option( 'sticky_posts' );
			//print_r($sticky);

			// These args will return only one sticky post
			$html="";
			$stickyArgs = array(
				'cat'=>[4619, -7266],
				'post__in'  => get_option( 'sticky_posts' ),
				// remove these to return all sticky posts
				'posts_per_page' => 1,
				'ignore_sticky_posts' => 0,
				'post__not_in' =>$notID
			);

			// create your query
			$stickyQuery = new WP_Query( $stickyArgs );

				if ( $stickyQuery->have_posts() ) {
					$stickyQuery->the_post();
					$categories = get_the_category();
					$categoriaActual = getCategorieNOportada();
					$category_link = get_category_link($categoriaActual );
					$category_Name = $categoriaActual->cat_name;
					$category_Slug = $categoriaActual->slug;
					$featured_img_url = get_the_post_thumbnail_url();
					$featured_img = get_the_post_thumbnail();
					$thumb_id = get_post_thumbnail_id();
					$image_alt = get_post_meta($thumb_id , '_wp_attachment_image_alt', true); 
					$bajada=get_the_excerpt();
					//echo("sticky <br>");
					$html.= '<a href="' . get_permalink( ) . '" class="ImgApertura"  title="' . get_the_title() . ' " >';
				   	$html.= '<img src="'.$featured_img_url.'" alt="'.$image_alt .'" />';
					$html.= '</a>';
					   if ( $caption = get_post( get_post_thumbnail_id() )->post_excerpt ) :
					$html.= "<p class='caption'>";
					$html.= get_the_post_thumbnail_caption();
					$html.= " </p>";
						endif;
				
					$html.='<span class="category">';
					$html.= '<a class="" href="' . $category_link  . ' " >';
					$html.=$category_Name ;
					$html.= '</a>';
				  	$html.='</span>';

					  if(get_field('Titulohome')) {
						$TituloHome = get_field('Titulohome');
						$html.='<h2 class="TitPortada"><a href="' . esc_url( get_permalink() ) . '" >'.$TituloHome. '</a></h2>';
						}else {
						$html.=the_title( '<h2 class="TitPortada"><a href="' . esc_url( get_permalink() ) . '"  >', '</a></h2>' , FALSE);
						}
						if(get_field('BajadaHome')) {
							$bajadaHome = get_field('BajadaHome');
							$html.='<div class="BajadaPortada">' .$bajadaHome .'</div>';
							}else {
							$html.='<div class="BajadaPortada">' .$bajada.'</div>';
							}
					$html.=AutorCoautor(1);
					$html.='<a href="' . esc_url( get_permalink() ) . '" class="CTAarrow"> <span class="material-icons">chevron_right</span></a>';
					array_push($notID,get_the_ID());
					wp_reset_postdata();
				
				} else{

					

						$queryfeed =  new WP_Query( ['cat'=>[4619, -7266], 'post_type' => ['post'], 'posts_per_page' => 1, 'order' => 'DESC' ,'orderby' => 'date','post__not_in' =>$notID] );

						while (  $queryfeed->have_posts() ) :
						$queryfeed->the_post();
						//content
						
						$categories = get_the_category();
						$categoriaActual = getCategorieNOportada();
						$category_link = get_category_link($categoriaActual );
						$category_Name = $categoriaActual->cat_name;
						$category_Slug = $categoriaActual->slug;
						$featured_img_url = get_the_post_thumbnail_url();
						$featured_img = get_the_post_thumbnail();
						$thumb_id = get_post_thumbnail_id();
						$image_alt = get_post_meta($thumb_id , '_wp_attachment_image_alt', true); 
						$bajada=get_the_excerpt();
						
						//echo("otra nota <br>");
						$html.= '<a href="' . get_permalink( ) . '" class="ImgApertura" title="' . get_the_title() . ' " >';
				   		$html.= '<img src="'.$featured_img_url.'" alt="'.$image_alt .'" />';
						$html.= '</a>';
						if ( $caption = get_post( get_post_thumbnail_id() )->post_excerpt ) :
							$html.= "<p class='caption'>";
							$html.= get_the_post_thumbnail_caption();
							$html.= " </p>";
								endif;
						$html.='<span class="category">';
						$html.= '<a class="" href="' . $category_link  . ' " >';
						$html.=$category_Name ;
						$html.= '</a>';
						$html.='</span>';
						if(get_field('Titulohome')) {
							$TituloHome = get_field('Titulohome');
							$html.='<h2 class="TitPortada"><a href="' . esc_url( get_permalink() ) . '">'.$TituloHome. '</a></h2>';
							}else {
							$html.=the_title( '<h2 class="TitPortada"><a href="' . esc_url( get_permalink() ) . '" >', '</a></h2>' , FALSE);
							}
							if(get_field('BajadaHome')) {
								$bajadaHome = get_field('BajadaHome');
								$html.='<div class="BajadaPortada">' .$bajadaHome .'</div>';
								}else {
								$html.='<div class="BajadaPortada">' .$bajada.'</div>';
								}
							
						
						$html.=AutorCoautor(1);
						$html.='<a href="' . esc_url( get_permalink() ) . '" class="CTAarrow"><span class="material-icons">chevron_right</span></a>';
						
						//termina content
						endwhile; // End of the loop.
						array_push($notID,get_the_ID());
						wp_reset_postdata();

						}
						echo $html;
			?>
			</div>
	<!--/PRIMERA NOTA-->
	<!--SEGUNTA NOTA-->
	<div class="PortadaFWCol2">
			<div class="PortadaFWCol2NOTA1 PortadaFW">
			<?php
			/*--- Create a sticky loop ---*/
			$html1="";
			$sticky1 = get_option( 'sticky_posts' );
			// These args will return only one sticky post
			$stickyArgs1 = array(
				
				'cat'=>6745, 
				'post__in'  => $sticky1,
				// remove these to return all sticky posts
				'posts_per_page' => 1,
				'ignore_sticky_posts' => 1,
				'post__not_in' =>$notID
			);

			// create your query
			$stickyQuery1 = new WP_Query( $stickyArgs1 );

				if ( $stickyQuery1->have_posts() ) {
					
					$stickyQuery1->the_post();
					$categories = get_the_category();
					$categoriaActual = getCategorieNOportada();
					$category_link = get_category_link($categoriaActual );
					$category_Name = $categoriaActual->cat_name;
					$category_Slug = $categoriaActual->slug;
					$featured_img_url = get_the_post_thumbnail_url();
					$featured_img = get_the_post_thumbnail();
					$thumb_id = get_post_thumbnail_id();
					$image_alt = get_post_meta($thumb_id , '_wp_attachment_image_alt', true); 
						
					//echo("sticky1 <br>");
					$html1.= '<a href="' . get_permalink( ) . '" class="ImgApertura" title="' . get_the_title() . ' " >';
				   	$html1.= '<img src="'.$featured_img_url.'" alt="'.$image_alt .'" />';
					$html1.= '</a>';
					
					$html1.='<span class="category">';
					$html1.= '<a class="" href="' . $category_link  . ' " >';
					$html1.=$category_Name ;
					$html1.= '</a>';
				  	$html1.='</span>';
					  if(get_field('Titulohome')) {
						$TituloHome = get_field('Titulohome');
						$html1.='<h2 class="TitPortadaSegunda"><a href="' . esc_url( get_permalink() ) . '" >'.$TituloHome. '</a></h2>';
						}else {
						$html1.=the_title( '<h2 class="TitPortadaSegunda"><a href="' . esc_url( get_permalink() ) . '">', '</a></h2>' , FALSE);
						}
					$html1.=AutorCoautor(1);
					$html1.='<a href="' . esc_url( get_permalink() ) . '" class="CTAarrow" > <span class="material-icons">chevron_right</span> </a>';
					array_push($notID,get_the_ID());
					wp_reset_postdata();
				
				} else{

					

						$queryfeed1 =  new WP_Query( ['cat '=>[6745, -7266],  'post_type' => ['post'], 'posts_per_page' => 1, 'order' => 'DESC' ,'orderby' => 'date','post__not_in' =>$sticky1] );

						while (  $queryfeed1->have_posts() ) :
						$queryfeed1->the_post();
						//content
						$categories = get_the_category();
						$categoriaActual = getCategorieNOportada();
						$category_link = get_category_link($categoriaActual );
						$category_Name = $categoriaActual->cat_name;
						$category_Slug = $categoriaActual->slug;
						$featured_img_url = get_the_post_thumbnail_url();
						$featured_img = get_the_post_thumbnail();
						$thumb_id = get_post_thumbnail_id();
						$image_alt = get_post_meta($thumb_id , '_wp_attachment_image_alt', true); 
						
						//echo("otra nota <br>");
						$html1.= '<a href="' . get_permalink( ) . '" class="ImgApertura" title="' . get_the_title() . ' " >';
				   		$html1.= '<img src="'.$featured_img_url.'" alt="'.$image_alt .'" />';
						$html1.= '</a>';
						
						$html1.='<span class="category">';
						$html1.= '<a class="" href="' . $category_link  . ' " >';
						$html1.=$category_Name ;
						$html1.= '</a>';
						$html1.='</span>';
						if(get_field('Titulohome')) {
							$TituloHome = get_field('Titulohome');
							$html1.='<h2 class="TitPortadaSegunda"><a href="' . esc_url( get_permalink() ) . '" >'.$TituloHome. '</a></h2>';
							}else {
							$html1.=the_title( '<h2 class="TitPortadaSegunda"><a href="' . esc_url( get_permalink() ) . '" >', '</a></h2>' , FALSE);
							}
						$html1.=AutorCoautor(1);
						$html1.='<a href="' . esc_url( get_permalink() ) . '" class="CTAarrow"> <span class="material-icons">chevron_right</span></a>';
						//termina content
						
						endwhile; // End of the loop.
						array_push($notID,get_the_ID());
						wp_reset_postdata();

						}
						echo $html1;
			?>
		   </div> <!--cierra nota 1 de dos-->
		   <div class="PortadaFWCol2NOTA2 PortadaFW">
			<?php
			$html="";
			/*--- Create a sticky loop ---*/
			$sticky2 = get_option( 'sticky_posts' );
			//print_r($sticky);

			// These args will return only one sticky post
			$stickyArgs2 = array(
				'cat'=>[703,704,732,694,2879,4399,2652,788,-7485], 
				'post__in'  => $sticky2,
				// remove these to return all sticky posts
				'posts_per_page' => 1,
				'post__not_in' =>$notID,
				'ignore_sticky_posts' => 1
			);

			// create your query
			$stickyQuery2 = new WP_Query( $stickyArgs2 );

				if ( $stickyQuery2->have_posts() ) {
					$stickyQuery2->the_post();
			
					
					$categories = get_the_category();
					$categoriaActual = getCategorieNOportada();
					$category_link = get_category_link($categoriaActual );
					$category_Name = $categoriaActual->cat_name;
					$category_Slug = $categoriaActual->slug;
					$featured_img_url = get_the_post_thumbnail_url();
						$featured_img = get_the_post_thumbnail();
						$thumb_id = get_post_thumbnail_id();
						$image_alt = get_post_meta($thumb_id , '_wp_attachment_image_alt', true); 
						
						//echo("sticky1 <br>");
						$html.= '<a href="' . get_permalink( ) . '" class="ImgApertura" title="' . get_the_title() . ' " >';
				   		$html.= '<img src="'.$featured_img_url.'" alt="'.$image_alt .'" />';
						$html.= '</a>';
					
					$html.='<span class="category">';
					$html.= '<a class="" href="' . $category_link  . ' " >';
					$html.=$category_Name ;
					$html.= '</a>';
				  	$html.='</span>';
					  if ( get_post_meta( $post->ID, 'Titulohome', true ) ) {
						$TituloHome = get_post_meta($post->ID, "Titulohome", true);
						$html.='<h2 class="TitPortadaSegunda"><a href="' . esc_url( get_permalink() ) . '" >'.$TituloHome. '</a></h2>';
						}else {
						$html.=the_title( '<h2 class="TitPortadaSegunda"><a href="' . esc_url( get_permalink() ) . '" >', '</a></h2>' , FALSE);
						}
					$html.=AutorCoautor(1);
						$html.='<a href="' . esc_url( get_permalink() ) . '" class="CTAarrow"> <span class="material-icons">chevron_right</span> </a>';
					array_push($notID,get_the_ID());
					wp_reset_postdata();
				
				} else{

				

						$queryfeed2 =  new WP_Query( ['cat'=>[703,704,732,694,2879,4399,2652,788,-7485],  'post_type' => ['post'], 'posts_per_page' => 1, 'order' => 'DESC' ,'orderby' => 'date','post__not_in' =>$notID] );

						while (  $queryfeed2->have_posts() ) :
						$queryfeed2->the_post();
						//content
						$categories = get_the_category();
						$categoriaActual = getCategorieNOportada();
						$category_link = get_category_link($categoriaActual );
						$category_Name = $categoriaActual->cat_name;
						$category_Slug = $categoriaActual->slug;
						$featured_img_url = get_the_post_thumbnail_url();
						$featured_img = get_the_post_thumbnail();
						$thumb_id = get_post_thumbnail_id();
						$image_alt = get_post_meta($thumb_id , '_wp_attachment_image_alt', true); 
						
						//echo("otra nota <br>");
						$html.= '<a href="' . get_permalink( ) . '" class="ImgApertura" title="' . get_the_title() . ' " >';
				   		$html.= '<img src="'.$featured_img_url.'" alt="'.$image_alt .'" />';
						$html.= '</a>';
						
						$html.='<span class="category">';
						$html.= '<a class="" href="' . $category_link  . ' " >';
						$html.=$category_Name ;
						$html.= '</a>';
						$html.='</span>';
						if ( get_post_meta( $post->ID, 'Titulohome', true ) ) {
							$TituloHome = get_post_meta($post->ID, "Titulohome", true);
							$html.='<h2 class="TitPortadaSegunda"><a href="' . esc_url( get_permalink() ) . '" >'.$TituloHome. '</a></h2>';
							}else {
							$html.=the_title( '<h2 class="TitPortadaSegunda"><a href="' . esc_url( get_permalink() ) . '" >', '</a></h2>' , FALSE);
							}
						$html.=AutorCoautor(1);
							$html.='<a href="' . esc_url( get_permalink() ) . '" class="CTAarrow"> <span class="material-icons">chevron_right</span> </a>';
						//termina content
						array_push($notID,get_the_ID());
						endwhile; // End of the loop.
						wp_reset_postdata();

						}
						echo $html;
			?>
		   </div> <!--cierra nota 2 de dos-->
 </div>
	<!--/SEGUNTA NOTA-->
	<div class="PortadaFWCol3">
		<!--OPINION-->
	<div class="headerOpinion"></div>
	<div class="BoxOpinionWrapper">
		<?php
		// These args will return only one sticky post
		$stickyArgs3 = array(
				
			'category__in'=>[693], 
			'post__in'  => get_option( 'sticky_posts' ),
			// remove these to return all sticky posts
			'posts_per_page' => 1,
			'ignore_sticky_posts' => 0
		);

		// create your query
		$stickyQuery3 = new WP_Query( $stickyArgs3 );

			if ( $stickyQuery3->have_posts() ) {
				//echo("sticky");
				$html="";
				$stickyQuery3->the_post();
				
				$html.="<div class='BoxOpinion'>";
				//content
				$html.=the_title( '<a href="' . esc_url( get_permalink() ) . '"  class="TitOpinion" >', '</a>' , FALSE);
				$html.=AutorCoautor(1);
				array_push($notID,get_the_ID());
				$html.="</div>";
				
			
			
				$queryfeedOpinion4 =  new WP_Query( ['category__in'=>[693],  'post_type' => ['post'], 'posts_per_page' => 3,'post__not_in' =>$notID] );
					while (  $queryfeedOpinion4->have_posts() ) :
				$queryfeedOpinion4->the_post();
				$html.="<div class='BoxOpinion'>";
				//content
				$html.=the_title( '<a href="' . esc_url( get_permalink() ) . '" class="TitOpinion" >', '</a>' , FALSE);
				$html.=AutorCoautor(1);
				array_push($notID,get_the_ID());
				$html.="</div>";
				//termina content
				endwhile; // End of the loop.
				wp_reset_postdata();	
				echo $html;
			
			} else{
		
			$html="";
			
			$queryfeedOpinion =  new WP_Query( ['category__in'=>[693],  'post_type' => ['post'], 'posts_per_page' => 4] );

			while (  $queryfeedOpinion->have_posts() ) :
			$queryfeedOpinion->the_post();
			$html.="<div class='BoxOpinion'>";
			//content
			$html.=the_title( '<a href="' . esc_url( get_permalink() ) . '"  class="TitOpinion" >', '</a>' , FALSE);
			$html.=AutorCoautor(1);
			array_push($notID,get_the_ID());
			$html.="</div>";
			//termina content
			endwhile; // End of the loop.
			wp_reset_postdata();	
			echo $html;
			}
			?>	
	</div><!--/OPINION-->
	</div>
	
</div>
</section>

<section class="GPSNoticias SeparadorGris">
<div class="GPSNoticiasWrapper SeparadorGrisWrapper">
<div class='TitSegmento'>[ GPS DE NOTICIAS ]</div>
<div class="GPSNoticiasBoxWrapper-">
<div class="swiper-container GPSSwiper">
      <div class="swiper-wrapper">
	
		<?php
		$html="";
			//GPSNoticias
			$queryGPSNoticias =  new WP_Query( ['category__in'=>[7255],  'post_type' => ['post'], 'posts_per_page' => 1,'post__not_in' =>$notID] );

			while (  $queryGPSNoticias->have_posts() ) :
			$queryGPSNoticias->the_post();
		
			//content
			
			echo "<div class='Bloque1 swiper-slide'><a href=' /gps-de-noticias/' title='GPS de Noticias - Lo que tenés que saber antes de arrancar el día' target='_blank'>";
			echo get_field('gps-bloque1');
			echo "</a></div>";
			echo "<div class='Bloque2 swiper-slide'><a href=' /gps-de-noticias/' title='GPS de Noticias - Lo que tenés que saber antes de arrancar el día' target='_blank'>";
			echo get_field('gps-bloque2');
			echo "</div>";
			echo "<div class='Bloque3 swiper-slide'>";
			echo get_field('gps-bloque3');
			echo "</div>";
			echo "<div class='Bloque4 swiper-slide'><a href=' /gps-de-noticias/' title='GPS de Noticias - Lo que tenés que saber antes de arrancar el día' target='_blank'>";
			echo get_field('gps-bloque4');
			echo "</div>";
			echo "<div class='Bloque5 swiper-slide'><a href=' /gps-de-noticias/' title='GPS de Noticias - Lo que tenés que saber antes de arrancar el día' target='_blank'>";
			echo get_field('gps-bloque5');
			echo "</div>";
			
		
			if ( get_post_meta( $post->ID, 'gps-bloque6', true ) ) {
				echo "<div class='Bloque6 swiper-slide'><a href=' /gps-de-noticias/' title='GPS de Noticias - Lo que tenés que saber antes de arrancar el día' target='_blank'>";
				echo get_field('gps-bloque6');
				echo "</a></div>";
				}
			if ( get_post_meta( $post->ID, 'gps-bloque7', true ) ) {
			echo "<div class='Bloque7 swiper-slide'><a href=' /gps-de-noticias/' title='GPS de Noticias - Lo que tenés que saber antes de arrancar el día' target='_blank'>";
			echo get_field('gps-bloque7');
			echo "</a></div>";
				}
			if ( get_post_meta( $post->ID, 'gps-bloque8', true ) ) {
				echo "<div class='Bloque8 swiper-slide'><a href=' /gps-de-noticias/' title='GPS de Noticias - Lo que tenés que saber antes de arrancar el día' target='_blank'>";
				echo get_field('gps-bloque8');
				echo "</a></div>";
				}
			if ( get_post_meta( $post->ID, 'gps-bloque9', true ) ) {
				echo "<div class='Bloque9 swiper-slide'><a href=' /gps-de-noticias/' title='GPS de Noticias - Lo que tenés que saber antes de arrancar el día' target='_blank'>";
				echo get_field('gps-bloque9');
				echo "</a></div>";
				}
			if ( get_post_meta( $post->ID, 'gps-bloque10', true ) ) {
				echo "<div class='Bloque10 swiper-slide'><a href=' /gps-de-noticias/' title='GPS de Noticias - Lo que tenés que saber antes de arrancar el día' target='_blank'>";
				echo get_field('gps-bloque10');
				echo "</a></div>";
				}
			
		

			array_push($notID,get_the_ID());
			//termina content
			endwhile; // End of the loop.
			wp_reset_postdata();	
			echo $html;
			//GPSNoticias
			?>
	</div>
	
    
    </div>
	<div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>
</div>
<div class='GPSNoticiasSus'>
<div class="GPSportadaTxt">
<?php
//$time = time();
//echo the_time('g:i a')


?>
<?php
date_default_timezone_set('America/Argentina/Buenos_Aires');
$horas = date("G");
$hora  = (int) $horas;		

if (($hora >= 0) && ($hora <= 12)){
  
echo ('<img draggable="false" role="img" class="emoji" alt="📩" src="https://s.w.org/images/core/emoji/13.0.1/svg/1f4e9.svg"> Suscribite a la <a href="/gps-am/" title="Link a GPS AM" style="display: inline-block;border-bottom: 1px solid #000!important;">newsletter</a><strong > GPS AM </strong><img draggable="false" role="img" class="emoji" alt="☕" src="https://s.w.org/images/core/emoji/13.0.1/svg/2615.svg"> De Lunes a Viernes, lo que tenés que saber antes de arrancar el día.');
echo('</div><div class="GPSportadaForm">');
echo('<script class="campaign-16" src="https://www.redaccion.com.ar/api/v1/campania/migrated/16"></script>');
}else {
  
	echo ('<img draggable="false" role="img" class="emoji" alt="📩" src="https://s.w.org/images/core/emoji/13.0.1/svg/1f4e9.svg"> Suscribite a la <a href="/gps-pm/" title="Link a GPS PM" style="display: inline-block;border-bottom: 1px solid #000!important;">newsletter</a><strong > GPS PM </strong><img draggable="false" role="img" class="emoji" alt="☕" src="https://s.w.org/images/core/emoji/13.0.1/svg/1f449-1f3fd.svg"> De Lunes a Viernes, lo que tenés que saber para terminar tu día.');
	
	echo('</div><div class="GPSportadaForm">');
echo('<script class="campaign-20" src="https://www.redaccion.com.ar/api/v1/campania/migrated/20"></script>');
}
?>




</div>

</div>
</div>
</section>

<section class="ActualidadBanners">
<div class="ActualidadWrapper" >

		<?php
		$html="";
			//ActualidadBanners
			$queryActualidadBanners =  new WP_Query( ['cat'=>[4619,39,4642,33,31,4643,4641,693,34,32,1035, -7266],  'post_type' => ['post'], 'posts_per_page' => 3,'post__not_in' =>$notID] );

			while (  $queryActualidadBanners->have_posts() ) :
			$queryActualidadBanners->the_post();
			
			$category = get_the_category();
			//$category_name = $category[0]->cat_name;
			$categoriaActual = getCategorieNOportada();
			$category_link = get_category_link($categoriaActual );
			$category_Name = $categoriaActual->cat_name;	
			//$category_link = get_category_link($category[0] ); 
			$featured_img_url = get_the_post_thumbnail_url();
			$featured_img = get_the_post_thumbnail();
			$thumb_id = get_post_thumbnail_id();
			$image_alt = get_post_meta($thumb_id , '_wp_attachment_image_alt', true); 

			 
			
			 //$categories = get_the_category();
			 $html.="<div class='BoxActualidad'>";
			
				   $html.= '<a href="' . get_permalink( ) . '" title="' . get_the_title() . ' " >';
				   $html.= '<div class="Rd_img" />';
				   $html.= '<img src="'.$featured_img_url.'" alt="'.$image_alt .'" />';
				   $html.="</div>";
				   $html.= '<div style="display:block;height:20px;"></div>';
				  
				   $html.= '<span class="category">';
				   $html.=  $category_Name;
				   $html.= '</span>';
			 
			 
					$html.='<div class="">';
					if ( get_post_meta( $post->ID, 'Titulohome', true ) ) {
					$TituloHome = get_post_meta($post->ID, "Titulohome", true);
					$html.= '<h2>'.$TituloHome.'</h2>' ;
					}else {
					$html.=the_title('<h2>','</h2>', FALSE);
					}
					$html.="</div>";
				

					$html.="<div class='Boxautor'>";
					$html.=AutorCoautorSL(1);
					$html.="</div>";

					$html.='<div class="CTAarrow"> <span class="material-icons">chevron_right</span></div>';

					$html.='</a>';
					$html.="</div>";
			   
				array_push($notID,get_the_ID());
			 
			
			//termina content
			endwhile; // End of the loop.
			wp_reset_postdata();	
			echo $html;
			//ActualidadBanners
			?>
			<div class="BoxActualidadBanners">
			<div class="BoxBanner1">
			 <?php if(get_field('banner1_news'))
				{
					$Banner1 = get_field('banner1_news');
					$Banner11 = (int) $Banner1;					
					if(function_exists('the_ad')) the_ad($Banner11);
				}
           		?>
			</div>
			<div class="BoxBanner2">
			<?php if(get_field('banner2_news'))
				{
					$Banner2 = get_field('banner2_news');
					$Banner22 = (int) $Banner2;					
					if(function_exists('the_ad')) the_ad($Banner22);
				}
           		?>
			</div>
			</div>


	
	
</div>
</section>

<!--newsletters--->

<section class="NewslettersSection NWSectionBG">
<div class="NewslettersWrapper" >
<div class="NewslettersBoxWrapper">

<?php      

// The Query -notas

$html='';
$queryNewsBanners =  new WP_Query( ['category__in'=>[703,704,732,694,2879,4399,2652,788,-7485],  'post_type' => ['post'], 'posts_per_page' => 6,'post__not_in' =>$notID] );

while ($queryNewsBanners->have_posts() ) :
	$queryNewsBanners->the_post();
   
    
    $category = get_the_category();
    $categoryName=$category[0]->cat_name;
    $category_link = get_category_link($category[0] );
	$featured_img_url = get_the_post_thumbnail_url();
	$featured_img = get_the_post_thumbnail();
	$thumb_id = get_post_thumbnail_id();
	$image_alt = get_post_meta($thumb_id , '_wp_attachment_image_alt', true); 
   
	
	$html.='<div class="NewslettersBox">';
	$html.= '<a href="' . get_permalink( ) . '" title="' . get_the_title() . ' " >';
	$html.= '<img src="'.$featured_img_url.'" alt="'.$image_alt .'" />';
	

	$html.= '<span class="category">';
	//$html.= '<a href="'. $category_link .' " title="Categoría '.$categoryName  .'" style="text-transform:uppercase;">'. $categoryName  .'</a>';
	$html.= $categoryName ;
	$html.= '</span>';


	$html.='<div class="">';
	if ( get_post_meta( $post->ID, 'Titulohome', true ) ) {
	$TituloHome = get_post_meta($post->ID, "Titulohome", true);
	$html.= '<h2>'.$TituloHome.'</h2>' ;
	}else {
	$html.=the_title('<h2>','</h2>', FALSE);
	}
	$html.="</div>";

	$html.="<div class='Boxautor'>";
	$html.=AutorCoautorSL(1);
	$html.="</div>";

	$html.='<div class="CTAarrow"> <span class="material-icons">chevron_right</span></div>';
	$html.='</a>';
					
	$html.='</div>';
  	array_push($notID,get_the_ID());
	endwhile; // End of the loop.
	wp_reset_postdata();	

	echo $html;
?>
<!--/FIN Box wrapper-->


			</div>
			
			<div class="BoxActualidadBanners">
			<div class="BoxParticipaCampana">
           <?php the_field('campana_participa'); ?>
			</div>
           <?php if(get_field('grupo_rotacion'))
				{
					$Banner11 = get_field('grupo_rotacion');
					
					$Banner1 = (int) $Banner11;					
					//if(function_exists('the_ad')) the_ad($Banner11);
					if(function_exists('get_ad_group')) the_ad_group($Banner11); 
				}
           ?>
<?php //if(function_exists('the_ad_group')) the_ad_group(5268); ?>

			</div>


	
	
</div>
</section>
<!--/FIN NEWSLETTERS-->
<section class="EspecialesNotas SeparadorGris">
<div class='EspecialesWrapperGroup'>

<div class='TitSegmento'>[ ESPECIALES ]</div>
<div class="EspecialesWrapper SeparadorGrisWrapper">

		<?php
		$html="";
			//ActualidadBanners
			if(get_field('id_especiales'))
				{
					$postIdEspeciales = get_field('id_especiales');	
				}
			$postIdFull = array_map('intval', explode(',', $postIdEspeciales));
			$queryEspeciales =  new WP_Query( ['post__in' => $postIdFull, 'ignore_sticky_posts' => 1, 'post_type' => [ 'post', 'page'], 'posts_per_page' => 3,'post__not_in' =>$notID] );


			while (  $queryEspeciales->have_posts() ) :

			$queryEspeciales->the_post();
			$featured_img_url = get_the_post_thumbnail_url();
			$featured_img = get_the_post_thumbnail();
			$thumb_id = get_post_thumbnail_id();
			$image_alt = get_post_meta($thumb_id , '_wp_attachment_image_alt', true); 
			
			$html.="<div class='BoxEspeciales'>";
				
				if(get_field('link_externo'))
				{
					
					$html.= '<a href="' .  get_field('link_externo') . '" title="' . get_the_title() . ' " target="_blank">';
				}else{
					$html.= '<a href="' . get_permalink( ) . '" title="' . get_the_title() . ' " >';
				}

			$html.= '<img src="'.$featured_img_url.'" alt="'.$image_alt .'" />';
			$html.='</a>';
			
			$html.='</div>';

		endwhile; // End of the loop.
		wp_reset_postdata();	
		echo $html;
		//ActualidadBanners
		?>
	

</div>
</div>
</section>



<!--ACTUALIDAD 4 NOTAS-->
<section class="ActualidadNotas">
<div class="ActualidadWrapper act-x-cuatro" >

		<?php
		$html="";
			//ActualidadBanners
			$queryActualidadBanners =  new WP_Query( ['cat'=>[39,4642,33,31,4643,4641,693,34,32,1035, -7266],  'post_type' => ['post'], 'posts_per_page' => 4,'post__not_in' =>$notID] );

			while (  $queryActualidadBanners->have_posts() ) :
			$queryActualidadBanners->the_post();
			
			$category = get_the_category();
			$category_name = $category[0]->cat_name;
			$category_link = get_category_link($category[0] ); 
			$featured_img_url = get_the_post_thumbnail_url();
			$featured_img = get_the_post_thumbnail();
			$thumb_id = get_post_thumbnail_id();
			$image_alt = get_post_meta($thumb_id , '_wp_attachment_image_alt', true); 
			
			
			 //$categories = get_the_category();
			 $html.="<div class='BoxActualidad'>";
			
				   $html.= '<a href="' . get_permalink( ) . '" title="' . get_the_title() . ' " >';
				   $html.= '<div class="Rd_img" />';
				   $html.= '<img src="'.$featured_img_url.'" alt="'.$image_alt .'" />';
				   $html.="</div>";
				   $html.= '<div style="display:block;height:20px;"></div>';
				  
				   $html.= '<span class="category">';
				   $html.=  $category_name ;
				   $html.= '</span>';
			 
			 
					$html.='<div class="">';
					if ( get_post_meta( $post->ID, 'Titulohome', true ) ) {
					$TituloHome = get_post_meta($post->ID, "Titulohome", true);
					$html.= '<h2>'.$TituloHome.'</h2>' ;
					}else {
					$html.=the_title('<h2>','</h2>', FALSE);
					}
					$html.="</div>";
				

					$html.="<div class='Boxautor'>";
					$html.=AutorCoautorSL(1);
					$html.="</div>";

					$html.='<div class="CTAarrow"> <span class="material-icons">chevron_right</span></div>';

					$html.='</a>';
					$html.="</div>";
			   
				
			   
				array_push($notID,get_the_ID());
			 
			
			//termina content
			endwhile; // End of the loop.
			wp_reset_postdata();	
			echo $html;
			//ActualidadBanners
			?>
		
	
	
</div>
</section>
<!--/cierra ACTUALIDAD 4 NOTAS-->

<!--sumate a la comunidad-->
<section class="SeparadorAmarillo">
<div class='SeparadorAmarilloWrapper'>

<div class='TitSegmento'>[ SUMATE A LA COMUNIDAD ]</div>
<div class="FullBlockMiembros ">



		<?php
		$querySumate =  new WP_Query( ['category__in'=>[1828],  'post_type' => ['post'], 'posts_per_page' => 1] );

			while (  $querySumate->have_posts() ) :
			$querySumate->the_post();
			echo '<div class="FullBlockMiembrosBox1 ">';
            the_title( '<h2 class="SideBar-title">', '</h2>' );
			 echo the_excerpt();	 
			 echo '</div><div class="FullBlockMiembrosBox2 ">';
			the_content(); 
			echo '<div class="CNboxSp">';
			if ( is_user_logged_in() ) {
   				
			} else {
	    	echo '<a class="rv4htx rv4hlic" href="https://bit.ly/338WcTR"><span class="rv4hlibm">QUIERO SER MIEMBRO</span></a>';
			
			}
		
			
			echo '</div>';
			echo '</div>';
			wp_reset_postdata();
        endwhile; // End of the loop.
      
        ?>
</div>

</div>
</section>
<!--/sumate a la comunidad-->

<!--ESPECIALES IG 4 NOTAS-->
<section class="IGNotas">
<div class="IGWrapper" >

		<?php
		$html="";
			//ActualidadBanners
			$queryActualidadBanners =  new WP_Query( ['category__in'=>[7266],  'post_type' => ['post'], 'posts_per_page' => 4,'post__not_in' =>$notID] );

			while (  $queryActualidadBanners->have_posts() ) :
			$queryActualidadBanners->the_post();
			
			$category = get_the_category();
			$category_name = $category[0]->cat_name;
			$category_link = get_category_link($category[0] ); 
			$featured_img_url = get_the_post_thumbnail_url();
			$featured_img = get_the_post_thumbnail();
			$thumb_id = get_post_thumbnail_id();
			$image_alt = get_post_meta($thumb_id , '_wp_attachment_image_alt', true); 
			$bajada = get_the_excerpt();
			
			
			 //$categories = get_the_category();
			 $html.="<div class='BoxIG'>";
			
				$html.= '<a href="' . get_permalink( ) . '" title="' . get_the_title() . ' " >';
				$html.= '<img src="'.$featured_img_url.'" alt="'.$image_alt .'" />';
				  
				//$html.='<div class="BajadaIG">' .get_excerpt(230) .'</div>';
				$html.='<div class="BajadaIG">' . $bajada .'</div>';

				$html.="<div class='Boxautor'>";
				$html.=AutorCoautorSL(1);
				$html.="</div>";
				$html.='<div class="CTAarrow"> <span class="material-icons">chevron_right</span></div>';

				$html.='</a>';
				$html.="</div>";
			   
				array_push($notID,get_the_ID());
			 
			
			//termina content
			endwhile; // End of the loop.
			wp_reset_postdata();	
			echo $html;
			//ActualidadBanners
			?>
		
	
	
</div>
</section>
<!--/cierra ESPECIALES IG 4 NOTAS-->

<!--la campaña del mes-->
<section class="Campania SeparadorGris">
<div class="CampaniaWrapper SeparadorGrisWrapper">
<div class='TitSegmento'>[ LA CAMPAÑA DEL MES ]</div>
<div class="CampaniaBoxWrapper">
		<?php
		$html="";
			//la campaña del mes
			$queryGPSNoticias =  new WP_Query( ['category__in'=>[4678],  'post_type' => ['post'], 'posts_per_page' => 1,'post__not_in' =>$notID] );

			while (  $queryGPSNoticias->have_posts() ) :
			$queryGPSNoticias->the_post();
			$featured_img_url = get_the_post_thumbnail_url();
			$featured_img = get_the_post_thumbnail();
			$thumb_id = get_post_thumbnail_id();
			$image_alt = get_post_meta($thumb_id , '_wp_attachment_image_alt', true); 
			$LinkCampaniaMes = get_field('LinkCampaniaMes');
			//content
			echo "<div class='CampaniaBox1 CampaniaBox'>";
			
			echo '<div class="Campania_img" />';
			echo'<img src="'.$featured_img_url.'" alt="'.$image_alt .'" />';
			echo "</div>";
			echo "</div>";

			echo "<div class='CampaniaBox2 CampaniaBox'>";
			echo '<a href="' .  $LinkCampaniaMes . '" title="' . get_the_title() . ' " >';
					if ( get_post_meta( $post->ID, 'Titulohome', true ) ) {
					$TituloHome = get_post_meta($post->ID, "Titulohome", true);
					echo'<h2>'.$TituloHome.'</h2>' ;
					}else {
					echo the_title('<h2>','</h2>', FALSE);
					}
			
			echo the_excerpt();	
			echo "</a>";
			echo "</div>";

			echo "<div class='CampaniaBox3 CampaniaBox'>";
			echo the_content(); 
			echo '<div class="CNboxSp">';
			
			echo '<a href="'. $LinkCampaniaMes .'" class="NombreHeader" title="Ingreso de Miembros"><span>CONOCÉ LAS INICIATIVAS</span></a>';
			
			echo '</div>';
			
			echo "</div>";

			array_push($notID,get_the_ID());
			//termina content
				wp_reset_postdata();
			endwhile; // End of the loop.
				
			//la campaña del mes
			?>
	
	
</div>

</div>
</section>
<!--/la campaña del mes-->




<!--MAS ACTUALIDAD--->

<section class="NewslettersSection">
<div class="NewslettersWrapper" >
<div class="NewslettersBoxWrapper" >

<?php      

// The Query -notas

$html='';
$queryNewsBanners2 =  new WP_Query( ['category__in'=>[703,704,732,694,2879,4399,2652,788],  'post_type' => ['post'], 'posts_per_page' => 6,'post__not_in' =>$notID] );

while ($queryNewsBanners2->have_posts() ) :
	$queryNewsBanners2->the_post();
   
    
    $category = get_the_category();
    $categoryName=$category[0]->cat_name;
    $category_link = get_category_link($category[0] );
	$featured_img_url = get_the_post_thumbnail_url();
	$featured_img = get_the_post_thumbnail();
	$thumb_id = get_post_thumbnail_id();
	$image_alt = get_post_meta($thumb_id , '_wp_attachment_image_alt', true); 
   
	
	$html.='<div class="NewslettersBox">';
	$html.= '<a href="' . get_permalink( ) . '" title="' . get_the_title() . ' " >';
	$html.= '<img src="'.$featured_img_url.'" alt="'.$image_alt .'" />';
	

	$html.= '<span class="category">';
	//$html.= '<a href="'. $category_link .' " title="Categoría '.$categoryName  .'" style="text-transform:uppercase;">'. $categoryName  .'</a>';
	$html.= $categoryName ;
	$html.= '</span>';


	$html.='<div class="">';
	if ( get_post_meta( $post->ID, 'Titulohome', true ) ) {
	$TituloHome = get_post_meta($post->ID, "Titulohome", true);
	$html.= '<h2>'.$TituloHome.'</h2>' ;
	}else {
	$html.=the_title('<h2>','</h2>', FALSE);
	}
	$html.="</div>";

	$html.="<div class='Boxautor'>";
	$html.=AutorCoautorSL(1);
	$html.="</div>";

	$html.='<div class="CTAarrow"> <span class="material-icons">chevron_right</span></div>';
	$html.='</a>';
					
	$html.='</div>';
  	array_push($notID,get_the_ID());
	endwhile; // End of the loop.
	wp_reset_postdata();	

	echo $html;
?>
<!--/FIN Box wrapper-->


			</div>
			<div class="BoxActualidadBanners">
			<div class="BoxBanner1">
			 <?php if(get_field('banner_5'))
				{
					$Banner5 = get_field('banner_5');
					$Banner55 = (int) $Banner5;					
					if(function_exists('the_ad')) the_ad($Banner55);
				}
           		?>
           	</div>
           	<div class="BoxBanner2">
           		 <?php if(get_field('banner_6'))
				{
					$Banner6 = get_field('banner_6');
					$Banner66 = (int) $Banner6;					
					if(function_exists('the_ad')) the_ad($Banner66);
				}
           		?>
           	</div>
           		<div class="BoxBanner3">
           		 <?php if(get_field('banner_7'))
				{
					$Banner7 = get_field('banner_7');
					$Banner77 = (int) $Banner7;					
					if(function_exists('the_ad')) the_ad($Banner77);
				}
           		?>
           	</div>
			</div>


</div>
</section>
<!--/FIN MAS NEWSLETTERS /ACTUALIDAD??-->




<!--FOCO PODCAST-->
<section class="FocoSegmento SeparadorGris">
<div class="FocoWrapper SeparadorGrisWrapper">
<div class='TitSegmento'>[ FOCO PODCAST ]</div>
<div class="FocoBoxWrapper">
		<?php
		$html="";
			//FOCO PODCAST
			$queryGPSNoticias =  new WP_Query( ['category__in'=>[7265],  'post_type' => ['post'], 'posts_per_page' => 2] );

			while (  $queryGPSNoticias->have_posts() ) :
			$queryGPSNoticias->the_post();
			//content
			echo "<div class='Bloque1Podcast'>";
			echo the_content(); 
			if ( get_post_meta( $post->ID, 'link_a_spotify', true ) ) {
			$link_a_spotify = get_post_meta($post->ID, "link_a_spotify", true);
			}else {
			$link_a_spotify = get_permalink();
			}
			echo '<a href="' . $link_a_spotify  . '" title="' . get_the_title() . ' " >';
			echo the_excerpt();	
			echo "</a>";

			echo "</div>";
			array_push($notID,get_the_ID());
			//termina content
			endwhile; // End of the loop.
			wp_reset_postdata();	

			//FOCO PODCAST
			?>
	
	
</div>
<div class="GPSportadaTxt" >
	<?php
	$TituloHome = get_field('texto_foco');
	?>
<img draggable="false" role="img" class="emoji" alt="📩" src="https://s.w.org/images/core/emoji/13.0.1/svg/1f4e9.svg"> Seguí a <a href="https://open.spotify.com/show/7vfLaLdiJoq2qCuy8O2OuW?si=iWQjvFx9SKW1XZZJWJq3dA&nd=1" target="_blank" title="Link a GPS AM" style="display: inline-block;border-bottom: 1px solid #000!important;"><strong >FOCO en Spotify</strong></a>.<?php echo $TituloHome; ?>
</div>
</div>
</section>
<!--/FOCO PODCASTS-->

<!--ACTUALIDAD 4 NOTAS-->
<section class="ActualidadNotas">
<div class="ActualidadWrapper act-x-cuatro" >

		<?php
		$html="";
			//ActualidadBanners
			$queryActualidadBanners =  new WP_Query( ['category__in'=>[4619, -7266],  'post_type' => ['post'], 'posts_per_page' => 4,'post__not_in' =>$notID] );

			while (  $queryActualidadBanners->have_posts() ) :
			$queryActualidadBanners->the_post();
			
			$category = get_the_category();
			//$category_name = $category[0]->cat_name;
			//$category_link = get_category_link($category[0] ); 
			$categoriaActual = getCategorieNOportada();
			$category_link = get_category_link($categoriaActual );
			$category_Name = $categoriaActual->cat_name;
			$featured_img_url = get_the_post_thumbnail_url();
			$featured_img = get_the_post_thumbnail();
			$thumb_id = get_post_thumbnail_id();
			$image_alt = get_post_meta($thumb_id , '_wp_attachment_image_alt', true); 
			
			
			 //$categories = get_the_category();
			 $html.="<div class='BoxActualidad'>";
			
			 $html.= '<a href="' . get_permalink( ) . '" title="' . get_the_title() . ' " >';
			 $html.= '<div class="Rd_img" />';
			 $html.= '<img src="'.$featured_img_url.'" alt="'.$image_alt .'" />';
			 $html.="</div>";
			 $html.= '<div style="display:block;height:20px;"></div>';
			
			 $html.= '<span class="category">';
			 $html.=  $category_Name;
			 $html.= '</span>';
	   
	   
			  $html.='<div class="">';
			  if ( get_post_meta( $post->ID, 'Titulohome', true ) ) {
			  $TituloHome = get_post_meta($post->ID, "Titulohome", true);
			  $html.= '<h2>'.$TituloHome.'</h2>' ;
			  }else {
			  $html.=the_title('<h2>','</h2>', FALSE);
			  }
			  $html.="</div>";
		  

			  $html.="<div class='Boxautor'>";
			  $html.=AutorCoautorSL(1);
			  $html.="</div>";

			  $html.='<div class="CTAarrow"> <span class="material-icons">chevron_right</span></div>';

			  $html.='</a>';
			  $html.="</div>";
		 
		
			   
				array_push($notID,get_the_ID());
			 
			
			//termina content
			endwhile; // End of the loop.
			wp_reset_postdata();	
			echo $html;
			//ActualidadBanners
			?>
		
	
	
</div>
</section>
<!--/cierra ACTUALIDAD 4 NOTAS-->


		<?php
		while ( have_posts() ) :
			the_post();
			the_content();
			endwhile; // End of the loop.
		?>

	</main><!-- #main -->
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<script>

if (screen.width >= 601) {

	var swiper = new Swiper(".GPSSwiper", {
        slidesPerView: 4,
        spaceBetween: 30,
		navigation: {
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev",
        },
      });
}
if (screen.width <= 600) {

	var swiper = new Swiper(".GPSSwiper", {
        slidesPerView: 1,
        spaceBetween: 30,
		navigation: {
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev",
        },
      });
}

     
 </script>
<?php

get_footer();

function tiene_category($cats, $catid) {

	foreach ($cats as $cat) {
	  if ($cat->term_id == $catid) {
		return true;
	  }
  
	}
	return false;
  }
