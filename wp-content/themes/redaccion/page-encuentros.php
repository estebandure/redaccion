<?php
/* Template Name: PageEncuentros */
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

get_header();
?>


<div class="ContentEncuentros">
	<div class="MiembrosTitle">
		
		 <?php
      the_title( '<h1 class="Mono-title">', '</h1>' ); 
      ?>
	</div>

</div>



<div id="content" class="site-content">
	<div id="" class="content-area">
		<main id="main" class="site-main EncuentrosMain">

    

		<?php
		 query_posts('post_type=EncuentroPost' );
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content-encuentros', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->



    

<?php
get_footer();


