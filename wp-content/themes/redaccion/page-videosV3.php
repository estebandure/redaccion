<?php
/* Template Name: Sección VideosV3
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

get_header();
?>


<div id="content" class="TopContent"> <!--ABRE TOP CONTENT--><!--cierra en el footer-->
<!--header y primer nota--->



 <header class="page-header authorHeader">

<?php while ( have_posts() ) :
            the_post();

       



the_title();
echo ('<div class="archive-description">');
echo the_content();
echo('</div>');
 endwhile; // End of the loop.
?>  




            
</header><!-- .page-header -->

<div class="ArchiveFirst"></div>


<!--/fin header y primer nota-->



	

      


<div id="" class="ContentHome3Column SecondScroll">         

<main id="main" class="CentralColHome2c">
  <div id="mainPlus">
<?php


        
            /* Start the Loop */
             query_posts('cat=944' );
            while ( have_posts() ) :
            the_post();

    //en funciones filtra las categorías que no van 


      get_template_part( 'template-parts/content-archivo-video', get_post_type() );
    

        endwhile;
echo ("</div>");//cierra mainplus
      if ( $wp_query->max_num_pages > 1 ) : 
     echo (" <nav class='BTBlacKAction'>");
       next_posts_link( 'CARGAR MÁS NOTAS' ); 
       echo( "</nav>");
     endif;  
    ?>

</main><!-- #main -->



<div class="SideBarHome3c RightSideBar" ><!--- div col derecha-->
<?php
require("colDerecha-archive-news-V3.php");
?>
</div><!--- cierra div col derecha-->


</div><!--  termina el #ArchiveV3 con 3/4 col-1/4 col -->



<script type="text/javascript">

        jQuery(document).ready(function(){


            jQuery('.BTBlacKAction a').click( function(e){

console.log("no se agrega al main todavia");
                e.preventDefault();

                
                var link = jQuery(this).attr('href');

                jQuery('.BTBlacKAction a').html('<span class="loader">Cargando más notas...</span>');

                $.get(link, function(data) {

                    var post =  $("#mainPlus .post ", data);

                    jQuery('#mainPlus').append(post);
                    console.log("se agrega al mainPlus");
                    var af = $('.BTBlacKAction a', data);
                    console.log(af);
                    var ahref= af.attr("href");
                    if(typeof ahref !== "undefined") {
                      jQuery('.BTBlacKAction a').html("CARGAR MÁS NOTAS");
                      jQuery('.BTBlacKAction a').attr("href", ahref);
                    } else {
                      jQuery('.BTBlacKAction').html('');
                    }
                     
                });
               });
             //alert(link);
             //jQuery('.BTBlacKAction').load(link+' .BTBlacKAction a');

            });

      

      </script>









<script>
jQuery(document).ready(function() { 





jQuery('.category-redaccion-abierta .archive-description p a').before('<br>');

jQuery(".TopContent .CentralColHome2c article:first-child").appendTo(".ArchiveFirst");


jQuery('#mc-embedded-subscribe-form').ajaxForm({
  target: '#mce-success-response'
});



jQuery("[cat=788]").css({ color: "#fe8228"}); //chillax
jQuery("[cat=703]").css({ color: "#9b53a5"}); //futuro
jQuery("[cat=704]").css({ color: "#a4ded2"});//oxigeno
jQuery("[cat=705]").css({ color: "#f5cb5b"});//gps am
jQuery("[cat=706]").css({ color: "#5c9bf8"});//gps pm
jQuery("[cat=732]").css({ color: "#c45381"});// siete parrafos
jQuery("[cat=732]").text("SIE7E PÁRRAFOS ");
jQuery("[id=732]").text("Recibí Sie7e Párrafos ");
jQuery("[cat=694]").css({ color: "#8FB981"});//sustentabilidad
jQuery("[cat=2652]").css({ color: "#ffc986"});//reaprender
jQuery("[cat=2879]").css({ color: "#162BA5"});//otra economia



jQuery('.NewsBoxPortada').ready(function(){
  if(jQuery(".mepr-already-logged-in").is(':visible')) {
    jQuery(".ActivaMembForm").remove();
    jQuery(".LoginMiembros h2").text("Tu cuenta");
  }

});


  


jQuery('.NewsBoxPortada').ready(function(){

//lo repito para la portada
if(jQuery(".newsPortada-gps-am").length>0) {
    jQuery(".Option-TopBoxAM").removeClass("NewsBoxHidden");
    jQuery( ".Option-TopBoxSustentables" ).remove()
    jQuery( ".Option-TopBoxPM" ).remove()
    jQuery( ".Option-TopBoxOxigeno" ).remove()
    jQuery( ".Option-TopBoxFuturo" ).remove()
    jQuery( ".Option-TopBoxSiete" ).remove()
    jQuery( ".Option-TopBoxChillax" ).remove()
    jQuery( ".Option-TopBoxReaprender" ).remove()
    jQuery( ".Option-TopBoxOtraEconomia" ).remove()
  }
if(jQuery(".newsPortada-sustentables").length>0) {
    jQuery(".Option-TopBoxSustentables").removeClass("NewsBoxHidden");
    jQuery( ".Option-TopBoxAM" ).remove()
    jQuery( ".Option-TopBoxPM" ).remove()
    jQuery( ".Option-TopBoxOxigeno" ).remove()
    jQuery( ".Option-TopBoxFuturo" ).remove()
    jQuery( ".Option-TopBoxSiete" ).remove()
    jQuery( ".Option-TopBoxChillax" ).remove()
    jQuery( ".Option-TopBoxReaprender" ).remove()
    jQuery( ".Option-TopBoxOtraEconomia" ).remove()
  }
  if(jQuery(".newsPortada-gps-pm").length>0) {
    jQuery(".Option-TopBoxPM").removeClass("NewsBoxHidden");
    jQuery( ".Option-TopBoxAM" ).remove()
    jQuery( ".Option-TopBoxSustentables" ).remove()
    jQuery( ".Option-TopBoxOxigeno" ).remove()
    jQuery( ".Option-TopBoxFuturo" ).remove()
    jQuery( ".Option-TopBoxSiete" ).remove()
    jQuery( ".Option-TopBoxChillax" ).remove()
    jQuery( ".Option-TopBoxReaprender" ).remove()
  }
if(jQuery(".newsPortada-oxigeno").length>0) {
    jQuery( ".Option-TopBoxOxigeno").removeClass("NewsBoxHidden");
    jQuery( ".Option-TopBoxAM" ).remove()
    jQuery( ".Option-TopBoxPM" ).remove()
    jQuery( ".Option-TopBoxSustentables" ).remove()
    jQuery( ".Option-TopBoxFuturo" ).remove()
    jQuery( ".Option-TopBoxSiete" ).remove()
    jQuery( ".Option-TopBoxChillax" ).remove()
    jQuery( ".Option-TopBoxReaprender" ).remove()
    jQuery( ".Option-TopBoxOtraEconomia" ).remove()
    jQuery( ".Option-TopBoxOtraEconomia" ).remove()
  }
  if(jQuery(".newsPortada-futuro").length>0) {
    jQuery(".Option-TopBoxFuturo").removeClass("NewsBoxHidden");
    jQuery( ".Option-TopBoxAM" ).remove()
    jQuery( ".Option-TopBoxPM" ).remove()
    jQuery( ".Option-TopBoxOxigeno" ).remove()
    jQuery( ".Option-TopBoxSustentables" ).remove()
    jQuery( ".Option-TopBoxSiete" ).remove()
    jQuery( ".Option-TopBoxChillax" ).remove()
    jQuery( ".Option-TopBoxReaprender" ).remove()
    jQuery( ".Option-TopBoxOtraEconomia" ).remove()
  }
  if(jQuery(".newsPortada-siete-parrafos").length>0) {
    jQuery(".Option-TopBoxSiete").removeClass("NewsBoxHidden");
    jQuery( ".Option-TopBoxAM" ).remove()
    jQuery( ".Option-TopBoxPM" ).remove()
    jQuery( ".Option-TopBoxOxigeno" ).remove()
    jQuery( ".Option-TopBoxSustentables" ).remove()
    jQuery( ".Option-TopBoxFuturo" ).remove()
    jQuery( ".Option-TopBoxChillax" ).remove()
    jQuery( ".Option-TopBoxReaprender" ).remove()
    jQuery( ".Option-TopBoxOtraEconomia" ).remove()
  }
  if(jQuery(".newsPortada-chillax").length>0) {
    jQuery(".Option-TopBoxChillax").removeClass("NewsBoxHidden");
    jQuery( ".Option-TopBoxAM" ).remove()
    jQuery( ".Option-TopBoxPM" ).remove()
    jQuery( ".Option-TopBoxOxigeno" ).remove()
    jQuery( ".Option-TopBoxSustentables" ).remove()
    jQuery( ".Option-TopBoxFuturo" ).remove()
    jQuery( ".Option-TopBoxSiete" ).remove()
    jQuery( ".Option-TopBoxReaprender" ).remove()
    jQuery( ".Option-TopBoxOtraEconomia" ).remove()
  }
  if(jQuery(".newsPortada-Reaprender").length>0) {
    jQuery(".Option-TopBoxReaprender").removeClass("NewsBoxHidden");
    jQuery( ".Option-TopBoxChillax" ).remove()
    jQuery( ".Option-TopBoxAM" ).remove()
    jQuery( ".Option-TopBoxPM" ).remove()
    jQuery( ".Option-TopBoxOxigeno" ).remove()
    jQuery( ".Option-TopBoxSustentables" ).remove()
    jQuery( ".Option-TopBoxFuturo" ).remove()
    jQuery( ".Option-TopBoxSiete" ).remove()
    jQuery( ".Option-TopBoxOtraEconomia" ).remove()
  }
  if(jQuery(".newsPortada-OtraEconomia").length>0) {
    jQuery(".Option-TopBoxOtraEconomia").removeClass("NewsBoxHidden");
    jQuery( ".Option-TopBoxChillax" ).remove()
    jQuery( ".Option-TopBoxAM" ).remove()
    jQuery( ".Option-TopBoxPM" ).remove()
    jQuery( ".Option-TopBoxOxigeno" ).remove()
    jQuery( ".Option-TopBoxSustentables" ).remove()
    jQuery( ".Option-TopBoxFuturo" ).remove()
    jQuery( ".Option-TopBoxSiete" ).remove()
    jQuery( ".Option-TopBoxReaprender" ).remove()
  }

  });


});
</script>
   


















<?php

get_footer();


function tiene_category($cats, $catid) {
  
  foreach ($cats as $cat) {
    if ($cat->term_id == $catid) {
      return true;
    }
 
  }
  return false;
}

