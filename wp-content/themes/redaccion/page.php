<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

get_header();
?>
<div class="rv4scb">
<aside class="aside"></aside>
<div id="content" class="site-content-full">
	
		<main id="main" class="site-main">



		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'page' );

			

		endwhile; // End of the loop.
		?>

</main><!-- #main -->

  
  </div><!-- /content -->
  </div><!---/rv4scb-->

<?php

get_footer();
