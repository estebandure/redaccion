<?php
/**
 * redaccion functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package redaccion


 */




if ( ! function_exists( 'redaccion_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function redaccion_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on redaccion, use a find and replace
		 * to change 'redaccion' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'redaccion', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'redaccion' ),
		) );




		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'redaccion_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'redaccion_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function redaccion_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'redaccion_content_width', 640 );
}
add_action( 'after_setup_theme', 'redaccion_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function redaccion_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'redaccion' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'redaccion' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));


register_sidebar(array('name'=>'subfooterleft', 'id' => 'subfooterleft'));
register_sidebar(array('name'=>'subfootercenter', 'id' => 'subfootercenter'));
register_sidebar(array('name'=>'subfootercenter1', 'id' => 'subfootercenter1'));
register_sidebar(array('name'=>'subfooterright', 'id' => 'subfooterright'));

}
add_action( 'widgets_init', 'redaccion_widgets_init' );








/**
 * Enqueue scripts and styles.
 */
function redaccion_scripts() {
	//wp_enqueue_style( 'redaccion-style', get_stylesheet_uri() );
 wp_enqueue_style( 'redaccion-style', get_stylesheet_uri(), array(), ''.rand() );
	wp_enqueue_script( 'redaccion-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'redaccion-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'redaccion_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}






function jptweak_remove_share() {
    remove_filter( 'the_content', 'sharing_display', 19 );
    remove_filter( 'the_excerpt', 'sharing_display', 19 );
    if ( class_exists( 'Jetpack_Likes' ) ) {
        remove_filter( 'the_content', array( Jetpack_Likes::init(), 'post_likes' ), 30, 1 );
    }
}

add_action( 'loop_start', 'jptweak_remove_share' );

function remove_img_attributes( $html ) {
$html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
return $html;
}
add_filter( 'post_thumbnail_html', 'remove_img_attributes', 10 );
add_filter( 'image_send_to_editor', 'remove_img_attributes', 10 );


/**
 * Add HTML5 theme support.
 */
function wpdocs_after_setup_theme() {
    add_theme_support( 'html5', array( 'search-form' ) );
}
add_action( 'after_setup_theme', 'wpdocs_after_setup_theme' );





function wpb_imagelink_setup() {
    $image_set = get_option( 'image_default_link_type' );
     
    if ($image_set !== 'none') {
        update_option('image_default_link_type', 'none');
    }
}
add_action('admin_init', 'wpb_imagelink_setup', 10);

//FORM FILTROS


function misha_filter_function(){
  $args = array(
    'orderby' => 'date', // we will sort posts by date
    'posts_per_page'=> 5 ,
    'order' => $_POST['date'] // ASC or DESC

  );
 
  // for taxonomies / categories
 if( isset( $_POST['categoryfilter'] ) )
    $args['tax_query'] = array(
      array(
        'taxonomy' => 'category',
        'field' => 'id',
        
        'terms' => $_POST['categoryfilter']

      )
    );
 
  
 
  $query = new WP_Query( $args );


 
  if( $query->have_posts() ) :

    while( $query->have_posts() ): 
      $query->the_post();


     // echo '<h2>' . $query->post->post_title . '</h2>';


       get_template_part( 'template-parts/content-portada', get_post_type() );

    endwhile;
    wp_reset_postdata();
  else :
    echo 'No se han encontrado resultados';
  endif;
 
  die();
}
 
 
add_action('wp_ajax_myfilter', 'misha_filter_function'); 
add_action('wp_ajax_nopriv_myfilter', 'misha_filter_function');

function sc_taglist(){
    return get_the_tag_list( ' ', ' | ' );
}
add_shortcode('tags', 'sc_taglist');

//agregar hoja de estilo


add_action( 'wp_enqueue_scripts', 'custom_enqueue_styles');

function custom_enqueue_styles() {
          wp_enqueue_style( 'custom-style', 
            get_stylesheet_directory_uri() . '/css/portadaV3.css?'.rand(), 
            array(), 
            wp_get_theme()->get('Version')
          );

          wp_enqueue_style( 'custom-style21', 
          get_stylesheet_directory_uri() . '/css/portada21.css?'.rand(), 
          array(), 
          wp_get_theme()->get('Version')
        );

          wp_enqueue_style( 'custom-style-page', 
            get_stylesheet_directory_uri() . '/css/pageExtra.css?'.rand(), 
            array(), 
            wp_get_theme()->get('Version')
          );

          wp_enqueue_style( 'custom-style-pages20', 
          get_stylesheet_directory_uri() . '/css/structure.css?'.rand(), 
          array(), 
          wp_get_theme()->get('Version')
        );

        wp_enqueue_style( 'custom-style-20', 
        get_stylesheet_directory_uri() . '/css/style-20.css?'.rand(), 
        array(), 
        wp_get_theme()->get('Version')
      );
      wp_enqueue_style( 'custom-style-21', 
      get_stylesheet_directory_uri() . '/css/swiper-bundle.min.css?'.rand(), 
      array(), 
      wp_get_theme()->get('Version')
    );


      

}



// Add Shortcode
function custom_shortcode() {

$autorInf = get_the_author_meta('description');
return "<div class=boxSindicadas > $autorInf </div>";

}

add_shortcode( 'autorinfo', 'custom_shortcode' );



//ad infinite scroll


add_action( 'after_setup_theme', 'my_child_setup' );
function my_child_setup() {
  add_theme_support( 'infinite-scroll', array(
    'container'      => 'main',
    'render'         => 'my_child_infinite_scroll_render',
    'posts_per_page' => 10,
    'footer'=> 'page',   
  ) );
}

function my_child_infinite_scroll_render() {
  while ( have_posts() ) {
    the_post();
    $categories = get_the_category();
    //echo $categories ;
    get_template_part( 'template-parts/content-archivo', get_post_type() );
  }
}



function listed_change_jetpack_infinite_scroll_js_settings( $settings ) {
  $settings['text'] = __( 'Cargar más notas', 'l18n' );
  return $settings;
}
add_filter( 'infinite_scroll_js_settings', 'listed_change_jetpack_infinite_scroll_js_settings' ); 



/* Desactivar wptexturize */
add_filter( 'run_wptexturize', '__return_false' );

remove_filter('the_content', 'wptexturize');
remove_filter('the_excerpt', 'wptexturize');
remove_filter('comment_text', 'wptexturize');
remove_filter('the_title', 'wptexturize');

/*categorias que filtra el archive*/
function exclude_stuff($query) { 
    if ( $query->is_author) {
        $query->set('cat', '-1005,-1,-38,-788,-703,-704,-705,-706,-732,-694,-36,-988,-2272,-2642,-4358,-4637,-4723,-2642,-6400');
    }
    return $query;
}

add_filter('pre_get_posts', 'exclude_stuff');

/*categorias que filtra el buscador*/
function wpb_search_filter( $query ) {
  if ( $query->is_search && !is_admin() )
      $query->set( 'cat','-4637, -6400, -6445, -6491' );
  return $query;
}
add_filter( 'pre_get_posts', 'wpb_search_filter' );


function SearchFilter($query) {
   // If 's' request variable is set but empty
   if (isset($_GET['s']) && empty($_GET['s']) && $query->is_main_query()){
      $query->is_search = true;
      $query->is_home = false;
   }
   return $query;
}
add_filter('pre_get_posts','SearchFilter');



// customLoop() - Notas propuestas por la comunidad .
function notas_comunidad($atts, $content = null) {
    extract(shortcode_atts(array(
        'posts_per_page' => '4',
        "category" => '',
    ), $atts));
 
    global $wp_query,$paged,$post;
 
    $temp = $wp_query;
    
// Define query
  $query_args = array(
    'posts_per_page' => $posts_per_page,
     'paged' => false,
     'category_name'=> $category,
     'post__not_in'=> [45745]
  );


    $wp_query = new WP_Query($query_args);
    
 
 
    ob_start();
    ?>
 
   <div class="Participacion-Feed site-contentFullPage ">
    <?php require("AutorCoautor.php");?>
    <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
     
      <article >
     <a href="<?php the_permalink() ?>" rel="bookmark" >  <?php the_post_thumbnail( ); ?></a> 
<?php the_title('<a href="'.get_the_permalink().' " alt="'.get_the_title().'"><h2 class="Participacion-titleFeed">','</h2></a>' );?>
<div class="ParticipacionAutor"><?php  AutorCoautor();?></div> 
</article>
    <?php endwhile; ?>
   
 </div>

 
    <?php
    $wp_query = null; $wp_query = $temp;
 
    $content = ob_get_contents();
 
    ob_end_clean();
 
    return  $content;
}
 
add_shortcode("Notas-Comunidad", "notas_comunidad");






// customLoop() - Notas  ACCIONES.
function notas_acciones($atts, $content = null) {
    extract(shortcode_atts(array(
        'posts_per_page' => '4',
        "category" => '',
    ), $atts));
 
    global $wp_query,$paged,$post;
 
    $temp = $wp_query;
    
// Define query
  $query_args = array(
    'posts_per_page' => $posts_per_page,
     'paged' => false,
     'category_name'=> $category,
     'post_type' => 'acciones'
  );


    $wp_query = new WP_Query($query_args);
    
 
 
    ob_start();
    ?>
 
      <div class="site-contentFullPage">
       
   <div class="LoopAcciones">
    <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
         <article class="acciones">
          <div class="ActionButtonContent">
<?php the_content(); ?>
            </div>
       </article>
    <?php endwhile; ?>
     </div>
    </div>

 
    <?php
    $wp_query = null; $wp_query = $temp;
 
    $content = ob_get_contents();
 
    ob_end_clean();
 
    return  $content;
}
 
add_shortcode("Notas-Acciones", "notas_acciones");

//notas miembros/

// customLoop() - Notas MIEMBROS .
function notas_miembros($atts, $content = null) {
    extract(shortcode_atts(array(
        'posts_per_page' => '4',
        "category" => '',
        "class" => '',
       
        
    ), $atts));
 
    global $wp_query,$paged,$post;
 
    $temp = $wp_query;
    
// Define query
  $query_args = array(
    'posts_per_page' => $posts_per_page,
     'paged' => false,
     'category_name'=> $category
  );


    $wp_query = new WP_Query($query_args);
    
    $class= $atts['class'];
  
  
 
    ob_start();
    ?>
 
   <div class="swiper-container-Feed <?php echo $class; ?>">
    <div class="swiper-container site-contentFullPage ">
    <div class="swiper-wrapper">
    
    <?php   
$cuentaPost=1;
    while ($wp_query->have_posts()) : $wp_query->the_post(); 
    
echo "<article class='swiper-slide'>";

  
  
 ?>
  <!--  <div class="swiper-slide-img">
 <?php //the_post_thumbnail( ); ?>
    </div>-->
     <?php 
     echo "<div class='swiper-slide-Card'>";
    // the_title('<h2 class="swiper-slide-Card-titleFeed">','</h2>' );
     the_content();
    //echo $category ;
     //$categoriess = get_the_category();
    //echo $categoriess[0]->cat_name;


echo "</div>";
   echo "</article>";

  wp_reset_postdata();  

 ?>

    <?php 
  
  endwhile; ?>
  </div><!--cierra swaper wrapper-->
 <!-- If we need pagination -->
 <div class="swiper-pagination"></div>
  <!-- Si tiene link-->
<?php //if (array_key_exists('link', $atts)) {
    
   // $link= $atts['link'];
  //  echo "<a href='".$link."' class='LinkSwiper'></a>";
  //}?>

 <!-- <div class="swiper-button-next"></div>
  <div class="swiper-button-prev"></div>-->
  
  </div><!--cierra swiper container-->
 </div><!--cierra periodismos content full-->

 
    <?php
    $wp_query = null; $wp_query = $temp;
 
    $content = ob_get_contents();
 
    ob_end_clean();
 
    return  $content;
   
}
 
add_shortcode("Notas-Miembros", "notas_miembros");

//fin notas miembros///




//notas miembros/

// customLoop() - Notas MIEMBROS Comentarios .
function notas_comentarios($atts, $content = null) {
    extract(shortcode_atts(array(
        'posts_per_page' => '10',
        "category" => '',
    ), $atts));
 
    global $wp_query,$paged,$post;
 
    $temp = $wp_query;
    
// Define query
  $query_args = array(
    'posts_per_page' => $posts_per_page,
     'paged' => false,
     'category_name'=> $category
  );


    $wp_query = new WP_Query($query_args);
    
 
 
    ob_start();
    ?>
 
   <div class="slideshow-container">
    
    <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
     
      <div class="mySlides fade BoxOpinionUsuario"> <div class="BoxOpinionUsuarioImgTxt">
    
 <div class="OpinionUsuario "><?php the_title('<q >','</q>' );?></div>
<div class="UsuarioBoxName "> <?php the_content(); ?></div>
    
   <?php if ( has_post_thumbnail() ) {
      echo ('<div class="mgUsuario">  ');
  the_post_thumbnail();
  echo ('</div>');
} 

?>
</div> </div>
    <?php endwhile; ?>
   
 </div>

<div style="text-align:center">
<?php 
$cuentaslide=1;
while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

  <span class="dot" onclick="currentSlide(<?=$cuentaslide ?>)"></span> 
 

<?php 
$cuentaslide++;
endwhile; ?>
 </div>
    <?php
    $wp_query = null; $wp_query = $temp;
 
    $content = ob_get_contents();
 
    ob_end_clean();
 
    return  $content;
}
 
add_shortcode("Notas-Comentarios", "notas_comentarios");

//fin notas miembros///


//notas miembros/

// customLoop() - Notas MIEMBROS Con foto cat y autor .
function notas_participacion($atts, $content = null) {
  extract(shortcode_atts(array(
      'posts_per_page' => '4',
      "category" => '',
  ), $atts));

  global $wp_query,$paged,$post;

  $temp = $wp_query;
  
// Define query
$query_args = array(
  'posts_per_page' => $posts_per_page,
   'paged' => false,
   'category_name'=> $category
);


  $wp_query = new WP_Query($query_args);
  


  ob_start();
  ?>

 <div class="slideshow-container">

  <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

    <?php 
  $titNota=get_the_title();
echo ("<div class='notaParticipacionBox'><div class='notaParticipacionBox_TH'>");
echo the_post_thumbnail();
echo ("</div><div class='notas_participacionBox_info'>");

echo '<h3 ><a href="' . get_permalink( $post->ID ) . '">' .$titNota. '</a></h3>';



echo ('<div class="AutorCoautor">');




//if ( $posts ):
//foreach ( $posts as $post ) : setup_postdata( $post );
// Setting up the coauthors variable
$coauthors = get_coauthors();

// Counter for the coauthors foreach loop below
$coauth = 0;

// Counting the number of objects in the array.
$len = count( $coauthors );
$Piclen = 100;

    // Meta data here...
foreach( $coauthors as $coauthor ):
        // Updating the counter.
 $Piclen--;
        // van las fotos de los autores
        $UserPic = get_avatar( $coauthor->ID );
        

endforeach;



    foreach( $coauthors as $coauthor ):


        // Getting the data for the current author
        $userdata = get_userdata( $coauthor->ID );


        // If one object in the array
        if ( $coauth == 0 ):
            // Just the authors name
            echo '<a class="url fn n" href="/author/' . $userdata->user_nicename  .'/">'. $userdata->display_name .'</a>';

// If last object in the array
        elseif ( $coauth == ($len - 1) ):

          //echo ($len);
            // Adding an "and" before the last object
            echo ' y <a class="url fn n" href="/author/' . $userdata->user_nicename  .'/">'. $userdata->display_name .'</a>';

        // If more than one object in the array
        elseif ( $coauth >= 1 ):
            // Adding a "comma" after the name
           // echo ($coauth);
          // echo ($len);
            echo ', <a class="url fn n" href="/author/' . $userdata->user_nicename  .'/">'. $userdata->display_name .'</a>';


        endif;

        // Updating the counter.
        $coauth++;
    endforeach;



echo '<span class="posted-on"> . ';
echo the_time('j M Y')  .' · ';
echo do_shortcode('[rt_reading_time]')  .' min ' ;
echo ' </span>';


    // More meta data here...

// endforeach;
//endif;



echo ("</div>");

echo ("</div></div>");

?>


  <?php endwhile; ?>
 
</div>

<div style="text-align:center">






</div>
  <?php
  $wp_query = null; $wp_query = $temp;

  $content = ob_get_contents();

  ob_end_clean();

  return  $content;
}

add_shortcode("Notas-participacion", "notas_participacion");

//fin notas miembros///




//notas eventos/

// customLoop() - Notas eventos.
function notas_eventos($atts, $content = null) {
  extract(shortcode_atts(array(
      'posts_per_page' => '30',
      "category" => '',
  ), $atts));

  global $wp_query,$paged,$post;

  $temp = $wp_query;
  
// Define query
$query_args = array(
  'posts_per_page' => $posts_per_page,
   'paged' => false,
   'orderby'=> 'eventstart', 
   'post_type' => ['tribe_events'],
   'category_name'=> $category
);


  $wp_query = new WP_Query($query_args);
  


  ob_start();
  ?>

 <div class="eventosRed">

 
  <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

 
   
   

<?php  
 $TituloEvento=get_the_title();
 echo ('<div class="eventoList">');

  echo ('<div class="THeventoList" >');
  echo tribe_event_featured_image();
  echo ('</div>');//ciera thumbnail
 echo  ("<h3 class='TiteventoList'> $TituloEvento  </h3>");
  echo ('<div class="eventoHora">');
  echo('<strong>');
  echo tribe_get_start_date( $post, false, 'j F ·');
  echo('</strong><em>');
  echo tribe_get_start_date( $post, false, ' G:i');
  
  echo (' hs</em></div>'); 



if ( ! has_excerpt() ) {
  echo '';
} else { 
    echo"<div class='BoxEventosDivHover'>"; 
    the_content(); 
    echo"</div> ";
}
echo"</div> ";
?>


  <?php endwhile; 
   wp_reset_postdata();
  ?>
 
</div>


  <?php
  $wp_query = null; $wp_query = $temp;

  $content = ob_get_contents();

  ob_end_clean();

  return  $content;
}

add_shortcode("Notas-Eventos", "notas_eventos");

//fin notas eventos///






//notas miembros/

// customLoop() - Notas MIEMBROS tips.
function notas_beneficios($atts, $content = null) {
    extract(shortcode_atts(array(
        'posts_per_page' => '30',
        "category" => '',
    ), $atts));
 
    global $wp_query,$paged,$post;
 
    $temp = $wp_query;
    
// Define query
  $query_args = array(
    'posts_per_page' => $posts_per_page,
     'paged' => false,
     'category_name'=> $category
  );


    $wp_query = new WP_Query($query_args);
    
 
 
    ob_start();
    ?>
 
   <div class="SectionBeneficios">
   
    <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
     
      <div class="BoxBeneficios">
      <div class="BeneficiosThumb">
  <div class="TipsFeed">
<?php 
 the_content(); 
echo "</div>";
   the_post_thumbnail( ); 
echo "</div>";
if ( ! has_excerpt() ) {
    echo '';
} else { 
      echo"<div class='BoxBeneficiosDivHover'>"; 
      the_excerpt(); 
      echo"</div> ";
}

 ?>

</div>
    <?php endwhile; ?>
   
 </div>


    <?php
    $wp_query = null; $wp_query = $temp;
 
    $content = ob_get_contents();
 
    ob_end_clean();
 
    return  $content;
}
 
add_shortcode("Notas-Beneficios", "notas_beneficios");

//fin notas miembros///



//Contenido relacionado en notas///

function contenido_relacionado($atts, $content = null) {
  extract(shortcode_atts(array(
      'posts_per_page' => '',
      "category" => '',
      "id" => '',
  ), $atts));

  global $wp_query,$paged,$post;

  $temp = $wp_query;
  $postId= $atts['id'];
  $postsPerPage= $atts['posts_per_page'];
  $postIdFull = array_map('intval', explode(',', $postId));
  
// Define query
$query_args = array(
  'posts_per_page' => $postsPerPage,
   'paged' => false,
   'ignore_sticky_posts' => 1,
   'post__in' =>  $postIdFull
);


$wp_query = new WP_Query($query_args);
  



  ob_start();
  ?>

 <div class="SectionContenidoRelacionado ">
  <h3> TAMBIÉN PODÉS LEER</h3> 
  <?php echo '<div class="SectionContenidoRelacionadoBox NumPost'.$postsPerPage.'">';?>
  <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
 
   
 <div class="aside_story">

   
<?php 


 the_post_thumbnail( ); 


    echo"<div class='aside_tit'>"; 
    echo"<a href='".get_permalink( $post->ID ) ."'>"; 
    the_title(); 
    echo"</a></div> ";


?>

</div>
  <?php endwhile; ?>
    
</div>
</div>


  <?php
  $wp_query = null; $wp_query = $temp;

  $content = ob_get_contents();

  ob_end_clean();

  return  $content;
}

add_shortcode("Contenido-relacionado", "contenido_relacionado");

//fin contenido_relacionado///



/*** Countdown Timer Shortcode ***/
function content_countdown($atts, $content = null){
  extract(shortcode_atts(array(
     'month' => '',
     'day'   => '',
     'year'  => ''
    ), $atts));
    $remain = ceil((mktime( 0,0,0,(int)$month,(int)$day,(int)$year) - time())/86400);
    if( $remain > 1 ){
        return $daysremain = "<div class=\"eventClose\"><strong>CIERRA EN $remain DÍAS</strong></div>";
    }else if($remain == 1 ){
    return $daysremain = "<div class=\"eventClose\"><strong>CIERRA EN $remain DÍA</strong></div>";
    }else{
        return "<div class=\"eventClose\"><strong>$content</strong></div>";
    }
}
add_shortcode('cdt', 'content_countdown');



add_filter('walker_nav_menu_start_el','dcms_agregar_descripcion',10,4);

function dcms_agregar_descripcion( $item_output, $item, $depth, $args ){
    if ( ! empty($item->description)){
        return str_replace( '</h2>', '</h2><p>'.$item->description.'</p>', $item_output );        
    }
    return $item_output;
}


//remover notas relacionadas de jetpack del pie de los posteos

function jetpackme_remove_rp() {
  if ( class_exists( 'Jetpack_RelatedPosts' ) ) {
      $jprp = Jetpack_RelatedPosts::init();
      $callback = array( $jprp, 'filter_add_target_to_dom' );

      remove_filter( 'the_content', $callback, 40 );
  }
}
add_action( 'wp', 'jetpackme_remove_rp', 20 );




function jetpackme_filter_exclude_category( $filters ) {
  $filters[] = array(
      'not' => array(
          'terms' => array(
              'category.slug' => array(
                  'gps-am',
                  'gps-pm',
                  'miembros-newsletter',
                  'beneficios',
                  'z-pagina-miembros',
                  'feed-portada',
              ),
          ),
      ),
  );
  return $filters;
}
add_filter( 'jetpack_relatedposts_filter_filters', 'jetpackme_filter_exclude_category' );





function add_alt_tags( $content ) {
  preg_match_all( '/<img (.*?)\/>/', $content, $images );
  if ( ! is_null( $images ) ) {
    foreach ( $images[1] as $index => $value ) {
      if ( ! preg_match( '/alt=/', $value ) ) {
        $new_img = str_replace(
          '<img',
          '<img alt="' . esc_attr( get_the_title() ) . '"',
          $images[0][$index] );
        $content = str_replace(
          $images[0][$index],
          $new_img,
          $content );
      }
    }
  }
  return $content;
}
add_filter( 'the_content', 'add_alt_tags', 99999 );




/*
* Creating a function to create our CPT
*/
 
function custom_post_type() {
 
  // Set UI labels for Custom Post Type
      $labels = array(
          'name'                => _x( 'redaccion-abierta', 'Post Type General Name', 'redaccion' ),
          'singular_name'       => _x( 'redaccion-abierta', 'Post Type Singular Name', 'redaccion' ),
          'menu_name'           => __( 'redaccion-abierta', 'redaccion' ),
          'parent_item_colon'   => __( 'Parent redaccion-abierta', 'redaccion' ),
          'all_items'           => __( 'All redaccion-abierta', 'redaccion' ),
          'view_item'           => __( 'View redaccion-abierta', 'redaccion' ),
          'add_new_item'        => __( 'Add New redaccion-abierta', 'redaccion' ),
          'add_new'             => __( 'Add New', 'redaccion' ),
          'edit_item'           => __( 'Edit redaccion-abierta', 'redaccion' ),
          'update_item'         => __( 'Update redaccion-abierta', 'redaccion' ),
          'search_items'        => __( 'Search redaccion-abierta', 'redaccion' ),
          'not_found'           => __( 'Not Found', 'redaccion' ),
          'not_found_in_trash'  => __( 'Not found in Trash', 'redaccion' ),
      );
       
  // Set other options for Custom Post Type
       
      $args = array(
          'label'               => __( 'redaccion-abierta', 'redaccion' ),
          'description'         => __( 'redaccion-abierta news and reviews', 'redaccion' ),
          'labels'              => $labels,
          // Features this CPT supports in Post Editor
          'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
          // You can associate this CPT with a taxonomy or custom taxonomy. 
          'taxonomies'          => array( 'genres' ),
          /* A hierarchical CPT is like Pages and can have
          * Parent and child items. A non-hierarchical CPT
          * is like Posts.
          */ 
          'hierarchical'        => false,
          'public'              => true,
          'show_ui'             => true,
          'show_in_menu'        => true,
          'show_in_nav_menus'   => true,
          'show_in_admin_bar'   => true,
          'menu_position'       => 5,
          'can_export'          => true,
          'has_archive'         => true,
          'exclude_from_search' => false,
          'publicly_queryable'  => true,
          'capability_type'     => 'post',
          'show_in_rest' => true,
   
      );
       
      // Registering your Custom Post Type
      register_post_type( 'redaccion-abierta', $args );
   
  }
   
  /* Hook into the 'init' action so that the function
  * Containing our post type registration is not 
  * unnecessarily executed. 
  */
   
  add_action( 'init', 'custom_post_type', 0 );


  function getCategorieNOportada(){
    $categories = get_the_category();
      $catActual=$categories[0];
      foreach($categories as $c) {
          if(($c->slug!="en-profundidad") && ($c->slug!="soluciones-para-america-latina") && ($c->slug!="ig-portada") && ($c->slug!="newsletters") && ($c->slug!="notas")){
            $catActual=$c;
          return $catActual;
          }
      }
      return $catActual;
    }
  
    add_filter( 'run_wptexturize', '__return_false' );



