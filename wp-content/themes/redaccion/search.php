<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package redaccion
 */

get_header();
?>


<div id="content" class="TopContent searchContent"> <!--ABRE TOP CONTENT--><!--cierra en el footer-->

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title-search">
					<?php
					/* translators: %s: search query. */
					printf( esc_html__( 'Resultados para la búsqueda de: %s', 'redaccion' ), '<span>' . get_search_query() . '</span>' );
					?>
				</h1>
			</header><!-- .page-header -->
			
<div id="" class="ContentHome3Column SecondScroll">  		

<main id="main" class="CentralColHome2c">
 

	
	

			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content-archivo', 'search' );

			endwhile;

			the_posts_navigation();
?>
		<?php else : ?>
            <header class="page-header">
				<h1 class="page-title-search">No se encontraron resultados</h1>
			</header>
               <div id="" class="ContentHome3Column SecondScroll">  		
				<main id="main" class="CentralColHome2c">
                  Tal vez quiera intentar con otros términos para su búsqueda.
                  <br>
               <?php get_search_form(); ?>
           </main>
           
         <?php endif ;?>

		</main><!-- #main -->





</div><!--  termina el #ArchiveV3 con 3/4 col-1/4 col -->

<?php
get_footer();


