<?php
/* Template Name: PageVideos */
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

get_header();
?>


<div class="ContentVideos">
	<div class="VideosTitle">
    <?php the_title( '<h1>', '</h1>' ); ?>
    <div class="VideosTitleBajada">Videos que explican las noticias y el mundo a tu alrededor. Suscribite a <a href="https://www.youtube.com/channel/UCbat15YVMn1uZEGWP8cpZXQ" title="Red/Accion en  YouTube" target="blank">nuestro canal de YouTube.</a></div>
	</div>

</div>



<div id="content" class="site-content">
    <div id="primary" class="content-area VideocContent-area">
        <main id="main" class="site-main VideoMain">

            <div class="MultimediaFirst">

    <?php
        query_posts('cat=944&posts_per_page=1' );
    
        while ( have_posts() ) :
            the_post();


             the_content();
            the_title('<h1><a href="'.get_the_permalink().' " alt="'.get_the_title().'">','</a></h1>' );
         echo " <div class='MultimediaFeedDate HiddenLG'> "; the_time('j'); echo " de "; the_time(' F'); echo " de "; the_time(' Y'); echo " </div> ";
         
        

        endwhile; // End of the loop.
        
        ?>

</div>


		<?php
        query_posts('cat=944&offset=1' );
      
        while ( have_posts() ) :
            the_post();
            echo "<div class='MultimediaFeed'><div class='MultimediaFeedVideo'>";
            the_content();
            echo "</div><div class='MultimediaFeedTitle'>";
          the_title('<h1><a href="'.get_the_permalink().' " alt="'.get_the_title().'">','</a></h1>' );
          echo " <div class='MultimediaFeedDate'> "; the_time('j'); echo " de "; the_time(' F'); echo " de "; the_time(' Y'); echo " </div> ";
          echo "</div></div>";

        endwhile; // End of the loop.
        
        ?>






		</main><!-- #main -->
	</div><!-- #primary -->



    

<?php
get_sidebar();
get_footer();


