<?php
/* Template Name: PageHome Feed portada Orgullo /
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

get_header();
?>














<div id="root">

<div class="rv4a1  rv4a2">


  <div id="main" class="rv4b1">







    <main id="site-content">
      <div class="rv4sc">
        <div class="rv4scb">




          <aside class="aside">


           


            <!-- end menu repetido -->


          </aside>


          <div class="main">
            <div class="principal">

              <div class="main_head">
                <div class="fecha" id="fecha1"></div>
              <script>
          var f = new Date();
          var weekday = new Array(7);
          weekday[0] =  "Domingo";
          weekday[1] = "Lunes";
          weekday[2] = "Martes";
          weekday[3] = "Miércoles";
          weekday[4] = "Jueves";
          weekday[5] = "Viernes";
          weekday[6] = "Sábado";
          var n = weekday[f.getDay()];

          var month = new Array(12);
          month[0] =  "Enero";
          month[1] = "febrero";
          month[2] = "Marzo";
          month[3] = "Abril";
          month[4] = "Mayo";
          month[5] = "Junio";
          month[6] = "Julio";
          month[7] =  "Agosto";
          month[8] = "Septiembre";
          month[9] = "Octubre";
          month[10] = "Noviembre";
          month[11] = "Diciembre";

          var m = month[f.getMonth()];

          document.getElementById("fecha1").innerHTML = f.getDate()+" de "+m+" de 20"+(f.getYear()-100);
          </script>

<script  defer="" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js" integrity="sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i" crossorigin="anonymous"></script>

       <?php



        //para redaccion  Orgullo
          $queryfeed =  new WP_Query( ['category__in'=>[7115],  'post_type' => ['post'], 'posts_per_page' => 1 ] );

        while (  $queryfeed->have_posts() ) :
          $queryfeed->the_post();

            $Actualizado = get_post_meta($post->ID, "Actualizado", true);
            echo('<div class="actualizado">'.$Actualizado.'</div>');
            echo('</div>');// ciera el main head
//content
            echo the_content();
//termina content
        endwhile; // End of the loop.


        ?>






            </div>



          </div> <!-- cierre de main -->

          <aside class="asideb">



          <?php
if ( is_user_logged_in() ) {

     $current_user = wp_get_current_user();

    echo "<div style='margin-bottom:1em'><h2 >Hola, " . $current_user->user_firstname .'</h2>';
    echo '<p>¿Tenés un dato, una historia o experiencia para compartir? Encontrá acá los temas que estamos trabajando y cómo podés ser parte. ';
    echo "<a style='display: block;margin: 16px 0 0 0;font-size: 14.4px;padding: 0.8em;' href='/otros-temas-que-estamos-investigando-y-como-podes-ayudarnos/' class='profundizar'>Participá</a> </p></div>";
} else {

  echo "<div style='margin-bottom:1em'><h2>SUMATE A LA COMUNIDAD</h2>";
  echo  "<p>Ayudá a que nuestro periodismo siga siendo abierto, sume más voces y logre mayor impacto.";
  echo "<a style='display: block;margin: 16px 0 0 0;font-size: 14.4px;padding: 0.8em;' href=' https://bit.ly/3kb5yp5' class='profundizar profundizarNar'>QUIERO SER CO-RESPONSABLE</a> </p></div>";
}
?>

  


            <h2>En profundidad</h2>
            <p>Cada día RED/ACCIÓN aborda un tema en profundidad para ayudarte a comprender mejor el mundo y ofrecerte soluciones para cambiarlo. </p>




  <!-- aside story -->
<?php require("AutorCoautor-portada.php");?>
<?php

$notID=[];
$html="";
$NotaID = get_the_ID($post->ID);
$query1 =  new WP_Query( ['category__in'=>[4619],  'post_type' => ['post'], 'posts_per_page' => 4 ] );

//echo $query3->request;

while ( $query1->have_posts()) {

   $html.="<div class='aside_story' id='Note$NotaID'>";
   $query1->the_post();

//$categories = get_the_category();

      $html.= '<a href="' . get_permalink( ) . '" title="' . get_the_title() . ' " >';
      $html.=get_the_post_thumbnail( );
      $html.='</a>';



   $html.='<div class="aside_tit">';
   if ( get_post_meta( $post->ID, 'TituloHome', true ) ) {
  $TituloHome = get_post_meta($post->ID, "TituloHome", true);
  $html.="<a href='" . get_permalink()  . "' > $TituloHome </a> ";
 }else {
   $html.=the_title( '<a href="' . esc_url( get_permalink() ) . '" >', '</a>' , FALSE);
 }
 $html.="</div>";
 $html.="<div class='aside_autor'>";
   $html.=AutorCoautor(1);
   $html.="</div>";
   $html.="</div>";
  array_push($notID,get_the_ID());

}

echo $html;
?>
 <!-- fin aside story -->



                  <div class="asideb_footer"> <a href="/secciones/en-profundidad/" class="profundizar">Más notas</a></div>
                  

              <?php
		//while ( have_posts() ) :
			 // the_post();
		   // the_content();
		//endwhile; // End of the loop.
		?>


                  <div style="width: 100%;max-width:250px; margin-bottom: 20px;">

                    <a href="https://hipotecario.com.ar/" target="_blank"><img src="https://www.redaccion.com.ar/wp-content/uploads/2020/07/Banner-300x300-1.gif" /></a>

                  </div>

                  <div style="width: 100%;max-width:250px; margin-bottom: 20px;">

                    <a href="https://bit.ly/3j6ieOg" target="_blank"><img src="https://www.redaccion.com.ar/wp-content/uploads/2020/07/BANdisplay_Segui-cuidandote_Consejos_fotografico-2_250x250.png" /></a>

                  </div>


                </aside>


              </div>
            </div>


          </main>

         

        </div>


        <div class="progress-container">
    <div class="progress-bar" id="progressBar"></div>
    <div class="mensajeConteo"> 0 posteos leídos </div>
            <div class="ShareBoxFooter">
                <p>¡Si te gustó esta manera de informarte compartila con tus amigos y amigas! </p>


                <div class="more_social"><ul>
                
    <li><a href="whatsapp://send?text=https://www.redaccion.com.ar" data-action="share/whatsapp/share" target="_blank" class="s-whatsapp">whatsapp</a></li>
    <li><a href="https://twitter.com/intent/tweet?text=https://www.redaccion.com.ar" target="_blank" class="s-twitter">twitter</a></li>
    <li><a href="https://www.facebook.com/sharer/sharer.php?u=http%3A//www.redaccion.com.ar" target="_blank" class="s-facebook">facebook</a></li>
    <li id="LinkUrl" style="position:relative"><a href="javascript:getlink();" class="s-link" >link</a></li>
    </ul></div>



               
        
              </div>

  </div>

  <script>//<![CDATA[
function getlink() {
var aux = document.createElement("input");
aux.setAttribute("value",window.location.href);
document.body.appendChild(aux);
aux.select();
document.execCommand("copy");
document.body.removeChild(aux);
var css = document.createElement("style");
var estilo = document.createTextNode("#aviso {display: inline-block;white-space: nowrap;color:#fff; position: absolute;z-index: 9999999;left: 90%;padding: 5px;background: #c1c1c1;border-radius: 8px;font-size: 10px;font-family: sans-serif;top: 0px;width: 70px;}");
css.appendChild(estilo);
document.head.appendChild(css);
var aviso = document.createElement("div");
aviso.setAttribute("id", "aviso");
var contenido = document.createTextNode("URL copiada");
aviso.appendChild(contenido);
LinkUrl.appendChild(aviso);
window.load = setTimeout("LinkUrl.removeChild(aviso)", 2000);
}
//]]></script>


<script>
jQuery(document).ready(function() {


       jQuery( ".card_content .showSingle" ).each(function() {

                    jQuery(this).click(function() {
                
                    jQuery(this).parent().next().toggle();

                    });
                });
                //chevron que cambia//
jQuery( ".card .info a.showSingle" ).on( "click", function() {
    jQuery( this ).toggleClass( "ChevronCierra" );
});
//fin chevron que cambia//






//NEWSLETTERS CHEQUEADAS//
jQuery('.IdNewsCards').each(function()  {


    var cardId = jQuery( this ).html();
    var cardIDli = (".CardBox"+cardId);
   // console.log(" card id "+cardIDli);


    jQuery( cardIDli+' input[type="checkbox"]').prop("checked", true);
 //   jQuery( this ).html(firstpart+"<small> "+last_part+"</small> ");

});

///FIN NEWLETTERS CHEQUEADAS
//altura iframe//
              jQuery( ".SMP iframe" ).each(function() {
                
                jQuery(this).load(function() {
             var alturita= jQuery(this).contents().height() + 40;
             console.log(alturita + "alturitaaa");
             jQuery(this).css("height",alturita );
                   });
                    });

  //contador de divs

var totalCard = jQuery('.principal').children().size() 
console.log(totalCard + "totalCard");


jQuery('.principal').children().each(function() {
  AtributoChild=jQuery(this).prop('nodeName');
  AtributoChildClass=jQuery(this).attr('class');  
    AtributoChildId=jQuery(this).attr('id');  
   
  if ((AtributoChild == "SCRIPT") ||  (AtributoChild == "FIGURE") ||  (AtributoChildClass == "burbuja") ||  (AtributoChildClass == "wp-block-group card-cierre") ||  (AtributoChildClass == "wp-block-group card_embed SMP") || (AtributoChildClass == "card ad") || (AtributoChildClass == "wp-block-group card-co-responsable") ||  (AtributoChildClass == "main_head") || (AtributoChildId == "te-necesitamos") || (AtributoChildId == "MainCards") ||  (AtributoChildId == "boxMain20")){
     //  console.log( "esto es "+AtributoChild);   
        totalCard --;
       
        //console.log(totalCard + " totalCard ahora");
        jQuery('.mensajeConteo').html(' 0 de '+ totalCard + ' posteos leídos '); 
      }
});

  
jQuery(window).on("scroll", function() {

  oldscroll = jQuery(window).scrollTop();
//console.log ('donde está el '+ oldscroll);
var singleCard= 0;
jQuery('.principal').children().each(function() {
   AtributoChild=jQuery(this).prop('nodeName');
  AtributoChildClass=jQuery(this).attr('class');  
    AtributoChildId=jQuery(this).attr('id');  


 // console.log( "esto es "+AtributoChild);  
  //console.log( "esto es "+AtributoChildClass);  
  if ((AtributoChild == "SCRIPT") ||  (AtributoChild == "FIGURE") ||  (AtributoChildClass == "burbuja") ||  (AtributoChildClass == "wp-block-group card-cierre") ||   (AtributoChildClass == "wp-block-group card_embed SMP") || (AtributoChildClass == "card ad") || (AtributoChildClass == "wp-block-group card-co-responsable") ||  (AtributoChildClass == "main_head") || (AtributoChildId == "te-necesitamos") || (AtributoChildId == "MainCards") ||  (AtributoChildId == "boxMain20")){
   //jQuery(this).remove();
      }
       
       else  {  
var offsetYImg = jQuery(this).offset();
var positionYImg = jQuery(this).offset();
var AlturaImg = jQuery(this).height();
var FinalImg = (offsetYImg.top + AlturaImg);
var FinalImgAltura = (offsetYImg.top );
var FinalImgPosition = (positionYImg.top );
//console.log("posición del scroll " +oldscroll);
//onsole.log("altura del bloque " +AlturaImg);
oldscrollscreen = (jQuery(window).scrollTop() +(AlturaImg +100)) ;  


//videos[index] = { 'o' : offsetYImg.top, 'h': FinalImg}

if ( oldscroll >= FinalImg) {
//console.log ('donde está el '+ oldscroll);
 // console.log(" donde se termina la imagen que estoy pasando " + FinalImg );
//console.log(" donde corta la cabecera  " + oldscrollscreen);
//console.log( " top de la imagen: " + offsetYImg.top );
//console.log( " altura de la imagen: " +  AlturaImg );
//console.log( " posicion de la imagen : " +  FinalImgPosition );
//console.log( "esto es "+AtributoChild);  
//console.log( "esto es "+AtributoChildClass);  

  //jQuery(this).addClass("sueniosActivo-");
  //jQuery(this).addClass(" CardNum"+singleCard); 
 // jQuery(this).unbind("scroll");
    

    if (singleCard  >= 0){
    jQuery('.mensajeConteo').html(+(singleCard+1)+' de '+ totalCard+"  posteos leídos" ); 
    //jQuery(this).css('background-color', 'red')
    }

}
 singleCard ++;
}


});


});





//progress bar///

var AnchoLine =jQuery( ".main" ).width()+40;
var LeftLineOffset=jQuery( ".main" ).offset();
var LeftLine=( LeftLineOffset.left)+1;
var ScreenWidth = jQuery(window).width();


if (ScreenWidth >= 769) {
  
   jQuery(".progress-container").css({ width: + AnchoLine  +"px"});
            jQuery(".progress-container").css({ left: + LeftLine  +"px"});
          }
          else{
        console.log(" ancho de pantalla "+ ScreenWidth  );
             jQuery(".progress-container").css({ width: + ScreenWidth  +"px"});
            jQuery(".progress-container").css('left','0');
            
         
          }




function progressBarScroll() {
  let winScroll = document.body.scrollTop || document.documentElement.scrollTop,
      height = document.documentElement.scrollHeight - document.documentElement.clientHeight,
      scrolled = (winScroll / height) * 100;
  document.getElementById("progressBar").style.width = scrolled + "%";
}

//fin progress bar///
var scrolloldposition=0;
var llegoalfinal=false;
//fin aviso llego al final///
jQuery(window).on("scroll", function() {

var offsetYFooter = jQuery(".footer").offset();
var AlturaFooter = jQuery(".footer").height()+60;
var FinalFooterposition = (offsetYFooter.top );
var SueltaBarra = (FinalFooterposition+(jQuery(".footer").height()/2));
barrascroll = jQuery(window).scrollTop();

//console.log(" posicion del footer "+FinalFooterposition);
//console.log(" altura del SueltaBarra "+SueltaBarra);
//console.log(" altura total "+scrollHeight);
//console.log(" scroll "+scrollPosition  );

 progressBarScroll();

    var scrollHeight = jQuery(document).height();
    var scrollPosition = jQuery(window).height() + jQuery(window).scrollTop();
    if (scrollPosition >scrolloldposition){
      jQuery('#progressBar').show();
    }
    else{
      //jQuery('#progressBar').hide();
      jQuery('#progressBar').unbind( )
    }
    scrolloldposition=scrollPosition;

    if ((llegoalfinal==false)&&(scrollPosition >=  FinalFooterposition) ){
      llegoalfinal=true;
     // jQuery('#progressBar').append('<div class="mensajefinal"> Felicitaciones, llegaste al final.</div>').fadeIn( "slow" );
      jQuery('.mensajeConteo').hide(); 
      //jQuery('.mensajeConteo').unbind("scroll"); 
      jQuery(".ShareBoxFooter").css('display', 'flex');
    }

   
			

    if (scrollPosition >=  FinalFooterposition) {
    // var asidew = jQuery('.aside').with();
  
          jQuery(".progress-container").css({ 'position': "relative", 'margin-bottom':'-10px', 'bottom':  "80px"});
          jQuery(".progress-container").addClass('progress-containerFinal');
    } else {
          if (ScreenWidth > 769) {
          jQuery(".progress-container").css({ 'bottom': '40px'});
          }else{
              jQuery(".progress-container").css({  'bottom':  "105px"});
                  }

      }


});
//fin aviso llego al final///






});





            </script>

<?php

get_footer();
