<?php
/* Template Name: PageSeccionesFull */
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

get_header();
?>

<div class="rv4scb">
<aside class="aside"></aside>
<div id="content" class="site-content-full">
	<div class="content-area-full">
		<main id="main" class="site-main">






     








		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content-min-full', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
  </div><!-- #primary -->
  
  </div><!-- /content -->
  </div><!---/rv4scb-->
<script>

jQuery(document).ready(function(){
 
/* Smooth scrolling para anclas */
 
  jQuery('a.smooth').on('click', function(e) {
    e.preventDefault();
    var $link = jQuery(this);
    var anchor = $link.attr('href');
    $('html, body').stop().animate({scrollTop: $(anchor).offset().top}, 1000);
});
 


jQuery('a.smooth').on('click', function(e) {
    e.preventDefault();
    var $link = jQuery(this);
    var anchor = $link.attr('href');
    $('html, body').stop().animate({scrollTop: $(anchor).offset().top}, 1000);
});




if(jQuery(window).width() <= 780) {
  var BeneficiosClick = jQuery('.SectionBeneficios .BoxBeneficios');


// var BoxBeneficiosDivMobile = jQuery(this);
BeneficiosClick.click(function() {

if (!jQuery(this).hasClass('BoxBeneficioshover') ){ 
 console.log("click en cajita click");
   jQuery(this).addClass('BoxBeneficioshover');

}else{
 console.log("sacar");
   jQuery(this).removeClass('BoxBeneficioshover');
 
}
});

}else{


  jQuery('.SectionBeneficios .BoxBeneficios').each(function()  { 
var BoxBeneficiosDiv = jQuery(this);

jQuery(BoxBeneficiosDiv).hover(
    function() {
        jQuery( this ).addClass( "BoxBeneficioshover" );
    }, function() {
        jQuery (this ).removeClass( "BoxBeneficioshover" );
    }
  );

});



}


var win = jQuery(window);
var loadinginf=0;
var currentinf=0;
var footerheight=jQuery(".footer").height();
//console.log(footerheight);

  // Each time the user scrolls
jQuery("#CargaAcciones").click(function(event) {
  event.preventDefault();
 // alert( "Handler for .click() called." );
  

   //console.log("carga scroll " + currentinf);

        if ((loadinginf==0)&&(currentinf<=20)) {
        jQuery('#loading').show();
        loadinginf=1;
        currentinf+=3;
        console.log("carga scroll " + currentinf);
        $.ajax({
          url: "https://www.redaccion.com.ar/infinitphpacciones/?de="+currentinf,
          dataType: 'html',
          success: function(html) {
          $('.LoopAcciones').append(html);
          $('#loading').hide();
          
          }, 
          complete: function() {
          loadinginf=0; 
          }
        });
      }
      
      });



   });  

//scrip para carousel
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
}


//fin scrip para carousel






var slideIndex1 = [1,1];
/* Class the members of each slideshow group with different CSS classes */
var slideId1 = ["mySlides1", "mySlides2"]
showSlides1(1, 0);
showSlides1(1, 1);

function plusSlides(nn, no) {
  showSlides1(slideIndex1[no] += nn, no);
}

function showSlides1(nn, no) {
  var ii;
  var xx = document.getElementsByClassName(slideId1[no]);
  if (nn > xx.length) {slideIndex1[no] = 1}
  if (nn < 1) {slideIndex1[no] = xx.length}
  for (ii = 0; ii < xx.length; ii++) {
    xx[ii].style.display = "none";
  }
  xx[slideIndex1[no]-1].style.display = "flex";
} 

</script>
 <!-- Initialize Swiper -->
 <script src="/wp-content/themes/redaccion/js/swiper.min.js"></script>
 <script type="text/javascript">

   var cssId = 'myCss';  // you could encode the css path itself to generate id..
if (!document.getElementById(cssId))
{
    var head  = document.getElementsByTagName('head')[0];
    var link  = document.createElement('link');
    link.id   = cssId;
    link.rel  = 'stylesheet';
    link.type = 'text/css';
    link.href = 'https://www.redaccion.com.ar/wp-content/themes/redaccion/css/swiper.min.css';
    link.media = 'all';
    head.appendChild(link);
}
 </script>
  <script>

var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
if (isMobile) {
 // alert("es mobile");
//
var swiper = new Swiper('.swiper-container', {
      slidesPerView: 1,
      spaceBetween: 20,
      slidesPerGroup: 1,
      loop: false,
      loopFillGroupWithBlank: true,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
//

}else{
 // alert("no es mobile");
//
var swiper = new Swiper('.swiper-container', {
      slidesPerView: 4,
      spaceBetween: 30,
      slidesPerGroup: 4,
      loop: true,
      loopFillGroupWithBlank: true,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
//
}



  </script>


<?php

get_footer();
