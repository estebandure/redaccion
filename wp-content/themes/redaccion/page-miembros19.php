<?php
/* Template Name: PageMiembros */
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

get_header();
?>
<div id="content" class="site-content-full">
	<div class="content-area-full">
		<main id="main" class="site-main">



		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content-min-full', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->
<script>
jQuery( document ).ready(function() {

  jQuery('a.smooth').on('click', function(e) {
    e.preventDefault();
    var $link = jQuery(this);
    var anchor = $link.attr('href');
    $('html, body').stop().animate({scrollTop: $(anchor).offset().top}, 1000);
});


 if(jQuery(window).width() <= 767) {


jQuery('.MiembrosMasInfo').click(function(e){
e.preventDefault();


	if (jQuery(this).parent().find('ul').hasClass("MiembrosMasInfoVisible")){
	jQuery(this).parent().find('ul').removeClass("MiembrosMasInfoVisible");
	jQuery(this).html('Más Información <i class="fas fa-chevron-down">');
	jQuery(this).parent().find('ul').attr( "MiembrosDisclaimer", "display:none;" )
		
	}else{
		jQuery(this).parent().find('ul').addClass("MiembrosMasInfoVisible");
		jQuery(this).parent().find('ul').attr( "MiembrosDisclaimer", "display:block;" )
		jQuery(this).html('Cerrar <i class="fas fa-chevron-up">');
		
	}
});

 }

 });

</script>

<?php

get_footer();


