<?php
/* Template Name: PageMiembros */
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

get_header();
?>


<div class="ContentMiembros">
	<div class="MiembrosTitle">
		<h1>Te invitamos a ser parte de la comunidad de RED/ACCIÓN</h1>

	</div>

</div>



<div id="content" class="site-content">
	<div id="primary" class="content-area">
		<main id="main" class="site-main">

    <div class="page-first-module">

      <p>Con tu participación hacemos un periodismo que escucha las preocupaciones de su comunidad, identifica sus desafíos y provee herramientas intelectuales y materiales para asumirlos.</p>

      <p>Nacimos como un medio del nuevo paradigma, del siglo XXI y sabemos que nuestra responsabilidad es ayudar a levantar la mirada y convertirnos en plataforma de debate, participación y pertenencia. </p>

      <p>Activá tu membresía por $150 por mes y sé parte de este cambio.</p>

      <h2>Beneficios:</h2>
      <ul>
        <li>1-Mono: Nuestro producto impreso desplegable en tu casa.</li>
        <li>2-Productos Especiales</li>
        <li>3-Encuentros Mensuales</li>
        <li>4-Entradas sin cargo para recitales</li>
      </ul>
    </div>















		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content-min', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
