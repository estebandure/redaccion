<?php
/* Template Name: Single PageHome 2020*/
/* Template Post Type: post*/
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package redaccion
 */

get_header();
?>

<div id="content" class="site-content">
	<div id="primary" class="content-area">
		<main id="mainPost" class="site-main">

<?php echo get_post_meta( $post_id, $field_name ); ?>

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content-notav4', get_post_type() );

			

			

		endwhile; // End of the loop.
		?>



<!--FIN POSTEO DESSARROLLADO-->

   </main><!-- #main -->
  </div><!-- #primary -->
</div><!-- /content -->




<?php
get_footer();

function tiene_category($cats, $catid) {
  
  foreach ($cats as $cat) {
    if ($cat->term_id == $catid) {
      return true;
    }
    
  }
  return false;
}