<?php function AutorCoautor($autCo=0){

 $html='<div class="AutorTiempo">';

//if ( $posts ):
    //foreach ( $posts as $post ) : setup_postdata( $post );
        // Setting up the coauthors variable
        $coauthors = get_coauthors();

        // Counter for the coauthors foreach loop below
        $coauth = 0;

        // Counting the number of objects in the array.
        $len = count( $coauthors );
        $Piclen = 100;
       
            // Meta data here...


  $html.='<span class="byline"> Por';

            foreach( $coauthors as $coauthor ):
               

                // Getting the data for the current author
                $userdata = get_userdata( $coauthor->ID );
               

                // If one object in the array
                if ( $coauth == 0 ):
                    // Just the authors name
                     $html.= ' <span class="author vcard"><a class="url fn n" href="/author/' . $userdata->user_nicename  .'/">'. $userdata->display_name .'</a></span>';

				// If last object in the array
                elseif ( $coauth == ($len - 1) ):

                	//echo ($len);
                    // Adding an "and" before the last object
                      $html.=' y <span class="author vcard"><a class="url fn n" href="/author/' . $userdata->user_nicename  .'/">'. $userdata->display_name .'</a></span>';

                // If more than one object in the array
                elseif ( $coauth >= 1 ):
                    // Adding a "comma" after the name
                   // echo ($coauth);
                  // echo ($len);
                      $html.= '<span class="author vcard">, <a class="url fn n" href="/author/' . $userdata->user_nicename  .'/">'. $userdata->display_name .'</a></span>';
                   
                
                endif; 

                // Updating the counter.
                $coauth++;
            endforeach;

  $html.= ' </span>';

  
        $html.= ' <span class="Tlectura"> · ';
        $html.= do_shortcode('[rt_reading_time]')  .' min ' ;
     $html.= ' </span>';


            // More meta data here...
      
   // endforeach;
//endif;
  $html.="</div>"; //FIN AUTOR -->    

if ($autCo==1) {
   return $html;
}
 echo $html;
} 
   ?>