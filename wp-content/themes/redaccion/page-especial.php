<?php
/* Template Name: PageEspecial */
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

get_header();
?>



<div style="display: block; width:100%">

<?php if ( have_posts() ) : ?>



      <?php

query_posts('cat=45&posts_per_page=1' );
      /* Start the Loop */
      while ( have_posts() ) :
        the_post();


        /*
         * Include the Post-Type-specific template for the content.
         * If you want to override this in a child theme, then include a file
         * called content-___.php (where ___ is the Post Type name) and that will be used instead.
         */
        get_template_part( 'template-parts/content-especial', get_post_type() );

      endwhile;

      the_posts_navigation();

    else :

      get_template_part( 'template-parts/content-portada', 'none' );

    endif;
    ?>

</div>
<div class="clear"></div>





<div id="content" class="site-content">
	<div id="primary" class="content-area">
		<main id="main" class="site-main">






     








		<?php

query_posts('posts_per_page=5' );


		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );

			

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
