<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package redaccion
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">
 <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js" integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe"
        crossorigin="anonymous"></script>
        <script defer src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script defer src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'redaccion' ); ?></a>

	<header id="masthead" class="site-header">

		





		<nav id="site-navigation" class="navbar">
			<span id="navbar-toggle"><i class="fas fa-bars"></i> </span>


        <div class="SiteHeaderW">

         <?php $header_image = get_header_image();
        if ( ! empty( $header_image ) ) : ?>
        <a  href="<?php echo esc_url( home_url( '/' ) ); ?>" id="logo"><img src="<?php echo esc_url( $header_image ); ?>"   class="header-image" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="" style="height:auto!important;" /></a>








			<?php endif; ?>

			<?php
			wp_nav_menu( array(
				'theme_location' => 'menu-1',
				'menu_id'        => 'main-nav',
			) );
			?>

		

	<div class="nav-icon" id="search-btn">    <i class="fas fa-search"></i>  </div>

		</div>	


		</nav><!-- #site-navigation -->


<div  class="search-drawer z-16 s-closed">
  			<div class="search-tool">
   				 <div id="search-nav"><!--<div id="search-close"><i class="fas fa-times"></i></div>-->
			<?php get_search_form(); ?>
				</div>
			</div>
	</div>
<div id="overlay"></div>
 



		
	</header><!-- #masthead -->


	<div id="content" class="site-content">
