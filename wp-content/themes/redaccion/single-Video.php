<?php
/*
 * Template Name: Video / Multimedia
 * Template Post Type: post, page, product


 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package redaccion
 */

get_header();
?>

<?php require("AutorCoautor.php");?>


<div class="rv4scb">
<aside class="aside"></aside>

	<div id="content" class="site-content">
	<div id="primary" class="content-area">
		<main id="mainPost" class="site-main">



		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content-video', get_post_type() );

			

			

		endwhile; // End of the loop.
		?>



<!--FIN POSTEO DESSARROLLADO-->

   </main><!-- #main -->
  </div><!-- #primary -->
</div><!-- /content -->
</div><!---/rv4scb-->



<?php
get_footer();

function tiene_category($cats, $catid) {
  
  foreach ($cats as $cat) {
    if ($cat->term_id == $catid) {
      return true;
    }
    
  }
  return false;
}