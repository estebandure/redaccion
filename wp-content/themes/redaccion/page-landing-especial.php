<?php
/* Template Name: Page Landing Especial */
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

get_header();
?>

<div class="LandingEspecial">

<div id="content" class="site-content-full">
	<div class="content-area-full">
		<main id="main" class="site-main">






     








		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content-min-full', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
    ?>
    
 

		</main><!-- #main -->
  </div><!-- #primary -->
  
  </div><!-- /content -->
  </div><!---/rv4scb-->
<script>

jQuery(document).ready(function(){

  $( ".page-template-page-landing-especial .ShareFullPage" ).insertBefore( ".LandingRedesShare" );
  jQuery('.CampaniaFija').html('<a class="rv4htx rv4hico2" href="https://www.redaccion.com.ar/en-el-mes-de-la-mujer-tenes-muchas-formas-de-achicar-la-brecha-de-genero-y-avanzar-hacia-la-igualdad/"><span><strong>Campaña del mes: </strong>Empoderemos a las mujeres</span></a>');
 
/* Smooth scrolling para anclas */
 
  jQuery('a.smooth').on('click', function(e) {
    e.preventDefault();
    var $link = jQuery(this);
    var anchor = $link.attr('href');
    $('html, body').stop().animate({scrollTop: $(anchor).offset().top}, 1000);
});
 


jQuery('a.smooth').on('click', function(e) {
    e.preventDefault();
    var $link = jQuery(this);
    var anchor = $link.attr('href');
    $('html, body').stop().animate({scrollTop: $(anchor).offset().top}, 1000);
});




if(jQuery(window).width() <= 780) {
  var BeneficiosClick = jQuery('.SectionBeneficios .BoxBeneficios');


// var BoxBeneficiosDivMobile = jQuery(this);
BeneficiosClick.click(function() {

if (!jQuery(this).hasClass('BoxBeneficioshover') ){ 
 console.log("click en cajita click");
   jQuery(this).addClass('BoxBeneficioshover');

}else{
 console.log("sacar");
   jQuery(this).removeClass('BoxBeneficioshover');
 
}
});

}else{


  jQuery('.SectionBeneficios .BoxBeneficios').each(function()  { 
var BoxBeneficiosDiv = jQuery(this);

jQuery(BoxBeneficiosDiv).hover(
    function() {
        jQuery( this ).addClass( "BoxBeneficioshover" );
    }, function() {
        jQuery (this ).removeClass( "BoxBeneficioshover" );
    }
  );

});



}


var win = jQuery(window);
var loadinginf=0;
var currentinf=0;
var footerheight=jQuery(".footer").height();
//console.log(footerheight);

  // Each time the user scrolls
jQuery("#CargaAcciones").click(function(event) {
  event.preventDefault();
 // alert( "Handler for .click() called." );
  

   //console.log("carga scroll " + currentinf);

        if ((loadinginf==0)&&(currentinf<=20)) {
        jQuery('#loading').show();
        loadinginf=1;
        currentinf+=3;
        console.log("carga scroll " + currentinf);
        $.ajax({
          url: "https://www.redaccion.com.ar/infinitphpacciones/?de="+currentinf,
          dataType: 'html',
          success: function(html) {
          $('.LoopAcciones').append(html);
          $('#loading').hide();
          
          }, 
          complete: function() {
          loadinginf=0; 
          }
        });
      }
      
      });



   });  










</script>
 <!-- Initialize Swiper -->
 <script src="/wp-content/themes/redaccion/js/swiper.min.js"></script>
 <script type="text/javascript">

   var cssId = 'myCss';  // you could encode the css path itself to generate id..
if (!document.getElementById(cssId))
{
    var head  = document.getElementsByTagName('head')[0];
    var link  = document.createElement('link');
    link.id   = cssId;
    link.rel  = 'stylesheet';
    link.type = 'text/css';
    link.href = 'https://www.redaccion.com.ar/wp-content/themes/redaccion/css/swiper.min.css';
    link.media = 'all';
    head.appendChild(link);
}
 </script>
  <script>

var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
if (isMobile) {
 // alert("es mobile");
//
var swiper = new Swiper('.swiper-container', {
      slidesPerView: 1,
      spaceBetween: 50,
      slidesPerGroup: 1,
      loop: false,
      loopFillGroupWithBlank: true,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
//

}else{
 // alert("no es mobile");
//
var swiper = new Swiper('.swiper-container', {
      slidesPerView: 1,
      spaceBetween: 30,
      slidesPerGroup: 1,
      loop: true,
      loopFillGroupWithBlank: true,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
//
}



  </script>

<script>
 var TxtType = function(el, toRotate, period) {
        this.toRotate = toRotate;
        this.el = el;
        this.loopNum = 0;
        this.period = parseInt(period, 10) || 2000;
        this.txt = '';
        this.tick();
        this.isDeleting = false;
    };

    TxtType.prototype.tick = function() {
        var i = this.loopNum % this.toRotate.length;
        var fullTxt = this.toRotate[i];

        if (this.isDeleting) {
        this.txt = fullTxt.substring(0, this.txt.length - 1);
        } else {
        this.txt = fullTxt.substring(0, this.txt.length + 1);
        }

        this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

        var that = this;
        var delta = 200 - Math.random() * 100;

        if (this.isDeleting) { delta /= 2; }

        if (!this.isDeleting && this.txt === fullTxt) {
        delta = this.period;
        this.isDeleting = true;
        } else if (this.isDeleting && this.txt === '') {
        this.isDeleting = false;
        this.loopNum++;
        delta = 500;
        }

        setTimeout(function() {
        that.tick();
        }, delta);
    };

    window.onload = function() {
        var elements = document.getElementsByClassName('typewrite');
        for (var i=0; i<elements.length; i++) {
            var toRotate = elements[i].getAttribute('data-type');
            var period = elements[i].getAttribute('data-period');
            if (toRotate) {
              new TxtType(elements[i], JSON.parse(toRotate), period);
            }
        }
        // INJECT CSS
        var css = document.createElement("style");
        css.type = "text/css";
        css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
        document.body.appendChild(css);
    };
</script>



<?php

get_footer();
