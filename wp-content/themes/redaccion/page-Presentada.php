<?php
/* Template Name: PagePresentada */
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

get_header();
$custom_field = get_post_meta($post->ID, "PresentadaPor", true); 
$PresentadaPorLogo = get_post_meta($post->ID, "PresentadaPorLogo", true); 
$HeaderPresentadoDesk = get_post_meta($post->ID, "HeaderPresentadoDesk", true);
$HeaderPresentadoMobile = get_post_meta($post->ID, "HeaderPresentadoMobile", true);
?>


<div class='ContentPresentados ' style="background:url(<?=$HeaderPresentadoDesk ?>)right top #FFF no-repeat; background-size: contain;">
	<div class="PresentadosTitle">
    <div class="PresentadosTitleVolanta">Presentado por: <img src="<?=$PresentadaPorLogo ?>"></div>
    <?php the_title( '<h1>', '</h1>' ); ?>

    <div class="PresentadosTitleBajada">



<?php
    while ( have_posts() ) :
      the_post();

       the_content();

    endwhile; // End of the loop.
    ?></div>
	</div>

</div>



<div id="content" class="site-content">
    <div id="primary" class="content-area VideocContent-area">
        <main id="main" class="site-main VideoMain">

            <div class="MultimediaFirst">



         <?php

query_posts("category_name=$custom_field" );
      /* Start the Loop */
      while ( have_posts() ) :
        the_post();

        get_template_part( 'template-parts/content-portada', get_post_type() );

      endwhile;

      
    ?>

</div>


	






		</main><!-- #main -->
	</div><!-- #primary -->





<script>

jQuery(document).ready(function(){
 


if(jQuery(window).width() <= 767) {

jQuery(".ContentPresentados").attr("style","background:url(<?=$HeaderPresentadoMobile ?>) center top no-repeat;");

}


});
</script>

    

<?php

get_footer();


