
<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package redaccion
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">



  <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,700,700i,900" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet"> 

	<link href="https://fonts.googleapis.com/css?family=Merriweather:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">

  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<script src="https://kit.fontawesome.com/2b529209a5.js" crossorigin="anonymous"></script>
     
        <script defer src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KXRB6QT');</script>
<!-- End Google Tag Manager -->


<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '339142676900996');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=339142676900996&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->



	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>


 <!-- menu repetido -->
 <div class="menu" >
  

 <div class="menu_items" >
<a href="#" class="cerrar-menu"><i class="fas fa-times"></i> </a>
<?php get_search_form(); ?>

<p class="SubTMenu">Conocé RED/ACCIÓN</p>


  <?php
wp_nav_menu( array(

'menu'              => 'conoce-redaccion', // (int|string|WP_Term) Desired menu. Accepts a menu ID, slug, name, or object.
'menu_class'        => 'items_ul', // (string) CSS class to use for the ul element which forms the menu. Default 'menu'.
'before'            => '  ', // (string) Text before the link markup.
'after'             => '', // (string) Text after the link markup.
'link_before'       => '<div class="items_icon"></div><div class="items_txt"><h2>', // (string) Text before the link text.
'link_after'        => '</h2></div> ', // (string) Text after the link text.
) );
?>



<p class="SubTMenu menu_mas">Más de RED/ACCIÓN</p>

  <?php
wp_nav_menu( array(
'menu'              => 'mas-sobre-nosotros',
'menu_class'        => 'items_ul', // (string) CSS class to use for the ul element which forms the menu. Default 'menu'.
'before'            => '  ', // (string) Text before the link markup.
'after'             => '', // (string) Text after the link markup.
'link_before'       => '<div class="items_icon"></div><div class="items_txt"><h2>', // (string) Text before the link text.
'link_after'        => '</h2></div> ', // (string) Text after the link text.
) );
?>
</div>

</div>
<!-- end menu repetido -->





<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KXRB6QT"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="page" class="site">
	
	<div class="rv4b2">
      <header class="rv4b4">
        <nav>
<ul class="rv4b3">
<li class="menu_botton rv4headli rv4hlib">
<button id="showmenu" aria-expanded="false" aria-label="Toggle menu" class="rv4htx rv4hlib" type="button" title="Toggle menu"><span class=""><svg class="rv4hico" xmlns="http://www.w3.org/2000/svg" width="29" height="38" viewBox="0 0 29 38"><style>.hamburger-icon__bottom,.hamburger-icon__middle,.hamburger-icon__top{fill: #000;transform-origin:50%;transition:all .3s}.open .hamburger-icon__top{transform:translateY(3px) translateX(-25%) rotate(45deg)}.open .hamburger-icon__bottom{transform:translateY(-4px) translateX(-25%) rotate(-45deg)}.open .hamburger-icon__middle{transform:translatex(-100%);opacity:0}</style><path class="hamburger-icon__top" d="M0 0h56v3H0z"></path><path class="hamburger-icon__middle" d="M0 10h56v3H0z"></path><path class="hamburger-icon__bottom" d="M0 20h56v3H0z"></path></svg></span></button>
</li>
<li class="menu-cabecera-sm"> 
  <?php
wp_nav_menu( array(
'menu'              => 'cabecera-21', // (int|string|WP_Term) Desired menu. Accepts a menu ID, slug, name, or object.
'menu_class'        => 'items_header_ul', // (string) CSS class to use for the ul element which forms the menu. Default 'menu'.
'before'            => '  ', // (string) Text before the link markup.
'after'             => '', // (string) Text after the link markup.
'link_before'       => '<div class="items_header">', // (string) Text before the link text.
'link_after'        => '</div> ', // (string) Text after the link text.
) );
?>
</li>
<li class="rv4headli rv4hilg"><a class="rv4htx rv4hilg" href="https://redaccion.com.ar/" title="Ir a la portada de RED/ACCIÓN"><h1 class="V4Tit">RED/ACCIÓN</h1></a></li>
<li class="fechaheader">
<div class="fecha" id="fecha1"></div>
           <script>
          var f = new Date();
          var weekday = new Array(7);
          weekday[0] =  "Domingo";
          weekday[1] = "Lunes";
          weekday[2] = "Martes";
          weekday[3] = "Miércoles";
          weekday[4] = "Jueves";
          weekday[5] = "Viernes";
          weekday[6] = "Sábado";
          var n = weekday[f.getDay()];

          var month = new Array(12);
          month[0] =  "Enero";
          month[1] = "febrero";
          month[2] = "Marzo";
          month[3] = "Abril";
          month[4] = "Mayo";
          month[5] = "Junio";
          month[6] = "Julio";
          month[7] =  "Agosto";
          month[8] = "Septiembre";
          month[9] = "Octubre";
          month[10] = "Noviembre";
          month[11] = "Diciembre";

          var m = month[f.getMonth()];

          document.getElementById("fecha1").innerHTML = f.getDate()+" de "+m+" de 20"+(f.getYear()-100);
          </script>
</li>
<li class="rv4headli rv4hlipf"><a class="rv4htx rv4hlic" href="https://bit.ly/338WcTR"><span class="rv4hlibm">QUIERO SER MIEMBRO</span></a> </li>
<li class="rv4headli rv4hico2">       
<?php
if ( is_user_logged_in() ) {
$current_user = wp_get_current_user();
echo '<p class="NombreHeader" >Hola,  <a href="/miembros-corresponsables/" title="Ingreso de Miembros">'. $current_user->user_firstname .' </a> </p>' ;
} else {
echo '<a href="/membresias/login/" class="NombreHeader NombreHeaderOff" title="Ingreso de Miembros"><span>Iniciar sesión</span></a>';
}
?>
 </li>
<li class="TopRedes"> 
          <?php
wp_nav_menu( array(
'menu'              => 'redes-21',
'menu_class'        => 'items_ul', // (string) CSS class to use for the ul element which forms the menu. Default 'menu'.
'before'            => '', // (string) Text before the link markup.
'after'             => '', // (string) Text after the link markup.
'link_before'       => '', // (string) Text before the link text.
'link_after'        => '', // (string) Text after the link text.
) );
?>
         </li>
        </ul>

      </nav></header>

    </div>
