<?php
/**
 * Template Name: News Presentadas / con logo
 * Template Post Type: post
 * The template for mostrar newsletter con sponsors
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package redaccion
 */

get_header();
?>

<?php require("AutorCoautor.php");?>

   

<div class="rv4scb">
<aside class="aside"></aside>
<div id="content" class="site-content single-Content">
  
	<div id="primary" class="content-area">
		<main id="mainPost" class="site-main News-Presentadas">






    <!--POSTEO DESARROLLADO--->


		<?php
      $notID=[];
        $elposteoID=0;
		while ( have_posts() ) :
      the_post();
      $categoryThis = get_the_category(); 
      $categoryThisslug= $categoryThis[0]->slug; 
      
      $elposteoID=get_the_ID();
      get_template_part( 'template-parts/content-NotasV3', get_post_type() );
      
      //para categoria especial de news
      if ( get_post_meta( $post->ID, 'NewsBox', true ) ) {
        $CatNewsBox = get_post_meta($post->ID, "NewsBox", true);
       }else{
        $CatNewsBox ="NewsDefault";
       }

    
     
      array_push($notID,get_the_ID());
      wp_reset_postdata();
		endwhile; // End of the loop.
		?>



<!--FIN POSTEO DESSARROLLADO-->
<!-- 3 notas al pie del singl-->
<p class="SubTMenu">EN PROFUNDIDAD</p>
  <DIV class="NotasPieSingle">

<?php


$html="";

$NotaID = get_the_ID($post->ID);
$query1 =  new WP_Query( ['category__in'=>[4619],  'post_type' => ['post'], 'post__not_in' =>$notID,  'posts_per_page' => 3 ] );

//echo $query3->request;

while ( $query1->have_posts()) {

   $html.="<div class='' id='Note$NotaID'>";
   $query1->the_post();
   $tituloDesc= get_the_title();

//$categories = get_the_category();

      $html.= '<a href="' . get_permalink( ) . '" title="' .  $tituloDesc . ' " >';
      $html.=get_the_post_thumbnail( );
      $html.='</a>';



   $html.='<div class="aside_tit">';
   if ( get_post_meta( $post->ID, 'TituloHome', true ) ) {
  $TituloHome = get_post_meta($post->ID, "TituloHome", true);
  $html.="<a href='" . get_permalink()  . "' > $TituloHome </a> ";
 }else {
   $html.=the_title( '<a href="' . esc_url( get_permalink() ) . '" >', '</a>' , FALSE);
 }


 $html.="</div>";
 $html.="<div class='aside_autor'>";
   $html.=AutorCoautor(1);
   $html.="</div>";
   $html.="</div>";
   wp_reset_postdata();

}

echo $html;
?>


  </div>
<!-- fin de las 3 notas al pi del single-->
   </main><!-- #main -->

   



  </div><!-- #primary -->
</div><!-- /content -->


<aside class="asideb AsidebPost"><!-- col derecha-->
   
            <p>Cada día RED/ACCIÓN aborda un tema en profundidad para ayudarte a comprender mejor el mundo y ofrecerte soluciones para cambiarlo. </p>

<!-- aside story -->

<a style="display: block;margin: 16px 0 0 0;font-size: 14.4px;padding: 0.8em;" href="/" class="profundizar">Seguir navegando por Red/Acción</a>

<br>
<br>
<p class="News21FUTURO futuro"><script class="campaign-5" src="https://www.redaccion.com.ar/api/v1/campania/migrated/5"></script></p>

<p class="News21REAPRENDER reaprender"><script class="campaign-4" src="https://www.redaccion.com.ar/api/v1/campania/migrated/4"></script></p>

<p  class="News21DIRCOMSNET dircomsnet"><script class="campaign-3" src="https://www.redaccion.com.ar/api/v1/campania/migrated/3"></script></p>

<p  class="News21CHILLAX chillax"><script class="campaign-2" src="https://www.redaccion.com.ar/api/v1/campania/migrated/2"></script></p>

<p  class="News21OXIGENO oxigeno"><script class="campaign-22" src="https://www.redaccion.com.ar/api/v1/campania/migrated/22"></script></p>

<p  class="News21PLANETA planeta"><script class="campaign-21" src="https://www.redaccion.com.ar/api/v1/campania/migrated/21"></script></p>

<p  class="News21OTRA-ECONOMIA otra-economia"><script class="campaign-15" src="https://www.redaccion.com.ar/api/v1/campania/migrated/15"></script></p>

<p  class="News21SIETEPARRAFOS siete-parrafos"><script class="campaign-23" src="https://www.redaccion.com.ar/api/v1/campania/migrated/23"></script></p>

<p  class="News21GPS-PM gps-pm"><script class="campaign-20" src="https://www.redaccion.com.ar/api/v1/campania/migrated/20"></script></p>
<p  class="News21GPS-AM gps-am"><script class="campaign-16" src="https://www.redaccion.com.ar/api/v1/campania/migrated/16"></script></p>

<?php

 if ( class_exists( 'Jetpack_RelatedPosts' ) ) {
  echo do_shortcode( '[jetpack-related-posts]' );
}
     ?>

<?php

$notID=[];
$html="";
$NotaID = get_the_ID($post->ID);
$query1 =  new WP_Query( ['category__in'=>[4619], 'post__not_in' =>$notID,  'post_type' => ['post'], 'posts_per_page' => 4 ] );

//echo $query3->request;

while ( $query1->have_posts()) {

   $html.="<div class='aside_story' id='Note$NotaID'>";
   $query1->the_post();
   $tituloDesc1= get_the_title();
//$categories = get_the_category();

      $html.= '<a href="' . get_permalink() . '" title="' . $tituloDesc1. ' " >';
      $html.=get_the_post_thumbnail( );
      $html.='</a>';



   $html.='<div class="aside_tit">';
   if ( get_post_meta( $post->ID, 'TituloHome', true ) ) {
  $TituloHome = get_post_meta($post->ID, "TituloHome", true);
  $html.="<a href='" . get_permalink()  . "' > $TituloHome </a> ";
 }else {
   $html.=the_title( '<a href="' . esc_url( get_permalink() ) . '" >', '</a>' , FALSE);
 }
 $html.="</div>";
 $html.="<div class='aside_autor'>";
   $html.=AutorCoautor(1);
   $html.="</div>";
   $html.="</div>";
   wp_reset_postdata();
  array_push($notID,get_the_ID());

}


?>
 <!-- fin aside story -->


</aside><!-- fin col derecha-->
  </div><!---/rv4scb-->


  <script>
jQuery(document).ready(function() { 
  var ThisCatNewsBox="<?php echo $CatNewsBox; ?>";
  var ThisCatPost = "<?php echo $categoryThisslug; ?>";
  //console.log("categoria actual "+ThisCatPost);
  //console.log("categoria elegida "+ThisCatNewsBox);

  if (ThisCatNewsBox!="NewsDefault"){
   // console.log("categoria elegida  ok"+ThisCatNewsBox);
    jQuery("."+ThisCatNewsBox).css("display", "block");
 }else{//abre el else de newsbox


//categoria cuando no hay nada seteado
 if (ThisCatPost=="ambiente"){
jQuery(".News21PLANETA").css("display", "block");

 }else if(ThisCatPost=="comunidades"){
jQuery(".News21OXIGENO").css("display", "block");

 }
 else if(ThisCatPost=="impacto-social"){
jQuery(".News21OXIGENO").css("display", "block");
jQuery(".News21OTRA-ECONOMIA").css("display", "block");
 }
 else if(ThisCatPost=="genero-y-diversidad"){
jQuery(".News21GPS-AM").css("display", "block");

 }
 else if(ThisCatPost=="cultura-y-diversion"){
jQuery(".News21CHILLAX").css("display", "block");
jQuery(".News21SIETEPARRAFOS").css("display", "block");
 }
 else if (ThisCatPost=="educacion"){
jQuery(".News21REAPRENDER").css("display", "block");
 }
 else if (ThisCatPost=="tecnologia"){
jQuery(".News21FUTURO").css("display", "block");
 }
 else if (ThisCatPost=="vida-cotidiana"){
jQuery(".News21GPS-AM").css("display", "block");
 }
 else if (ThisCatPost=="actualidad"){
jQuery(".News21GPS-AM").css("display", "block");
 }
 else {jQuery(".News21GPS-AM").css("display", "block");}
 //fin categoria cuando no hay nada seteado
} //cierra el else de newsbox
});



  </script>




<?php

get_footer();

function tiene_category($cats, $catid) {
  
  foreach ($cats as $cat) {
    if ($cat->term_id == $catid) {
      return true;
    }
    
  }
  return false;
}