<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

?>



<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>













<div class="<?php echo wp_strip_all_tags( get_the_excerpt(), true ); ?> <?php echo get_post_type(); ?> Aligner"  style="background-image:url(<?= wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', false )[0] ?> );">

	<div class="Aligner-item Aligner-item-top">

	 <div class="CategoriaBox Aligner-item"><?php the_category( '  '  ); ?></div>
   
		<?php
		if ( is_singular() ) :
			the_title( '<h3 class="bajadaBox Aligner-item">','</h3>' ); //Home H1
		else :
			the_title( '<h3 class="bajadaBox Aligner-item">','</h3>' ); //Home H1
		endif;

		if ( 'post' === get_post_type() ) :
			?>
		
		<?php endif; ?>
	
<!--BOX CONTENT-->
		<div class="Box-content">
		<?php
		the_content( );

		
		?>
	</div><!-- .bos-content -->



</div>

	


</div>





</div><!-- #post-<?php the_ID(); ?> -->
