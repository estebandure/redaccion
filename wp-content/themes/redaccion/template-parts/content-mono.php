<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

?>






<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


   <div class="MonoMagContent">

   <div class="MonoCover"><img src="<?php the_post_thumbnail_url(); ?>"/></div>
<div class="MonoData">
   <span class="Mono-date">
      <?php the_time('F' ) ?> de <?php the_time('Y ') ?>  
    </span>
     <?php
      the_title( '<h2 class="Mono-title">', '</h2>' ); 
      ?>
      <div class="Mono-description"><?php
$content = get_the_content();
print $content;
?></div>
	</div>  
	  

  </div>








 







 





</article><!-- #post-<?php the_ID(); ?> -->
