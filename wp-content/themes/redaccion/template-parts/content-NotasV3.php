<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

?>





<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>




  <?php
    $category = get_the_category();
    //$category_link = get_category_link($category[0] );
    $categoriaActual = getCategorieNOportada();
    $category_link = get_category_link($categoriaActual );
    $category_Name = $categoriaActual->cat_name;
     ?>







<div class="CategoriaPresentada">
    <span class="category">
    <a href="<?php echo esc_url( $category_link ); ?>" title="<?php echo $category_Name; ?>" style="text-transform:uppercase;"><?php echo $category_Name; ?></a>
  </span>
</div>
<div class="CategoriaPresentada">
<?php

  if ( get_post_meta( $post->ID, 'PresentadaPorLogo', true ) ) {

    if ( get_post_meta( $post->ID, 'LogoZize', true ) ) {
$LogoZize = get_post_meta($post->ID, "LogoZize", true);

    }else{
      $LogoZize ="35";
    }

    $PresentadaPorLogo = get_post_meta($post->ID, "PresentadaPorLogo", true);
echo ("<div class=\"PresentadosTitleVolanta\">Presentada por: <img src='$PresentadaPorLogo ' style='height:".$LogoZize."px'></div>");
  }

?>
</div>

	<header class="entry-header">





 <!-- .entry-footer -->
		<?php
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' ); //post H1
		else :
			the_title( '<h2 class="entry-title">', '</h2>' ); //Home H1
		endif;

		if ( 'post' === get_post_type() ) :
			?>

<img src="https://www.redaccion.com.ar/wp-content/uploads/2021/07/iniciativa-dirscom-redaccion.png" alt=" Una iniciativa de Dircoms + RED/ACCION" class="DirComnsIniciat" style="height:40px; width:auto !important;"> 


 <?php  if ( ! has_excerpt() ) {
    echo '';
} else {
      echo"<div class='bajadaNota BajadaSingle'>";
      the_excerpt();
      echo"</div> ";
}?>


      <div class="author-box entry-meta">
        <div class="left">



<?php //if ( $posts ):
    //foreach ( $posts as $post ) : setup_postdata( $post );
        // Setting up the coauthors variable
        $coauthors = get_coauthors();

        // Counter for the coauthors foreach loop below
        $coauth = 0;

        // Counting the number of objects in the array.
        $len = count( $coauthors );
        $Piclen = 100;

            // Meta data here...
 foreach( $coauthors as $coauthor ):
                // Updating the counter.
 				$Piclen--;
                // van las fotos de los autores
                $UserPic = get_avatar( $coauthor->ID );
                 echo '<span class="avatar_thumb" style="z-index:'.($Piclen).'">' . $UserPic . '</span>';

 endforeach;

echo '<div><span class="byline"> Por';

            foreach( $coauthors as $coauthor ):


                // Getting the data for the current author
                $userdata = get_userdata( $coauthor->ID );


                // If one object in the array
                if ( $coauth == 0 ):
                    // Just the authors name
                    echo ' <span class="author vcard"><a class="url fn n" href="/author/' . $userdata->user_nicename  .'/">'. $userdata->display_name .'</a></span>';

				// If last object in the array
                elseif ( $coauth == ($len - 1) ):

                	//echo ($len);
                    // Adding an "and" before the last object
                    echo ' y <span class="author vcard"><a class="url fn n" href="/author/' . $userdata->user_nicename  .'/">'. $userdata->display_name .'</a></span>';

                // If more than one object in the array
                elseif ( $coauth >= 1 ):
                    // Adding a "comma" after the name
                   // echo ($coauth);
                  // echo ($len);
                    echo '<span class="author vcard">, <a class="url fn n" href="/author/' . $userdata->user_nicename  .'/">'. $userdata->display_name .'</a></span>';


                endif;

                // Updating the counter.
                $coauth++;
            endforeach;

echo ' </span>';

echo '<span class="posted-on">';
      echo the_time('j M Y')  .' · ';
      echo do_shortcode('[rt_reading_time]')  .' min ' ;
   echo ' </span>';

 echo ' </div>';
            // More meta data here...

   // endforeach;
//endif;
 ?>

        </div>
        <div class="right" style="text-align:right;white-space:nowrap;">

          <?php
                if ( function_exists( 'sharing_display' ) ) {
                    sharing_display( '', true );
                }

                if ( class_exists( 'Jetpack_Likes' ) ) {
                    $custom_likes = new Jetpack_Likes;
                    echo $custom_likes->post_likes( '' );
                }
          ?>
        </div>

		<?php endif; ?>
  </div>


<?php
// Must be inside a loop.

if ( has_post_thumbnail() ) {

  $thumbnail_id = get_post_meta( $post->ID, '_thumbnail_id', true );
  $img_alt = get_post_meta ( $thumbnail_id, '_wp_attachment_image_alt', true );

if(! $img_alt){
    $img_alt = get_the_title();
}

echo ("<div class='thNoteCaption'>");
   echo ("<img src='");
    the_post_thumbnail_url() ;
    echo ( "' alt='");
    echo $img_alt;
   echo ( "'/>");
 if ( $caption = get_post( get_post_thumbnail_id() )->post_excerpt ) :
    echo ("<p class='caption'>");
      the_post_thumbnail_caption();
        echo(" </p>");
    endif;
echo ("</div>");


}

?>





	</header><!-- .entry-header -->



	<div class="entry-content">





		<?php
		the_content();
		?>
	</div><!-- .entry-content -->

<div class="pieNotaMiembros" style="display:none"> <strong >¡Gracias a nuestra comunidad de miembros podemos hacer RED/ACCIÓN!</strong><br>
Tu aporte es fundamental para que nuestro periodismo sea abierto, sume más voces y logre un mayor impacto. <a href="/quiero-ser-miembro/">¿Te sumás?</a>
</div>


</article>




<!-- #post-<?php the_ID(); ?> -->
 <div class="ShareBottomArticle">


