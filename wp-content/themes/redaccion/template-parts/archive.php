<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

get_header();
?>
<div id="content" class="site-content">
	<div id="primary" class="content-area">
		
		<main id="main" class="site-main">

	

  


		<?php if ( have_posts() ) : 

			?>

			<header class="page-header authorHeader">


			
				

			<?php if (is_category()){ 
single_cat_title();
echo ('<div class="archive-description">');
echo category_description( $category_id );
echo('</div>');
}
?>	




	


<?php if (is_author($author)){ 

 the_author(); 
				the_archive_description( '<div class="archive-description">', '</div>' );
				//echo get_the_author_link();
				echo get_avatar($author, $id_or_email, $size, $default, $alt, $args );

}
?>
 

			</header><!-- .page-header -->










			<?php


			
			/* Start the Loop */
			while ( have_posts() ) :
			the_post();

		

$categories = get_the_category();
    if (tiene_category($categories, 944)) {
      get_template_part( 'template-parts/content-video', get_post_type() );
    } elseif (tiene_category($categories, 1)) {
    	continue;
    } elseif (tiene_category($categories, 38)) {
    	continue;
    }
    elseif (tiene_category($categories, 788)) {
    	continue;
    }  elseif (tiene_category($categories, 703)) {
    	continue;
    }elseif (tiene_category($categories, 705)) {
    	continue;
    } elseif (tiene_category($categories, 706)) {
    	continue;
    } elseif (tiene_category($categories, 704)) {
    	continue;
    } elseif (tiene_category($categories, 732)) {
    	continue;
    } elseif (tiene_category($categories, 694)) {
    	continue;
    } elseif (tiene_category($categories, 36)) {
    	continue;
    } elseif (tiene_category($categories, 944)) {
    	continue;
    }elseif (tiene_category($categories, 988)) {
        continue;
    }else {
  get_template_part( 'template-parts/content-archivo', get_post_type() );
    }

				

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content-archivo', 'none' );

		endif;
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();


function tiene_category($cats, $catid) {
  
  foreach ($cats as $cat) {
    if ($cat->term_id == $catid) {
      return true;
    }
 
  }
  return false;
}

