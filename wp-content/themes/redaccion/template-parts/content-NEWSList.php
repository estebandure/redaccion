<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

?>






<div class="pageFull" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


 








<header class="pageFull-header">





 <!-- .entry-footer -->
		<?php

		
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' ); //post H1
		else :
			the_title( '<h2 class="entry-title">', '</h2>' ); //Home H1
		endif;

		if ( 'post' === get_post_type() ) :
			?>

		<?php endif; ?>
	</header><!-- .entry-header -->
	



	<div class="Page-full-content">







		<?php
		the_content( sprintf(
			wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers */
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'redaccion' ),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			),
			get_the_title()
		) );

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'redaccion' ),
			'after'  => '</div>',
		) );
		?>
	</div><!-- .entry-content -->




</div><!-- #post-<?php the_ID(); ?> -->
