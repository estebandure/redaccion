<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

?>





<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
 



  <?php
    $category = get_the_category();
    $category_link = get_category_link($category[0] );
     ?>
  

 





	<header class="NewsAmPM-header">


  <?php
    if ( is_singular() ) :
      the_title( '<h1 class="NewstitleAmPM">', '</h1>' ); //post H1
  
    endif;

    if ( 'post' === get_post_type() ) :
      ?>




<?php //if ( $posts ):
    //foreach ( $posts as $post ) : setup_postdata( $post );
        // Setting up the coauthors variable
        $coauthors = get_coauthors();

        // Counter for the coauthors foreach loop below
        $coauth = 0;

        // Counting the number of objects in the array.
        $len = count( $coauthors );
        $Piclen = 100;
       


echo '<div><span class="bylineNews"> Por';

            foreach( $coauthors as $coauthor ):
               

                // Getting the data for the current author
                $userdata = get_userdata( $coauthor->ID );
               

                // If one object in the array
                if ( $coauth == 0 ):
                    // Just the authors name
                    echo ' <span class="author vcard"><a class="url fn n" href="/author/' . $userdata->user_nicename  .'/">'. $userdata->display_name .'</a></span>';

        // If last object in the array
                elseif ( $coauth == ($len - 1) ):

                  //echo ($len);
                    // Adding an "and" before the last object
                    echo ' y <span class="author vcard"><a class="url fn n" href="/author/' . $userdata->user_nicename  .'/">'. $userdata->display_name .'</a></span>';

                // If more than one object in the array
                elseif ( $coauth >= 1 ):
                    // Adding a "comma" after the name
                   // echo ($coauth);
                  // echo ($len);
                    echo '<span class="author vcard">, <a class="url fn n" href="/author/' . $userdata->user_nicename  .'/">'. $userdata->display_name .'</a></span>';
                   
                
                endif; 

                // Updating the counter.
                $coauth++;
            endforeach;

echo ' </span>';



 echo ' </div>';
            // More meta data here...
      

   // endforeach;
//endif;
 ?>
 <!-- .entry-footer -->
	
<!---fecha--->
<div class="NewsDate">
          <div class="" id="fecha1"></div>
          <script>
          var f = new Date();
          var weekday = new Array(7);
          weekday[0] =  "Domingo";
          weekday[1] = "Lunes";
          weekday[2] = "Martes";
          weekday[3] = "Miércoles";
          weekday[4] = "Jueves";
          weekday[5] = "Viernes";
          weekday[6] = "Sábado";
          var n = weekday[f.getDay()];

          var month = new Array(12);
          month[0] =  "Enero";
          month[1] = "febrero";
          month[2] = "Marzo";
          month[3] = "Abril";
          month[4] = "Mayo";
          month[5] = "Junio";
          month[6] = "Juli0";
          month[7] =  "Agosto";
          month[8] = "Septiembre";
          month[9] = "Octubre";
          month[10] = "Noviembre";
          month[11] = "Diciembre";
         
          var m = month[f.getMonth()];

          document.getElementById("fecha1").innerHTML = n+" "+f.getDate()+" de "+m+" de 20"+(f.getYear()-100);
          </script>
        </div>


<!---/fecha--->

  </header><!-- .entry-header -->

  <div class="entry-content">

 <?php  if ( ! has_excerpt() ) {
    echo '';
} else { 
      echo"<div >"; 
      the_excerpt(); 
      echo"</div> ";
}?>





   

  <?php

    if ( get_post_meta( $post->ID, 'PresentadaPorLogo', true ) ) {

      $PresentadaPorLogo = get_post_meta($post->ID, "PresentadaPorLogo", true); 
  echo ("<div class=\"PresentadosTitleVolanta News\">Presentada por: <img src='$PresentadaPorLogo '></div>");
    } 

 endif; ?>  



<div class="thNoteCaption">
   <img src="<?php the_post_thumbnail_url(); ?>"/>
 <?php if ( $caption = get_post( get_post_thumbnail_id() )->post_excerpt ) : ?>
    <p class="caption"><?php  the_post_thumbnail_caption();  ?></p>
  <?php endif; ?>
</div>

  




	





		<?php
		the_content();
		?>
	</div><!-- .entry-content -->


 


</article>

<!-- #post-<?php the_ID(); ?> -->
 