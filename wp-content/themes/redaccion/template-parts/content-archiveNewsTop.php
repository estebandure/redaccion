<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

?>

<?php 

$categories = get_the_category();
$catActual=$categories[0];
$categorySlug=$catActual->slug;	
$ThePostClass =  'class="' . join( ' ', get_post_class( ) ) . ' ArchiveFirst"';

require("box-newsletters.php");?>



<article id="post-<?php the_ID(); ?>" <?php echo  $ThePostClass ?> >

  <?php 
  $textoCentrado="FirstTextoCentrado";
  if ( has_post_thumbnail() ) {
    echo "<a href='" . esc_url( get_permalink() ) . "' ><div class='ArchivePost_thumbnail'> <img src='";
  the_post_thumbnail_url();
  echo "'/></a></div>";
  $textoCentrado="";
} 
?>






<div class="ArchivePost_content <?=$textoCentrado?>"><!--abre contenido header-->

<!-- titulo y bajada-->
 <?php
    
      the_title( '<h2><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); //Home H1
     
    if ( 'post' === get_post_type() ) :
      ?>


 <?php  if ( ! has_excerpt() ) {
    echo '';
} else { 
     
      echo"<div class='bajadaNota'> ";
      the_excerpt(); 
      echo" </div> ";
}?>
<!-- / titulo y bajada-->





	






 <!-- .entry-footer -->
   
      <div class="author-box entry-meta">
       


<?php //if ( $posts ):
    //foreach ( $posts as $post ) : setup_postdata( $post );
        // Setting up the coauthors variable
        $coauthors = get_coauthors();

        // Counter for the coauthors foreach loop below
        $coauth = 0;

        // Counting the number of objects in the array.
        $len = count( $coauthors );
        $Piclen = 100;
       
            // Meta data here...


echo '<span class="byline"> Por';

            foreach( $coauthors as $coauthor ):
               

                // Getting the data for the current author
                $userdata = get_userdata( $coauthor->ID );
               

                // If one object in the array
                if ( $coauth == 0 ):
                    // Just the authors name
                    echo ' <span class="author vcard"><a class="url fn n" href="/author/' . $userdata->user_nicename  .'/">'. $userdata->display_name .'</a></span>';

        // If last object in the array
                elseif ( $coauth == ($len - 1) ):

                  //echo ($len);
                    // Adding an "and" before the last object
                    echo ' y <span class="author vcard"><a class="url fn n" href="/author/' . $userdata->user_nicename  .'/">'. $userdata->display_name .'</a></span>';

                // If more than one object in the array
                elseif ( $coauth >= 1 ):
                    // Adding a "comma" after the name
                   // echo ($coauth);
                  // echo ($len);
                    echo '<span class="author vcard">, <a class="url fn n" href="/author/' . $userdata->user_nicename  .'/">'. $userdata->display_name .'</a></span>';
                   
                
                endif; 

                // Updating the counter.
                $coauth++;
            endforeach;

echo ' </span>';
echo "<span class='SeparadorOculto'>&nbsp;·&nbsp;</span>";
echo '<span class="posted-on">';
      echo  the_time('j M Y') .'';
     
      echo do_shortcode('[rt_reading_time]')  .' min ' ;
   echo ' </span>';


            // More meta data here...
      
   // endforeach;
//endif;
 ?>


     
      
      </div>
    <?php endif; ?>
  






</div><!--cierra contenido del article-->

</article><!-- #post-<?php the_ID(); ?> -->
