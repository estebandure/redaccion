<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

?>









	<img style="display:none;" src="<?php the_post_thumbnail_url(); ?>"/>



  









	<header class="page-header authorHeader headerNews">





 <!-- .entry-footer -->
		<?php
		if ( is_singular() ) :
			the_title( '' ); //post H1
		
		endif;

		if ( 'post' === get_post_type() ) :
			?>
      <p>
       get_the_excerpt()
    
      </P>
		<?php endif; ?>
	</header><!-- .entry-header -->



	<div class="entry-news">
		<?php
		the_content( sprintf(
			wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers */
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'redaccion' ),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			),
			get_the_title()
		) );

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'redaccion' ),
			'after'  => '</div>',
		) );
		?>
	</div><!-- .entry-content -->




<!-- #post-<?php the_ID(); ?> -->
