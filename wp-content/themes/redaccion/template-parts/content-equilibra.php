<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

?>






<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


  








	<header class="entry-header">





 <!-- .entry-footer -->
		<?php
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' ); //post H1
		else :
			the_title( '<h2 class="entry-title">', '</h2>' ); //Home H1
		endif;

				if ( 'post' === get_post_type() ) :
			?>
      <div class="author-box entry-meta">
        <div class="left">

          <span class="avatar_thumb">
            <?php echo get_avatar( get_the_author_meta( 'ID' ), 36 ); ?>



          </span>

          <?php
          redaccion_posted_by();
          ?>





        </div>
        <div class="right" style="text-align:right;">
          <?php
                if ( function_exists( 'sharing_display' ) ) {
                    sharing_display( '', true );
                }

                if ( class_exists( 'Jetpack_Likes' ) ) {
                    $custom_likes = new Jetpack_Likes;
                    echo $custom_likes->post_likes( '' );
                }
          ?>
        </div>
      </div>
		<?php endif; ?>
	</header><!-- .entry-header -->



	<div class="entry-content">
		<?php
		the_content( sprintf(
			wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers */
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'redaccion' ),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			),
			get_the_title()
		) );

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'redaccion' ),
			'after'  => '</div>',
		) );
		?>
	</div><!-- .entry-content -->




</article><!-- #post-<?php the_ID(); ?> -->
