<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <footer class="entry-footer">
		<?php redaccion_entry_footer(); ?>
	</footer><!-- .entry-footer -->



	<header class="entry-header">

    <?php redaccion_post_thumbnail(); ?>
    <?php if ( $caption = get_post( get_post_thumbnail_id() )->post_excerpt ) : ?>
      <p class="caption"><?php echo $caption; ?></p>
    <?php endif; ?>

		<?php
		if ( is_singular() ) :
			the_title( '<h3 class="entry-title">', '</h3>' ); //post H1
		else :
			the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' ); //Home H1
		endif;

		if ( 'post' === get_post_type() ) :
			?>
			<div class="entry-meta">
				<?php
				redaccion_posted_by();
        redaccion_posted_on();
				?>
			</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->



	<div class="entry-content">
		<?php
		the_content( sprintf(
			wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers */
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'redaccion' ),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			),
			get_the_title()
		) );

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'redaccion' ),
			'after'  => '</div>',
		) );
		?>
	</div><!-- .entry-content -->


</article><!-- #post-<?php the_ID(); ?> -->
