<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

?>









  <div class="HeaderFotoDelDia">

    <div class="FechaFotoDelDia">
 

  
   
          <div class="" id="fecha2" style="display: inline; font-weight: 700;"></div>    <span style="color:#ff6a5f;font-weight:bold;padding: 0 5px;">::</span>    Periodismo humano 
          <script>
          var f = new Date();
          var weekday = new Array(7);
          weekday[0] =  "Domingo";
          weekday[1] = "Lunes";
          weekday[2] = "Martes";
          weekday[3] = "Miércoles";
          weekday[4] = "Jueves";
          weekday[5] = "Viernes";
          weekday[6] = "Sábado";
          var n = weekday[f.getDay()];

           var M = new Date();
          var month = new Array(12);
          month[0] =  "Enero";
          month[1] = "Febrero";
          month[2] = "Marzo";
          month[3] = "Abril";
          month[4] = "Mayo";
          month[5] = "Junio";
          month[6] = "Julio";
          month[7] = "Agosto";
          month[8] = "Septiembre";
          month[9] = "Octubre";
          month[10] = "Noviembre";
          month[11] = "Diciembre";
          var nm = month[M.getMonth()];

          document.getElementById("fecha2").innerHTML = n+" "+f.getDate()+" de "+nm+" de 20"+(f.getYear()-100);
          </script>
       
 

   </div>

  </div>

<article id="post-<?php the_ID(); ?>" class="fotoDelDia">


	<img src="<?php the_post_thumbnail_url(); ?>"/>



<div class="RazonBg"></div>


	<div class="FotoDelDiaContent ">

    <div class="FotoDelDiaContentFull">


    <?php
    $category = get_the_category();

    $category_link = get_category_link($category[0] );

    ?>

     <span class="category">
   <?php echo $category[0]->cat_name; ?>
  </span>

 <!-- .entry-header -->
    <?php
    if ( is_singular() ) :
      the_title( '<h2>', '</h2>' ); //post H1
    else :
      the_title( '<h2>', '</h2>' ); //Home H1
    endif;

    if ( 'post' === get_post_type() ) :
      ?>
   
    <?php endif; ?>
<!-- .entry-header -->



<div class="FullBajada">

      <?php the_excerpt('');
?>
</div>


		<?php
		the_content( sprintf(
			wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers */
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'redaccion' ),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			),
			get_the_title()
		) );

	
		?>

  </div>
	</div><!-- .entry-content -->




</article><!-- #post-<?php the_ID(); ?> -->
