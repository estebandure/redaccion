<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

?>






<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


  <div class="post-top" >
 <div>
    <?php
    $category = get_the_category();

    $category_link = get_category_link($category[0] );

    ?>



	  </div>


    <div style="display: flex;">
	  <?php edit_post_link( __( 'Editar nota', 'twentyfifteen' ), '<span class="edit-link">', '</span>' ); ?>
     <?php
    if ( get_post_meta( $post->ID, 'PresentadaPorLogo', true ) ) {

      $PresentadaPorLogo = get_post_meta($post->ID, "PresentadaPorLogo", true);
  echo ("<div class=\"PresentadosTitleVolanta\">Presentada por: <img src='$PresentadaPorLogo '></div>");
    }

?>


	  </div>

  </div>

<?php
$categories = get_the_category();
if (tiene_category($categories, 944)) {
       the_content();
    } else{
      echo "<img src='";
     the_post_thumbnail_url();
      echo "'/>";
    }
    ?>





  <?php if ( $caption = get_post( get_post_thumbnail_id() )->post_excerpt ) : ?>
    <p class="caption"><?php  the_post_thumbnail_caption();  ?></p>
  <?php endif; ?>


 <span class="category">
    <a href="<?php echo esc_url( $category_link ); ?>" title="<?php echo $category[0]->cat_name; ?>" style="text-transform:uppercase;"><?php echo $category[0]->cat_name; ?></a>
  </span>


	<header class="entry-header">





 <!-- .entry-footer -->
		<?php

			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); //Home H1

		if ( 'post' === get_post_type() ) :


			?>

<div class="entry-content">


 <?php  if ( ! has_excerpt() ) {
    echo 'nada';
} else {
      $BajadaNota =  the_excerpt();
      echo"<div class='bajadaNota'> $BajadaNota </div> ";
}?>


</div>
      <div class="author-box entry-meta">
        <div class="left">






<?php //if ( $posts ):
    //foreach ( $posts as $post ) : setup_postdata( $post );
        // Setting up the coauthors variable
        $coauthors = get_coauthors();

        // Counter for the coauthors foreach loop below
        $coauth = 0;

        // Counting the number of objects in the array.
        $len = count( $coauthors );
        $Piclen = 100;

            // Meta data here...
    foreach( $coauthors as $coauthor ):
                // Updating the counter.
        $Piclen--;
                // van las fotos de los autores
                $UserPic = get_avatar( $coauthor->ID );
                 echo '<span class="avatar_thumb" style="z-index:'.($Piclen).'">' . $UserPic . '</span>';

 endforeach;

echo ' <div><span class="byline"> Por';

            foreach( $coauthors as $coauthor ):


                // Getting the data for the current author
                $userdata = get_userdata( $coauthor->ID );


                // If one object in the array
                if ( $coauth == 0 ):
                    // Just the authors name
                    echo ' <span class="author vcard"><a class="url fn n" href="/author/' . $userdata->user_nicename  .'/">'. $userdata->display_name .'</a></span>';

        // If last object in the array
                elseif ( $coauth == ($len - 1) ):

                  //echo ($len);
                    // Adding an "and" before the last object
                    echo ' <br>y <span class="author vcard"><a class="url fn n" href="/author/' . $userdata->user_nicename  .'/">'. $userdata->display_name .'</a></span>';

                // If more than one object in the array
                elseif ( $coauth >= 1 ):
                    // Adding a "comma" after the name
                   // echo ($coauth);
                  // echo ($len);
                    echo '<span class="author vcard">, <a class="url fn n" href="/author/' . $userdata->user_nicename  .'/">'. $userdata->display_name .'</a></span>';


                endif;

                // Updating the counter.
                $coauth++;
            endforeach;

echo ' </span>';

echo '<span class="posted-on">';

      echo the_time('j M Y')  .' · ';
      echo do_shortcode('[rt_reading_time]')  .' min ' ;
   echo ' </span>';

 echo ' </div>';
            // More meta data here...

   // endforeach;
//endif;
 ?>





        </div>
        <div class="right" style="text-align:right;">
          <?php
                if ( function_exists( 'sharing_display' ) ) {
                    sharing_display( '', true );
                }

                if ( class_exists( 'Jetpack_Likes' ) ) {
                    $custom_likes = new Jetpack_Likes;
                    echo $custom_likes->post_likes( '' );
                }
          ?>
        </div>
      </div>
		<?php endif; ?>
	</header><!-- .entry-header -->








</article><!-- #post-<?php the_ID(); ?> -->
