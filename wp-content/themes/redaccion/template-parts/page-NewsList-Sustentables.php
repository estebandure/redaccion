<?php
/* Template Name: PageNewsListSustentables*/
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */


get_header();
 $PresentadaPorLogo = get_post_meta($post->ID, "PresentadaPorLogo", true);
$PresentadaPorLogo2 = get_post_meta($post->ID, "PresentadaPorLogo2", true);
 $PresentadaPorLogo3 = get_post_meta($post->ID, "PresentadaPorLogo3", true);
?>


<div id="content" class="NewslettersPage TopContent"> <!--ABRE TOP CONTENT--><!--cierra en el footer-->

<?php

    if ( get_post_meta( $post->ID, 'PresentadaPorLogo', true ) ) {
       echo ("<div class=\"CategoriaPresentada\"><div class=\"PresentadosTitleVolanta\"><span>Presentada por: </span> <img src='$PresentadaPorLogo '>");

          if ( get_post_meta( $post->ID, 'PresentadaPorLogo2', true ) ) {
            echo ("<img class=\"CategoriaPresentadaLogo2\" src='$PresentadaPorLogo2 '>");
          }

          if ( get_post_meta( $post->ID, 'PresentadaPorLogo3', true ) ) {
            echo ("<img src='$PresentadaPorLogo3 '>");
          }


  echo ("</div></div>");

    }


?>
<!--header y primer nota--->



<header class="">

	<?php
		while ( have_posts() ) {
			the_post();

			the_title( '<h1 class="entry-news-title">', '</h1>' );
			echo "<div class='entry-news'>";
			the_content( );
			echo "</div>";
}

		?>

</header><!-- .page-header -->

<div class="ArchiveFirst"></div>
<!--/fin header y primer nota-->



<div id="" class="ContentHome3Column SecondScroll archive">

<main id="main" class="CentralColHome2c">







		<?php
		query_posts('cat=694' );
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content-archivo', 'page' );


		endwhile;


      the_posts_navigation();



		?>

		</main><!-- #main -->


<div class="SideBarHome3c RightSideBar" ><!--- div col derecha-->
<?php
require("colDerecha-archive-news-V3.php");
?>
</div> <!---cierra div col derecha-->


</div><!--  termina el #ArchiveV3 con 3/4 col-1/4 col -->






<script>
jQuery(document).ready(function() {








jQuery(".TopContent .CentralColHome2c article:first-child").appendTo(".ArchiveFirst");


jQuery('#mc-embedded-subscribe-form').ajaxForm({
  target: '#mce-success-response'
});



jQuery("[cat=788]").css({ color: "#fe8228"}); //chillax
jQuery("[cat=703]").css({ color: "#9b53a5"}); //futuro
jQuery("[cat=704]").css({ color: "#a4ded2"});//oxigeno
jQuery("[cat=705]").css({ color: "#f5cb5b"});//gps am
jQuery("[cat=706]").css({ color: "#5c9bf8"});//gps pm
jQuery("[cat=732]").css({ color: "#c45381"});// siete parrafos
jQuery("[cat=732]").text("SIE7E PÁRRAFOS ");
jQuery("[cat=694]").css({ color: "#8FB981"});//sustentabilidad



jQuery('.NewsBoxPortada').ready(function(){
  if(jQuery(".mepr-already-logged-in").is(':visible')) {
    jQuery(".ActivaMembForm").remove();
    jQuery(".LoginMiembros h2").text("Tu cuenta");
  }

});





jQuery('.NewsBoxPortada').ready(function(){

//lo repito para la portada
if(jQuery(".newsPortada-gps-am").length>0) {
    jQuery(".Option-TopBoxAM").removeClass("NewsBoxHidden");
    jQuery( ".Option-TopBoxSustentables" ).remove()
    jQuery( ".Option-TopBoxPM" ).remove()
    jQuery( ".Option-TopBoxOxigeno" ).remove()
    jQuery( ".Option-TopBoxFuturo" ).remove()
    jQuery( ".Option-TopBoxSiete" ).remove()
    jQuery( ".Option-TopBoxChillax" ).remove()
  }
if(jQuery(".newsPortada-sustentables").length>0) {
    jQuery(".Option-TopBoxSustentables").removeClass("NewsBoxHidden");
    jQuery( ".Option-TopBoxAM" ).remove()
    jQuery( ".Option-TopBoxPM" ).remove()
    jQuery( ".Option-TopBoxOxigeno" ).remove()
    jQuery( ".Option-TopBoxFuturo" ).remove()
    jQuery( ".Option-TopBoxSiete" ).remove()
    jQuery( ".Option-TopBoxChillax" ).remove()
  }
  if(jQuery(".newsPortada-gps-pm").length>0) {
    jQuery(".Option-TopBoxPM").removeClass("NewsBoxHidden");
    jQuery( ".Option-TopBoxAM" ).remove()
    jQuery( ".Option-TopBoxSustentables" ).remove()
    jQuery( ".Option-TopBoxOxigeno" ).remove()
    jQuery( ".Option-TopBoxFuturo" ).remove()
    jQuery( ".Option-TopBoxSiete" ).remove()
    jQuery( ".Option-TopBoxChillax" ).remove()
  }
if(jQuery(".newsPortada-oxigeno").length>0) {
    jQuery( ".Option-TopBoxOxigeno").removeClass("NewsBoxHidden");
    jQuery( ".Option-TopBoxAM" ).remove()
    jQuery( ".Option-TopBoxPM" ).remove()
    jQuery( ".Option-TopBoxSustentables" ).remove()
    jQuery( ".Option-TopBoxFuturo" ).remove()
    jQuery( ".Option-TopBoxSiete" ).remove()
    jQuery( ".Option-TopBoxChillax" ).remove()
  }
  if(jQuery(".newsPortada-futuro").length>0) {
    jQuery(".Option-TopBoxFuturo").removeClass("NewsBoxHidden");
    jQuery( ".Option-TopBoxAM" ).remove()
    jQuery( ".Option-TopBoxPM" ).remove()
    jQuery( ".Option-TopBoxOxigeno" ).remove()
    jQuery( ".Option-TopBoxSustentables" ).remove()
    jQuery( ".Option-TopBoxSiete" ).remove()
    jQuery( ".Option-TopBoxChillax" ).remove()
  }
  if(jQuery(".newsPortada-siete-parrafos").length>0) {
    jQuery(".Option-TopBoxSiete").removeClass("NewsBoxHidden");
    jQuery( ".Option-TopBoxAM" ).remove()
    jQuery( ".Option-TopBoxPM" ).remove()
    jQuery( ".Option-TopBoxOxigeno" ).remove()
    jQuery( ".Option-TopBoxSustentables" ).remove()
    jQuery( ".Option-TopBoxFuturo" ).remove()
    jQuery( ".Option-TopBoxChillax" ).remove()
  }
  if(jQuery(".newsPortada-chillax").length>0) {
    jQuery(".Option-TopBoxChillax").removeClass("NewsBoxHidden");
    jQuery( ".Option-TopBoxAM" ).remove()
    jQuery( ".Option-TopBoxPM" ).remove()
    jQuery( ".Option-TopBoxOxigeno" ).remove()
    jQuery( ".Option-TopBoxSustentables" ).remove()
    jQuery( ".Option-TopBoxFuturo" ).remove()
    jQuery( ".Option-TopBoxSiete" ).remove()
  }

  });


});
</script>









<?php

get_footer();
