<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

?>






<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


   <div class="EncuentroContent">

   <div class="EncuentroCover" style="background: url(<?php the_post_thumbnail_url(); ?>) center; background-size: cover;"></div>
<div class="EncuentroData">
  
     <?php
      the_title( '<h2 class="Encuentro-title">', '</h2>' ); 
      ?>
      <div class="Encuentro-description"><?php
$content = get_the_content();
print $content;
?></div>
	</div>  
	  

  </div>








 







 





</article><!-- #post-<?php the_ID(); ?> -->
