<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

?>



  <?php
    $category = get_the_category();
    $category_link = get_category_link($category[0] );
    $VideoEspecialID = get_post_meta($post->ID, "VideoEspecialID", true); 
     ?>


<article id="" <?php post_class(); ?>>
 
<div id="<?php the_ID(); ?>"></div>


<!--video content-->
 
  <?php

    if ( get_post_meta( $post->ID, 'VideoEspecialID', true ) ) {
       //echo ("<div class=\"CategoriaPresentada\"><div class=\"PresentadosTitleVolanta\"><span>Presentada por: </span><img src='$PresentadaPorLogo '>");

       echo (" <iframe id=\"AnchoVideo\" src=\"https://www.youtube.com/embed/$VideoEspecialID");

         echo("\" allowfullscreen=\"\" width=\"100%\" ></iframe>");
    } 


?> 

  <!--//fin video content-->




<div class="CategoriaPresentada">
   
  
</div>

	<header class="entry-header">
        
	</header><!-- .entry-header --> 


<main id="main" class="CentralColVideoScroll">


    <?php
    
      the_title( '<h2 class="entry-title">', '</h2>' ); //post H1
    ?>




 <?php  if ( ! has_excerpt() ) {
    echo '';
} else { 
      echo"<div class='bajadaNota BajadaSingle'>"; 
      the_excerpt(); 
      echo"</div> ";
}?>

	<div class="entry-content">

<?php
		the_content();
		?>
</div><!-- .entry-content -->


    </main><!-- #main -->
 
 

</article>

<!-- #post-<?php the_ID(); ?> -->
