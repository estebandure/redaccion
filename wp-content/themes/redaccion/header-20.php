<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package redaccion
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,700,700i,900" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet"> 
  <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">


	<link href="https://fonts.googleapis.com/css?family=Merriweather:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
 <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js" integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe"
        crossorigin="anonymous"></script>
     
        <script defer src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KXRB6QT');</script>
<!-- End Google Tag Manager -->


<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '339142676900996');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=339142676900996&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->



	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>


 <!-- menu repetido -->
 <div class="menu" >
   
 <div class="menu_items" >

<?php get_search_form(); ?>

<p class="SubTMenu">Conocé RED/ACCIÓN</p>


  <?php
wp_nav_menu( array(

'menu'              => 'conoce-redaccion', // (int|string|WP_Term) Desired menu. Accepts a menu ID, slug, name, or object.
'menu_class'        => 'items_ul', // (string) CSS class to use for the ul element which forms the menu. Default 'menu'.
'before'            => '  ', // (string) Text before the link markup.
'after'             => '', // (string) Text after the link markup.
'link_before'       => '<div class="items_icon"></div><div class="items_txt"><h2>', // (string) Text before the link text.
'link_after'        => '</h2></div> ', // (string) Text after the link text.
) );
?>



<p class="SubTMenu menu_mas">Más de RED/ACCIÓN</p>

  <?php
wp_nav_menu( array(
'menu'              => 'mas-sobre-nosotros',

'menu_class'        => 'items_ul', // (string) CSS class to use for the ul element which forms the menu. Default 'menu'.
'before'            => '  ', // (string) Text before the link markup.
'after'             => '', // (string) Text after the link markup.
'link_before'       => '<div class="items_icon"></div><div class="items_txt"><h2>', // (string) Text before the link text.
'link_after'        => '</h2></div> ', // (string) Text after the link text.
) );
?>
</div>

</div>
<!-- end menu repetido -->





<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KXRB6QT"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="page" class="site">
	
	<div class="rv4b2">
      <header class="rv4b4">
        <nav>
          <ul class="rv4b3">


            <li class="menu_botton rv4headli rv4hlib">


              <button id="showmenu" aria-expanded="false" aria-label="Toggle menu" class="rv4htx rv4hlib" type="button" title="Toggle menu"><span class=""><svg class="rv4hico" xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 12 12"><style>.hamburger-icon__bottom,.hamburger-icon__middle,.hamburger-icon__top{fill: #000;transform-origin:50%;transition:all .3s}.open .hamburger-icon__top{transform:translateY(3px) translateX(-25%) rotate(45deg)}.open .hamburger-icon__bottom{transform:translateY(-4px) translateX(-25%) rotate(-45deg)}.open .hamburger-icon__middle{transform:translatex(-100%);opacity:0}</style><path class="hamburger-icon__top" d="M0 0h12v2H0z"></path><path class="hamburger-icon__middle" d="M0 5h12v2H0z"></path><path class="hamburger-icon__bottom" d="M0 10h12v2H0z"></path></svg></span></button>


            </li>


            <li class="rv4headli rv4hico2"><a class="rv4htx rv4hico2" href="#"><span></span></a></li>

           

            <?php
            $queryheader =  new WP_Query( ['category__in'=>[4678],  'post_type' => ['post'], 'posts_per_page' => 1 ] );
    
      
        while (  $queryheader->have_posts() ) :
          $queryheader->the_post();
            $LinkCampaniaMes = get_post_meta($post->ID, "LinkCampaniaMes", true);
          
    echo(' <li class="rv4headli rv4hico2 CampaniaFija"><a class="rv4htx rv4hico2" href="'.$LinkCampaniaMes.'"><span><strong>Campaña del mes: </strong>');
 the_title( );
echo("</span></a></li>");
        endwhile; // End of the loop.
        
        wp_reset_postdata();
        ?>





            <li class="rv4headli rv4hilg"><a class="rv4htx rv4hilg" href="https://redaccion.com.ar/" title="Ir a la portada de RED/ACCIÓN"><h1 class="V4Tit">RED/ACCIÓN</h1>
          </a></li>



          <li class="rv4headli rv4hico2"><a class="rv4htx rv4hlic" href="https://bit.ly/338WcTR"><span class="rv4hlibm">QUIERO SER CO-RESPONSABLE</span></a></li>
          <li class="rv4headli rv4hlipf">
          <?php
          if ( is_user_logged_in() ) {

$current_user = wp_get_current_user();

echo '<p class="NombreHeader" >Hola,  '. $current_user->user_firstname .'</p>';
echo ' <a href="/miembros-corresponsables/" class="rv4htx rv4hlipf IngresoMiembros SoymiembrosCorresponsables" title="Ingreso de Miembros"><span>Soy miembro</span></a> ';


} else {
echo '<a href="/ingresar" class="rv4htx rv4hlipf IngresoMiembros ingmiembrosCorresponsables" title="Ingreso de Miembros"><span>Iniciar sesión</span></a>';
}
?>


           <div aria-hidden="true" class="_2a07c"><div class="_5c8f6 _21413 bf6b2"></div><div class="_7c25e"></div></div>
          </li>
        </ul>

      </nav></header>

    </div>
