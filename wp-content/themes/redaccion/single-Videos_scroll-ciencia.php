<?php
/**
* Template Name: Post especial Videos cientificas
 * Template Post Type: post
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package redaccion
 */


get_header();
 $PresentadaPorLogo = get_post_meta($post->ID, "PresentadaPorLogo", true); 
$PresentadaPorLogo2 = get_post_meta($post->ID, "PresentadaPorLogo2", true); 
 $PresentadaPorLogo3 = get_post_meta($post->ID, "PresentadaPorLogo3", true); 
$excerpt = get_the_excerpt();
?>





<div id="content" class="VideosScrollContent"> <!--ABRE TOP CONTENT--><!--cierra en el footer-->

<?php

    if ( get_post_meta( $post->ID, 'PresentadaPorLogo', true ) ) {
       echo ("<div class=\"CategoriaPresentada\"><div class=\"PresentadosTitleVolanta\"><span>Presentada por: </span><img src='$PresentadaPorLogo '>");

          if ( get_post_meta( $post->ID, 'PresentadaPorLogo2', true ) ) {
            echo ("<img class=\"CategoriaPresentadaLogo2\" src='$PresentadaPorLogo2 '>");
          }
          
          if ( get_post_meta( $post->ID, 'PresentadaPorLogo3', true ) ) {
            echo ("<img src='$PresentadaPorLogo3 '>");
          }

     
  echo ("</div></div>");

    } 


?> 
<!--header y primer nota--->


<header class="VideosScrollHead">

  <div class="VideosScrollTop">
<?php   the_title( '<h1 class="entry-title">', '</h1>' );  ?>
<div class="bajadaNota BajadaSingle">
 

 <?php  
       
      echo  $excerpt ;

?>
  <div class="DateSpot">11 de febrero de 2020</div>
 
</div>







<!---AUTOR COAUTOR-->

  <div class="author-box entry-meta">
        <div class="left">




<?php //if ( $posts ):
    //foreach ( $posts as $post ) : setup_postdata( $post );
        // Setting up the coauthors variable
        $coauthors = get_coauthors();

        // Counter for the coauthors foreach loop below
        $coauth = 0;

        // Counting the number of objects in the array.
        $len = count( $coauthors );
        $Piclen = 100;
       
            // Meta data here...
 foreach( $coauthors as $coauthor ):
                // Updating the counter.
        $Piclen--;  
                // van las fotos de los autores               
                $UserPic = get_avatar( $coauthor->ID );
                 echo '<span class="avatar_thumb" style="z-index:'.($Piclen).'">' . $UserPic . '</span>';
                 
 endforeach;

echo '<div><span class="byline"> Por';

            foreach( $coauthors as $coauthor ):
               

                // Getting the data for the current author
                $userdata = get_userdata( $coauthor->ID );
               

                // If one object in the array
                if ( $coauth == 0 ):
                    // Just the authors name
                    echo ' <span class="author vcard"><a class="url fn n" href="/author/' . $userdata->user_nicename  .'/">'. $userdata->display_name .'</a></span>';

        // If last object in the array
                elseif ( $coauth == ($len - 1) ):

                  //echo ($len);
                    // Adding an "and" before the last object
                    echo ' y <span class="author vcard"><a class="url fn n" href="/author/' . $userdata->user_nicename  .'/">'. $userdata->display_name .'</a></span>';

                // If more than one object in the array
                elseif ( $coauth >= 1 ):
                    // Adding a "comma" after the name
                   // echo ($coauth);
                  // echo ($len);
                    echo '<span class="author vcard">, <a class="url fn n" href="/author/' . $userdata->user_nicename  .'/">'. $userdata->display_name .'</a></span>';
                   
                
                endif; 

                // Updating the counter.
                $coauth++;
            endforeach;

echo ' </span>';



 echo ' </div>';
            // More meta data here...
      
   // endforeach;
//endif;
 ?>

        </div> 

<!--FIN AUTOR COAUTOR-->
   <div class="right" style="text-align:right;white-space:nowrap;">
      
          <?php
                if ( function_exists( 'sharing_display' ) ) {
                    sharing_display( '', true );
                }

                if ( class_exists( 'Jetpack_Likes' ) ) {
                    $custom_likes = new Jetpack_Likes;
                    echo $custom_likes->post_likes( '' );
                }
          ?>
        </div>

</div>


  </div><!-- cierra el top-->
      



<?php
$html="";
     
   while ( have_posts() ) :
    the_post();
    $html.='<div class="entry-content">';
    $html.=get_the_content();
    $html.='</div>';
    endwhile; // End of the loop.

?>

<?php
      $htmlVideos="";
    
    query_posts('cat=3660&orderby=modified&order=ASC' );
    while ( have_posts() ) :
      the_post(); 

      $thumbVideosV=get_the_post_thumbnail();
       //$VideoEspecialID = get_post_meta($post->ID, "VideoEspecialID", true); 
      $VideoEspecialID = get_the_ID($post->ID); 
      $title = get_the_title();
      //$excerptNote = get_the_excerpt();

$htmlVideos.="<a href='#$VideoEspecialID' class='smooth'>";
     
//$htmlVideos.="<div id='Box$VideoEspecialID' class='BoxVideoEspecial'>";
$htmlVideos.= $thumbVideosV;
$htmlVideos.= "<h2 class='entry-news-title'>";
$htmlVideos.= $title ;
$htmlVideos.= "</h2>" ;  

    
     // $htmlVideos.="<div class='bajadaNota BajadaSingle'>"; 
    // $htmlVideos.= $excerptNote; 
    // $htmlVideos.="</div> ";

$htmlVideos.="<div class='BTBlacKAction'>LEER HISTORIA</div>";
//$htmlVideos.="</div>";
$htmlVideos.=" </a>";

    endwhile;
    ?>


<!--abre barra con videos-->
<div class="BarraVideosFixed1">
<!--Acá levanta videos y texto presentación-->
    <div class='bloquePresentacionVideos'>
      <?php echo $htmlVideos; ?>  
    </div> 
<!--///-->
</div> 
<!--cierra barra con videos-->








</header><!-- .page-header -->


<!-- contentido nota principal-->
<div class="CentralColVideoScroll">
<article>
<?php
echo $html;
?> 
</article>
</div>
<!-- / contentido nota principal-->



<!--acá levanta el contenido-->

<div class="CentralColVideoScroll">
      <?php
      query_posts('cat=3660&orderby=modified&order=ASC' );
    
    while ( have_posts() ) :
      the_post();
      get_template_part( 'template-parts/content-especial-SIN-videos', 'page' );
   endwhile;
    ?>
  </div>















<script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
 
<script>
jQuery(document).ready(function() { 

jQuery('a.smooth').on('click', function(e) {
    e.preventDefault();
    var $link = jQuery(this);
    var anchor = $link.attr('href');
    jQuery('html, body').stop().animate({scrollTop: jQuery(anchor).offset().top}, 1000);
    console.log("link anchor"+anchor);
});
///////////////////////////////////






 ///////////////////////////////


    var AnchoPage = "";
 
    AnchoPage =  jQuery(window).width();
  
    console.log(AnchoPage);

    if(AnchoPage >= 1900) {
     // console.log(AnchoPage + "más de 1024");
        
    }
    if(AnchoPage >= 1900) {
      //console.log(AnchoPage + "más de 1024");
        
    }
 

var oldscrollV=0;

//stickyGPSenviar

//console.log(" footer: "+FooterYY);

footerHH = jQuery(".footer").height() ;


var videos = []
var n =0;

jQuery('#AnchoVideo ').each(function(index, elem) { 

var offsetYvideo = jQuery(this).offset();
var AlturaVideo = jQuery(this).height();
var FinalVideo = (offsetYvideo.top + AlturaVideo);
console.log( " top: " + offsetYvideo.top );
console.log( " altura video: " +  AlturaVideo );
console.log( " final video: " +  FinalVideo );
videos[index] = { 'o' : offsetYvideo.top, 'h': FinalVideo}
});
 

///sss

jQuery(window).on("scroll", function() {




//  oldscrollV = jQuery(window).scrollTop();
// console.log( oldscrollV + "posicion del scroll"); 

FooterYY = jQuery(".post-template-single-Videos_scroll-ciencia .site").height();
   
  var screen = jQuery(window).scrollTop();
  var footerLast=(FooterYY-(footerHH*2));

    var primerScroll = jQuery(window).height();
     jQuery(".BarraVideosFixed" ).fadeOut();

    if(jQuery(window).scrollTop()   >= primerScroll) {
       //console.log("pase el primer scrooll"); 


        if(jQuery(window).scrollTop() +jQuery(window).height()  >= FooterYY) {
       //console.log("voy por el footer"); 
     jQuery(".BarraVideosFixed" ).fadeOut();
     jQuery(".BarraVideosFixed").removeClass("stickyGPSenviar");
     jQuery(".BarraVideosFixed").css({ display: "none"});
 
    } else {
  //  console.log("arriba");
  jQuery(".BarraVideosFixed" ).fadeIn();
  jQuery(".BarraVideosFixed").addClass("stickyGPSenviar");


}

     }else{
     
      jQuery(".BarraVideosFixed").removeClass("stickyGPSenviar");
       jQuery(".BarraVideosFixed" ).fadeOut();
     }
  
  


 
    //jQuery(".BarraVideosFixed").removeClass("stickyGPSenviar");
    // jQuery(".BarraVideosFixed").css({ display: "none"});
   






});






});
</script>









<?php

get_footer();
?>
<!--abre barra con videos-->
<div class="BarraVideosFixed">
<!--Acá levanta videos y texto presentación-->
    <div class='bloquePresentacionVideos'>
      <?php echo $htmlVideos; ?>  
    </div> 
<!--///-->
</div> 
<!--cierra barra con videos-->
