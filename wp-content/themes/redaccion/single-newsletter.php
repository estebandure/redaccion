<?php
/**
* Template Name: Newsletters AM-PM
 * Template Post Type: post
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package redaccion
 */

get_header();
?>

<?php require("AutorCoautor.php");?>

   



<div id="content" class="site-content single-Content">
	<div id="primary" class="content-area">
		<main id="mainPost" class="site-main">






    <!--POSTEO DESARROLLADO--->


		<?php

        $elposteoID=0;
		while ( have_posts() ) :
			the_post();
            $elposteoID=get_the_ID();
			get_template_part( 'template-parts/content-NewsAmPm', get_post_type() );

		endwhile; // End of the loop.
		?>



<!--FIN POSTEO DESSARROLLADO-->

   </main><!-- #main -->
  </div><!-- #primary -->
</div><!-- /content -->


<?php require("segundoscrol-v3.php");?>





<?php

get_footer();

function tiene_category($cats, $catid) {
  
  foreach ($cats as $cat) {
    if ($cat->term_id == $catid) {
      return true;
    }
    
  }
  return false;
}