<?php
/**
* Template Name: Post Pics 100% Sin cap
 * Template Post Type: post
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package redaccion
 */


get_header();
 $PresentadaPorLogo = get_post_meta($post->ID, "PresentadaPorLogo", true); 
$PresentadaPorLogo2 = get_post_meta($post->ID, "PresentadaPorLogo2", true); 
 $PresentadaPorLogo3 = get_post_meta($post->ID, "PresentadaPorLogo3", true); 
$excerpt = get_the_excerpt();
?>





<div id="content" > <!--ABRE TOP CONTENT--><!--cierra en el footer-->

<?php

    if ( get_post_meta( $post->ID, 'PresentadaPorLogo', true ) ) {
       echo ("<div class=\"CategoriaPresentada\"><div class=\"PresentadosTitleVolanta\"><span>Presentada por: </span><img src='$PresentadaPorLogo '>");

          if ( get_post_meta( $post->ID, 'PresentadaPorLogo2', true ) ) {
            echo ("<img class=\"CategoriaPresentadaLogo2\" src='$PresentadaPorLogo2 '>");
          }
          
          if ( get_post_meta( $post->ID, 'PresentadaPorLogo3', true ) ) {
            echo ("<img src='$PresentadaPorLogo3 '>");
          }

     
  echo ("</div></div>");

    } 


?> 
<!--header y primer nota--->


<header class="VideosScrollHead">

  <div class="VideosScrollTop">
<?php   the_title( '<h1 class="entry-title">', '</h1>' );  ?>
<div class="bajadaNota BajadaSingle">
 

 <?php  
       
      echo  $excerpt ;

?>

<div class="DateSpot"><?php the_time( 'j \d\e F \d\e Y' ); ?></div>
</div>







<!---AUTOR COAUTOR-->

  <div class="author-box entry-meta">
        <div class="left">




<?php //if ( $posts ):
    //foreach ( $posts as $post ) : setup_postdata( $post );
        // Setting up the coauthors variable
        $coauthors = get_coauthors();

        // Counter for the coauthors foreach loop below
        $coauth = 0;

        // Counting the number of objects in the array.
        $len = count( $coauthors );
        $Piclen = 100;
       
            // Meta data here...
 foreach( $coauthors as $coauthor ):
                // Updating the counter.
        $Piclen--;  
                // van las fotos de los autores               
                $UserPic = get_avatar( $coauthor->ID );
                 echo '<span class="avatar_thumb" style="z-index:'.($Piclen).'">' . $UserPic . '</span>';
                 
 endforeach;

echo '<div><span class="byline"> <em class="ByCronica">Por </em> ';

            foreach( $coauthors as $coauthor ):
               

                // Getting the data for the current author
                $userdata = get_userdata( $coauthor->ID );
               

                // If one object in the array
                if ( $coauth == 0 ):
                    // Just the authors name
                    echo ' <span class="author vcard"><a class="url fn n" href="/author/' . $userdata->user_nicename  .'/">'. $userdata->display_name .'</a></span>';

        // If last object in the array
                elseif ( $coauth == ($len - 1) ):

                  //echo ($len);
                    // Adding an "and" before the last object
                    echo ' <strong class="separaAutor"></strong><span class="author vcard"> y  <a class="url fn n" href="/author/' . $userdata->user_nicename  .'/">'. $userdata->display_name .'</a></span>';

                // If more than one object in the array
                elseif ( $coauth >= 1 ):
                    // Adding a "comma" after the name
                   // echo ($coauth);
                  // echo ($len);
                    echo '<span class="author vcard">, <a class="url fn n" href="/author/' . $userdata->user_nicename  .'/">'. $userdata->display_name .'</a></span>';
                   
                
                endif; 

                // Updating the counter.
                $coauth++;
            endforeach;

echo ' </span>';



 echo ' </div>';
            // More meta data here...
      
   // endforeach;
//endif;
 ?>

        </div> 

<!--FIN AUTOR COAUTOR-->
   <div class="right" style="text-align:right;white-space:nowrap;">
      
          <?php
                if ( function_exists( 'sharing_display' ) ) {
                    sharing_display( '', true );
                }

                if ( class_exists( 'Jetpack_Likes' ) ) {
                    $custom_likes = new Jetpack_Likes;
                    echo $custom_likes->post_likes( '' );
                }
          ?>
        </div>

</div>


<?php
// Must be inside a loop.

if ( has_post_thumbnail() ) {

echo ("<div class='thNoteCaption'>");
   echo ("<img src='");
    the_post_thumbnail_url() ;
   echo ( "'/>");
 if ( $caption = get_post( get_post_thumbnail_id() )->post_excerpt ) :
    echo ("<p class='caption'>");
      the_post_thumbnail_caption();
        echo(" </p>");
    endif;
echo ("</div>");


}

?>


  </div><!-- cierra el top-->
      



<?php
$html="";



     
   while ( have_posts() ) :
    the_post();
    $html.='<div class="entry-content">';
    $html.=get_the_content();
    $html.='</div>';
    endwhile; // End of the loop.

?>


</header><!-- .page-header -->


<!-- contentido nota principal-->
<div class="CentralColPicFullWidth">
<article>
<?php
echo $html;
?> 
</article>

<!-- #post-<?php the_ID(); ?> -->
<div class="ShareBottomArticle ">

<span>Compartí este contenido</span>

         <?php
               if ( function_exists( 'sharing_display' ) ) {
                   sharing_display( '', true );
               }

               if ( class_exists( 'Jetpack_Likes' ) ) {
                   $custom_likes = new Jetpack_Likes;
                   echo $custom_likes->post_likes( '' );
               }
         ?>
       </div>


</div>
<!-- / contentido nota principal-->






















<script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>


<script>
jQuery(document).ready(function() {




  //contador de divs

var totalModulos = jQuery('.CentralColPicFullWidth h2').size() 
///console.log(totalModulos + "totalModulos");
jQuery('.mensajeConteo').html(''+ totalModulos + ' capítulos'); 


  //CUENTA LOS H2//
  jQuery(window).on("scroll", function() {

oldscroll = jQuery(window).scrollTop();
//console.log("oldscroll  "+oldscroll );
var singleCard= 0;
jQuery('.CentralColPicFullWidth h2').each(function() {

var offsetYImg = jQuery(this).offset();
var positionYImg = jQuery(this).offset();
var AlturaImg = jQuery(this).height();
var FinalImg = (offsetYImg.top + AlturaImg);
//console.log("FinalImg "+FinalImg);
var FinalImgAltura = (offsetYImg.top );
var FinalImgPosition = (positionYImg.top );

var ventanaAlto = jQuery(window).height();

oldscrollscreen = (jQuery(window).scrollTop() +(AlturaImg +100)) ;  
oldscrollVentana= (jQuery(window).scrollTop() +(ventanaAlto -100)) ;  

if ( oldscrollVentana >= FinalImg) {
  if (singleCard  >= 0){
  jQuery('.mensajeConteo').html("Capítulo "+(singleCard+1)+' de '+ totalModulos ); 
  //jQuery(this).css('background-color', 'red')
  }

}
singleCard ++;


});


});
//FIN  CUENTA LOS H2//
  

//progress bar///
function progressBarScroll() {
  let winScroll = document.body.scrollTop || document.documentElement.scrollTop,
      height = document.documentElement.scrollHeight - document.documentElement.clientHeight,
      scrolled = (winScroll / height) * 100;
  document.getElementById("progressBar").style.width = scrolled + "%";
}

window.onscroll = function () {
  progressBarScroll();
};

 //FIN PROGRESS BAR//           



 var scrolloldposition=0;
var llegoalfinal=false;

//fin aviso llego al final///
jQuery(window).on("scroll", function() {


  //posición de la barra//
var ScreenWidth=jQuery(window).width();
var offsetYFooter = jQuery(".footer").offset();
var AlturaFooter = jQuery(".footer").height()+60;
var FinalFooterposition = (offsetYFooter.top );
var SueltaBarra = (FinalFooterposition+(jQuery(".footer").height()/2));
barrascroll = jQuery(window).scrollTop();
var scrollHeight = jQuery(document).height();
var scrollPosition = jQuery(window).height() + jQuery(window).scrollTop();
    if (scrollPosition >scrolloldposition){
      jQuery('#progressBar').show();
    }
    else{
      //jQuery('#progressBar').hide();
      jQuery('#progressBar').unbind( )
    }
    scrolloldposition=scrollPosition;

    if ((llegoalfinal==false)&&(scrollPosition >=  FinalFooterposition) ){
      llegoalfinal=true;
      //jQuery('#progressBar').append('<div class="mensajefinal"> Felicitaciones, llegaste al final.</div>').fadeIn( "slow" );
     // jQuery('.mensajeConteo').hide(); 
      //jQuery('.mensajeConteo').unbind("scroll"); 
      jQuery(".ShareBoxFooter").css('display', 'flex');
    }

   
			

    if (scrollPosition >=  FinalFooterposition) {
    // var asidew = jQuery('.aside').with();
  
          jQuery(".progress-container").css({ 'position': "relative", 'margin-bottom':'-10px', 'bottom':  "60px"});
          jQuery(".progress-container").addClass('progress-containerFinal');
    } else {
          if (ScreenWidth > 769) {
          jQuery(".progress-container").css({ 'bottom': '40px'});
          }else{
          jQuery(".progress-container").css({  'bottom':  "105px"});
      
           }

       }

       //fin posición de la barra//


});
//fin aviso llego al final///



});





            </script>







<?php

get_footer();


