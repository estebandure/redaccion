<?php
/* Template Name: PageHome preview*/
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

get_header();
?>














<div id="root">

<div class="rv4a1  rv4a2">


  <div id="main" class="rv4b1">


    
  



    <main id="site-content">
      <div class="rv4sc">
        <div class="rv4scb">




          <aside class="aside">


            <!-- menu repetido -->

            <div class="menu_items" >

            <?php get_search_form(); ?>
            
            <p class="SubTMenu">Conocé RED/ACCIÓN</p>


              <?php
		wp_nav_menu( array(

     'menu'              => 'conoce-redaccion', // (int|string|WP_Term) Desired menu. Accepts a menu ID, slug, name, or object.
    'menu_class'        => 'items_ul', // (string) CSS class to use for the ul element which forms the menu. Default 'menu'.
    'before'            => '  ', // (string) Text before the link markup.
    'after'             => '', // (string) Text after the link markup.
    'link_before'       => '<div class="items_icon"></div><div class="items_txt"><h2>', // (string) Text before the link text.
    'link_after'        => '</h2></div> ', // (string) Text after the link text.
			) );
			?>



<p class="SubTMenu menu_mas">Más de RED/ACCIÓN</p>

              <?php
		wp_nav_menu( array(
    'menu'              => 'mas-sobre-nosotros',
  
    'menu_class'        => 'items_ul', // (string) CSS class to use for the ul element which forms the menu. Default 'menu'.
    'before'            => '  ', // (string) Text before the link markup.
    'after'             => '', // (string) Text after the link markup.
    'link_before'       => '<div class="items_icon"></div><div class="items_txt"><h2>', // (string) Text before the link text.
    'link_after'        => '</h2></div> ', // (string) Text after the link text.
			) );
			?>
            </div>


            <!-- end menu repetido -->


          </aside>


          <div class="main">
            <div class="principal">

              <div class="main_head">
                <div class="fecha" id="fecha1"></div>
              <script>
          var f = new Date();
          var weekday = new Array(7);
          weekday[0] =  "Domingo";
          weekday[1] = "Lunes";
          weekday[2] = "Martes";
          weekday[3] = "Miércoles";
          weekday[4] = "Jueves";
          weekday[5] = "Viernes";
          weekday[6] = "Sábado";
          var n = weekday[f.getDay()];

          var month = new Array(12);
          month[0] =  "Enero";
          month[1] = "febrero";
          month[2] = "Marzo";
          month[3] = "Abril";
          month[4] = "Mayo";
          month[5] = "Junio";
          month[6] = "Julio";
          month[7] =  "Agosto";
          month[8] = "Septiembre";
          month[9] = "Octubre";
          month[10] = "Noviembre";
          month[11] = "Diciembre";
         
          var m = month[f.getMonth()];

          document.getElementById("fecha1").innerHTML = f.getDate()+" de "+m+" de 20"+(f.getYear()-100);
          </script>

<script  defer="" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js" integrity="sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i" crossorigin="anonymous"></script>
       
       <?php
    

        
           //para xenote  $queryfeed =  new WP_Query( ['category__in'=>[63],  'post_type' => ['post'], 'posts_per_page' => 1 ] );
            
        //para redaccion    
        $queryfeed =  new WP_Query( ['category__in'=>[4723],  'post_type' => ['post'], 'posts_per_page' => 1 ] );
 
        while (  $queryfeed->have_posts() ) :
          $queryfeed->the_post();

            $Actualizado = get_post_meta($post->ID, "Actualizado", true);
            echo('<div class="actualizado">'.$Actualizado.'</div>');
            echo('</div>');// ciera el main head
//content
            echo the_content();
//termina content
        endwhile; // End of the loop.

   
        ?>
                
                
               

           

            </div>

    <!-- <div class="progress-container">
    <div class="progress-bar" id="progressBar"></div>
  </div>-->

          </div> <!-- cierre de main -->

          <aside class="asideb">
            <h2>En profundidad</h2>
            <p>Cada día RED/ACCIÓN aborda un tema en profundidad para ayudarte a comprender mejor el mundo y ofrecerte soluciones para cambiarlo. </p>




  <!-- aside story -->
<?php require("AutorCoautor-portada.php");?>
<?php

$notID=[];
$html="";
$NotaID = get_the_ID($post->ID); 
$query1 =  new WP_Query( ['category__in'=>[4619],  'post_type' => ['post'], 'posts_per_page' => 4 ] );

//echo $query3->request;

while ( $query1->have_posts()) {

   $html.="<div class='aside_story' id='Note$NotaID'>";
   $query1->the_post();

//$categories = get_the_category();

      $html.= '<a href="' . get_permalink( ) . '" title="' . get_the_title(). ' " >';
      $html.=get_the_post_thumbnail( );
      $html.='</a>';
   


   $html.='<div class="aside_tit">';
   if ( get_post_meta( $post->ID, 'TituloHome', true ) ) {
  $TituloHome = get_post_meta($post->ID, "TituloHome", true);
  $html.="<a href='" . get_permalink()  . "' > $TituloHome </a> ";
 }else {
   $html.=the_title( '<a href="' . esc_url( get_permalink() ) . '" >', '</a>' , FALSE);
 } 
 $html.="</div>";
 $html.="<div class='aside_autor'>";
   $html.=AutorCoautor(1);
   $html.="</div>";
   $html.="</div>";
  array_push($notID,get_the_ID());

}

echo $html;
?>
 <!-- fin aside story -->

          

                  <div class="asideb_footer">
                    <a href="/secciones/en-profundidad/" class="profundizar">Más notas</a>
                  </div>


                </aside>


              </div>
            </div>


          </main>

          <footer class="dock" id="tab-bar">




            <a class="rv4dkl2 pie_actualidad" title="Actualidad" href="/secciones/actualidad/">
             
              <span>Actualidad</span>
            </a>


            <a class="rv4dkl2 pie_en-profundidad" title="En Profundidad" href="/secciones/en-profundidad/">
          
              <span>En Profundidad</span>
            </a>


            <a class="rv4dkl2 pie_newsletters" title="Newletters" href="/newsletters/">
            
              <span>Newsletters</span>
            </a>


            <a class="rv4dkl2 pie_miembros" title="Miembros" href="/miembros/"  >
             
              <span>Co-Responsables</span>
            </a>


          </footer>

        </div>


  




<script>
jQuery(document).ready(function() {

  
   

             
           
             

                jQuery( ".card_content .showSingle" ).each(function() { 
                  
                    jQuery(this).click(function() {
                    //jQuery('.targetDiv').not('#div' + jQuery(this).attr('target')).hide();
                    //jQuery('#div' + jQuery(this).attr('target')).toggle();
                    jQuery(this).parent().next().toggle();

                    });
                });
                //chevron que cambia//
jQuery( ".card .info a.showSingle" ).on( "click", function() {
    jQuery( this ).toggleClass( "ChevronCierra" );
});
//fin chevron que cambia//






//NEWSLETTERS CHEQUEADAS//
jQuery('.IdNewsCards').each(function()  { 
    var cardId = jQuery( this ).html();
    var cardIDli = (".CardBox"+cardId);
   // console.log(" card id "+cardIDli);
   

    jQuery( cardIDli+' input[type="checkbox"]').prop("checked", true);
 //   jQuery( this ).html(firstpart+"<small> "+last_part+"</small> ");

});

///FIN NEWLETTERS CHEQUEADAS
jQuery( ".SMP iframe" ).each(function() {
                
                jQuery(this).load(function() {
                  setTimeout(
                    
                     function () { 
                     var alturita= jQuery(this).contents().height() + 40;
             console.log(alturita + "alturitaaa");
                    jQuery(this).css("height",alturita );
                    }
                    
                    
                    
                    , 50);
  
             //jQuery(this).css("height",alturita );
                   });

                  
                    });

					
});
			




            </script>

<?php

get_footer();
