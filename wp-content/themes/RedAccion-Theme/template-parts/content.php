<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package redaccion
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<? if( has_post_thumbnail( $post_id ) ): ?>
    <div class="post-image">
        <img title="image title" alt="thumb image" class="wp-post-image"
             src="<?=wp_get_attachment_url( get_post_thumbnail_id() ); ?>" style="width:100%; height:auto;">
    </div>
<? endif; ?>
	<header class="entry-header">
		<?php

		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( esc_html__( ', ', 'redaccion' ) );
		if ( $categories_list && redaccion_categorized_blog() ) {
			printf( '<span class="cat-links">' . esc_html__( ' %1$s', 'redaccion' ) . '</span>', $categories_list ); // WPCS: XSS OK.
		}


			if ( is_single() ) {
				the_title( '<h1 class="entry-title">', '</h1>' );
			} else {
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			}

		if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php redaccion_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php
		endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'redaccion' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'redaccion' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php redaccion_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
