<?php
/* Template Name: PageHome */
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package redaccion
 */

get_header();
?>



<div style="display: block; width:100%">

<?php if ( have_posts() ) : ?>



      <?php

query_posts('cat=38&posts_per_page=1' );
      /* Start the Loop */
      while ( have_posts() ) :
        the_post();


        /*
         * Include the Post-Type-specific template for the content.
         * If you want to override this in a child theme, then include a file
         * called content-___.php (where ___ is the Post Type name) and that will be used instead.
         */
        get_template_part( 'template-parts/content-destacado-portada', get_post_type() );

      endwhile;

      the_posts_navigation();

    else :

      get_template_part( 'template-parts/content-portada', 'none' );

    endif;
    ?>

</div>
<div class="clear"></div>





<div id="content" class="site-content">

  <div id="primary" class="content-area">
    <main id="main" class="site-main">


  

  






		<!-- empieza loop primeras de portada-->
		<?php if ( have_posts() ) : ?>



			<?php

query_posts('cat=37&posts_per_page=3' );
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();


				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content-portada', get_post_type() );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content-portada', 'none' );

		endif;
		?>

<!-- termina loop primeras de portada-->

<!-- empieza modulo1 portada-->

<?php



$query2 = new WP_Query( [ 'cat'=>36, 'posts_per_page' => 8 ] );

?>


<?php
// aca va 1 de cat = 8

if ( $query2->have_posts() ) {
    $query2->the_post();
    //echo '<li>' . get_the_title( $query2->post->ID ) . '</li>';
    get_template_part( 'template-parts/content-main-boxes', get_post_type() );
}

?>



<?php

// The Query
$query1 = new WP_Query( ['category__in'=>652, 'category__not_in' => array( 1,36,37,38 ), 'posts_per_page' => 15 ] );


$n=0;
while (( $query1->have_posts()) && ($n<3) ) {
    $query1->the_post();
   // echo '<li>'.$n.' - ' . get_the_title() . '</li>';
    get_template_part( 'template-parts/content-portada', get_post_type() );


    $n++;
}

?>




<?php
// aca va 1 de cat = 8

if ( $query2->have_posts() ) {
    $query2->the_post();
    //echo '<li>' . get_the_title( $query2->post->ID ) . '</li>';
    get_template_part( 'template-parts/content-main-boxes', get_post_type() );
}

?>

<?php
// Aca van 3
$n=0;
while (( $query1->have_posts()) && ($n<3) ) {
    $query1->the_post();
   // echo '<li>'.$n.' - ' . get_the_title() . '</li>';
    get_template_part( 'template-parts/content-portada', get_post_type() );
    $n++;
}
?>




<?php
// aca va 1 de cat = 8

if ( $query2->have_posts() ) {
    $query2->the_post();
   // echo '<li>' . get_the_title( $query2->post->ID ) . '</li>';
    get_template_part( 'template-parts/content-main-boxes', get_post_type() );
}

?>





<?php
// Aca van 3
$n=0;
while (( $query1->have_posts()) && ($n<3) ) {
    $query1->the_post();
   // echo '<li>'.$n.' - ' . get_the_title() . '</li>';
    get_template_part( 'template-parts/content-portada', get_post_type() );
    $n++;
}
?>




<?php
// aca va 1 de cat = 8

if ( $query2->have_posts() ) {
    $query2->the_post();
   // echo '<li>' . get_the_title( $query2->post->ID ) . '</li>';
    get_template_part( 'template-parts/content-main-boxes', get_post_type() );
}

?>

<?php
// Aca van 3
$n=0;
while (( $query1->have_posts()) && ($n<3) ) {
    $query1->the_post();
   // echo '<li>'.$n.' - ' . get_the_title() . '</li>';
    get_template_part( 'template-parts/content-portada', get_post_type() );
    $n++;
}
?>

<?php
// aca va 1 de cat = 8

if ( $query2->have_posts() ) {
    $query2->the_post();
   // echo '<li>' . get_the_title( $query2->post->ID ) . '</li>';
    get_template_part( 'template-parts/content-main-boxes', get_post_type() );
}

?>

<?php
// Aca van 3
$n=0;
while (( $query1->have_posts()) && ($n<3) ) {
    $query1->the_post();
   // echo '<li>'.$n.' - ' . get_the_title() . '</li>';
    get_template_part( 'template-parts/content-portada', get_post_type() );
    $n++;
}
?>


		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
