<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'redaccion_live' );

/** MySQL database username */
define( 'DB_USER', 'redaccion_live' );

/** MySQL database password */
define( 'DB_PASSWORD', 'redaccion_live' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '>}Q]`FM0AG$-B}aBA)Do_LW+u>^D>|9{(wug@FYH4z]Q65D7[,|F$Lpeu[6sAHgA' );
define( 'SECURE_AUTH_KEY',  '%oj;P]VCr j3#V$bS|N)t?;jkR#VxSH9yGl /Jc@UDNi~In]8ou#YY4hUSK7M~NZ' );
define( 'LOGGED_IN_KEY',    'k]D#`~Z5.PR-$}1Uo74p]Nm1qZz@Bd`czQK]~lF#!Z{pyZRD rU+=O@?uH+Qozbz' );
define( 'NONCE_KEY',        'H]<Yr%9(E+Kt?>}uEfB,G9$5%BA@YkEOR]([2**}5:4u-]peO|LhJ(1XUBR//hd0' );
define( 'AUTH_SALT',        '`<zijH9n:@?5l_`r.jnmn2zN5g8}]Z_iybI2PxGJk-O[NccJq-YDUc[SU*{KX|vG' );
define( 'SECURE_AUTH_SALT', 'AX;gYj&Lp.LJx=Lfy{kH& a}g?o5bxU&<WTDPC6>~}}$tr^W<Ed1`^dMW>V<2z{k' );
define( 'LOGGED_IN_SALT',   '{~=+;B%cU)#_pzcg_^.2P-U:^ <%hb1]b,1`V<0A;pFn&Dv.uti82$PIg^> B9bx' );
define( 'NONCE_SALT',       'bj_;xcu,?V(D/seFztlwJ-3H9?%Exr^}.9`mmRFFSF(NkYkskhE,|c%O4tl`qa.Q' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_hpb28f_';

define( 'WP_DEBUG', true );
define( 'WP_DEBUG_LOG', true);
define( 'WP_DEBUG_DISPLAY', true );
define( 'SCRIPT_DEBUG', true );

define( 'WP_HOME', 'https://esteban.redaccion.local.com.ar/' );
define( 'WP_SITEURL', 'https://esteban.redaccion.local.com.ar/' );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
